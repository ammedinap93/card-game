export function isEmptyObject(obj) {
  for (var x in obj) {
    return false;
  }
  return true;
}

export const getCopyOfAnObject = (object) => {
  try {
    const objectCopy = JSON.parse(JSON.stringify(object));
    return objectCopy;
  } catch (error) {
    console.error("Error copying an object:", error);
    return null;
  }
};
