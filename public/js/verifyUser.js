import { logOut } from "./logOut.js";

export const verifyUser = () => {
  // Get the verified flag from sessionStorage
  const verified = sessionStorage.getItem("verified");
  if (verified === true || verified === "true") {
    return;
  }
  logOut();
};
