import { verifyUser } from "./verifyUser.js";

verifyUser();

let isDragging = false;
let selectedCardGameId = null;
let selectedCardGame = null;
let cardGames = null;
let selectedMatchType = "public";
let startClickPosition = null;
let endClickPosition = null;
let selectedNumPlayers = null;

// Function to fetch card games from the server
async function fetchCardGames() {
  try {
    const response = await fetch("/card-games");
    const cardGames = await response.json();
    return cardGames;
  } catch (error) {
    console.error("Error fetching card games:", error);
    return [];
  }
}

// Function to create a carousel item element
function createCarouselItem(cardGame) {
  const carouselItem = document.createElement("div");
  carouselItem.classList.add("carousel-item");

  // Create the image name based on the pattern
  const paddedId = cardGame.id.toString().padStart(2, "0");
  const imageName = `cover_${paddedId}.jpg`;

  carouselItem.innerHTML = `
      <img src="/assets/images/${imageName}" alt="${cardGame.name}">
      <h3>${cardGame.name}</h3>
      <p>${cardGame.description}</p>
    `;
  carouselItem.addEventListener("mousedown", (e) => {
    startClickPosition = e.pageX;
  });
  carouselItem.addEventListener("mouseup", (e) => {
    endClickPosition = e.pageX;
    if (startClickPosition === endClickPosition) {
      selectCarouselItem(carouselItem);
    }
  });
  return carouselItem;
}

// Function to select a carousel item
function selectCarouselItem(item) {
  const carouselItems = document.querySelectorAll(".carousel-item");
  carouselItems.forEach((item) => item.classList.remove("selected"));
  item.classList.add("selected");
  item.scrollIntoView({ behavior: "smooth", inline: "center" });
  selectedCardGameId = item.dataset.cardGameId;
  selectedCardGame = cardGames.find(
    (cardGame) => cardGame.id === Number(selectedCardGameId)
  );
  populateNumPlayersList(selectedCardGame);
  selectedNumPlayers = selectedCardGame.recommended_num_players;
}

// Function to initialize the carousel
async function initializeCarousel() {
  cardGames = await fetchCardGames();
  const carousel = document.getElementById("card-game-carousel");
  cardGames.forEach((cardGame) => {
    const carouselItem = createCarouselItem(cardGame);
    carouselItem.dataset.cardGameId = cardGame.id;
    carousel.appendChild(carouselItem);
  });
  selectCarouselItem(carousel.firstElementChild);

  // Add event listeners for mouse events
  let startX;
  let scrollLeft;

  carousel.addEventListener("mousedown", (e) => {
    isDragging = true;
    startX = e.pageX - carousel.offsetLeft;
    scrollLeft = carousel.scrollLeft;
  });

  carousel.addEventListener("mousemove", (e) => {
    if (!isDragging) return;
    e.preventDefault();
    const x = e.pageX - carousel.offsetLeft;
    const walk = (x - startX) * 2;
    carousel.scrollLeft = scrollLeft - walk;
  });

  carousel.addEventListener("mouseup", () => {
    isDragging = false;
  });

  carousel.addEventListener("mouseleave", () => {
    isDragging = false;
  });
}

// Function to handle match type selection
function handleMatchTypeChange(event) {
  selectedMatchType = event.target.value;
}

// Function to initialize the match type radio buttons
function initializeMatchTypeButtons() {
  const matchTypeButtons = document.querySelectorAll(
    'input[name="match-type"]'
  );
  matchTypeButtons.forEach((button) => {
    button.addEventListener("change", handleMatchTypeChange);
  });
}

// Function to populate the number of players list
function populateNumPlayersList(cardGame) {
  const numPlayersList = document.getElementById("num-players-list");
  numPlayersList.innerHTML = "";

  for (let i = cardGame.min_num_players; i <= cardGame.max_num_players; i++) {
    const numElement = document.createElement("span");
    numElement.classList.add("num-player");
    numElement.textContent = i;
    if (i === cardGame.recommended_num_players) {
      numElement.classList.add("selected");
      selectedNumPlayers = i;
    }
    numElement.addEventListener("click", () => {
      selectNumPlayers(i);
    });
    numPlayersList.appendChild(numElement);
  }
}

// Function to select the number of players
function selectNumPlayers(num) {
  const numElements = document.querySelectorAll(".num-player");
  numElements.forEach((numElement) => {
    numElement.classList.remove("selected");
  });
  numElements[num - selectedCardGame.min_num_players].classList.add("selected");
  selectedNumPlayers = num;
}

// Function to initialize the number of players list
function initializeNumPlayersList() {
  if (selectedCardGame) {
    populateNumPlayersList(selectedCardGame);
  }
}

// Initialize the carousel and match type buttons when the page loads
window.addEventListener("load", () => {
  initializeCarousel().then(() => {
    initializeNumPlayersList();
  });
  initializeMatchTypeButtons();
});

// Get references to the elements
const lobbyCodeInput = document.getElementById("lobby-code-input");
const readyButton = document.getElementById("ready-button");

// Function to handle the "I'm Ready" button click
function handleReadyClick() {
  const options = {
    cardGameId: selectedCardGameId,
    matchType: selectedMatchType,
    lobbyCode: lobbyCodeInput.value || null,
    preferredNumPlayers: selectedNumPlayers,
  };
  localStorage.setItem("gameOptions", JSON.stringify(options));
  window.location.href = "/game";
}

// Add click event listener to the "I'm Ready" button
readyButton.addEventListener("click", handleReadyClick);
