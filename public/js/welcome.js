const signUpButton = document.getElementById("sign-up");
const signInButton = document.getElementById("sign-in");
const guestButton = document.getElementById("guest");

const signUpForm = document.getElementById("sign-up-form");
const signUpUsername = document.getElementById("sign-up-username");
const signUpPassword = document.getElementById("sign-up-password");
const signUpConfirmPassword = document.getElementById(
  "sign-up-confirm-password"
);

const signInForm = document.getElementById("sign-in-form");
const signInUsername = document.getElementById("sign-in-username");
const signInPassword = document.getElementById("sign-in-password");

signUpButton.addEventListener("click", () => {
  // Hide the sign-in form
  signInForm.style.display = "none";
  // Hide the sign-up button
  signUpButton.style.display = "none";
  // Show the sign-up form
  signUpForm.style.display = "block";
  // Show the sign-in button
  signInButton.style.display = "block";
});

signUpForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  // Validate the form
  if (signUpUsername.value === "guest") {
    alert(`Username cannot be "guest".`);
    return;
  }

  if (
    signUpPassword.value.length < 8 ||
    !/\d/.test(signUpPassword.value) ||
    !/[a-zA-Z]/.test(signUpPassword.value)
  ) {
    alert(
      "Password must be at least 8 characters long and contain at least one letter and one number."
    );
    return;
  }

  if (signUpPassword.value !== signUpConfirmPassword.value) {
    alert("Passwords do not match.");
    return;
  }

  // Make a POST request to the server
  const response = await fetch("/signup", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: signUpUsername.value,
      password: signUpPassword.value,
    }),
  });

  const data = await response.json();

  if (data.error) {
    alert(data.error);
  } else {
    if (data.user && data.user.id && data.user.username) {
      // Store the token in localStorage
      localStorage.setItem("token", data.token);
      // Store the username in localStorage
      localStorage.setItem("username", data.user.username);
      // Store the user ID in localStorage
      localStorage.setItem("userId", data.user.id);
      // Store the verified flag in sessionStorage
      sessionStorage.setItem("verified", "true");
      // Redirect to the main scene
      window.location.href = "/main";
    }
  }
});

signInButton.addEventListener("click", () => {
  // Hide the sign-up form
  signUpForm.style.display = "none";
  // Hide the sign-in button
  signInButton.style.display = "none";
  // Show the sign-in form
  signInForm.style.display = "block";
  // Show the sign-uo button
  signUpButton.style.display = "block";
});

signInForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  const response = await fetch("/signin", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: signInUsername.value,
      password: signInPassword.value,
    }),
  });

  const data = await response.json();

  if (data.error) {
    alert(data.error);
  } else {
    if (data.user && data.user.id && data.user.username) {
      // Store the token in localStorage
      localStorage.setItem("token", data.token);
      // Store the username in localStorage
      localStorage.setItem("username", data.user.username);
      // Store the user ID in localStorage
      localStorage.setItem("userId", data.user.id);
      // Store the verified flag in sessionStorage
      sessionStorage.setItem("verified", "true");
      // Redirect to the main scene
      window.location.href = "/main";
    }
  }
});

guestButton.addEventListener("click", () => {
  // Store the verified flag in sessionStorage
  sessionStorage.setItem("verified", "true");
  // Redirect to the main scene
  window.location.href = "/main";
});
