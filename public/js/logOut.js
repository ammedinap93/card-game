export const logOut = () => {
  // Delete the token, username, and userId from Local Storage
  localStorage.removeItem("token");
  localStorage.removeItem("username");
  localStorage.removeItem("userId");
  // Remove the verified flag in sessionStorage
  sessionStorage.removeItem("verified");
  // Redirect to the welcome page
  window.location.href = "/welcome";
};
