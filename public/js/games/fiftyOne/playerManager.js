export function getPlayerLabelPosition(
  scene,
  index,
  playerIndex,
  totalPlayers
) {
  const positions = [
    {
      x: scene.sys.game.config.width / 2,
      y: scene.sys.game.config.height - 20,
      rotation: 0,
    }, // Bottom
    {
      x: scene.sys.game.config.width - 20,
      y: scene.sys.game.config.height / 2,
      rotation: Phaser.Math.DegToRad(-90),
    }, // Right
    {
      x: scene.sys.game.config.width / 2,
      y: 20,
      rotation: 0,
    }, // Top
    {
      x: 20,
      y: scene.sys.game.config.height / 2,
      rotation: Phaser.Math.DegToRad(90),
    }, // Left
  ];

  let adjustedIndex;
  if (totalPlayers === 2) {
    adjustedIndex = ((index - playerIndex + 2) % 2) * 2; // Use bottom and top positions
  } else if (totalPlayers === 3) {
    adjustedIndex = ((index - playerIndex + 3) % 3) * 1.334; // Use bottom, right, and top positions
  } else {
    adjustedIndex = (index - playerIndex + 4) % 4;
  }
  return positions[Math.floor(adjustedIndex)];
}

function createOrUpdatePlayerLabel(
  scene,
  player,
  index,
  playerIndex,
  totalPlayers,
  existingLabel
) {
  const labelPosition = getPlayerLabelPosition(
    scene,
    index,
    playerIndex,
    totalPlayers
  );

  if (existingLabel) {
    existingLabel.setText(player.username);
    existingLabel.setPosition(labelPosition.x, labelPosition.y);
    existingLabel.setRotation(labelPosition.rotation);
    return existingLabel;
  } else {
    return scene.add
      .text(labelPosition.x, labelPosition.y, player.username, {
        font: "18px regular-font",
        fill: "#ffffff",
      })
      .setOrigin(0.5)
      .setRotation(labelPosition.rotation);
  }
}

export function updatePlayerLabels(scene, gameState, chatMessages) {
  const totalPlayers = gameState.distributionOfSeats.length;

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const player = gameState.lobby.players.find((p) => p.id === playerId);
    const message = chatMessages[playerId];

    // Create or update the player label
    scene.playerLabels[playerId] = createOrUpdatePlayerLabel(
      scene,
      player,
      index,
      scene.playerIndex,
      totalPlayers,
      scene.playerLabels[playerId]
    );

    // If there's a chat message, display it
    if (message) {
      showChatMessage(scene, scene.playerLabels[playerId], message);
    } else {
      // Ensure the label shows the player's username
      resetToUsername(scene, scene.playerLabels[playerId], player.username);
    }
  });

  // Remove labels for players who are no longer in the game
  Object.keys(scene.playerLabels).forEach((playerId) => {
    if (!gameState.distributionOfSeats.includes(playerId)) {
      scene.playerLabels[playerId].destroy();
      delete scene.playerLabels[playerId];
    }
  });
}

function showChatMessage(scene, label, message) {
  // Stop any ongoing animations
  scene.tweens.killTweensOf(label);

  // Update label style for chat message with black border
  label.setStyle({
    font: "18px regular-font",
    fill: "#ffff00",
    stroke: "#000000",
    strokeThickness: 3,
  });
  label.setText(message);

  scene.tweens.add({
    targets: label,
    scaleX: 1.2,
    scaleY: 1.2,
    duration: 200,
    yoyo: true,
    ease: "Quad.easeInOut",
    repeat: 2,
    onComplete: () => {
      scene.time.delayedCall(3000, () => {
        scene.tweens.add({
          targets: label,
          alpha: 0,
          duration: 200,
          onComplete: () => {
            resetToUsername(scene, label, label.playerUsername);
          },
        });
      });
    },
  });
}

function resetToUsername(scene, label, username) {
  // Stop any ongoing animations
  scene.tweens.killTweensOf(label);

  // Reset label style and text to show username without border
  label.setStyle({
    font: "18px regular-font",
    fill: "#ffffff",
    stroke: null,
    strokeThickness: 0,
  });
  label.setText(username);
  label.setScale(1);
  label.setAlpha(1);

  // Store the username on the label object for future reference
  label.playerUsername = username;
}
