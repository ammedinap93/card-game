import { FIFTY_ONE_GAME_STATUS } from "./fiftyOneConstants.js";
import { createButton } from "./uiElements.js";

const MAX_NUM_ROUNDS_TO_DISPLAY = 10;

class ScoresSummaryScene extends Phaser.Scene {
  constructor() {
    super({ key: "ScoresSummaryScene" });
  }

  init(data) {
    this.socket = data.socket;
    this.gameState = data.gameState;
    this.gameStats = data.gameStats;
    this.playerId = this.socket.id;
    this.summaryTimer = null;
  }

  create() {
    this.add
      .rectangle(
        0,
        0,
        this.sys.game.config.width,
        this.sys.game.config.height,
        0x000000,
        0.9
      )
      .setOrigin(0);

    const title = this.add
      .text(this.sys.game.config.width / 2, 50, "Scores Summary", {
        fontFamily: "regular-font",
        fontSize: "32px",
        fill: "#ffffff",
      })
      .setOrigin(0.5);

    const playerIds = this.gameState.distributionOfSeats;
    const playerCount = playerIds.length;
    const columnWidth = this.sys.game.config.width / (playerCount + 1);

    // Add player names, ready status, and win count
    playerIds.forEach((playerId, index) => {
      const player = this.gameState.lobby.players.find(
        (p) => p.id === playerId
      );
      const playerName = player.username;
      const isReady = this.gameState.playersReadyToContinue[playerId];
      const readyStatus = isReady ? " ✅" : "";

      // Add player name and ready status
      this.add
        .text(columnWidth * (index + 1), 100, `${playerName}${readyStatus}`, {
          fontFamily: "regular-font",
          fontSize: "24px",
          fill: "#ffffff",
        })
        .setOrigin(0.5);

      // Add win count if game stats are available
      if (this.gameStats && this.gameStats[playerId] !== undefined) {
        const wins = this.gameStats[playerId];
        const winText = wins === 1 ? `(${wins} win)` : `(${wins} wins)`;
        this.add
          .text(columnWidth * (index + 1), 130, winText, {
            fontFamily: "regular-font",
            fontSize: "18px",
            fill: "#ffffff",
          })
          .setOrigin(0.5);
      }
    });

    // Add hand labels and scores
    const totalHands = this.gameState.playersScoresByHand[playerIds[0]].length;
    this.gameState.playersScoresByHand[playerIds[0]].forEach((_, handIndex) => {
      if (totalHands - handIndex <= MAX_NUM_ROUNDS_TO_DISPLAY) {
        const hand = handIndex + 1;
        this.add
          .text(columnWidth / 2, 180 + handIndex * 40, `${hand}.`, {
            fontFamily: "regular-font",
            fontSize: "18px",
            fill: "#ffffff",
          })
          .setOrigin(0.5);

        playerIds.forEach((playerId, playerIndex) => {
          const score = this.gameState.playersScoresByHand[playerId][handIndex];
          this.add
            .text(
              columnWidth * (playerIndex + 1),
              180 + handIndex * 40,
              score,
              {
                fontFamily: "regular-font",
                fontSize: "18px",
                fill: "#ffffff",
              }
            )
            .setOrigin(0.5);
        });
      }
    });

    // Add total scores
    this.add
      .text(columnWidth / 2, 180 + MAX_NUM_ROUNDS_TO_DISPLAY * 40, "Total", {
        fontFamily: "regular-font",
        fontSize: "24px",
        fill: "#ffffff",
      })
      .setOrigin(0.5);
    playerIds.forEach((playerId, playerIndex) => {
      const totalScore = this.gameState.playersScoresByHand[playerId].reduce(
        (sum, score) => sum + score,
        0
      );
      this.add
        .text(
          columnWidth * (playerIndex + 1),
          180 + MAX_NUM_ROUNDS_TO_DISPLAY * 40,
          totalScore,
          { fontFamily: "regular-font", fontSize: "24px", fill: "#ffffff" }
        )
        .setOrigin(0.5);
    });

    // Add winner label
    const winnerLabel = this.add
      .text(
        this.sys.game.config.width / 2,
        180 + MAX_NUM_ROUNDS_TO_DISPLAY * 40 + 50,
        "",
        {
          fontFamily: "regular-font",
          fontSize: "32px",
          fill: "#ffffff",
        }
      )
      .setOrigin(0.5);

    if (this.gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_game) {
      const winnerId = this.gameState.winnerPlayer;
      if (winnerId) {
        const winnerName = this.gameState.lobby.players.find(
          (p) => p.id === winnerId
        ).username;
        winnerLabel.setText(`Winner: ${winnerName}! 🏆`);
      } else {
        winnerLabel.setText("It's a tie! 🤝");
      }
    }

    // Show different UI elements based on the game status
    if (this.gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand) {
      // Add timer text
      this.timerText = this.add
        .text(
          this.sys.game.config.width / 2,
          this.sys.game.config.height - 120,
          this.summaryTimer ? `Time remaining: ${this.summaryTimer}s` : "",
          {
            fontFamily: "regular-font",
            fontSize: "18px",
            fill: "#ffffff",
          }
        )
        .setOrigin(0.5);

      // Add continue button
      const isPlayerReady =
        this.gameState.playersReadyToContinue[this.playerId];
      this.continueButton = createButton(
        this,
        this.sys.game.config.width / 2,
        this.sys.game.config.height - 80,
        "Continue",
        !isPlayerReady, // Show the button if the player is not ready
        true,
        () => {
          this.socket.emit("readyToContinue");
          this.continueButton.setVisible(false);
        }
      );

      // Add players ready text
      this.playersReadyText = this.add
        .text(
          this.sys.game.config.width / 2,
          this.sys.game.config.height - 40,
          this.getPlayersReadyText(),
          {
            fontFamily: "regular-font",
            fontSize: "18px",
            fill: "#ffffff",
          }
        )
        .setOrigin(0.5);
    } else if (
      this.gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_game
    ) {
      // Add "Play Again" button
      this.playAgainButton = createButton(
        this,
        this.sys.game.config.width / 2 - 100,
        this.sys.game.config.height - 80,
        "Play Again",
        true,
        true,
        () => {
          window.location.reload();
        }
      );

      // Add "Main Menu" button
      this.mainMenuButton = createButton(
        this,
        this.sys.game.config.width / 2 + 100,
        this.sys.game.config.height - 80,
        "Main Menu",
        true,
        true,
        () => {
          window.location.href = "/main";
        }
      );
    }

    if (this.allPlayersReady()) {
      this.time.delayedCall(1000, () => {
        this.scene.stop();
      });
    }

    // Listen for timer updates
    this.socket.on("timerUpdate", (timerData) => {
      if (timerData.timerName === "summary") {
        this.updateTimer(timerData.remainingTime);
      }
    });

    // Check if any player is ready to continue
    const anyPlayerReady = Object.values(
      this.gameState.playersReadyToContinue
    ).some((ready) => ready);

    // Play the summary sound effect only if no player is ready
    if (!anyPlayerReady) {
      this.sound.play("summary");
    }
  }

  getPlayersReadyText() {
    const readyCount = Object.values(
      this.gameState.playersReadyToContinue
    ).filter((ready) => ready).length;
    const totalPlayers = this.gameState.distributionOfSeats.length;
    return `Players ready: ${readyCount}/${totalPlayers}`;
  }

  allPlayersReady() {
    return Object.values(this.gameState.playersReadyToContinue).every(
      (ready) => ready
    );
  }

  updateTimer(remainingTime) {
    this.summaryTimer = remainingTime;
    this.timerText.setText(`Time remaining: ${this.summaryTimer}s`);

    if (this.summaryTimer <= 0) {
      this.scene.stop();
    }
  }
}

export default ScoresSummaryScene;
