import * as CardManager from "./cardManager.js";
import * as PlayerManager from "./playerManager.js";
import * as AudioManager from "./audioManager.js";
import * as UIElements from "./uiElements.js";
import * as AnimationManager from "./animationManager.js";
import ScoresSummaryScene from "./scoresSummaryScene.js";
import { createChatUI } from "../chatUI.js";
import { FIFTY_ONE_GAME_STATUS } from "./fiftyOneConstants.js";

class FiftyOneScene extends Phaser.Scene {
  constructor() {
    super({ key: "FiftyOneScene" });
  }

  preload() {}

  create() {
    this.scene.add("ScoresSummaryScene", ScoresSummaryScene, false);

    UIElements.createBackgroundImage(this);

    this.playerInTurnLabel = UIElements.createPlayerInTurnLabel(
      this,
      "Loading..."
    );
    this.pauseOverlay = UIElements.createPauseOverlay(this);

    this.callButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 20,
      "Call",
      false,
      false,
      () => this.call()
    );

    this.exchangeCardsButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 50,
      "Exchange Cards",
      false,
      false,
      () => this.exchangeCards()
    );

    this.challengeButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 20,
      "Challenge",
      false,
      false,
      () => this.challenge()
    );

    this.notChallengeButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 50,
      "Not Challenge",
      false,
      false,
      () => this.notChallenge()
    );

    this.errorLabel = this.add
      .text(
        this.sys.game.config.width / 2,
        this.sys.game.config.height - 160,
        "",
        { font: "18px regular-font", fill: "#ff0000" }
      )
      .setOrigin(0.5);

    this.chatMessages = {};
    this.chatMessageLabels = {};
    this.cardOriginalDepths = {};
    this.playerLabels = {};
    this.playerCards = {};
    this.takingCards = [];
    this.discardingCards = [];
    this.gameState = {};
    this.gameStats = null;
    this.playerTurnTimer = null;
    this.playerTurnInProgressTimer = null;
    this.dealerTimer = null;
    this.waitingForChallengersTimer = null;

    const { cardPlaceSounds, cardSlideSounds } =
      AudioManager.createSoundEffects(this);
    this.cardPlaceSounds = cardPlaceSounds;
    this.cardSlideSounds = cardSlideSounds;

    this.socket = this.scene.settings.data.socket;
    this.socket.on("gameState", (gameState) => {
      console.log("Game state received:", gameState);
      this.updateGameState(gameState);
    });
    this.socket.on("chatMessage", (chatMessage) => {
      console.log("Chat message received:", chatMessage);
      this.updateChatMessages(chatMessage);
    });
    this.socket.on("timerUpdate", (timerData) => {
      // console.log("Timer update received:", timerData);
      this.updateTimer(timerData);
    });
    this.socket.on("gameStats", (gameStats) => {
      console.log("Game stats received:", gameStats);
      this.gameStats = gameStats;
    });

    this.chatUI = createChatUI(
      this,
      this.socket,
      this.sys.game.config.width - 20,
      this.sys.game.config.height - 50,
      "right"
    );
  }

  updateGameState(gameState) {
    this.gameState = gameState;
    this.playerIndex = gameState.distributionOfSeats.indexOf(this.socket.id);
    this.updateCallButton(gameState);
    this.updateExchangeCardsButton(gameState);
    this.updateChallengeButton(gameState);
    this.updateNotChallengeButton(gameState);
    PlayerManager.updatePlayerLabels(this, gameState, this.chatMessages);
    UIElements.updatePlayerInTurnLabel(this, gameState);
    UIElements.updateDeckPile(this, gameState);
    UIElements.updateDiscardPile(this, gameState);
    UIElements.updateFaceUpCards(this, gameState);
    CardManager.updatePlayerCards(this, gameState);
    AnimationManager.animateDealingCard(this, gameState);
    AnimationManager.animateDiscardingCard(this, gameState);
    AnimationManager.animateDiscardingFaceUpCards(this, gameState);
    AnimationManager.animateExchangingCards(this, gameState);
    UIElements.updatePauseOverlay(this, gameState);

    if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand ||
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_game
    ) {
      this.time.delayedCall(1000, () => {
        this.scene.launch("ScoresSummaryScene", {
          socket: this.socket,
          gameState: this.gameState,
          gameStats: this.gameStats,
        });
      });
    }
  }

  updateChatMessages(chatMessage) {
    this.chatMessages[chatMessage.player.id] = chatMessage.message;
    PlayerManager.updatePlayerLabels(this, this.gameState, this.chatMessages);

    // Remove the message after 2.5 seconds
    this.time.delayedCall(2500, () => {
      delete this.chatMessages[chatMessage.player.id];
    });
  }

  updateTimer(timerData) {
    const { timerName, remainingTime } = timerData;

    if (timerName === "playerTurn") {
      UIElements.updatePlayerTurnTimer(this, remainingTime);
    } else if (timerName === "playerTurnInProgress") {
      UIElements.updatePlayerTurnInProgressTimer(this, remainingTime);
    } else if (timerName === "dealer") {
      UIElements.updateDealerTimer(this, remainingTime);
    } else if (timerName === "waitingForChallengers") {
      UIElements.updateWaitingForChallengersTimer(this, remainingTime);
    }
  }

  updateCallButton(gameState) {
    const shouldShowButton =
      gameState.playerInTurn === this.socket.id &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.roundNumber > 1;

    this.callButton.setVisible(shouldShowButton);
  }

  updateChallengeButton(gameState) {
    const shouldShowButton =
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers &&
      gameState.caller !== this.socket.id &&
      gameState.challengerCandidates.includes(this.socket.id);

    this.challengeButton.setVisible(shouldShowButton);
  }

  updateExchangeCardsButton(gameState) {
    const shouldShowButton =
      gameState.playerInTurn === this.socket.id &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.faceUpCards.length > 0;

    this.exchangeCardsButton.setVisible(shouldShowButton);

    if (!shouldShowButton) {
      this.clearSelectedCards();
    }
  }

  updateNotChallengeButton(gameState) {
    const shouldShowButton =
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers &&
      gameState.caller !== this.socket.id &&
      gameState.challengerCandidates.includes(this.socket.id);

    this.notChallengeButton.setVisible(shouldShowButton);
  }

  exchangeCards() {
    if (
      this.takingCards.length === this.discardingCards.length &&
      this.takingCards.length > 0
    ) {
      this.socket.emit("exchangeCards", {
        takingCards: this.takingCards,
        discardingCards: this.discardingCards,
      });
      this.clearSelectedCards();
    } else {
      this.errorLabel.setText(
        "Select an equal number of cards to exchange (at least one)."
      );
      this.time.delayedCall(3000, () => {
        this.errorLabel.setText("");
      });
    }
  }

  clearSelectedCards() {
    this.takingCards = [];
    this.discardingCards = [];
    this.errorLabel.setText("");
    // Clear tints from face-up cards and player's hand
    if (this.faceUpCardSprites) {
      this.faceUpCardSprites
        .getChildren()
        .forEach((sprite) => sprite.clearTint());
    }
    if (this.playerCards[this.socket.id]) {
      this.playerCards[this.socket.id].forEach((sprite) => sprite.clearTint());
    }
  }

  isCompleteHand(hand) {
    if (hand.length !== 5) return false;

    const suit = hand[0].card.split("_")[1];
    let score = 0;

    for (const card of hand) {
      const [rank, cardSuit] = card.card.split("_");
      if (cardSuit !== suit) return false;

      if (["jack", "queen", "king"].includes(rank)) {
        score += 10;
      } else if (rank === "ace") {
        score += 11;
      } else {
        score += parseInt(rank);
      }
    }

    return score >= 31;
  }

  call() {
    this.clearSelectedCards();

    if (
      this.gameState.playerInTurn === this.socket.id &&
      this.gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      this.gameState.roundNumber > 1
    ) {
      const playerHand = this.gameState.playersHands[this.socket.id];
      if (this.isCompleteHand(playerHand)) {
        this.socket.emit("call");
      } else {
        this.showErrorMessage(
          "Your hand is not complete. You need 5 cards of the same suit with a total value of at least 31 points."
        );
      }
    }
  }

  challenge() {
    if (
      this.gameState.gameStatus ===
        FIFTY_ONE_GAME_STATUS.waiting_for_challengers &&
      this.gameState.caller !== this.socket.id &&
      this.gameState.challengerCandidates.includes(this.socket.id)
    ) {
      const playerHand = this.gameState.playersHands[this.socket.id];
      if (this.isCompleteHand(playerHand)) {
        this.socket.emit("challenge");
      } else {
        this.showErrorMessage(
          "Your hand is not complete. You need 5 cards of the same suit with a total value of at least 31 points."
        );
      }
    }
  }

  notChallenge() {
    if (
      this.gameState.gameStatus ===
        FIFTY_ONE_GAME_STATUS.waiting_for_challengers &&
      this.gameState.caller !== this.socket.id &&
      this.gameState.challengerCandidates.includes(this.socket.id)
    ) {
      const playerHand = this.gameState.playersHands[this.socket.id];
      if (this.isCompleteHand(playerHand)) {
        this.socket.emit("notChallenge");
      } else {
        this.showErrorMessage(
          "Your hand is not complete. You need 5 cards of the same suit with a total value of at least 31 points."
        );
      }
    }
  }

  showErrorMessage(message) {
    this.errorLabel.setText(message);
    this.time.delayedCall(5000, () => {
      this.errorLabel.setText("");
    });
  }
}

export default FiftyOneScene;
