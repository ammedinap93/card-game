import * as AudioManager from "./audioManager.js";
import { FIFTY_ONE_GAME_STATUS } from "./fiftyOneConstants.js";

export const CARD_WIDTH = 256;
export const CARD_HEIGHT = 356;
export const CARD_SCALE = 0.3;

export function getCardTexture(card) {
  const [rank, _] = card.split("_");
  return rank === "joker" ? "jokers" : "playingCards";
}

export function setCardFrame(cardSprite, card) {
  const [rank, suit] = card.split("_");
  if (rank !== "joker") {
    const suitIndex = ["clubs", "hearts", "spades", "diamonds"].indexOf(suit);
    const rankIndex = [
      "ace",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "jack",
      "queen",
      "king",
    ].indexOf(rank);
    cardSprite.setFrame(suitIndex * 13 + rankIndex);
  } else {
    cardSprite.setFrame(suit === "red" ? 1 : 0);
  }
}

export function createCardSprite(scene, card, isVisible) {
  const texture = getCardTexture(card);
  const cardSprite = scene.add
    .sprite(0, 0, texture, 0)
    .setOrigin(0.5)
    .setScale(CARD_SCALE)
    .setInteractive();

  if (isVisible) {
    setCardFrame(cardSprite, card);
  } else {
    cardSprite.setTexture("cardBacks");
    cardSprite.setFrame(0);
  }

  return cardSprite;
}

function getCardPosition(
  scene,
  index,
  playerIndex,
  cardIndex,
  numCards,
  isCurrentPlayer,
  totalPlayers
) {
  const positions = [
    {
      x: scene.sys.game.config.width / 2,
      y: scene.sys.game.config.height - 90,
      rotation: 0,
      offsetX: getPlayerHandCardOffset(numCards, isCurrentPlayer),
      offsetY: 0,
    }, // Bottom
    {
      x: scene.sys.game.config.width - 90,
      y: scene.sys.game.config.height / 2,
      rotation: Phaser.Math.DegToRad(-90),
      offsetX: 0,
      offsetY: getPlayerHandCardOffset(numCards, isCurrentPlayer),
    }, // Right
    {
      x: scene.sys.game.config.width / 2,
      y: 90,
      rotation: 0,
      offsetX: getPlayerHandCardOffset(numCards, isCurrentPlayer),
      offsetY: 0,
    }, // Top
    {
      x: 90,
      y: scene.sys.game.config.height / 2,
      rotation: Phaser.Math.DegToRad(90),
      offsetX: 0,
      offsetY: getPlayerHandCardOffset(numCards, isCurrentPlayer),
    }, // Left
  ];

  let adjustedIndex;
  if (totalPlayers === 2) {
    adjustedIndex = ((index - playerIndex + 2) % 2) * 2; // Use bottom and top positions
  } else if (totalPlayers === 3) {
    adjustedIndex = ((index - playerIndex + 3) % 3) * 1.334; // Use bottom, right, and top positions
  } else {
    adjustedIndex = (index - playerIndex + 4) % 4;
  }

  const { x, y, rotation, offsetX, offsetY } =
    positions[Math.floor(adjustedIndex)];

  const totalWidth = (numCards - 1) * offsetX;
  const totalHeight = (numCards - 1) * offsetY;
  const startX = x - totalWidth / 2;
  const startY = y - totalHeight / 2;

  return {
    x: startX + cardIndex * offsetX,
    y: startY + cardIndex * offsetY,
    rotation: rotation,
  };
}

export function getPlayerCardPosition(
  scene,
  playerIndex,
  currentPlayerIndex,
  handLength,
  cardIndex,
  totalPlayers
) {
  const position = getCardPosition(
    scene,
    playerIndex,
    currentPlayerIndex,
    cardIndex,
    handLength,
    playerIndex === currentPlayerIndex,
    totalPlayers
  );
  return { x: position.x, y: position.y, rotation: position.rotation };
}

function getPlayerHandCardOffset(numCards, isCurrentPlayer) {
  let offset = 30;
  if (isCurrentPlayer && numCards > 14) {
    offset = 450 / numCards;
  } else if (!isCurrentPlayer && numCards > 8) {
    offset = 270 / numCards;
  }
  return offset;
}

function isOverFaceUpCards(scene, x, y) {
  const faceUpCardsRect = scene.faceUpCardsDropZone.getBounds();
  return faceUpCardsRect.contains(x, y);
}

function setupDraggableCard(
  scene,
  cardSprite,
  id,
  gameState,
  index,
  playerIndex,
  handLength
) {
  scene.input.setDraggable(cardSprite);
  cardSprite.on("dragstart", () => {
    if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.playerInTurn === scene.socket.id
    ) {
      return; // Prevent dragging if it's the player's turn to select cards for exchange easily.
    }
    scene.cardOriginalDepths[id] = cardSprite.depth;
    cardSprite.setDepth(1000);
    AudioManager.playRandomSound(scene.cardSlideSounds);
  });
  cardSprite.on("drag", (pointer) => {
    if (gameState.paused) return; // Prevent dragging if the game is paused
    if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.playerInTurn === scene.socket.id
    ) {
      return; // Prevent dragging if it's the player's turn to select cards for exchange easily.
    }
    cardSprite.x = pointer.x;
    cardSprite.y = pointer.y;
  });
  cardSprite.on("dragend", (pointer) => {
    if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.playerInTurn === scene.socket.id
    ) {
      return; // Prevent dragging if it's the player's turn to select cards for exchange easily.
    }
    const playerCards = scene.playerCards[scene.socket.id];
    const originalIndex = playerCards.indexOf(cardSprite);
    const originalPosition = getCardPosition(
      scene,
      index,
      playerIndex,
      originalIndex,
      handLength,
      true,
      gameState.distributionOfSeats.length
    );

    if (gameState.paused) {
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
      return;
    }
    if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === scene.socket.id &&
      isOverFaceUpCards(scene, pointer.x, pointer.y)
    ) {
      scene.socket.emit("discardCard", { cardId: id });
      AudioManager.playRandomSound(scene.cardPlaceSounds);
    } else if (Math.abs(pointer.y - originalPosition.y) > 90) {
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
    } else {
      const playerCardPositions = playerCards.map((card, idx) =>
        getCardPosition(
          scene,
          gameState.distributionOfSeats.indexOf(scene.socket.id),
          playerIndex,
          idx,
          handLength,
          true,
          gameState.distributionOfSeats.length
        )
      );
      const targetIndex = playerCardPositions.findIndex(
        (position) => pointer.x < position.x
      );
      if (targetIndex === originalIndex) {
        cardSprite.x = originalPosition.x;
        cardSprite.y = originalPosition.y;
        cardSprite.setDepth(scene.cardOriginalDepths[id]);
      } else if (targetIndex === -1) {
        scene.socket.emit("moveCard", {
          cardId: id,
          newIndex: playerCards.length - 1,
        });
      } else {
        scene.socket.emit("moveCard", {
          cardId: id,
          newIndex: targetIndex,
        });
      }
      AudioManager.playRandomSound(scene.cardSlideSounds);
    }
  });
}

function setupHoverCard(scene, cardSprite, id) {
  cardSprite.setInteractive();
  cardSprite.on("pointerover", () => {
    scene.cardOriginalDepths[id] = cardSprite.depth;
    cardSprite.setDepth(1000);
  });
  cardSprite.on("pointerout", () => {
    cardSprite.setDepth(scene.cardOriginalDepths[id]);
  });
}

export function setupCardInteraction(scene, cardSprite, card, isFaceUpCard) {
  cardSprite.setInteractive();
  cardSprite.on("pointerdown", () => {
    if (
      scene.gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      scene.gameState.playerInTurn === scene.socket.id
    ) {
      if (isFaceUpCard) {
        toggleCardSelection(scene, card, cardSprite, scene.takingCards);
      } else {
        toggleCardSelection(scene, card, cardSprite, scene.discardingCards);
      }
    }
  });
}

function toggleCardSelection(scene, card, cardSprite, selectionArray) {
  const index = selectionArray.findIndex((c) => c.id === card.id);
  if (index === -1) {
    selectionArray.push(card);
    cardSprite.setTint(0x6495ed);
  } else {
    selectionArray.splice(index, 1);
    cardSprite.clearTint();
  }
}

export function getFaceUpCardPosition(scene, index) {
  let x = scene.sys.game.config.width / 2;
  let y = scene.sys.game.config.height / 2;

  switch (index) {
    case 0:
      x += 100;
      break;
    case 1:
      x += 200;
      break;
    case 2:
      x += 100;
      y -= 120;
      break;
    case 3:
      x += 200;
      y -= 120;
      break;
  }

  return { x, y };
}

export function getDiscardPilePosition(scene) {
  let x = scene.sys.game.config.width / 2 - 200;
  let y = scene.sys.game.config.height / 2 + 150;
  let rotation = Phaser.Math.DegToRad(-45);

  return { x, y, rotation };
}

export function updatePlayerCards(scene, gameState) {
  // Clear existing player cards
  if (scene.playerCards) {
    Object.values(scene.playerCards).forEach((cards) =>
      cards.forEach((card) => card.destroy())
    );
  }
  scene.playerCards = {};

  const playerIndex = scene.playerIndex;

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const playerHand = gameState.playersHands[playerId] || [];
    const playerCards = [];

    playerHand.forEach(({ id, card }, cardIndex) => {
      if (gameState.dealingCard && gameState.dealingCard.id === id) {
        return; // Ignore the card if it's the same as the dealing card
      }

      if (
        gameState.takingCards.some((c) => c.id === id) ||
        gameState.discardingCards.some((c) => c.id === id)
      ) {
        return; // Ignore the card if it's being exchanged
      }

      const isCurrentPlayer = playerId === scene.socket.id;
      let cardSprite = createCardSprite(scene, card, isCurrentPlayer);

      const cardPosition = getCardPosition(
        scene,
        index,
        playerIndex,
        cardIndex,
        playerHand.length,
        isCurrentPlayer,
        gameState.distributionOfSeats.length
      );
      cardSprite.setPosition(cardPosition.x, cardPosition.y);
      cardSprite.setRotation(cardPosition.rotation);
      cardSprite.setDepth(cardIndex);
      scene.cardOriginalDepths[id] = cardIndex;

      if (isCurrentPlayer) {
        setupDraggableCard(
          scene,
          cardSprite,
          id,
          gameState,
          index,
          playerIndex,
          playerHand.length
        );
        setupCardInteraction(scene, cardSprite, { id, card }, false);
      }

      playerCards.push(cardSprite);
    });

    scene.playerCards[playerId] = playerCards;
  });

  return scene.playerCards;
}
