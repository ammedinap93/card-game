import * as CardManager from "./cardManager.js";
import { FIFTY_ONE_GAME_STATUS } from "./fiftyOneConstants.js";

export function createBackgroundImage(scene) {
  const backgroundImage = scene.add.image(0, 0, "background").setOrigin(0);
  backgroundImage.setDisplaySize(
    scene.sys.game.config.width,
    scene.sys.game.config.height
  );
  return backgroundImage;
}

export function createButton(scene, x, y, text, visible, centered, callback) {
  const button = scene.add.text(x, y, text, {
    font: "18px bold-font",
    fill: "#ffff00", // Yellow color
  });
  if (centered) {
    button.setOrigin(0.5);
  } else {
    button.setOrigin(0, 0.5);
  }
  button
    .setInteractive({ useHandCursor: true })
    .setVisible(visible)
    .on("pointerdown", callback)
    .on("pointerover", () => button.setStyle({ fill: "#ffffff" })) // White on hover
    .on("pointerout", () => button.setStyle({ fill: "#ffff00" })); // Back to yellow

  return button;
}

export function createPlayerInTurnLabel(scene, message) {
  return scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2 + 68,
      message,
      {
        font: "18px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
}

export function updatePlayerInTurnLabel(scene, gameState) {
  const playerInTurn = gameState.lobby.players.find(
    (p) => p.id === gameState.playerInTurn
  );

  let turnMessage = "";
  if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.ready_to_deal) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn to deal. ${scene.dealerTimer || ""}`
        : `${playerInTurn.username} deals. ${scene.dealerTimer || ""}`;
  } else if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.dealing_cards) {
    turnMessage = "Dealing cards";
  } else if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn! ${scene.playerTurnTimer || ""}`
        : `${playerInTurn.username}'s turn: ${scene.playerTurnTimer || ""}`;
  } else if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn_in_progress
  ) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `Discard one of your cards to end the turn. ${
            scene.playerTurnInProgressTimer || ""
          }`
        : `Waiting for ${playerInTurn.username}... ${
            scene.playerTurnInProgressTimer || ""
          }`;
  } else if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers
  ) {
    const caller = gameState.lobby.players.find(
      (p) => p.id === gameState.caller
    );
    if (gameState.caller === scene.socket.id) {
      turnMessage = `You called. Waiting for challengers... ${
        scene.waitingForChallengersTimer || ""
      }`;
    } else if (gameState.challengerCandidates.includes(scene.socket.id)) {
      turnMessage = `${caller.username} called. Do you want to challenge? ${
        scene.waitingForChallengersTimer || ""
      }`;
    } else {
      turnMessage = `${caller.username} called. Waiting for challengers... ${
        scene.waitingForChallengersTimer || ""
      }`;
    }
  }

  scene.playerInTurnLabel.setText(turnMessage);
}

export function updatePlayerTurnTimer(scene, remainingTime) {
  scene.playerTurnTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updatePlayerTurnInProgressTimer(scene, remainingTime) {
  scene.playerTurnInProgressTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateDealerTimer(scene, remainingTime) {
  scene.dealerTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateWaitingForChallengersTimer(scene, remainingTime) {
  scene.waitingForChallengersTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateDeckPile(scene, gameState) {
  if (scene.deckPile) {
    scene.deckPile.clear(true, true);
  } else {
    scene.deckPile = scene.add.group();
  }

  gameState.deckPile.forEach((card, index) => {
    const cardSprite = scene.add
      .sprite(
        scene.sys.game.config.width / 2,
        scene.sys.game.config.height / 2,
        "cardBacks",
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(index)
      .setInteractive();

    cardSprite.on("pointerdown", () => {
      if (gameState.paused) return; // Prevent interaction if the game is paused
      scene.clearSelectedCards();
      if (
        gameState.gameStatus === FIFTY_ONE_GAME_STATUS.ready_to_deal &&
        gameState.dealer === scene.socket.id
      ) {
        scene.socket.emit("dealCards");
      } else if (
        gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
        gameState.playerInTurn === scene.socket.id
      ) {
        scene.socket.emit("takeCard", { fromDeckPile: true });
      }
    });

    scene.deckPile.add(cardSprite);
  });
}

export function updateDiscardPile(scene, gameState) {
  if (scene.discardPile) {
    scene.discardPile.clear(true, true);
  } else {
    scene.discardPile = scene.add.group();
  }

  if (gameState.discardPile.length > 0) {
    if (
      gameState.discardingCards &&
      gameState.discardingCards.length > 0 &&
      gameState.discardPile.length <= 4
    ) {
      return; // Ignore the cards if it's the same as the discarding cards
    }
    const { x, y, rotation } = CardManager.getDiscardPilePosition(scene);
    const cardSprite = scene.add
      .sprite(x, y, "cardBacks", 0)
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setRotation(rotation);

    scene.discardPile.add(cardSprite);
  }
}

export function updateFaceUpCards(scene, gameState) {
  if (scene.faceUpCardSprites) {
    scene.faceUpCardSprites.clear(true, true);
  } else {
    scene.faceUpCardSprites = scene.add.group();
  }

  // Create new face-up card sprites
  gameState.faceUpCards.forEach((card, index) => {
    if (
      gameState.discardingCard &&
      gameState.discardingCard.id === card.id &&
      gameState.discardingCard.playerId !== scene.socket.id
    ) {
      return; // Ignore the card if it's the same as the discarding card
    }

    if (
      gameState.takingCards.some((c) => c.id === card.id) ||
      gameState.discardingCards.some((c) => c.id === card.id)
    ) {
      return; // Ignore the card if it's being exchanged
    }

    const { x, y } = CardManager.getFaceUpCardPosition(scene, index);
    const cardSprite = CardManager.createCardSprite(scene, card.card, true);
    cardSprite.setPosition(x, y);
    CardManager.setupCardInteraction(scene, cardSprite, card, true);
    scene.faceUpCardSprites.add(cardSprite);
  });

  createFaceUpCardsDropZone(scene);
}

export function createFaceUpCardsDropZone(scene) {
  // Update or create the face up cards drop zone
  if (scene.faceUpCardsDropZone) {
    scene.faceUpCardsDropZone.destroy();
  }
  scene.faceUpCardsDropZone = scene.add
    .rectangle(
      scene.sys.game.config.width / 2 + 150,
      scene.sys.game.config.height / 2 - 60,
      200,
      250,
      0xffffff,
      0
    )
    .setStrokeStyle();
}

export function createPauseOverlay(scene) {
  const pauseOverlay = scene.add
    .rectangle(
      0,
      0,
      scene.sys.game.config.width,
      scene.sys.game.config.height,
      0x000000,
      0.5
    )
    .setOrigin(0);
  pauseOverlay.setDepth(15000);
  pauseOverlay.setVisible(false);

  const pauseText = scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2,
      "Game Paused",
      {
        font: "32px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
  pauseText.setDepth(15010);
  pauseText.setVisible(false);

  return { pauseOverlay, pauseText };
}

export function updatePauseOverlay(scene, gameState) {
  const { pauseOverlay, pauseText } = scene.pauseOverlay;
  pauseOverlay.setVisible(gameState.paused);
  pauseText.setVisible(gameState.paused);
}
