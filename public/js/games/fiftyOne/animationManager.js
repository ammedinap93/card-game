import * as CardManager from "./cardManager.js";
import * as AudioManager from "./audioManager.js";
import { FIFTY_ONE_GAME_STATUS } from "./fiftyOneConstants.js";

export function animateDealingCard(scene, gameState) {
  if (gameState.dealingCard) {
    const { card, playerId } = gameState.dealingCard;
    const targetPlayerIndex = gameState.distributionOfSeats.indexOf(playerId);
    const playerPosition = CardManager.getPlayerCardPosition(
      scene,
      targetPlayerIndex,
      scene.playerIndex,
      gameState.playersHands[playerId].length,
      gameState.playersHands[playerId].length - 1,
      gameState.distributionOfSeats.length
    );

    const dealingCard = scene.add
      .sprite(
        scene.sys.game.config.width / 2,
        scene.sys.game.config.height / 2,
        "cardBacks",
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(1000);

    scene.tweens.add({
      targets: dealingCard,
      x: playerPosition.x,
      y: playerPosition.y,
      rotation: playerPosition.rotation,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardSlideSounds);
      },
      onComplete: () => {
        dealingCard.destroy();
      },
    });
  }
}

export function animateDiscardingCard(scene, gameState) {
  if (
    gameState.discardingCard &&
    gameState.discardingCard.playerId !== scene.socket.id
  ) {
    const { card, playerId } = gameState.discardingCard;

    const originPlayerIndex = gameState.distributionOfSeats.indexOf(playerId);
    let playerPosition = CardManager.getPlayerCardPosition(
      scene,
      originPlayerIndex,
      scene.playerIndex,
      gameState.playersHands[playerId].length,
      gameState.playersHands[playerId].length - 1,
      gameState.distributionOfSeats.length
    );

    const faceUpCardIndex = gameState.faceUpCards.length - 1;
    const endPos = CardManager.getFaceUpCardPosition(scene, faceUpCardIndex);

    const discardingCard = scene.add
      .sprite(
        playerPosition.x,
        playerPosition.y,
        CardManager.getCardTexture(card),
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(1000)
      .setRotation(playerPosition.rotation);

    CardManager.setCardFrame(discardingCard, card);

    scene.tweens.add({
      targets: discardingCard,
      x: endPos.x,
      y: endPos.y,
      rotation: 0,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardSlideSounds);
      },
      onComplete: () => {
        discardingCard.destroy();
        AudioManager.playRandomSound(scene.cardPlaceSounds);
      },
    });
  }
}

export function animateDiscardingFaceUpCards(scene, gameState) {
  if (
    gameState.gameStatus ===
      FIFTY_ONE_GAME_STATUS.discarding_from_face_up_cards &&
    gameState.discardingCards &&
    gameState.discardingCards.length > 0
  ) {
    gameState.discardingCards.forEach((card, index) => {
      const faceUpCardPosition = CardManager.getFaceUpCardPosition(
        scene,
        index
      );
      const discardPilePosition = CardManager.getDiscardPilePosition(scene);

      const discardingCard = scene.add
        .sprite(
          faceUpCardPosition.x,
          faceUpCardPosition.y,
          CardManager.getCardTexture(card.card),
          0
        )
        .setOrigin(0.5)
        .setScale(CardManager.CARD_SCALE)
        .setDepth(1000);

      CardManager.setCardFrame(discardingCard, card.card);

      scene.tweens.add({
        targets: discardingCard,
        x: discardPilePosition.x,
        y: discardPilePosition.y,
        rotation: discardPilePosition.rotation,
        duration: 500,
        onStart: () => {
          AudioManager.playRandomSound(scene.cardSlideSounds);
        },
        onComplete: () => {
          discardingCard.destroy();
          AudioManager.playRandomSound(scene.cardPlaceSounds);
        },
      });
    });
  }
}

export function animateExchangingCards(scene, gameState) {
  if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.taking_from_face_up_cards &&
    gameState.takingCards.length > 0 &&
    gameState.discardingCards.length > 0
  ) {
    const playerInTurnIndex = gameState.distributionOfSeats.indexOf(
      gameState.playerInTurn
    );

    gameState.takingCards.forEach((takingCard) => {
      const startPos = CardManager.getFaceUpCardPosition(
        scene,
        takingCard.originalIndex
      );
      const handCardIndex = gameState.playersHands[
        gameState.playerInTurn
      ].findIndex((card) => card.id === takingCard.id);
      const endPos = CardManager.getPlayerCardPosition(
        scene,
        playerInTurnIndex,
        scene.playerIndex,
        gameState.playersHands[gameState.playerInTurn].length,
        handCardIndex,
        gameState.distributionOfSeats.length
      );

      const sprite = scene.add
        .sprite(
          startPos.x,
          startPos.y,
          CardManager.getCardTexture(takingCard.card)
        )
        .setOrigin(0.5)
        .setScale(CardManager.CARD_SCALE)
        .setDepth(1000);

      CardManager.setCardFrame(sprite, takingCard.card);

      scene.tweens.add({
        targets: sprite,
        x: endPos.x,
        y: endPos.y,
        rotation: endPos.rotation,
        duration: 500,
        onStart: () => {
          AudioManager.playRandomSound(scene.cardSlideSounds);
        },
        onComplete: () => {
          sprite.destroy();
          AudioManager.playRandomSound(scene.cardPlaceSounds);
        },
      });
    });

    gameState.discardingCards.forEach((discardingCard) => {
      const startPos = CardManager.getPlayerCardPosition(
        scene,
        playerInTurnIndex,
        scene.playerIndex,
        gameState.playersHands[gameState.playerInTurn].length,
        discardingCard.originalIndex,
        gameState.distributionOfSeats.length,
      );
      const faceUpCardIndex = gameState.faceUpCards.findIndex(
        (card) => card.id === discardingCard.id
      );
      const endPos = CardManager.getFaceUpCardPosition(scene, faceUpCardIndex);

      const sprite = scene.add
        .sprite(
          startPos.x,
          startPos.y,
          CardManager.getCardTexture(discardingCard.card)
        )
        .setOrigin(0.5)
        .setScale(CardManager.CARD_SCALE)
        .setDepth(1000)
        .setRotation(startPos.rotation);

      CardManager.setCardFrame(sprite, discardingCard.card);

      scene.tweens.add({
        targets: sprite,
        x: endPos.x,
        y: endPos.y,
        rotation: 0,
        duration: 500,
        onStart: () => {
          AudioManager.playRandomSound(scene.cardSlideSounds);
        },
        onComplete: () => {
          sprite.destroy();
          AudioManager.playRandomSound(scene.cardPlaceSounds);
        },
      });
    });
  }
}
