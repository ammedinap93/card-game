export const FIFTY_ONE_GAME_STATUS = {
  ready_to_deal: "ready_to_deal",
  dealing_cards: "dealing_cards",
  player_turn: "player_turn",
  taking_from_deck_pile: "taking_from_deck_pile",
  taking_from_face_up_cards: "taking_from_face_up_cards",
  discarding_from_player: "discarding_from_player",
  discarding_from_face_up_cards: "discarding_from_face_up_cards",
  player_turn_in_progress: "player_turn_in_progress",
  waiting_for_challengers: "waiting_for_challengers",
  end_of_the_hand: "end_of_the_hand",
  end_of_the_game: "end_of_the_game",
};
