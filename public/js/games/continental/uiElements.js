import * as CardManager from "./cardManager.js";
import { CONTINENTAL_GAME_STATUS } from "./continentalConstants.js";

export function createBackgroundImage(scene) {
  const backgroundImage = scene.add.image(0, 0, "background").setOrigin(0);
  backgroundImage.setDisplaySize(
    scene.sys.game.config.width,
    scene.sys.game.config.height
  );
  return backgroundImage;
}

export function createGoalDescriptionLabel(scene) {
  return scene.add
    .text(20, scene.sys.game.config.height - 20, "", {
      font: "18px regular-font",
      fill: "#ffffff",
    })
    .setOrigin(0, 0.5);
}

export function createButton(scene, x, y, text, visible, centered, callback) {
  const button = scene.add.text(x, y, text, {
    font: "18px bold-font",
    fill: "#ffff00", // Yellow color
  });
  if (centered) {
    button.setOrigin(0.5);
  } else {
    button.setOrigin(0, 0.5);
  }
  button
    .setInteractive({ useHandCursor: true })
    .setVisible(visible)
    .on("pointerdown", callback)
    .on("pointerover", () => button.setStyle({ fill: "#ffffff" })) // White on hover
    .on("pointerout", () => button.setStyle({ fill: "#ffff00" })); // Back to yellow

  return button;
}

export function createPlayerInTurnLabel(scene, message) {
  return scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2 + 68,
      message,
      {
        font: "18px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
}

export function updatePlayerInTurnLabel(scene, gameState) {
  const playerInTurn = gameState.lobby.players.find(
    (p) => p.id === gameState.playerInTurn
  );

  let turnMessage = "";
  if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.ready_to_deal) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn to deal. ${scene.dealerTimer || ""}`
        : `${playerInTurn.username} deals. ${scene.dealerTimer || ""}`;
  } else if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.dealing_cards) {
    turnMessage = "Dealing cards";
  } else if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn! ${scene.playerTurnTimer || ""}`
        : `${playerInTurn.username}'s turn: ${scene.playerTurnTimer || ""}`;
  } else if (
    gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn_in_progress
  ) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `Discard one of your cards to end the turn. ${
            scene.playerTurnInProgressTimer || ""
          }`
        : `Waiting for ${playerInTurn.username}... ${
            scene.playerTurnInProgressTimer || ""
          }`;
  } else if (
    gameState.gameStatus ===
    CONTINENTAL_GAME_STATUS.waiting_for_picking_card_out_of_turn
  ) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `Waiting for players to pick out of turn: ${
            scene.pickCardOutOfTurnTimer || ""
          }`
        : `Do you want to pick out of turn? ${
            scene.pickCardOutOfTurnTimer || ""
          }`;
  }

  scene.playerInTurnLabel.setText(turnMessage);
}

export function updatePickCardOutOfTurnTimer(scene, remainingTime) {
  scene.pickCardOutOfTurnTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updatePlayerTurnTimer(scene, remainingTime) {
  scene.playerTurnTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updatePlayerTurnInProgressTimer(scene, remainingTime) {
  scene.playerTurnInProgressTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateDealerTimer(scene, remainingTime) {
  scene.dealerTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateDeckPile(scene, gameState) {
  if (scene.deckPile) {
    scene.deckPile.clear(true, true);
  } else {
    scene.deckPile = scene.add.group();
  }

  gameState.deckPile.forEach((card, index) => {
    const cardSprite = scene.add
      .sprite(
        scene.sys.game.config.width / 2,
        scene.sys.game.config.height / 2,
        "cardBacks",
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(index)
      .setInteractive();

    cardSprite.on("pointerdown", () => {
      if (gameState.paused) return; // Prevent interaction if the game is paused
      if (
        gameState.gameStatus === CONTINENTAL_GAME_STATUS.ready_to_deal &&
        gameState.dealer === scene.socket.id
      ) {
        scene.socket.emit("dealCards");
      } else if (
        gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn &&
        gameState.playerInTurn === scene.socket.id
      ) {
        scene.socket.emit("takeCard", { fromDeckPile: true });
      }
    });

    scene.deckPile.add(cardSprite);
  });
}

export function updateDiscardPile(scene, gameState) {
  if (scene.discardPile) {
    scene.discardPile.clear(true, true);
  } else {
    scene.discardPile = scene.add.group();
  }

  if (gameState.discardPile.length > 0) {
    let topCard = gameState.discardPile[gameState.discardPile.length - 1];

    // Check if the card is the same as the discarding card
    if (
      gameState.discardingCard &&
      gameState.discardingCard.id === topCard.id &&
      gameState.discardingCard.playerId !== scene.socket.id
    ) {
      topCard =
        gameState.discardPile.length > 1
          ? gameState.discardPile[gameState.discardPile.length - 2]
          : null;
    }

    if (topCard) {
      const cardSprite = CardManager.createCardSprite(
        scene,
        topCard.card,
        true
      );
      cardSprite.setPosition(
        scene.sys.game.config.width / 2 + 100,
        scene.sys.game.config.height / 2
      );

      // Add blue tint if the player is waiting to pick out of turn
      if (
        gameState.gameStatus ===
          CONTINENTAL_GAME_STATUS.waiting_for_picking_card_out_of_turn &&
        gameState.waitingForPickingCardOutOfTurn.includes(scene.socket.id)
      ) {
        cardSprite.setTint(0x6495ed); // Cornflower Blue
      }

      cardSprite.on("pointerdown", () => {
        if (gameState.paused) return; // Prevent interaction if the game is paused
        if (
          gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn &&
          gameState.playerInTurn === scene.socket.id
        ) {
          scene.socket.emit("takeCard", { fromDeckPile: false });
        } else if (
          (gameState.gameStatus ===
            CONTINENTAL_GAME_STATUS.player_turn_in_progress ||
            gameState.gameStatus ===
              CONTINENTAL_GAME_STATUS.waiting_for_picking_card_out_of_turn) &&
          gameState.playerInTurn !== scene.socket.id &&
          !topCard.isDead
        ) {
          scene.socket.emit("pickCardOutOfTurn");
        }
      });

      scene.discardPile.add(cardSprite);
    }
  }

  // Update or create the discard pile drop zone
  if (scene.discardPileDropZone) {
    scene.discardPileDropZone.destroy();
  }
  scene.discardPileDropZone = scene.add
    .rectangle(
      scene.sys.game.config.width / 2 + 100,
      scene.sys.game.config.height / 2,
      90,
      120,
      0xffffff,
      0
    )
    .setStrokeStyle();
}

export function createPauseOverlay(scene) {
  const pauseOverlay = scene.add
    .rectangle(
      0,
      0,
      scene.sys.game.config.width,
      scene.sys.game.config.height,
      0x000000,
      0.5
    )
    .setOrigin(0);
  pauseOverlay.setDepth(15000);
  pauseOverlay.setVisible(false);

  const pauseText = scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2,
      "Game Paused",
      {
        font: "32px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
  pauseText.setDepth(15010);
  pauseText.setVisible(false);

  return { pauseOverlay, pauseText };
}

export function updatePauseOverlay(scene, gameState) {
  const { pauseOverlay, pauseText } = scene.pauseOverlay;
  pauseOverlay.setVisible(gameState.paused);
  pauseText.setVisible(gameState.paused);
}
