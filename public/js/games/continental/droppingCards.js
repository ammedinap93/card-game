export function isATrio(cards, errors = []) {
  if (cards.length < 3) {
    errors.push("A trio must have at least 3 cards.");
    return false;
  }

  const counts = {};
  let redAces = 0;
  let jokers = 0;

  cards.forEach((card) => {
    const [rank, suit] = card.card.split("_");
    if (rank === "ace" && (suit === "hearts" || suit === "diamonds")) {
      redAces++;
    } else if (rank === "joker") {
      jokers++;
    } else {
      counts[rank] = (counts[rank] || 0) + 1;
    }
  });

  const regularRanks = Object.keys(counts);
  const regularCards = regularRanks.reduce(
    (sum, rank) => sum + counts[rank],
    0
  );

  if (
    regularRanks.length === 0 ||
    (regularRanks.length === 1 && regularRanks[0] === "ace")
  ) {
    // If there are no regular cards or the regular cards are black aces,
    // the red aces count as regular cards
    if (regularCards + redAces + jokers !== cards.length) {
      errors.push("Invalid combination of cards for a trio.");
      return false;
    }
    if (regularCards + redAces <= jokers) {
      errors.push("Too many wild cards in the trio.");
      return false;
    }
    return true;
  } else {
    // If there are regular cards and they aren't black aces,
    // the red aces count as wild cards
    if (regularRanks.length !== 1) {
      errors.push("A trio must consist of cards of the same rank.");
      return false;
    }
    if (regularCards + redAces + jokers !== cards.length) {
      errors.push("Invalid combination of cards for a trio.");
      return false;
    }
    if (regularCards <= redAces + jokers) {
      errors.push("Too many wild cards in the trio.");
      return false;
    }
    return true;
  }
}

export function isARun(cards, errors = []) {  
  if (isAValidRun(cards, errors)) {
    return true;
  } else {
    errors = [];
    const reversedCards = cards.toReversed();
    return isAValidRun(reversedCards, errors);
  }  
}

function isAValidRun(cards, errors = []) {
  if (cards.length < 4) {
    errors.push("A run must have at least 4 cards.");
    return false;
  }

  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Determine the suit of the run
  let runSuit;
  for (const card of cards) {
    const [rank, suit] = card.card.split("_");
    if (
      rank !== "joker" &&
      !(rank === "ace" && (suit === "hearts" || suit === "diamonds"))
    ) {
      runSuit = suit;
      break;
    }
  }

  if (!runSuit) {
    errors.push("No regular cards found in the run.");
    return false;
  }

  // Check if all regular cards have the same suit
  if (
    !cards.every((card) => {
      const [rank, suit] = card.card.split("_");
      return (
        rank === "joker" ||
        (rank === "ace" && (suit === "hearts" || suit === "diamonds")) ||
        suit === runSuit
      );
    })
  ) {
    errors.push("All regular cards in a run must have the same suit.");
    return false;
  }

  const convertedRanks = new Array(cards.length).fill(null);

  // Fill convertedRanks array
  while (convertedRanks.includes(null)) {
    for (let i = 0; i < convertedRanks.length; i++) {
      if (convertedRanks[i] === null) {
        const [rank, suit] = cards[i].card.split("_");
        if (
          rank !== "joker" &&
          !(rank === "ace" && (suit === "hearts" || suit === "diamonds"))
        ) {
          convertedRanks[i] = ranks.indexOf(rank);
        } else {
          const nextRank = convertedRanks[i + 1];
          const prevRank = convertedRanks[i - 1];
          if (nextRank !== null && nextRank !== undefined) {
            convertedRanks[i] = (nextRank - 1 + ranks.length) % ranks.length;
          } else if (prevRank !== null && prevRank !== undefined) {
            convertedRanks[i] = (prevRank + 1) % ranks.length;
          }
        }
      }
    }
  }

  // Verify sequence and count regular/wild cards
  let regularCards = 0;
  let wildCards = 0;
  for (let i = 0; i < convertedRanks.length - 1; i++) {
    if ((convertedRanks[i] + 1) % ranks.length !== convertedRanks[i + 1]) {
      errors.push("Invalid sequence in the run.");
      return false;
    }

    const [rank, suit] = cards[i].card.split("_");
    if (rank === "joker") {
      wildCards++;
    } else if (rank === "ace" && (suit === "hearts" || suit === "diamonds")) {
      if (suit === runSuit && convertedRanks[i] === 0) {
        regularCards++;
      } else {
        wildCards++;
      }
    } else {
      regularCards++;
    }
  }

  // Count the last card
  const [lastRank, lastSuit] = cards[cards.length - 1].card.split("_");
  if (lastRank === "joker") {
    wildCards++;
  } else if (
    lastRank === "ace" &&
    (lastSuit === "hearts" || lastSuit === "diamonds")
  ) {
    if (lastSuit === runSuit && convertedRanks[cards.length - 1] === 0) {
      regularCards++;
    } else {
      wildCards++;
    }
  } else {
    regularCards++;
  }

  if (regularCards <= wildCards) {
    errors.push("Too many wild cards in the run.");
    return false;
  }

  return true;
}

export function isAValidInitialDrop(combinations, goal, errors = []) {
  // Check for unique card IDs
  const allCards = combinations.flat();
  const uniqueCardIds = new Set(allCards.map((card) => card.id));
  if (uniqueCardIds.size !== allCards.length) {
    errors.push("Duplicate cards found in the combinations.");
    return false;
  }

  const combinationTypes = {
    TT: (combinations) => {
      if (combinations.length !== 2) {
        errors.push("Two trios required.");
        return false;
      }
      return combinations.every((combination, index) => {
        const trioErrors = [];
        const isTrio = isATrio(combination, trioErrors);
        if (!isTrio) {
          errors.push(`Trio ${index + 1}: ${trioErrors.join(" ")}`);
        }
        return isTrio;
      });
    },
    TR: (combinations) => {
      if (combinations.length !== 2) {
        errors.push("One trio and one run required.");
        return false;
      }
      const trioErrors = [];
      const runErrors = [];
      const hasTrio = combinations.some((combo) => isATrio(combo, trioErrors));
      const hasRun = combinations.some((combo) => isARun(combo, runErrors));
      if (!hasTrio) errors.push(`Trio: ${trioErrors.join(" ")}`);
      if (!hasRun) errors.push(`Run: ${runErrors.join(" ")}`);
      return hasTrio && hasRun;
    },
    RR: (combinations) => {
      if (combinations.length !== 2) {
        errors.push("Two runs required.");
        return false;
      }
      return combinations.every((combination, index) => {
        const runErrors = [];
        const isRun = isARun(combination, runErrors);
        if (!isRun) {
          errors.push(`Run ${index + 1}: ${runErrors.join(" ")}`);
        }
        return isRun;
      });
    },
    TTT: (combinations) => {
      if (combinations.length !== 3) {
        errors.push("Three trios required.");
        return false;
      }
      return combinations.every((combination, index) => {
        const trioErrors = [];
        const isTrio = isATrio(combination, trioErrors);
        if (!isTrio) {
          errors.push(`Trio ${index + 1}: ${trioErrors.join(" ")}`);
        }
        return isTrio;
      });
    },
    TRT: (combinations) => {
      if (combinations.length !== 3) {
        errors.push("Two trios and one run required.");
        return false;
      }
      const trioCount = combinations.filter((combo) => {
        const trioErrors = [];
        return isATrio(combo, trioErrors);
      }).length;
      const runCount = combinations.filter((combo) => {
        const runErrors = [];
        return isARun(combo, runErrors);
      }).length;
      if (trioCount !== 2 || runCount !== 1) {
        errors.push(
          `Invalid combination: ${trioCount} trios and ${runCount} runs found.`
        );
        return false;
      }
      return true;
    },
    RTR: (combinations) => {
      if (combinations.length !== 3) {
        errors.push("One trio and two runs required.");
        return false;
      }
      const trioCount = combinations.filter((combo) => {
        const trioErrors = [];
        return isATrio(combo, trioErrors);
      }).length;
      const runCount = combinations.filter((combo) => {
        const runErrors = [];
        return isARun(combo, runErrors);
      }).length;
      if (trioCount !== 1 || runCount !== 2) {
        errors.push(
          `Invalid combination: ${trioCount} trios and ${runCount} runs found.`
        );
        return false;
      }
      return true;
    },
    TTTT: (combinations) => {
      if (combinations.length !== 4) {
        errors.push("Four trios required.");
        return false;
      }
      return combinations.every((combination, index) => {
        const trioErrors = [];
        const isTrio = isATrio(combination, trioErrors);
        if (!isTrio) {
          errors.push(`Trio ${index + 1}: ${trioErrors.join(" ")}`);
        }
        return isTrio;
      });
    },
    RRR: (combinations) => {
      if (combinations.length !== 3) {
        errors.push("Three runs required.");
        return false;
      }
      return combinations.every((combination, index) => {
        const runErrors = [];
        const isRun = isARun(combination, runErrors);
        if (!isRun) {
          errors.push(`Run ${index + 1}: ${runErrors.join(" ")}`);
        }
        return isRun;
      });
    },
  };

  const isValid = combinationTypes[goal];
  return isValid ? isValid(combinations) : false;
}

export function isAValidDrop(cardToDrop, targetCombination, addToStart = false) {
  const newCombination = addToStart
    ? [cardToDrop, ...targetCombination]
    : [...targetCombination, cardToDrop];
  return isATrio(newCombination) || isARun(newCombination);
}
