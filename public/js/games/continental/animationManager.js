import * as CardManager from "./cardManager.js";
import * as AudioManager from "./audioManager.js";
import { CONTINENTAL_GAME_STATUS } from "./continentalConstants.js";

export function animateDealingCard(scene, gameState) {
  if (gameState.dealingCard) {
    const { card, playerId } = gameState.dealingCard;
    const targetPlayerIndex = gameState.distributionOfSeats.indexOf(playerId);
    const playerPosition = CardManager.getPlayerCardPosition(
      scene,
      targetPlayerIndex,
      scene.playerIndex,
      gameState.playersHands[playerId].length,
      gameState.playersHands[playerId].length - 1,
      gameState.distributionOfSeats.length
    );

    const dealingCard = scene.add
      .sprite(
        gameState.gameStatus ===
          CONTINENTAL_GAME_STATUS.taking_from_discard_pile
          ? scene.sys.game.config.width / 2 + 100
          : scene.sys.game.config.width / 2,
        scene.sys.game.config.height / 2,
        gameState.gameStatus ===
          CONTINENTAL_GAME_STATUS.taking_from_discard_pile
          ? CardManager.getCardTexture(card)
          : "cardBacks",
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(1000);

    if (
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.taking_from_discard_pile
    ) {
      CardManager.setCardFrame(dealingCard, card);
    }

    // Animate the card from the deck pile or discard pile to the player's hand position
    scene.tweens.add({
      targets: dealingCard,
      x: playerPosition.x,
      y: playerPosition.y,
      rotation: playerPosition.rotation,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardSlideSounds);
      },
      onComplete: () => {
        dealingCard.destroy();
      },
    });
  }
}

export function animateDiscardingCard(scene, gameState) {
  if (
    gameState.discardingCard &&
    gameState.discardingCard.playerId !== scene.socket.id
  ) {
    const { card, playerId } = gameState.discardingCard;
    let playerPosition = null;
    if (
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.discarding_from_player &&
      playerId
    ) {
      const originPlayerIndex = gameState.distributionOfSeats.indexOf(playerId);
      playerPosition = CardManager.getPlayerCardPosition(
        scene,
        originPlayerIndex,
        scene.playerIndex,
        gameState.playersHands[playerId].length,
        gameState.playersHands[playerId].length - 1,
        gameState.distributionOfSeats.length
      );
    }

    const discardingCard = scene.add
      .sprite(
        gameState.gameStatus === CONTINENTAL_GAME_STATUS.discarding_from_player
          ? playerPosition.x
          : scene.sys.game.config.width / 2,
        gameState.gameStatus === CONTINENTAL_GAME_STATUS.discarding_from_player
          ? playerPosition.y
          : scene.sys.game.config.height / 2,
        gameState.gameStatus === CONTINENTAL_GAME_STATUS.discarding_from_player
          ? CardManager.getCardTexture(card)
          : "cardBacks",
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(1000)
      .setRotation(
        gameState.gameStatus === CONTINENTAL_GAME_STATUS.discarding_from_player
          ? playerPosition.rotation
          : 0
      );

    if (
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.discarding_from_player
    ) {
      CardManager.setCardFrame(discardingCard, card);
    }

    // Animate the card from the deck pile or the player's hand to the discard pile position
    scene.tweens.add({
      targets: discardingCard,
      x: scene.sys.game.config.width / 2 + 100,
      y: scene.sys.game.config.height / 2,
      rotation: 0,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardSlideSounds);
      },
      onComplete: () => {
        discardingCard.destroy();
        AudioManager.playRandomSound(scene.cardPlaceSounds);
      },
    });
  }
}

export function animateDroppingCard(scene, gameState) {
  const playerId = gameState.playerInTurn;
  if (gameState.droppingCard && playerId !== scene.socket.id) {
    const { card, targetCombinationKey, targetCombinationIndex, addToStart } =
      gameState.droppingCard;
    const playerPosition = CardManager.getPlayerCardPosition(
      scene,
      gameState.distributionOfSeats.indexOf(playerId),
      scene.playerIndex,
      gameState.playersHands[playerId].length,
      gameState.playersHands[playerId].length - 1,
      gameState.distributionOfSeats.length
    );

    const droppingCard = scene.add
      .sprite(
        playerPosition.x,
        playerPosition.y,
        CardManager.getCardTexture(card.card),
        0
      )
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setDepth(1000)
      .setRotation(playerPosition.rotation);
    CardManager.setCardFrame(droppingCard, card.card);

    const targetCombination =
      scene.playerCombinations[targetCombinationKey][targetCombinationIndex];
    const targetPosition =
      targetCombination.cards[
        addToStart ? 0 : targetCombination.cards.length - 1
      ];

    scene.tweens.add({
      targets: droppingCard,
      x: targetPosition.x,
      y: targetPosition.y,
      rotation: targetPosition.rotation,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardSlideSounds);
      },
      onComplete: () => {
        droppingCard.destroy();
        AudioManager.playRandomSound(scene.cardPlaceSounds);
      },
    });
  }
}
