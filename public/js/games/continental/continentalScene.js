import * as CardManager from "./cardManager.js";
import * as PlayerManager from "./playerManager.js";
import * as AudioManager from "./audioManager.js";
import * as UIElements from "./uiElements.js";
import * as AnimationManager from "./animationManager.js";
import ScoresSummaryScene from "./scoresSummaryScene.js";
import { createChatUI } from "../chatUI.js";
import { CONTINENTAL_GAME_STATUS } from "./continentalConstants.js";

class ContinentalScene extends Phaser.Scene {
  constructor() {
    super({ key: "ContinentalScene" });
  }

  preload() {}

  create() {
    this.scene.add("ScoresSummaryScene", ScoresSummaryScene, false);

    UIElements.createBackgroundImage(this);

    this.goalDescriptionLabel = UIElements.createGoalDescriptionLabel(this);
    this.playerInTurnLabel = UIElements.createPlayerInTurnLabel(
      this,
      "Loading..."
    );
    this.pauseOverlay = UIElements.createPauseOverlay(this);

    this.dropCardsButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 50,
      "Drop Cards",
      false,
      false,
      () => this.startInitialDroppingCards()
    );

    this.chatMessages = {};
    this.chatMessageLabels = {};
    this.cardOriginalDepths = {};
    this.playerLabels = {};
    this.playerCards = {};
    this.gameState = {};
    this.gameStats = null;
    this.pickCardOutOfTurnTimer = null;
    this.playerTurnTimer = null;
    this.playerTurnInProgressTimer = null;
    this.dealerTimer = null;

    const { cardPlaceSounds, cardSlideSounds } =
      AudioManager.createSoundEffects(this);
    this.cardPlaceSounds = cardPlaceSounds;
    this.cardSlideSounds = cardSlideSounds;

    this.socket = this.scene.settings.data.socket;
    this.socket.on("gameState", (gameState) => {
      console.log("Game state received:", gameState);
      this.updateGameState(gameState);
    });
    this.socket.on("chatMessage", (chatMessage) => {
      console.log("Chat message received:", chatMessage);
      this.updateChatMessages(chatMessage);
    });
    this.socket.on("timerUpdate", (timerData) => {
      // console.log("Timer update received:", timerData);
      this.updateTimer(timerData);
    });
    this.socket.on("gameStats", (gameStats) => {
      console.log("Game stats received:", gameStats);
      this.gameStats = gameStats;
    });

    this.chatUI = createChatUI(
      this,
      this.socket,
      this.sys.game.config.width - 20,
      this.sys.game.config.height - 50,
      "right"
    );
  }

  updateGameState(gameState) {
    this.gameState = gameState;
    this.playerIndex = gameState.distributionOfSeats.indexOf(this.socket.id);
    this.goalDescriptionLabel.setText(gameState.hand.goalDescription);
    this.updateDropCardsButton(gameState);
    PlayerManager.updatePlayerLabels(this, gameState, this.chatMessages);
    UIElements.updatePlayerInTurnLabel(this, gameState);
    UIElements.updateDeckPile(this, gameState);
    UIElements.updateDiscardPile(this, gameState);
    CardManager.updatePlayerCards(this, gameState);
    CardManager.updatePlayerCombinations(this, gameState);
    AnimationManager.animateDealingCard(this, gameState);
    AnimationManager.animateDiscardingCard(this, gameState);
    AnimationManager.animateDroppingCard(this, gameState);
    UIElements.updatePauseOverlay(this, gameState);

    if (
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand ||
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_game
    ) {
      this.time.delayedCall(1000, () => {
        this.scene.launch("ScoresSummaryScene", {
          socket: this.socket,
          gameState: this.gameState,
          gameStats: this.gameStats,
        });
      });
    }
  }

  updateChatMessages(chatMessage) {
    this.chatMessages[chatMessage.player.id] = chatMessage.message;
    PlayerManager.updatePlayerLabels(this, this.gameState, this.chatMessages);

    // Remove the message after 2.5 seconds
    this.time.delayedCall(2500, () => {
      delete this.chatMessages[chatMessage.player.id];
    });
  }

  updateTimer(timerData) {
    const { timerName, remainingTime } = timerData;

    if (timerName === "pickCardOutOfTurn") {
      UIElements.updatePickCardOutOfTurnTimer(this, remainingTime);
    } else if (timerName === "playerTurn") {
      UIElements.updatePlayerTurnTimer(this, remainingTime);
    } else if (timerName === "playerTurnInProgress") {
      UIElements.updatePlayerTurnInProgressTimer(this, remainingTime);
    } else if (timerName === "dealer") {
      UIElements.updateDealerTimer(this, remainingTime);
    }
  }

  updateDropCardsButton(gameState) {
    const shouldShowButton =
      gameState.playerInTurn === this.socket.id &&
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.fromDeckPile &&
      gameState.roundNumber > 1 &&
      (!gameState.playersCombinations[this.socket.id] ||
        gameState.playersCombinations[this.socket.id].length === 0);

    if (shouldShowButton) {
      this.dropCardsButton.setVisible(true);
    } else {
      this.dropCardsButton.setVisible(false);
    }
  }

  startInitialDroppingCards() {
    this.scene.pause();
    this.scene.launch("InitialDroppingCardsScene", {
      socket: this.socket,
      gameState: this.gameState,
    });
  }
}

export default ContinentalScene;
