import * as CardManager from "./cardManager.js";
import * as UIElements from "./uiElements.js";
import { isATrio, isARun, isAValidInitialDrop } from "./droppingCards.js";

class InitialDroppingCardsScene extends Phaser.Scene {
  constructor() {
    super({ key: "InitialDroppingCardsScene" });
  }

  init(data) {
    this.socket = data.socket;
    this.gameState = data.gameState;
    this.combinations = [];
    this.combinationsCards = [null, null, null, null];
    this.goalProgression = "";
    this.cardOriginalDepths = {};
  }

  create() {
    UIElements.createBackgroundImage(this);

    // Create the "Dropping Cards" label with the goal description
    const goalDescription = this.gameState.hand.goalDescription;
    this.add.text(20, 20, `Dropping Cards - ${goalDescription}`, {
      font: "24px regular-font",
      fill: "#ffffff",
    });

    // Add instructions
    this.instructionsText = this.add
      .text(
        this.sys.game.config.width / 2,
        this.sys.game.config.height - 220,
        'Select the cards for a trio or a run, then click "Confirm Trio" or "Confirm Run"',
        {
          font: "18px regular-font",
          fill: "#ffffff",
          align: "center",
        }
      )
      .setOrigin(0.5);

    // Add "Confirm Trio" and "Confirm Run" buttons
    this.confirmTrioButton = UIElements.createButton(
      this,
      this.sys.game.config.width / 2 - 100,
      this.sys.game.config.height - 190,
      "Confirm Trio",
      true,
      true,
      () => this.confirmCombination("trio")
    );

    this.confirmRunButton = UIElements.createButton(
      this,
      this.sys.game.config.width / 2 + 100,
      this.sys.game.config.height - 190,
      "Confirm Run",
      true,
      true,
      () => this.confirmCombination("run")
    );

    const playerHand = this.gameState.playersHands[this.socket.id];
    this.playerCards = CardManager.createCardGroup(
      this,
      playerHand,
      this.sys.game.config.width / 2,
      this.sys.game.config.height - 90,
      this.sys.game.config.width * 0.8,
      "horizontal",
      0,
      true,
      false,
    );

    // Add a "Confirm Drop" button
    this.confirmDropButton = UIElements.createButton(
      this,
      this.sys.game.config.width - 150,
      30,
      "Confirm Drop",
      true,
      false,
      () => this.confirmDrop()
    );

    // Add a "Cancel" button
    this.cancelButton = UIElements.createButton(
      this,
      this.sys.game.config.width - 150,
      60,
      "Cancel",
      true,
      false,
      () => this.cancelDrop()
    );

    // Add a "Clear" button
    this.clearButton = UIElements.createButton(
      this,
      this.sys.game.config.width - 150,
      90,
      "Clear",
      true,
      false,
      () => this.clearCombinations()
    );

    this.selectedCards = [];
    this.setupCardSelection();
    this.updateCombinationsDisplay();
    this.updateButtonStates();
  }

  setupCardSelection() {
    this.playerCards.forEach((cardSprite) => {
      cardSprite.on("pointerdown", () => this.toggleCardSelection(cardSprite));
    });
  }

  toggleCardSelection(cardSprite) {
    if (this.selectedCards.includes(cardSprite)) {
      this.selectedCards = this.selectedCards.filter((c) => c !== cardSprite);
      cardSprite.y += 20;
    } else {
      this.selectedCards.push(cardSprite);
      cardSprite.y -= 20;
    }
  }

  confirmCombination(type) {
    const combination = this.selectedCards.map((cardSprite) => ({
      id: cardSprite.cardId,
      card: cardSprite.cardValue,
    }));

    const errors = [];
    let isValid;

    if (type === "trio") {
      isValid = isATrio(combination, errors);
    } else {
      isValid = isARun(combination, errors);
    }

    if (!isValid) {
      this.showError(errors.join("\n"));
      return;
    }

    this.combinations.push(combination);
    this.goalProgression += type === "trio" ? "T" : "R";
    this.updateCombinationsDisplay();
    this.updateButtonStates();

    // Remove the selected cards from the player's hand
    this.selectedCards.forEach((cardSprite) => {
      this.playerCards = this.playerCards.filter((c) => c !== cardSprite);
      cardSprite.destroy();
    });

    this.selectedCards = [];
    this.rearrangePlayerCards();
  }

  updateCombinationsDisplay() {
    // Clear existing combination displays
    this.combinationsCards.forEach((group) => {
      if (group) {
        group.forEach((cardSprite) => cardSprite.destroy());
      }
    });

    // Create new combination displays
    this.combinations.forEach((combination, index) => {
      const yPosition = 120 + index * 110;
      this.combinationsCards[index] = CardManager.createCardGroup(
        this,
        combination,
        this.sys.game.config.width / 2,
        yPosition,
        this.sys.game.config.width * 0.6,
        "horizontal",
        0,
        true,
        true,
      );
    });
  }

  updateButtonStates() {
    const goal = this.gameState.hand.goal;
    let remainingGoal = goal;

    // Calculate the remaining goal
    for (let char of this.goalProgression) {
      const index = remainingGoal.indexOf(char);
      if (index !== -1) {
        remainingGoal =
          remainingGoal.slice(0, index) + remainingGoal.slice(index + 1);
      }
    }

    // Update Confirm Trio button
    this.confirmTrioButton.setAlpha(remainingGoal.includes("T") ? 1 : 0.5);
    this.confirmTrioButton.disableInteractive();
    if (remainingGoal.includes("T")) {
      this.confirmTrioButton.setInteractive();
    }

    // Update Confirm Run button
    this.confirmRunButton.setAlpha(remainingGoal.includes("R") ? 1 : 0.5);
    this.confirmRunButton.disableInteractive();
    if (remainingGoal.includes("R")) {
      this.confirmRunButton.setInteractive();
    }

    // Update Confirm Drop button
    const isGoalComplete = remainingGoal.length === 0;
    this.confirmDropButton.setAlpha(isGoalComplete ? 1 : 0.5);
    this.confirmDropButton.disableInteractive();
    if (isGoalComplete) {
      this.confirmDropButton.setInteractive();
    }
  }

  rearrangePlayerCards() {
    const playerHand = this.playerCards.map((cardSprite) => ({
      id: cardSprite.cardId,
      card: cardSprite.cardValue,
    }));

    this.playerCards.forEach((cardSprite) => cardSprite.destroy());

    this.playerCards = CardManager.createCardGroup(
      this,
      playerHand,
      this.sys.game.config.width / 2,
      this.sys.game.config.height - 90,
      this.sys.game.config.width * 0.8,
      "horizontal",
      0,
      true,
      false,
    );

    this.setupCardSelection();
  }

  confirmDrop() {
    const errors = [];
    const isValid = isAValidInitialDrop(
      this.combinations,
      this.gameState.hand.goal,
      errors
    );

    if (isValid) {
      // Emit the event with the combinations
      this.socket.emit("initialDroppingCards", {
        combinations: this.combinations,
      });

      // Stop this scene and resume the ContinentalScene
      this.scene.stop();
      this.scene.resume("ContinentalScene");
    } else {
      // Show errors if the drop is invalid
      this.showError(errors.join("\n"));
    }
  }

  cancelDrop() {
    this.scene.stop();
    this.scene.resume("ContinentalScene");
  }

  clearCombinations() {
    // Destroy all combination card sprites
    this.combinationsCards.forEach((group) => {
      if (group) {
        group.forEach((cardSprite) => cardSprite.destroy());
      }
    });

    // Clear the combinations array
    this.combinations = [];

    // Deselect all currently selected cards
    this.selectedCards.forEach((cardSprite) => {
      cardSprite.y += 20;
    });
    this.selectedCards = [];

    // Restore the original player cards
    const originalHand = this.gameState.playersHands[this.socket.id];
    this.playerCards.forEach((cardSprite) => cardSprite.destroy());
    this.playerCards = CardManager.createCardGroup(
      this,
      originalHand,
      this.sys.game.config.width / 2,
      this.sys.game.config.height - 90,
      this.sys.game.config.width * 0.8,
      "horizontal",
      0,
      true,
      false,
    );

    // Reset card selection
    this.setupCardSelection();

    this.goalProgression = "";
    this.updateButtonStates();
  }

  showError(message) {
    if (this.errorText) {
      this.errorText.destroy();
    }
    this.instructionsText.setVisible(false);
    this.errorText = this.add
      .text(
        this.sys.game.config.width / 2,
        this.sys.game.config.height - 220,
        message,
        {
          font: "18px regular-font",
          fill: "#ff0000",
          align: "center",
        }
      )
      .setOrigin(0.5);
    this.time.delayedCall(3000, () => {
      if (this.errorText) {
        this.errorText.destroy();
      }
      this.instructionsText.setVisible(true);
    });
  }
}

export default InitialDroppingCardsScene;
