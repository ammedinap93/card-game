import * as AudioManager from "./audioManager.js";
import { CONTINENTAL_GAME_STATUS } from "./continentalConstants.js";
import { isAValidDrop } from "./droppingCards.js";

export const CARD_WIDTH = 256;
export const CARD_HEIGHT = 356;
export const CARD_SCALE = 0.3;

export function getCardTexture(card) {
  const [rank, _] = card.split("_");
  return rank === "joker" ? "jokers" : "playingCards";
}

export function setCardFrame(cardSprite, card) {
  const [rank, suit] = card.split("_");
  if (rank !== "joker") {
    const suitIndex = ["clubs", "hearts", "spades", "diamonds"].indexOf(suit);
    const rankIndex = [
      "ace",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "jack",
      "queen",
      "king",
    ].indexOf(rank);
    cardSprite.setFrame(suitIndex * 13 + rankIndex);
  } else {
    cardSprite.setFrame(suit === "red" ? 1 : 0);
  }
}

export function createCardSprite(scene, card, isVisible) {
  const texture = getCardTexture(card);
  const cardSprite = scene.add
    .sprite(0, 0, texture, 0)
    .setOrigin(0.5)
    .setScale(CARD_SCALE)
    .setInteractive();

  if (isVisible) {
    setCardFrame(cardSprite, card);
  } else {
    cardSprite.setTexture("cardBacks");
    cardSprite.setFrame(0);
  }

  return cardSprite;
}

function getCardPosition(
  scene,
  index,
  playerIndex,
  cardIndex,
  numCards,
  isCurrentPlayer,
  totalPlayers
) {
  const positions = [
    {
      x: scene.sys.game.config.width / 2,
      y: scene.sys.game.config.height - 90,
      rotation: 0,
      offsetX: getPlayerHandCardOffset(numCards, isCurrentPlayer),
      offsetY: 0,
    }, // Bottom
    {
      x: scene.sys.game.config.width - 90,
      y: scene.sys.game.config.height / 2,
      rotation: Phaser.Math.DegToRad(-90),
      offsetX: 0,
      offsetY: getPlayerHandCardOffset(numCards, isCurrentPlayer),
    }, // Right
    {
      x: scene.sys.game.config.width / 2,
      y: 90,
      rotation: 0,
      offsetX: getPlayerHandCardOffset(numCards, isCurrentPlayer),
      offsetY: 0,
    }, // Top
    {
      x: 90,
      y: scene.sys.game.config.height / 2,
      rotation: Phaser.Math.DegToRad(90),
      offsetX: 0,
      offsetY: getPlayerHandCardOffset(numCards, isCurrentPlayer),
    }, // Left
  ];

  let adjustedIndex;
  if (totalPlayers === 2) {
    adjustedIndex = ((index - playerIndex + 2) % 2) * 2; // Use bottom and top positions
  } else if (totalPlayers === 3) {
    adjustedIndex = ((index - playerIndex + 3) % 3) * 1.334; // Use bottom, right, and top positions
  } else {
    adjustedIndex = (index - playerIndex + 4) % 4;
  }

  const { x, y, rotation, offsetX, offsetY } =
    positions[Math.floor(adjustedIndex)];

  const totalWidth = (numCards - 1) * offsetX;
  const totalHeight = (numCards - 1) * offsetY;
  const startX = x - totalWidth / 2;
  const startY = y - totalHeight / 2;

  return {
    x: startX + cardIndex * offsetX,
    y: startY + cardIndex * offsetY,
    rotation: rotation,
  };
}

export function getPlayerCardPosition(
  scene,
  playerIndex,
  currentPlayerIndex,
  handLength,
  cardIndex,
  totalPlayers
) {
  const position = getCardPosition(
    scene,
    playerIndex,
    currentPlayerIndex,
    cardIndex,
    handLength,
    playerIndex === currentPlayerIndex,
    totalPlayers
  );
  return { x: position.x, y: position.y, rotation: position.rotation };
}

function getPlayerHandCardOffset(numCards, isCurrentPlayer) {
  let offset = 30;
  if (isCurrentPlayer && numCards > 14) {
    offset = 450 / numCards;
  } else if (!isCurrentPlayer && numCards > 8) {
    offset = 270 / numCards;
  }
  return offset;
}

function isOverDiscardPile(scene, x, y) {
  const discardPileRect = scene.discardPileDropZone.getBounds();
  return discardPileRect.contains(x, y);
}

function isOverACombination(scene, x, y) {
  for (const combinations of Object.values(scene.playerCombinations)) {
    for (const combination of combinations) {
      const rect = combination.rectangle;
      if (rect.getBounds().contains(x, y)) {
        return true;
      }
    }
  }
  return false;
}

export function getTargetCombinationDetails(scene, x, y) {
  for (const [playerId, combinations] of Object.entries(
    scene.playerCombinations
  )) {
    for (const combination of combinations) {
      const rect = combination.rectangle;
      const bounds = rect.getBounds();
      if (bounds.contains(x, y)) {
        let addToStart;
        if (combination.orientation === "horizontal") {
          addToStart = x < bounds.centerX;
        } else {
          // vertical
          addToStart = y < bounds.centerY;
        }

        return {
          playerId: playerId,
          combinationIndex: combination.index,
          orientation: combination.orientation,
          addToStart: addToStart,
        };
      }
    }
  }
  return null;
}

function setupDraggableCard(
  scene,
  cardSprite,
  id,
  gameState,
  index,
  playerIndex,
  handLength
) {
  scene.input.setDraggable(cardSprite);
  cardSprite.on("dragstart", () => {
    scene.cardOriginalDepths[id] = cardSprite.depth;
    cardSprite.setDepth(1000);
    AudioManager.playRandomSound(scene.cardSlideSounds);
  });
  cardSprite.on("drag", (pointer) => {
    if (gameState.paused) return; // Prevent dragging if the game is paused
    cardSprite.x = pointer.x;
    cardSprite.y = pointer.y;
  });
  cardSprite.on("dragend", (pointer) => {
    const playerCards = scene.playerCards[scene.socket.id];
    const originalIndex = playerCards.indexOf(cardSprite);
    const originalPosition = getCardPosition(
      scene,
      index,
      playerIndex,
      originalIndex,
      handLength,
      true,
      gameState.distributionOfSeats.length
    );

    if (gameState.paused) {
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
      return;
    }
    if (
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === scene.socket.id &&
      isOverDiscardPile(scene, pointer.x, pointer.y)
    ) {
      scene.socket.emit("discardCard", { cardId: id });
      AudioManager.playRandomSound(scene.cardPlaceSounds);
    } else if (
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === scene.socket.id &&
      gameState.playersCombinations[scene.socket.id] &&
      gameState.playersCombinations[scene.socket.id].length > 0 &&
      isOverACombination(scene, pointer.x, pointer.y)
    ) {
      const combinationDetails = getTargetCombinationDetails(
        scene,
        pointer.x,
        pointer.y
      );
      if (combinationDetails) {
        const combination =
          gameState.playersCombinations[combinationDetails.playerId][
            combinationDetails.combinationIndex
          ];
        const card = gameState.playersHands[scene.socket.id][originalIndex];
        if (isAValidDrop(card, combination, combinationDetails.addToStart)) {
          scene.socket.emit("dropCard", {
            cardToDrop: { id: card.id, card: card.card },
            targetCombinationKey: combinationDetails.playerId,
            targetCombinationIndex: combinationDetails.combinationIndex,
            addToStart: combinationDetails.addToStart,
          });
          AudioManager.playRandomSound(scene.cardPlaceSounds);
          return;
        }
      }
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
    } else if (Math.abs(pointer.y - originalPosition.y) > 90) {
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
    } else {
      const playerCardPositions = playerCards.map((card, idx) =>
        getCardPosition(
          scene,
          gameState.distributionOfSeats.indexOf(scene.socket.id),
          playerIndex,
          idx,
          handLength,
          true,
          gameState.distributionOfSeats.length
        )
      );
      const targetIndex = playerCardPositions.findIndex(
        (position) => pointer.x < position.x
      );
      if (targetIndex === originalIndex) {
        cardSprite.x = originalPosition.x;
        cardSprite.y = originalPosition.y;
        cardSprite.setDepth(scene.cardOriginalDepths[id]);
      } else if (targetIndex === -1) {
        scene.socket.emit("moveCard", {
          cardId: id,
          newIndex: playerCards.length - 1,
        });
      } else {
        scene.socket.emit("moveCard", {
          cardId: id,
          newIndex: targetIndex,
        });
      }
      AudioManager.playRandomSound(scene.cardSlideSounds);
    }
  });
}

function setupHoverCard(scene, cardSprite, id) {
  cardSprite.setInteractive();
  cardSprite.on("pointerover", () => {
    scene.cardOriginalDepths[id] = cardSprite.depth;
    cardSprite.setDepth(1000);
  });
  cardSprite.on("pointerout", () => {
    cardSprite.setDepth(scene.cardOriginalDepths[id]);
  });
}

export function updatePlayerCards(scene, gameState) {
  // Clear existing player cards
  if (scene.playerCards) {
    Object.values(scene.playerCards).forEach((cards) =>
      cards.forEach((card) => card.destroy())
    );
  }
  scene.playerCards = {};

  const playerIndex = scene.playerIndex;

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const playerHand = gameState.playersHands[playerId] || [];
    const playerCards = [];

    playerHand.forEach(({ id, card }, cardIndex) => {
      if (gameState.dealingCard && gameState.dealingCard.id === id) {
        return; // Ignore the card if it's the same as the dealing card
      }

      const isCurrentPlayer = playerId === scene.socket.id;
      let cardSprite = createCardSprite(scene, card, isCurrentPlayer);

      const cardPosition = getCardPosition(
        scene,
        index,
        playerIndex,
        cardIndex,
        playerHand.length,
        isCurrentPlayer,
        gameState.distributionOfSeats.length
      );
      cardSprite.setPosition(cardPosition.x, cardPosition.y);
      cardSprite.setRotation(cardPosition.rotation);
      cardSprite.setDepth(cardIndex);
      scene.cardOriginalDepths[id] = cardIndex;

      if (isCurrentPlayer) {
        setupDraggableCard(
          scene,
          cardSprite,
          id,
          gameState,
          index,
          playerIndex,
          playerHand.length
        );
      }

      playerCards.push(cardSprite);
    });

    scene.playerCards[playerId] = playerCards;
  });

  return scene.playerCards;
}

export function createCardGroup(
  scene,
  cards,
  x,
  y,
  maxWidth,
  orientation,
  angle,
  autoCenter = false,
  hover = false
) {
  const cardSprites = [];

  cards.forEach((card, index) => {
    const cardSprite = createCardSprite(scene, card.card, true);
    cardSprite.cardId = card.id;
    cardSprite.cardValue = card.card;
    cardSprite.setRotation(Phaser.Math.DegToRad(angle));
    cardSprites.push(cardSprite);
  });

  const totalCards = cardSprites.length;

  let offset = Math.min(30, maxWidth / totalCards);

  let startX = x;
  let startY = y;

  if (autoCenter) {
    if (orientation === "horizontal") {
      startX = x - ((totalCards - 1) * offset) / 2;
    } else {
      startY =
        y - (totalCards * offset + (CARD_WIDTH * CARD_SCALE - offset)) / 2;
    }
  }

  cardSprites.forEach((cardSprite, index) => {
    if (orientation === "horizontal") {
      cardSprite.setPosition(startX + index * offset, startY);
    } else {
      cardSprite.setPosition(startX, startY + index * offset);
    }
    cardSprite.setDepth(index);
    if (hover) {
      setupHoverCard(scene, cardSprite, cardSprite.cardId);
    }
  });

  return cardSprites;
}

export function updatePlayerCombinations(scene, gameState) {
  // Clear existing combinations and rectangles
  if (scene.playerCombinations) {
    Object.values(scene.playerCombinations).forEach((combinations) =>
      combinations.forEach((combination) => {
        combination.cards.forEach((card) => card.destroy());
        combination.rectangle.destroy();
      })
    );
  }
  scene.playerCombinations = {};

  const playerIndex = scene.playerIndex;

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const playerCombinations = gameState.playersCombinations[playerId] || [];
    const combinationSprites = [];

    if (playerCombinations.length > 0) {
      const totalWidth = 270;
      const spacing = 10;
      const maxCombinationWidth =
        (totalWidth -
          (playerCombinations.length * CARD_WIDTH * CARD_SCALE) / 2 -
          (playerCombinations.length - 1) * spacing) /
        playerCombinations.length;

      let verticalAdjustment, horizontalAdjustment;
      switch (playerCombinations.length) {
        case 2:
          verticalAdjustment = 40;
          horizontalAdjustment = 110;
          break;
        case 3:
          verticalAdjustment = 105;
          horizontalAdjustment = 140;
          break;
        case 4:
          verticalAdjustment = 140;
          horizontalAdjustment = 160;
          break;
      }

      let startX, startY, orientation, angle;

      const adjustedIndex = (index - playerIndex + 4) % 4;

      switch (adjustedIndex) {
        case 0: // Bottom
          startX = scene.sys.game.config.width / 2 - horizontalAdjustment;
          startY = scene.sys.game.config.height - 210;
          orientation = "horizontal";
          angle = 0;
          break;
        case 1: // Right
          startX = scene.sys.game.config.width - 210;
          startY = scene.sys.game.config.height / 2 + verticalAdjustment;
          orientation = "vertical";
          angle = -90;
          break;
        case 2: // Top
          startX = scene.sys.game.config.width / 2 - horizontalAdjustment;
          startY = 210;
          orientation = "horizontal";
          angle = 0;
          break;
        case 3: // Left
          startX = 210;
          startY = scene.sys.game.config.height / 2 + verticalAdjustment;
          orientation = "vertical";
          angle = 90;
          break;
      }

      playerCombinations.forEach((combination, combinationIndex) => {
        if (combination.length > 0) {
          let x, y;
          let offset = 0;
          if (combinationIndex > 0) {
            if (orientation === "horizontal") {
              offset = Math.min(
                30,
                maxCombinationWidth /
                  playerCombinations[combinationIndex - 1].length
              );
            } else {
              offset = Math.min(
                30,
                maxCombinationWidth /
                  playerCombinations[combinationIndex].length
              );
            }
          }
          if (orientation === "horizontal") {
            x =
              startX +
              combinationIndex *
                (maxCombinationWidth +
                  spacing +
                  CARD_WIDTH * CARD_SCALE -
                  offset);
            y = startY;
          } else {
            x = startX;
            y =
              startY -
              combinationIndex *
                (maxCombinationWidth +
                  spacing +
                  CARD_WIDTH * CARD_SCALE -
                  offset);
          }

          const filteredCards = combination.filter((card) => {
            if (
              gameState.droppingCard &&
              gameState.droppingCard.card &&
              gameState.droppingCard.card.id === card.id &&
              gameState.playerInTurn !== scene.socket.id
            ) {
              return false;
            }
            return true;
          });

          const combinationCards = createCardGroup(
            scene,
            filteredCards,
            x,
            y,
            maxCombinationWidth,
            orientation,
            angle,
            false,
            true
          );

          // Create rectangle for the combination
          offset = Math.min(30, maxCombinationWidth / combination.length);
          const rectWidth =
            orientation === "horizontal"
              ? maxCombinationWidth + CARD_WIDTH * CARD_SCALE - offset
              : CARD_HEIGHT * CARD_SCALE;
          const rectHeight =
            orientation === "horizontal"
              ? CARD_HEIGHT * CARD_SCALE
              : maxCombinationWidth + CARD_WIDTH * CARD_SCALE - offset;
          const rectX =
            orientation === "horizontal"
              ? x - (CARD_WIDTH * CARD_SCALE) / 2
              : x - (CARD_HEIGHT * CARD_SCALE) / 2;
          const rectY =
            orientation === "horizontal"
              ? y - (CARD_HEIGHT * CARD_SCALE) / 2
              : y - (CARD_WIDTH * CARD_SCALE) / 2;

          const rectangle = scene.add
            .rectangle(rectX, rectY, rectWidth, rectHeight)
            .setOrigin(0)
            .setStrokeStyle();

          combinationSprites.push({
            cards: combinationCards,
            rectangle: rectangle,
            key: playerId,
            index: combinationIndex,
            orientation: orientation,
          });
        }
      });
    }

    scene.playerCombinations[playerId] = combinationSprites;
  });

  return scene.playerCombinations;
}
