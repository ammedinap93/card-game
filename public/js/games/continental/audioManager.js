export function createSoundEffects(scene) {
  const cardPlaceSounds = [
    scene.sound.add("cardPlace1"),
    scene.sound.add("cardPlace2"),
    scene.sound.add("cardPlace3"),
  ];

  const cardSlideSounds = [
    scene.sound.add("cardSlide1"),
    scene.sound.add("cardSlide2"),
    scene.sound.add("cardSlide3"),
  ];

  return { cardPlaceSounds, cardSlideSounds };
}

export function playRandomSound(sounds) {
  const randomIndex = Math.floor(Math.random() * sounds.length);
  sounds[randomIndex].play();
}
