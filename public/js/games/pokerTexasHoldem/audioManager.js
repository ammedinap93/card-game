import {
  POKER_TEXAS_HOLDEM_ACTIONS,
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_STAGES,
} from "./pokerTexasHoldemConstants.js";

export function createSoundEffects(scene) {
  const cardPlaceSounds = [
    scene.sound.add("cardPlace1"),
    scene.sound.add("cardPlace2"),
    scene.sound.add("cardPlace3"),
  ];

  const cardSlideSounds = [
    scene.sound.add("cardSlide1"),
    scene.sound.add("cardSlide2"),
    scene.sound.add("cardSlide3"),
  ];

  const chipCollideSounds = [
    scene.sound.add("chipsCollide1"),
    scene.sound.add("chipsCollide2"),
    scene.sound.add("chipsCollide3"),
  ];

  const newMessageSound = scene.sound.add("newMessage");
  const newTurnSound = scene.sound.add("newTurn");
  const texasHoldemSound = scene.sound.add("texasHoldem");
  const foldSound = scene.sound.add("fold");
  const checkSound = scene.sound.add("check");
  const betSound = scene.sound.add("bet");
  const callSound = scene.sound.add("call");
  const raiseSound = scene.sound.add("raise");
  const smallBlindSound = scene.sound.add("smallBlind");
  const bigBlindSound = scene.sound.add("bigBlind");
  const allInSound = scene.sound.add("allIn");
  const flopSound = scene.sound.add("flop");
  const turnSound = scene.sound.add("turn");
  const riverSound = scene.sound.add("river");
  const showdownSound = scene.sound.add("showdown");

  return {
    cardPlaceSounds,
    cardSlideSounds,
    chipCollideSounds,
    newMessageSound,
    newTurnSound,
    texasHoldemSound,
    foldSound,
    checkSound,
    betSound,
    callSound,
    raiseSound,
    smallBlindSound,
    bigBlindSound,
    allInSound,
    flopSound,
    turnSound,
    riverSound,
    showdownSound,
  };
}

export function playRandomSound(sounds) {
  const randomIndex = Math.floor(Math.random() * sounds.length);
  sounds[randomIndex].play();
}

export function playActionSound(scene, gameState) {
  if (
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action
  ) {
    let playerId = gameState.playerInTurn;
    const isSmallBlind = gameState.smallBlindPlayer === playerId;
    const isBigBlind = gameState.bigBlindPlayer === playerId;
    if (gameState.playersActions && gameState.playersActions[playerId]) {
      switch (gameState.playersActions[playerId]) {
        case POKER_TEXAS_HOLDEM_ACTIONS.fold:
          scene.foldSound.play();
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.check:
          scene.checkSound.play();
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.call:
          scene.callSound.play();
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.bet:
          scene.betSound.play();
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.raise:
          scene.raiseSound.play();
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.all_in:
          scene.allInSound.play();
          break;
        default:
          break;
      }
    } else if (isSmallBlind) {
      scene.smallBlindSound.play();
    } else if (isBigBlind) {
      scene.bigBlindSound.play();
    }
  }
}

export function playStageSound(scene, gameState) {
  if (POKER_TEXAS_HOLDEM_STAGES.includes(gameState.gameStatus)) {
    switch (gameState.gameStatus) {
      case POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_flop:
        scene.flopSound.play();
        break;
      case POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_turn:
        scene.turnSound.play();
        break;
      case POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_river:
        scene.riverSound.play();
        break;
      case POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown:
        scene.showdownSound.play();
        break;
      default:
        break;
    }
  }
}
