export const POKER_TEXAS_HOLDEM_GAME_STATUS = {
  posting_blinds: "posting_blinds",
  ready_to_deal_hole_cards: "ready_to_deal_hole_cards",
  dealing_hole_cards: "dealing_hole_cards",
  preflop_betting_round: "preflop_betting_round",
  ready_to_deal_flop: "ready_to_deal_flop",
  dealing_flop: "dealing_flop",
  flop_betting_round: "flop_betting_round",
  ready_to_deal_turn: "ready_to_deal_turn",
  dealing_turn: "dealing_turn",
  turn_betting_round: "turn_betting_round",
  ready_to_deal_river: "ready_to_deal_river",
  dealing_river: "dealing_river",
  river_betting_round: "river_betting_round",
  initializing_showdown: "initializing_showdown",
  showdown: "showdown",
  processing_action: "processing_action",
  exchanging_chips: "exchanging_chips",
  pushing_bets: "pushing_bets",
  awarding_pots: "awarding_pots",
  end_of_the_hand: "end_of_the_hand",
  end_of_the_game: "end_of_the_game",
};

export const POKER_TEXAS_HOLDEM_BETTING_ROUNDS = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.preflop_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.turn_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.river_betting_round,
];

export const POKER_TEXAS_HOLDEM_READY_TO_DEAL = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_hole_cards,
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop,
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn,
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_river,
];

export const POKER_TEXAS_HOLDEM_DEALING_CARDS = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_hole_cards,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_flop,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_turn,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_river,
];

export const POKER_TEXAS_HOLDEM_STAGES = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_flop,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_turn,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_river,
  POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown,
];

export const POKER_TEXAS_HOLDEM_ACTIONS = {
  fold: "fold",
  check: "check",
  bet: "bet",
  call: "call",
  raise: "raise",
  all_in: "all-in",
  show_cards: "show-cards",
};

export const POKER_TEXAS_HOLDEM_CHIP_VALUES = {
  white: 1,
  red: 5,
  blue: 10,
  green: 25,
  black: 100,
};
