import * as CardManager from "./cardManager.js";
import * as ChipManager from "./chipManager.js";
import * as AudioManager from "./audioManager.js";

export function animateDealingCard(scene, gameState) {
  if (gameState.dealingCard) {
    const { card, playerId } = gameState.dealingCard;
    const {
      x: deckX,
      y: deckY,
      rotation: deckRotation,
    } = CardManager.getDeckPilePosition(scene);

    let targetPosition;

    if (playerId) {
      // Dealing to a player
      const targetPlayerIndex = gameState.distributionOfSeats.indexOf(playerId);
      const totalPlayers = gameState.distributionOfSeats.length;
      targetPosition = CardManager.getPlayerCardPosition(
        scene,
        targetPlayerIndex,
        scene.playerIndex,
        gameState.playersHands[playerId].length,
        gameState.playersHands[playerId].length - 1,
        totalPlayers
      );
    } else {
      // Dealing a community card
      const faceUpCardIndex = gameState.faceUpCards.length - 1;
      targetPosition = CardManager.getFaceUpCardPosition(
        scene,
        faceUpCardIndex
      );
    }

    const dealingCard = scene.add
      .sprite(deckX, deckY, "cardBacks", 0)
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setRotation(deckRotation)
      .setDepth(10000);

    scene.tweens.add({
      targets: dealingCard,
      x: targetPosition.x,
      y: targetPosition.y,
      rotation: targetPosition.rotation || 0,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardSlideSounds);
      },
      onComplete: () => {
        dealingCard.destroy();
      },
    });
  }
}

export function animateDiscardingCard(scene, gameState) {
  if (gameState.discardingCard) {
    const {
      x: deckX,
      y: deckY,
      rotation: deckRotation,
    } = CardManager.getDeckPilePosition(scene);
    const {
      x: discardX,
      y: discardY,
      rotation: discardRotation,
    } = CardManager.getDiscardPilePosition(scene);

    const discardingCard = scene.add
      .sprite(deckX, deckY, "cardBacks", 0)
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setRotation(deckRotation)
      .setDepth(10000);

    scene.tweens.add({
      targets: discardingCard,
      x: discardX,
      y: discardY,
      rotation: discardRotation,
      duration: 500,
      onStart: () => {
        AudioManager.playRandomSound(scene.cardPlaceSounds);
      },
      onComplete: () => {
        discardingCard.destroy();
      },
    });
  }
}

export function animateDiscardingCards(scene, gameState) {
  if (gameState.discardingCards && gameState.discardingCards.length > 0) {
    const {
      x: discardX,
      y: discardY,
      rotation: discardRotation,
    } = CardManager.getDiscardPilePosition(scene);

    gameState.discardingCards.forEach((card, index) => {
      const cardSprite = CardManager.createCardSprite(scene, card.card, false);
      const originalPos = CardManager.getPlayerCardPosition(
        scene,
        gameState.distributionOfSeats.indexOf(card.playerId),
        scene.playerIndex,
        0,
        0,
        gameState.distributionOfSeats.length
      );

      cardSprite.setPosition(originalPos.x, originalPos.y);
      cardSprite.setRotation(originalPos.rotation);
      cardSprite.setDepth(10000);

      scene.tweens.add({
        targets: cardSprite,
        x: discardX,
        y: discardY,
        rotation: discardRotation,
        duration: 500,
        delay: index * 100,
        onStart: () => {
          AudioManager.playRandomSound(scene.cardPlaceSounds);
        },
        onComplete: () => {
          cardSprite.destroy();
        },
      });
    });
  }
}

export function animateBettingChips(scene, gameState) {
  if (gameState.bettingChips && gameState.bettingChips.length > 0) {
    const animatedSprites = [];
    let animationsRemaining = gameState.bettingChips.length;

    const completeAnimation = () => {
      animationsRemaining--;
      if (animationsRemaining <= 0) {
        // All animations have completed. Now we can:
        // Destroy all animated sprites
        animatedSprites.forEach((sprite) => sprite.destroy());

        // Refresh all chip stacks and bets
        gameState.bettingChips = [];
        ChipManager.updateChips(scene, gameState);
      }
    };

    gameState.bettingChips.forEach((chip, index) => {
      scene.time.delayedCall(index * 100, () => {
        const originalSprite = scene.chipSprites[chip.id];
        if (!originalSprite) {
          console.error("Chip sprite not found:", chip);
          completeAnimation(); // Important: decrement even on error
          return;
        }

        const endPos = ChipManager.getPlayerBetPosition(
          scene,
          gameState,
          chip.playerId
        );

        const startPos = { x: originalSprite.x, y: originalSprite.y };
        const texture = ChipManager.getChipTexture(chip.color);

        // Create a new sprite for animation
        const animatedChipSprite = scene.add
          .image(startPos.x, startPos.y, texture)
          .setScale(ChipManager.CHIP_SCALE)
          .setDepth(10000)
          .setFlipX(originalSprite.flipX);

        animatedSprites.push(animatedChipSprite);

        scene.tweens.add({
          targets: animatedChipSprite,
          x: endPos.x + chip.offsetX,
          y: endPos.y + chip.offsetY,
          rotation: Phaser.Math.DegToRad(Phaser.Math.Between(-15, 15)), // random slight tilt
          duration: 500,
          ease: "Power2",
          onStart: () => {
            AudioManager.playRandomSound(scene.chipCollideSounds);
          },
          onComplete: completeAnimation,
        });
      });
    });

    // Fallback to ensure refresh happens even if animations somehow don't complete
    scene.time.delayedCall(gameState.bettingChips.length * 100 + 1000, () => {
      if (animationsRemaining > 0) {
        console.warn(
          `${animationsRemaining} betting chip animations did not complete on time. Forcing refresh.`
        );
        animatedSprites.forEach((sprite) => sprite.destroy());
        gameState.bettingChips = [];
        ChipManager.updateChips(scene, gameState);
      }
    });
  }
}

export function animatePushingChips(scene, gameState) {
  if (gameState.pushingChips && gameState.pushingChips.length > 0) {
    const animatedSprites = [];
    let animationsRemaining = gameState.pushingChips.length;

    const completeAnimation = () => {
      animationsRemaining--;
      if (animationsRemaining <= 0) {
        // All animations have completed. Now we can:
        // Destroy all animated sprites
        animatedSprites.forEach((sprite) => sprite.destroy());

        // Refresh all chip stacks and bets
        gameState.pushingChips = [];
        ChipManager.updateChips(scene, gameState);
      }
    };

    gameState.pushingChips.forEach((chip, index) => {
      scene.time.delayedCall(index * 100, () => {
        const originalSprite = scene.chipSprites[chip.id];
        if (!originalSprite) {
          console.error("Chip sprite not found:", chip);
          completeAnimation(); // Important: decrement even on error
          return;
        }

        const potPosition = ChipManager.getPotCenterPosition(
          scene,
          chip.potIndex,
          gameState.pots.length
        );

        const { radiusX, radiusY } = ChipManager.getPotSize(
          gameState.pots[chip.potIndex],
          gameState.pots.length
        );

        const startPos = { x: originalSprite.x, y: originalSprite.y };
        const texture = ChipManager.getChipTexture(chip.color);

        // Create a new sprite for animation
        const animatedChipSprite = scene.add
          .image(startPos.x, startPos.y, texture)
          .setScale(ChipManager.CHIP_SCALE)
          .setDepth(10000)
          .setFlipX(originalSprite.flipX);

        animatedSprites.push(animatedChipSprite);

        scene.tweens.add({
          targets: animatedChipSprite,
          x: potPosition.x + chip.offsetX * radiusX,
          y: potPosition.y + chip.offsetY * radiusY,
          rotation: Phaser.Math.DegToRad(Phaser.Math.Between(-15, 15)), // random slight tilt
          duration: 500,
          ease: "Power2",
          onStart: () => {
            AudioManager.playRandomSound(scene.chipCollideSounds);
          },
          onComplete: completeAnimation,
        });
      });
    });

    // Fallback to ensure refresh happens even if animations somehow don't complete
    scene.time.delayedCall(gameState.pushingChips.length * 100 + 1000, () => {
      if (animationsRemaining > 0) {
        console.warn(
          `${animationsRemaining} pushing chip animations did not complete on time. Forcing refresh.`
        );
        animatedSprites.forEach((sprite) => sprite.destroy());
        gameState.pushingChips = [];
        ChipManager.updateChips(scene, gameState);
      }
    });
  }
}

export function animateAwardingChips(scene, gameState) {
  if (gameState.awardingChips && gameState.awardingChips.length > 0) {
    const animatedSprites = [];
    let animationsRemaining = gameState.awardingChips.length;
    let animationDelay = Math.min(100, 1000 / animationsRemaining);

    const completeAnimation = () => {
      animationsRemaining--;
      if (animationsRemaining <= 0) {
        // All animations have completed. Now we can:
        // Destroy all animated sprites
        animatedSprites.forEach((sprite) => sprite.destroy());

        // Refresh all chip stacks and bets
        gameState.awardingChips = [];
        ChipManager.updateChips(scene, gameState);
      }
    };

    gameState.awardingChips.forEach((awardingChip, index) => {
      scene.time.delayedCall(index * animationDelay, () => {
        const originalSprite = scene.chipSprites[awardingChip.id];
        if (!originalSprite) {
          console.error("Chip sprite not found:", awardingChip);
          completeAnimation(); // Important: decrement even on error
          return;
        }

        const startPos = { x: originalSprite.x, y: originalSprite.y };

        const winnerStack =
          gameState.playersStacks[awardingChip.winnerPlayerId];
        const transitionChipIds = [
          ...gameState.bettingChips.map((chip) => chip.id),
          ...gameState.awardingChips.map((chip) => chip.id),
        ];

        const colorStack = (winnerStack[awardingChip.color] || []).filter(
          (chip) => !transitionChipIds.includes(chip.id)
        );

        let endPos;
        if (colorStack && colorStack.length > 0) {
          const topChipId = colorStack[colorStack.length - 1].id;
          const topChipSprite = scene.chipSprites[topChipId];
          if (topChipSprite) {
            endPos = {
              x: topChipSprite.x,
              y: topChipSprite.y - ChipManager.CHIP_VERTICAL_OFFSET,
            };
          }
        }

        if (!endPos) {
          const winnerCardPos = CardManager.getPlayerCardPosition(
            scene,
            gameState.distributionOfSeats.indexOf(awardingChip.winnerPlayerId),
            scene.playerIndex,
            gameState.playersHands[awardingChip.winnerPlayerId].length,
            awardingChip.winnerPlayerId === scene.socket.id
              ? gameState.playersHands[awardingChip.winnerPlayerId].length - 1
              : 0,
            gameState.distributionOfSeats.length
          );
          const basePos = ChipManager.getPlayerStackBasePosition(winnerCardPos);
          endPos = {
            x: basePos.x,
            y: basePos.y,
          };
        }

        const texture = ChipManager.getChipTexture(awardingChip.color);

        const animatedChipSprite = scene.add
          .image(startPos.x, startPos.y, texture)
          .setScale(ChipManager.CHIP_SCALE)
          .setDepth(10000)
          .setFlipX(originalSprite.flipX);

        animatedSprites.push(animatedChipSprite);

        scene.tweens.add({
          targets: animatedChipSprite,
          x: endPos.x + awardingChip.offsetX,
          y: endPos.y + awardingChip.offsetY,
          rotation: Phaser.Math.DegToRad(Phaser.Math.Between(-15, 15)),
          duration: 500,
          ease: "Power2",
          onStart: () => {
            AudioManager.playRandomSound(scene.chipCollideSounds);
          },
          onComplete: completeAnimation,
        });
      });
    });

    // Fallback to ensure refresh happens even if animations somehow don't complete
    scene.time.delayedCall(gameState.awardingChips.length * animationDelay + 1000, () => {
      if (animationsRemaining > 0) {
        console.warn(
          `${animationsRemaining} awarding chip animations did not complete on time. Forcing refresh.`
        );
        animatedSprites.forEach((sprite) => sprite.destroy());
        gameState.awardingChips = [];
        ChipManager.updateChips(scene, gameState);
      }
    });
  }
}
