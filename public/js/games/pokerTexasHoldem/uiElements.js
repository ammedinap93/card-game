import * as CardManager from "./cardManager.js";
import { CHIP_SCALE, getChipFrontTexture } from "./chipManager.js";
import {
  POKER_TEXAS_HOLDEM_ACTIONS,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS,
  POKER_TEXAS_HOLDEM_DEALING_CARDS,
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_READY_TO_DEAL,
} from "./pokerTexasHoldemConstants.js";

export function createBackgroundImage(scene) {
  const backgroundImage = scene.add.image(0, 0, "background").setOrigin(0);
  backgroundImage.setDisplaySize(
    scene.sys.game.config.width,
    scene.sys.game.config.height
  );
  return backgroundImage;
}

export function createButton(scene, x, y, text, visible, centered, callback) {
  const button = scene.add.text(x, y, text, {
    font: "18px bold-font",
    fill: "#ffff00", // Yellow color
  });
  if (centered) {
    button.setOrigin(0.5);
  } else {
    button.setOrigin(0, 0.5);
  }
  button
    .setInteractive({ useHandCursor: true })
    .setVisible(visible)
    .on("pointerdown", callback)
    .on("pointerover", () => button.setStyle({ fill: "#ffffff" })) // White on hover
    .on("pointerout", () => button.setStyle({ fill: "#ffff00" })); // Back to yellow

  return button;
}

export function createPlayerInTurnLabel(scene, message) {
  return scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2 + 68,
      message,
      {
        font: "18px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
}

export function updatePlayerInTurnLabel(scene, gameState) {
  const playerInTurn = gameState.lobby.players.find(
    (p) => p.id === gameState.playerInTurn
  );

  let turnMessage = "";
  if (gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds) {
    const isSmallBlind = gameState.smallBlindPlayer === gameState.playerInTurn;
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn to post the ${
            isSmallBlind ? "Small Blind" : "Big Blind"
          }: ${scene.blindsTimer || ""}`
        : `${playerInTurn.username} posts the ${
            isSmallBlind ? "Small Blind" : "Big Blind"
          }: ${scene.blindsTimer || ""}`;
  } else if (POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn to deal: ${scene.dealerTimer || ""}`
        : `${playerInTurn.username} deals: ${scene.dealerTimer || ""}`;
  } else if (POKER_TEXAS_HOLDEM_DEALING_CARDS.includes(gameState.gameStatus)) {
    turnMessage = "Dealing cards";
  } else if (
    POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
  ) {
    turnMessage =
      gameState.playerInTurn === scene.socket.id
        ? `It's your turn! ${scene.playerTurnTimer || ""}`
        : `${playerInTurn.username}'s turn: ${scene.playerTurnTimer || ""}`;
  } else if (
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action
  ) {
    let playerId = gameState.playerInTurn;
    if (gameState.playersActions[playerId]) {
      switch (gameState.playersActions[playerId]) {
        case POKER_TEXAS_HOLDEM_ACTIONS.fold:
          turnMessage = `${playerInTurn.username}: Fold!`;
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.check:
          turnMessage = `${playerInTurn.username}: Check!`;
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.call:
          turnMessage = `${playerInTurn.username}: Call!`;
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.bet:
          turnMessage = `${playerInTurn.username}: Bet!`;
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.raise:
          turnMessage = `${playerInTurn.username}: Raise!`;
          break;
        case POKER_TEXAS_HOLDEM_ACTIONS.all_in:
          turnMessage = `${playerInTurn.username}: All-in!`;
          break;
        default:
          break;
      }
    }
  }

  scene.playerInTurnLabel.setText(turnMessage);
}

export function updatePlayerTurnTimer(scene, remainingTime) {
  scene.playerTurnTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateDealerTimer(scene, remainingTime) {
  scene.dealerTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateBlindsTimer(scene, remainingTime) {
  scene.blindsTimer = remainingTime;
  updatePlayerInTurnLabel(scene, scene.gameState);
}

export function updateDeckPile(scene, gameState) {
  if (scene.deckPile) {
    scene.deckPile.clear(true, true);
  } else {
    scene.deckPile = scene.add.group();
  }

  const { x, y, rotation } = CardManager.getDeckPilePosition(scene);

  gameState.deckPile.forEach((card, index) => {
    const cardSprite = scene.add
      .sprite(x, y, "cardBacks", 0)
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setRotation(rotation)
      .setDepth(index)
      .setInteractive();

    cardSprite.on("pointerdown", () => {
      if (gameState.paused) return; // Prevent interaction if the game is paused

      if (
        (gameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_hole_cards ||
          gameState.gameStatus ===
            POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop ||
          gameState.gameStatus ===
            POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn ||
          gameState.gameStatus ===
            POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_river) &&
        gameState.dealer === scene.socket.id
      ) {
        scene.socket.emit("dealCards");
      }
    });

    scene.deckPile.add(cardSprite);
  });
}

export function updateDiscardPile(scene, gameState) {
  if (scene.discardPile) {
    scene.discardPile.clear(true, true);
  } else {
    scene.discardPile = scene.add.group();
  }

  if (gameState.discardPile.length > 0) {
    if (
      gameState.discardingCard &&
      gameState.discardingCard.id === gameState.discardPile[0].id
    ) {
      return; // Ignore the card if it's the same as the discarding card and it is the only card in the discard pile.
    }
    if (
      gameState.discardingCards &&
      gameState.discardingCards.length === gameState.discardPile.length
    ) {
      return; // Ignore the discarding cards if they are the only cards in the discard pile.
    }
    const { x, y, rotation } = CardManager.getDiscardPilePosition(scene);
    const cardSprite = scene.add
      .sprite(x, y, "cardBacks", 0)
      .setOrigin(0.5)
      .setScale(CardManager.CARD_SCALE)
      .setRotation(rotation);

    scene.discardPile.add(cardSprite);
  }
}

export function updateFaceUpCards(scene, gameState) {
  if (scene.faceUpCardSprites) {
    scene.faceUpCardSprites.clear(true, true);
  } else {
    scene.faceUpCardSprites = scene.add.group();
  }

  // Create new face-up card sprites
  gameState.faceUpCards.forEach((card, index) => {
    if (gameState.dealingCard && gameState.dealingCard.id === card.id) {
      return; // Ignore the card if it's the same as the dealing card
    }

    const { x, y } = CardManager.getFaceUpCardPosition(scene, index);
    const cardSprite = CardManager.createCardSprite(scene, card.card, true);
    cardSprite.setPosition(x, y);
    scene.faceUpCardSprites.add(cardSprite);
  });
}

export function updateDealerButton(scene, gameState) {
  if (!gameState.dealer) return; // Early exit if no dealer is assigned yet

  const dealerIndex = gameState.distributionOfSeats.indexOf(gameState.dealer);
  if (dealerIndex === -1) return; // Exit if dealer is not in the distribution

  const centerX = scene.sys.game.config.width / 2;
  const centerY = scene.sys.game.config.height / 2;

  if (!scene.dealerButton) {
    scene.dealerButton = scene.add
      .image(centerX, centerY, "dealerButton")
      .setOrigin(0.5)
      .setScale(0.3)
      .setDepth(500);
  }
  const totalPlayers = gameState.distributionOfSeats.length;
  const playerPosition = CardManager.getPlayerCardPosition(
    scene,
    dealerIndex,
    scene.playerIndex,
    gameState.playersHands[gameState.dealer].length,
    gameState.playersHands[gameState.dealer].length > 0
      ? gameState.playersHands[gameState.dealer].length - 1
      : 0,
    totalPlayers
  );

  // Calculate angle from center to player
  const angle = Math.atan2(
    playerPosition.y - centerY,
    playerPosition.x - centerX
  );

  // Use fixed offset values for more control
  const offsetDistance = 75; // Distance from player's card position
  const buttonX = playerPosition.x - Math.cos(angle) * offsetDistance;
  const buttonY = playerPosition.y - Math.sin(angle) * offsetDistance;

  // Add a position check before setting
  if (
    !scene.dealerButton.x ||
    scene.dealerButton.x !== buttonX ||
    scene.dealerButton.y !== buttonY
  ) {
    scene.tweens.add({
      targets: scene.dealerButton,
      x: buttonX,
      y: buttonY,
      duration: 500,
      ease: "Power2",
    });
  }
}

export function createPauseOverlay(scene) {
  const pauseOverlay = scene.add
    .rectangle(
      0,
      0,
      scene.sys.game.config.width,
      scene.sys.game.config.height,
      0x000000,
      0.5
    )
    .setOrigin(0);
  pauseOverlay.setDepth(15000);
  pauseOverlay.setVisible(false);

  const pauseText = scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2,
      "Game Paused",
      {
        font: "32px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
  pauseText.setDepth(15010);
  pauseText.setVisible(false);

  return { pauseOverlay, pauseText };
}

export function updatePauseOverlay(scene, gameState) {
  const { pauseOverlay, pauseText } = scene.pauseOverlay;
  pauseOverlay.setVisible(gameState.paused);
  pauseText.setVisible(gameState.paused);
}

export function createExchangingChipsOverlay(scene) {
  const overlay = scene.add.rectangle(
    0,
    0,
    scene.sys.game.config.width,
    scene.sys.game.config.height,
    0x000000,
    0.7
  );
  overlay.setOrigin(0);
  overlay.setDepth(20000);
  overlay.visible = false;

  const box = scene.add.rectangle(
    scene.sys.game.config.width / 2,
    scene.sys.game.config.height / 2,
    600,
    300,
    0x333333
  );
  box.setOrigin(0.5);
  box.setDepth(20001);
  box.visible = false;

  const headerText = scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2 - 120,
      "Exchanging Chips",
      {
        font: "32px bold-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5)
    .setDepth(20002)
    .setVisible(false);

  const outgoingText = scene.add
    .text(
      scene.sys.game.config.width / 2 - 200,
      scene.sys.game.config.height / 2 - 50,
      "Outgoing:",
      {
        font: "24px regular-font",
        fill: "#ffff00",
      }
    )
    .setOrigin(0.5)
    .setDepth(20002)
    .setVisible(false);

  const incomingText = scene.add
    .text(
      scene.sys.game.config.width / 2 + 200,
      scene.sys.game.config.height / 2 - 50,
      "Incoming:",
      {
        font: "24px regular-font",
        fill: "#00ff00",
      }
    )
    .setOrigin(0.5)
    .setDepth(20002)
    .setVisible(false);

  return { overlay, box, headerText, outgoingText, incomingText };
}

export function updateExchangingChipsOverlay(scene, gameState) {
  if (!scene.exchangingChipsOverlay) return;

  const { overlay, box, headerText, outgoingText, incomingText } =
    scene.exchangingChipsOverlay;
  const isExchanging = gameState.gameStatus === "exchanging_chips";

  // Initialize exchangeChipSprites array if it doesn't exist
  if (!scene.exchangeChipSprites) {
    scene.exchangeChipSprites = [];
  }

  // Always destroy previous chip sprites and texts
  scene.exchangeChipSprites.forEach((sprite) => sprite.destroy());
  scene.exchangeChipSprites = [];

  [overlay, box, headerText, outgoingText, incomingText].forEach((el) =>
    el.setVisible(isExchanging)
  );

  if (isExchanging) {
    const countChips = (chips) =>
      chips.reduce((acc, chip) => {
        acc[chip.color] = (acc[chip.color] || 0) + 1;
        return acc;
      }, {});

    const outgoingChips = countChips(gameState.exchangingChips.outgoingChips);
    const incomingChips = countChips(gameState.exchangingChips.incomingChips);

    // Helper function to display chips
    const displayChips = (chips, startX, startY, direction = 1) => {
      let yOffset = 0;
      Object.entries(chips).forEach(([color, count]) => {
        const chipSprite = scene.add
          .image(startX, startY + yOffset, getChipFrontTexture(color))
          .setScale(CHIP_SCALE)
          .setDepth(20002);
        const countText = scene.add
          .text(startX + 40 * direction, startY + yOffset, `x ${count}`, {
            font: "18px regular-font",
            fill: "#ffffff",
          })
          .setOrigin(direction < 0 ? 1 : 0, 0.5)
          .setDepth(20002);
        scene.exchangeChipSprites.push(chipSprite, countText);
        yOffset += 40;
      });
    };

    displayChips(
      outgoingChips,
      scene.sys.game.config.width / 2 - 250,
      scene.sys.game.config.height / 2,
      1
    );
    displayChips(
      incomingChips,
      scene.sys.game.config.width / 2 + 250,
      scene.sys.game.config.height / 2,
      -1
    );
  }
}
