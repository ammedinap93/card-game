import * as AudioManager from "./audioManager.js";
import { getPlayerLabelPosition } from "./playerManager.js";
import { POKER_TEXAS_HOLDEM_ACTIONS } from "./pokerTexasHoldemConstants.js";

export const CARD_WIDTH = 256;
export const CARD_HEIGHT = 356;
export const CARD_SCALE = 0.25;

export function getCardTexture(card) {
  const [rank, _] = card.split("_");
  return rank === "joker" ? "jokers" : "playingCards";
}

export function setCardFrame(cardSprite, card) {
  const [rank, suit] = card.split("_");
  if (rank !== "joker") {
    const suitIndex = ["clubs", "hearts", "spades", "diamonds"].indexOf(suit);
    const rankIndex = [
      "ace",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "jack",
      "queen",
      "king",
    ].indexOf(rank);
    cardSprite.setFrame(suitIndex * 13 + rankIndex);
  } else {
    cardSprite.setFrame(suit === "red" ? 1 : 0);
  }
}

export function createCardSprite(scene, card, isVisible) {
  const texture = getCardTexture(card);
  const cardSprite = scene.add
    .sprite(0, 0, texture, 0)
    .setOrigin(0.5)
    .setScale(CARD_SCALE)
    .setInteractive();

  if (isVisible) {
    setCardFrame(cardSprite, card);
  } else {
    cardSprite.setTexture("cardBacks");
    cardSprite.setFrame(0);
  }

  return cardSprite;
}

function getCardPosition(
  scene,
  index,
  playerIndex,
  cardIndex,
  numCards,
  isCurrentPlayer,
  totalPlayers
) {
  const playerPosition = getPlayerLabelPosition(
    scene,
    index,
    playerIndex,
    totalPlayers
  );
  let { x, y, rotation: labelRotation } = playerPosition;

  let rotation = labelRotation + Math.PI / 2 + Math.PI;

  let cardSpacing = getPlayerHandCardOffset(isCurrentPlayer);

  // Calculate an offset to move cards away from the center
  const cardOffsetMagnitude = 60;
  const cardOffsetX = cardOffsetMagnitude * Math.cos(rotation);
  const cardOffsetY = cardOffsetMagnitude * Math.sin(rotation);

  // Adjust card spacing based on player position
  const spacingOffsetX = cardSpacing * Math.sin(rotation);
  const spacingOffsetY = -cardSpacing * Math.cos(rotation);

  // Calculate card position
  const cardX = isCurrentPlayer
    ? x - (cardOffsetX + (cardIndex - (numCards - 1) / 2) * spacingOffsetX)
    : x + cardOffsetX + (cardIndex - (numCards - 1) / 2) * spacingOffsetX;
  const cardY =
    y + cardOffsetY + (cardIndex - (numCards - 1) / 2) * spacingOffsetY;

  // Rotate cards to face the center
  const cardRotation = rotation + Math.PI / 2;

  return { x: cardX, y: cardY, rotation: cardRotation };
}

export function getPlayerCardPosition(
  scene,
  playerIndex,
  currentPlayerIndex,
  handLength,
  cardIndex,
  totalPlayers
) {
  const position = getCardPosition(
    scene,
    playerIndex,
    currentPlayerIndex,
    cardIndex,
    handLength,
    playerIndex === currentPlayerIndex,
    totalPlayers
  );
  return { x: position.x, y: position.y, rotation: position.rotation };
}

function getPlayerHandCardOffset(isCurrentPlayer) {
  let offset = isCurrentPlayer ? 25 : 20;
  return offset;
}

function setupDraggableCard(
  scene,
  cardSprite,
  id,
  gameState,
  index,
  playerIndex,
  handLength,
  totalPlayers
) {
  scene.input.setDraggable(cardSprite);
  cardSprite.on("dragstart", () => {
    scene.cardOriginalDepths[id] = cardSprite.depth;
    cardSprite.setDepth(1000);
    AudioManager.playRandomSound(scene.cardSlideSounds);
  });
  cardSprite.on("drag", (pointer) => {
    if (gameState.paused) return; // Prevent dragging if the game is paused
    cardSprite.x = pointer.x;
    cardSprite.y = pointer.y;
  });
  cardSprite.on("dragend", (pointer) => {
    const playerCards = scene.playerCards[scene.socket.id];
    const originalIndex = playerCards.indexOf(cardSprite);
    const originalPosition = getCardPosition(
      scene,
      index,
      playerIndex,
      originalIndex,
      handLength,
      true,
      totalPlayers
    );

    if (gameState.paused) {
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
      return;
    }
    if (Math.abs(pointer.y - originalPosition.y) > 90) {
      cardSprite.x = originalPosition.x;
      cardSprite.y = originalPosition.y;
      cardSprite.setDepth(scene.cardOriginalDepths[id]);
      AudioManager.playRandomSound(scene.cardSlideSounds);
    } else {
      const playerCardPositions = playerCards.map((card, idx) =>
        getCardPosition(
          scene,
          gameState.distributionOfSeats.indexOf(scene.socket.id),
          playerIndex,
          idx,
          handLength,
          true,
          totalPlayers
        )
      );
      const targetIndex = playerCardPositions.findIndex(
        (position) => pointer.x < position.x
      );
      if (targetIndex === originalIndex) {
        cardSprite.x = originalPosition.x;
        cardSprite.y = originalPosition.y;
        cardSprite.setDepth(scene.cardOriginalDepths[id]);
      } else if (targetIndex === -1) {
        scene.socket.emit("moveCard", {
          cardId: id,
          newIndex: playerCards.length - 1,
        });
      } else {
        scene.socket.emit("moveCard", {
          cardId: id,
          newIndex: targetIndex,
        });
      }
      AudioManager.playRandomSound(scene.cardSlideSounds);
    }
  });
}

export function getFaceUpCardPosition(scene, index) {
  let x = scene.sys.game.config.width / 2;
  let y = scene.sys.game.config.height / 2;

  switch (index) {
    case 0:
      x -= 116;
      break;
    case 1:
      x -= 38;
      break;
    case 2:
      x += 38;
      break;
    case 3:
      x += 116;
      break;
    case 4:
      x += 194;
      break;
  }

  return { x, y };
}

export function getDiscardPilePosition(scene) {
  let x = scene.sys.game.config.width / 2 - 150;
  let y = scene.sys.game.config.height / 2 + 110;
  let rotation = Phaser.Math.DegToRad(-45);

  return { x, y, rotation };
}

export function getDeckPilePosition(scene) {
  let x = scene.sys.game.config.width / 2 - 200;
  let y = scene.sys.game.config.height / 2;
  let rotation = 0;

  return { x, y, rotation };
}

export function updatePlayerCards(scene, gameState) {
  const totalPlayers = gameState.distributionOfSeats.length;

  // Clear existing player cards
  if (scene.playerCards) {
    Object.values(scene.playerCards).forEach((cards) =>
      cards.forEach((card) => card.destroy())
    );
  }
  scene.playerCards = {};

  const playerIndex = scene.playerIndex;

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const playerHand = gameState.playersHands[playerId] || [];
    const playerCards = [];

    playerHand.forEach(({ id, card }, cardIndex) => {
      if (gameState.dealingCard && gameState.dealingCard.id === id) {
        return; // Ignore the card if it's the same as the dealing card
      }

      const isCurrentPlayer = playerId === scene.socket.id;
      let shouldShowCard = isCurrentPlayer;
      if (
        gameState.playersActions[playerId] ===
        POKER_TEXAS_HOLDEM_ACTIONS.show_cards
      ) {
        shouldShowCard = true;
      }
      let cardSprite = createCardSprite(scene, card, shouldShowCard);

      const cardPosition = getCardPosition(
        scene,
        index,
        playerIndex,
        cardIndex,
        playerHand.length,
        isCurrentPlayer,
        totalPlayers
      );
      cardSprite.setPosition(cardPosition.x, cardPosition.y);
      cardSprite.setRotation(cardPosition.rotation);
      cardSprite.setDepth(cardIndex);
      scene.cardOriginalDepths[id] = cardIndex;

      if (isCurrentPlayer) {
        setupDraggableCard(
          scene,
          cardSprite,
          id,
          gameState,
          index,
          playerIndex,
          playerHand.length,
          totalPlayers
        );
      }

      playerCards.push(cardSprite);
    });

    scene.playerCards[playerId] = playerCards;
  });

  return scene.playerCards;
}
