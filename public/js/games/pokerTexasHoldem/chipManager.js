import { getPlayerCardPosition } from "./cardManager.js";
import { POKER_TEXAS_HOLDEM_CHIP_VALUES } from "./pokerTexasHoldemConstants.js";

export const CHIP_SCALE = 0.5;
export const CHIP_VERTICAL_OFFSET = 5; // Space between chips in a vertical stack
const STACK_HORIZONTAL_OFFSET = 35; // Space between chip stacks
const STACK_DISTANCE_FROM_CARDS = 50; // Distance of chip stacks from the cards
const ROW_VERTICAL_OFFSET = 35; // Distance between chip stack rows
const POT_VERTICAL_OFFSET = 100;
const POT_MAX_HORIZONTAL_OFFSET = 150;
const POT_MAX_RADIUS_X = 50;
const POT_MAX_RADIUS_Y = 25;

const CHIP_BASE_DEPTHS = {
  left: {
    white: 2000,
    red: 3000,
    blue: 4000,
    green: 5000,
    black: 6000,
  },
  right: {
    white: 4000,
    red: 3000,
    blue: 2000,
    green: 6000,
    black: 5000,
  },
};

const BET_BASE_DEPTHS = {
  top: 7000,
  bottom: 1000,
};

const topRowChips = ["white", "red", "blue"];
const bottomRowChips = ["green", "black"];

export function getChipTexture(color) {
  switch (color) {
    case "white":
      return "chipWhiteBlue";
    case "red":
      return "chipRedWhite";
    case "blue":
      return "chipBlueWhite";
    case "green":
      return "chipGreenWhite";
    case "black":
      return "chipBlackWhite";
    default:
      console.error("Unknown chip color:", color);
      return "chipWhiteBlue"; // default to white chip
  }
}

export function getChipFrontTexture(color) {
  switch (color) {
    case "white":
      return "chipWhiteBlueFront";
    case "red":
      return "chipRedWhiteFront";
    case "blue":
      return "chipBlueWhiteFront";
    case "green":
      return "chipGreenWhiteFront";
    case "black":
      return "chipBlackWhiteFront";
    default:
      console.error("Unknown chip color:", color);
      return "chipWhiteBlueFront"; // default to white chip
  }
}

export function getPlayerBetPosition(scene, gameState, playerId) {
  const playerIndex = gameState.distributionOfSeats.indexOf(playerId);
  const handLength = gameState.playersHands[playerId].length;
  const cardPos = getPlayerCardPosition(
    scene,
    playerIndex,
    scene.playerIndex,
    handLength,
    playerId === scene.socket.id ? handLength - 1 : 0,
    gameState.distributionOfSeats.length
  );

  const { x: chipBaseX, y: chipBaseY } = getPlayerStackBasePosition(cardPos);

  // Calculate angle from center to player
  const centerX = scene.sys.game.config.width / 2;
  const centerY = scene.sys.game.config.height / 2;
  const angle = Math.atan2(chipBaseY - centerY, chipBaseX - centerX);

  const offsetDistance = 110; // Distance from player's stack position
  const betBaseX = chipBaseX - Math.cos(angle) * offsetDistance;
  const betBaseY = chipBaseY - Math.sin(angle) * offsetDistance;

  return { x: betBaseX, y: betBaseY };
}

export function getPlayerStackBasePosition(cardPos) {
  const chipBaseX =
    cardPos.x + Math.cos(cardPos.rotation) * STACK_DISTANCE_FROM_CARDS;
  const chipBaseY =
    cardPos.y + Math.sin(cardPos.rotation) * STACK_DISTANCE_FROM_CARDS;
  return { x: chipBaseX, y: chipBaseY };
}

export function updatePlayerChips(scene, gameState) {
  const totalPlayers = gameState.distributionOfSeats.length;
  const playerIndex = gameState.distributionOfSeats.indexOf(scene.socket.id);

  scene.playerChips = scene.playerChips || {};

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const stack = gameState.playersStacks[playerId];
    const handLength = gameState.playersHands[playerId].length;

    // Filter out chips that are in betting or awarding transition
    const transitionChipIds = getTransitionChipIds(gameState);

    const filteredStack = Object.fromEntries(
      Object.entries(stack).map(([color, chips]) => [
        color,
        chips.filter((chip) => !transitionChipIds.includes(chip.id)),
      ])
    );

    // Use a card's position and rotation as reference
    const cardPos = getPlayerCardPosition(
      scene,
      index,
      playerIndex,
      handLength,
      playerId === scene.socket.id ? handLength - 1 : 0,
      totalPlayers
    );

    // Adjust chip base position relative to card position and rotation
    const { x: chipBaseX, y: chipBaseY } = getPlayerStackBasePosition(cardPos);

    // If chip group for this player already exists, destroy it
    if (scene.playerChips[playerId]) {
      scene.playerChips[playerId].clear(true, true);
    } else {
      scene.playerChips[playerId] = scene.add.group();
    }

    // Calculate direction vector perpendicular to card rotation for row offset
    const perpendicularX = Math.sin(cardPos.rotation);
    const perpendicularY = -Math.cos(cardPos.rotation);

    // Determine if player is on top or bottom half (affects bottom row positioning)
    const isTopHalf = cardPos.y < scene.sys.game.config.height / 2;

    const side =
      Math.abs(cardPos.rotation) > Math.PI &&
      Math.abs(cardPos.rotation) < Math.PI * 2
        ? "right"
        : "left";

    // Function to process a row of chip stacks
    const processChipRow = (chipColors, rowOffset) => {
      let stackOffset = 0;
      chipColors.forEach((color) => {
        const colorStack = filteredStack[color];
        if (colorStack && colorStack.length > 0) {
          const stackX =
            chipBaseX +
            Math.cos(cardPos.rotation) * stackOffset +
            perpendicularX * rowOffset;
          const stackY =
            chipBaseY +
            Math.sin(cardPos.rotation) * stackOffset +
            perpendicularY * rowOffset;
          createChipColorStack(
            scene,
            colorStack,
            stackX,
            stackY,
            side,
            scene.playerChips[playerId]
          );
          stackOffset += STACK_HORIZONTAL_OFFSET;
        }
      });
    };

    // Process top row
    processChipRow(topRowChips, 0);

    // Process bottom row, adjust offset based on top/bottom position
    const bottomRowOffset = isTopHalf
      ? ROW_VERTICAL_OFFSET
      : -ROW_VERTICAL_OFFSET;
    processChipRow(bottomRowChips, bottomRowOffset);
  });
}

export function createChipColorStack(
  scene,
  colorStack,
  stackX,
  stackY,
  side,
  group
) {
  const color = colorStack[0].color; // All chips in a stack have the same color
  const chipTexture = getChipTexture(color);
  const baseDepth = CHIP_BASE_DEPTHS[side][color];

  colorStack.forEach((chip, i) => {
    const chipSprite = scene.add
      .image(
        stackX + chip.offsetX,
        stackY - i * CHIP_VERTICAL_OFFSET + chip.offsetY,
        i === 0 ? chipTexture + "Border" : chipTexture
      )
      .setScale(CHIP_SCALE)
      .setDepth(baseDepth + i);
    chipSprite.flipX = chip.flipX;

    group.add(chipSprite);
    scene.chipSprites[chip.id] = chipSprite;
  });
}

export function createChipStack(scene, chips, x, y, baseDepth, group) {
  chips.forEach((chip, i) => {
    const chipTexture = getChipTexture(chip.color);
    const chipSprite = scene.add
      .image(
        x + chip.offsetX,
        y - i * CHIP_VERTICAL_OFFSET + chip.offsetY,
        i === 0 ? chipTexture + "Border" : chipTexture
      )
      .setScale(CHIP_SCALE)
      .setDepth(baseDepth + i);
    chipSprite.flipX = chip.flipX;

    group.add(chipSprite);
    scene.chipSprites[chip.id] = chipSprite;
  });
}

export function updatePlayerBets(scene, gameState) {
  scene.playerBets = scene.playerBets || {};

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const bet = gameState.playersBets[playerId];

    // Filter out chips that are in betting or awarding transition
    const transitionChipIds = getTransitionChipIds(gameState);

    const filteredBet = bet.filter(
      (chip) => !transitionChipIds.includes(chip.id)
    );

    const { x: betBaseX, y: betBaseY } = getPlayerBetPosition(
      scene,
      gameState,
      playerId
    );

    if (scene.playerBets[playerId]) {
      scene.playerBets[playerId].clear(true, true);
    } else {
      scene.playerBets[playerId] = scene.add.group();
    }

    const isTopHalf = betBaseY < scene.sys.game.config.height / 2;

    if (filteredBet.length > 0) {
      createChipStack(
        scene,
        filteredBet,
        betBaseX,
        betBaseY,
        isTopHalf ? BET_BASE_DEPTHS.top : BET_BASE_DEPTHS.bottom,
        scene.playerBets[playerId]
      );
    }
  });
}

export function betTotalAmount(bet) {
  return bet.reduce(
    (total, chip) => total + POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color],
    0
  );
}

export function getPotCenterPosition(scene, potIndex, totalPots) {
  const centerY = scene.sys.game.config.height / 2 - POT_VERTICAL_OFFSET;
  const totalWidth = (totalPots - 1) * POT_MAX_HORIZONTAL_OFFSET;
  const centerX =
    scene.sys.game.config.width / 2 -
    totalWidth / 2 +
    potIndex * POT_MAX_HORIZONTAL_OFFSET;
  return { x: centerX, y: centerY };
}

export function getPotSize(pot, totalPots) {
  const chipCount = pot.chips.length;
  const maxChipCount = 100; // Adjust this based on expected chip counts
  const scale =
    Math.min(1, chipCount / maxChipCount) * (1 / Math.sqrt(totalPots));
  return {
    radiusX: POT_MAX_RADIUS_X * scale,
    radiusY: POT_MAX_RADIUS_Y * scale,
  };
}

export function createChipHeap(
  scene,
  chips,
  centerX,
  centerY,
  radiusX,
  radiusY,
  baseDepth,
  group
) {
  chips.forEach((chip, i) => {
    const chipTexture = getChipTexture(chip.color);
    const chipSprite = scene.add
      .image(
        centerX + chip.offsetX * radiusX,
        centerY + chip.offsetY * radiusY,
        chipTexture
      )
      .setScale(CHIP_SCALE)
      .setDepth(baseDepth + i);
    chipSprite.flipX = chip.flipX;

    group.add(chipSprite);
    scene.chipSprites[chip.id] = chipSprite;
  });
}

export function getTransitionChipIds(gameState) {
  return [
    ...gameState.bettingChips.map((chip) => chip.id),
    ...gameState.awardingChips.map((chip) => chip.id),
    ...gameState.pushingChips.map((chip) => chip.id),
  ];
}

export function updatePots(scene, gameState) {
  if (scene.pots) {
    scene.pots.clear(true, true);
  } else {
    scene.pots = scene.add.group();
  }

  const transitionChipIds = getTransitionChipIds(gameState);
  const totalPots = gameState.pots.length;

  gameState.pots.forEach((pot, index) => {
    const filteredChips = pot.chips.filter(
      (chip) => !transitionChipIds.includes(chip.id)
    );
    if (filteredChips.length === 0) return;

    const { x: centerX, y: centerY } = getPotCenterPosition(
      scene,
      index,
      totalPots
    );
    const { radiusX, radiusY } = getPotSize(pot, totalPots);

    createChipHeap(
      scene,
      filteredChips,
      centerX,
      centerY,
      radiusX,
      radiusY,
      8000,
      scene.pots
    );
  });
}

export function updateChips(scene, gameState) {
  updatePlayerChips(scene, gameState);
  updatePlayerBets(scene, gameState);
  updatePots(scene, gameState);
}
