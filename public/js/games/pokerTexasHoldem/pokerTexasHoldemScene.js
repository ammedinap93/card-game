import * as CardManager from "./cardManager.js";
import * as PlayerManager from "./playerManager.js";
import * as ChipManager from "./chipManager.js";
import * as AudioManager from "./audioManager.js";
import * as UIElements from "./uiElements.js";
import * as AnimationManager from "./animationManager.js";
import HandSummaryScene from "./handSummaryScene.js";
import { createChatUI } from "../chatUI.js";
import {
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS,
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_STAGES,
} from "./pokerTexasHoldemConstants.js";
import { isEmptyObject } from "../../helpers.js";

class PokerTexasHoldemScene extends Phaser.Scene {
  constructor() {
    super({ key: "PokerTexasHoldemScene" });
  }

  preload() {}

  create() {
    this.scene.add("HandSummaryScene", HandSummaryScene, false);

    UIElements.createBackgroundImage(this);

    this.playerInTurnLabel = UIElements.createPlayerInTurnLabel(
      this,
      "Loading..."
    );
    this.pauseOverlay = UIElements.createPauseOverlay(this);
    this.exchangingChipsOverlay = UIElements.createExchangingChipsOverlay(this);

    this.postBlindButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 20,
      "",
      false,
      false,
      () => {
        this.socket.emit("postBlind");
      }
    );

    this.foldButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 20,
      "Fold",
      false,
      false,
      () => {
        this.socket.emit("fold");
      }
    );

    this.checkButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 50,
      "Check",
      false,
      false,
      () => {
        this.socket.emit("check");
      }
    );

    this.betButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 80,
      "Bet",
      false,
      false,
      () => {
        this.socket.emit("bet");
      }
    );

    this.callButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 50,
      "Call",
      false,
      false,
      () => {
        this.socket.emit("call");
      }
    );

    this.raiseButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 80,
      "Raise",
      false,
      false,
      () => {
        this.socket.emit("raise");
      }
    );

    this.showCardsButton = UIElements.createButton(
      this,
      20,
      this.sys.game.config.height - 50,
      "Show Cards",
      false,
      false,
      () => {
        this.socket.emit("showCards");
      }
    );

    this.errorLabel = this.add
      .text(
        this.sys.game.config.width / 2,
        this.sys.game.config.height - 160,
        "",
        { font: "18px regular-font", fill: "#ff0000" }
      )
      .setOrigin(0.5);

    this.chatMessages = {};
    this.cardOriginalDepths = {};
    this.playerLabels = {};
    this.playerCards = {};
    this.playerChips = {};
    this.playerBets = {};
    this.pots = null;
    this.chipSprites = {};
    this.gameState = {};
    this.gameStats = null;
    this.playerTurnTimer = null;
    this.dealerTimer = null;
    this.blindsTimer = null;

    const {
      cardPlaceSounds,
      cardSlideSounds,
      chipCollideSounds,
      newMessageSound,
      newTurnSound,
      texasHoldemSound,
      foldSound,
      checkSound,
      betSound,
      callSound,
      raiseSound,
      smallBlindSound,
      bigBlindSound,
      allInSound,
      flopSound,
      turnSound,
      riverSound,
      showdownSound,
    } = AudioManager.createSoundEffects(this);
    this.cardPlaceSounds = cardPlaceSounds;
    this.cardSlideSounds = cardSlideSounds;
    this.chipCollideSounds = chipCollideSounds;
    this.newMessageSound = newMessageSound;
    this.newTurnSound = newTurnSound;
    this.texasHoldemSound = texasHoldemSound;
    this.foldSound = foldSound;
    this.checkSound = checkSound;
    this.betSound = betSound;
    this.callSound = callSound;
    this.raiseSound = raiseSound;
    this.smallBlindSound = smallBlindSound;
    this.bigBlindSound = bigBlindSound;
    this.allInSound = allInSound;
    this.flopSound = flopSound;
    this.turnSound = turnSound;
    this.riverSound = riverSound;
    this.showdownSound = showdownSound;

    // Track previous game state
    this.previousGameState = {};

    this.socket = this.scene.settings.data.socket;
    this.socket.on("gameState", (gameState) => {
      console.log("Game state received:", gameState);
      this.updateGameState(gameState);
    });
    this.socket.on("chatMessage", (chatMessage) => {
      console.log("Chat message received:", chatMessage);
      this.updateChatMessages(chatMessage);
    });
    this.socket.on("timerUpdate", (timerData) => {
      this.updateTimer(timerData);
    });
    this.socket.on("gameStats", (gameStats) => {
      console.log("Game stats received:", gameStats);
      this.gameStats = gameStats;
    });

    this.chatUI = createChatUI(
      this,
      this.socket,
      this.sys.game.config.width - 20,
      this.sys.game.config.height - 50,
      "right"
    );
  }

  updateGameState(gameState) {
    if (
      gameState.playerInTurn !== this.previousGameState.playerInTurn &&
      gameState.playerInTurn === this.socket.id
    ) {
      this.newTurnSound.play();
    }

    if (
      gameState.gameStatus !== this.previousGameState.gameStatus &&
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action
    ) {
      this.time.delayedCall(1000, () => {
        AudioManager.playActionSound(this, gameState);
      });
    }

    if (
      gameState.gameStatus !== this.previousGameState.gameStatus &&
      POKER_TEXAS_HOLDEM_STAGES.includes(gameState.gameStatus)
    ) {
      this.time.delayedCall(1500, () => {
        AudioManager.playStageSound(this, gameState);
      });
    }

    if (isEmptyObject(this.previousGameState)) {
      this.time.delayedCall(2000, () => {
        this.texasHoldemSound.play();
      });
    }

    // Store current game state as previous for next comparison
    this.previousGameState = JSON.parse(JSON.stringify(gameState));

    this.gameState = gameState;
    this.playerIndex = gameState.distributionOfSeats.indexOf(this.socket.id);
    PlayerManager.updatePlayerLabels(this, gameState, this.chatMessages);
    UIElements.updatePlayerInTurnLabel(this, gameState);
    UIElements.updateDeckPile(this, gameState);
    UIElements.updateDiscardPile(this, gameState);
    UIElements.updateFaceUpCards(this, gameState);
    UIElements.updateDealerButton(this, gameState);
    CardManager.updatePlayerCards(this, gameState);
    ChipManager.updateChips(this, gameState);
    AnimationManager.animateDealingCard(this, gameState);
    AnimationManager.animateDiscardingCard(this, gameState);
    AnimationManager.animateDiscardingCards(this, gameState);
    AnimationManager.animateBettingChips(this, gameState);
    AnimationManager.animatePushingChips(this, gameState);
    AnimationManager.animateAwardingChips(this, gameState);
    this.updatePostBlindButton(gameState);
    this.updateFoldButton(gameState);
    this.updateCheckButton(gameState);
    this.updateBetButton(gameState);
    this.updateCallButton(gameState);
    this.updateRaiseButton(gameState);
    this.updateShowCardsButton(gameState);
    UIElements.updatePauseOverlay(this, gameState);
    UIElements.updateExchangingChipsOverlay(this, gameState);

    if (
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand ||
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_game
    ) {
      this.time.delayedCall(1000, () => {
        this.scene.launch("HandSummaryScene", {
          socket: this.socket,
          gameState: this.gameState,
          gameStats: this.gameStats,
        });
      });
    }
  }

  updateChatMessages(chatMessage) {
    // Play new message sound
    this.newMessageSound.play();

    this.chatMessages[chatMessage.player.id] = chatMessage.message;
    PlayerManager.updatePlayerLabels(this, this.gameState, this.chatMessages);

    // Remove the message after 2.5 seconds
    this.time.delayedCall(2500, () => {
      delete this.chatMessages[chatMessage.player.id];
    });
  }

  updateTimer(timerData) {
    const { timerName, remainingTime } = timerData;

    if (timerName === "playerTurn") {
      UIElements.updatePlayerTurnTimer(this, remainingTime);
    } else if (timerName === "dealer") {
      UIElements.updateDealerTimer(this, remainingTime);
    } else if (timerName === "blinds") {
      UIElements.updateBlindsTimer(this, remainingTime);
    }
  }

  showErrorMessage(message) {
    this.errorLabel.setText(message);
    this.time.delayedCall(5000, () => {
      this.errorLabel.setText("");
    });
  }

  updatePostBlindButton(gameState) {
    if (
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds &&
      gameState.playerInTurn === this.socket.id
    ) {
      const isSmallBlind = gameState.smallBlindPlayer === this.socket.id;
      this.postBlindButton
        .setText(isSmallBlind ? "Post Small Blind" : "Post Big Blind")
        .setVisible(true);
    } else {
      this.postBlindButton.setVisible(false);
    }
  }

  updateFoldButton(gameState) {
    this.foldButton.setVisible(this.shouldShowFoldButton(gameState));
  }

  shouldShowFoldButton(gameState) {
    return (
      (POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
        gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown) &&
      gameState.playerInTurn === this.socket.id
    );
  }

  updateCheckButton(gameState) {
    this.checkButton.setVisible(this.shouldShowCheckButton(gameState));
  }

  shouldShowCheckButton(gameState) {
    if (
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.playerInTurn !== this.socket.id
    ) {
      return false;
    }

    const playerBet = gameState.playersBets[this.socket.id];
    const maxBet = Math.max(
      ...Object.values(gameState.playersBets).map(ChipManager.betTotalAmount)
    );

    return ChipManager.betTotalAmount(playerBet) === maxBet || maxBet === 0;
  }

  updateBetButton(gameState) {
    this.betButton.setVisible(this.shouldShowBetButton(gameState));
  }

  shouldShowBetButton(gameState) {
    return (
      POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) &&
      gameState.playerInTurn === this.socket.id &&
      Object.values(gameState.playersBets).every((bets) => bets.length === 0)
    );
  }

  updateCallButton(gameState) {
    this.callButton.setVisible(this.shouldShowCallButton(gameState));
  }

  shouldShowCallButton(gameState) {
    if (
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.playerInTurn !== this.socket.id
    ) {
      return false;
    }

    const playerBet = gameState.playersBets[this.socket.id];
    const maxBet = Math.max(
      ...Object.values(gameState.playersBets).map(ChipManager.betTotalAmount)
    );

    return ChipManager.betTotalAmount(playerBet) < maxBet;
  }

  updateRaiseButton(gameState) {
    this.raiseButton.setVisible(this.shouldShowRaiseButton(gameState));
  }

  shouldShowRaiseButton(gameState) {
    return (
      POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) &&
      gameState.playerInTurn === this.socket.id &&
      Object.values(gameState.playersBets).some((bets) => bets.length > 0)
    );
  }

  updateShowCardsButton(gameState) {
    this.showCardsButton.setVisible(this.shouldShowShowCardsButton(gameState));
  }

  shouldShowShowCardsButton(gameState) {
    return (
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown &&
      gameState.playerInTurn === this.socket.id &&
      gameState.playersActions[this.socket.id] === null
    );
  }
}

export default PokerTexasHoldemScene;
