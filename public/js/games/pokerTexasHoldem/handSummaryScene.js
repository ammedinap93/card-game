import {
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_CHIP_VALUES,
  POKER_TEXAS_HOLDEM_ACTIONS,
} from "./pokerTexasHoldemConstants.js";
import { createButton } from "./uiElements.js";

const MAX_NUM_ROWS = 10;
const MAX_NUM_COLUMNS = 4;
const ROW_HEIGHT = 50;

class HandSummaryScene extends Phaser.Scene {
  constructor() {
    super({ key: "HandSummaryScene" });
  }

  init(data) {
    this.socket = data.socket;
    this.gameState = data.gameState;
    this.gameStats = data.gameStats;
    this.playerId = this.socket.id;
    this.summaryTimer = null;
  }

  create() {
    this.add
      .rectangle(
        0,
        0,
        this.sys.game.config.width,
        this.sys.game.config.height,
        0x000000,
        0.9
      )
      .setOrigin(0);

    const title = this.add
      .text(this.sys.game.config.width / 2, 50, "Hand Summary", {
        fontFamily: "regular-font",
        fontSize: "32px",
        fill: "#ffffff",
      })
      .setOrigin(0.5);

    this.createHandSummary();

    // Add winner label
    const winnerLabel = this.add
      .text(
        this.sys.game.config.width / 2,
        180 + MAX_NUM_ROWS * ROW_HEIGHT + 50,
        "",
        {
          fontFamily: "bold-font",
          fontSize: "32px",
          fill: "#ffffff",
        }
      )
      .setOrigin(0.5);

    if (
      this.gameState.gameStatus ===
      POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_game
    ) {
      const winnerId = this.gameState.winnerPlayer;
      if (winnerId) {
        const winnerName = this.gameState.lobby.players.find(
          (p) => p.id === winnerId
        ).username;
        winnerLabel.setText(`Winner: ${winnerName}! 🏆`);
      } else {
        winnerLabel.setText("It's a tie! 🤝");
      }
    }

    // Add busted label
    const bustedLabel = this.add
      .text(
        this.sys.game.config.width / 2,
        180 + MAX_NUM_ROWS * ROW_HEIGHT + 50,
        "",
        {
          fontFamily: "bold-font",
          fontSize: "32px",
          fill: "#ff0000",
        }
      )
      .setOrigin(0.5);

    if (
      this.gameState.gameStatus !==
        POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_game &&
      this.gameState.bustedPlayers.includes(this.playerId)
    ) {
      bustedLabel.setText("BUSTED! 😨");
    }

    // Show different UI elements based on the game status
    if (
      this.gameState.gameStatus ===
        POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand &&
      !this.gameState.bustedPlayers.includes(this.playerId)
    ) {
      // Add timer text
      this.timerText = this.add
        .text(
          this.sys.game.config.width / 2,
          this.sys.game.config.height - 120,
          this.summaryTimer ? `Time remaining: ${this.summaryTimer}s` : "",
          {
            fontFamily: "regular-font",
            fontSize: "18px",
            fill: "#ffffff",
          }
        )
        .setOrigin(0.5);

      // Add continue button
      const isPlayerReady =
        this.gameState.playersReadyToContinue[this.playerId];
      this.continueButton = createButton(
        this,
        this.sys.game.config.width / 2,
        this.sys.game.config.height - 80,
        "Continue",
        !isPlayerReady, // Show the button if the player is not ready
        true,
        () => {
          this.socket.emit("readyToContinue");
          this.continueButton.setVisible(false);
        }
      );

      // Add players ready text
      this.playersReadyText = this.add
        .text(
          this.sys.game.config.width / 2,
          this.sys.game.config.height - 40,
          this.getPlayersReadyText(),
          {
            fontFamily: "regular-font",
            fontSize: "18px",
            fill: "#ffffff",
          }
        )
        .setOrigin(0.5);
    } else if (
      this.gameState.gameStatus ===
        POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_game ||
      this.gameState.bustedPlayers.includes(this.playerId)
    ) {
      // Add "Play Again" button
      this.playAgainButton = createButton(
        this,
        this.sys.game.config.width / 2 - 100,
        this.sys.game.config.height - 80,
        "Play Again",
        true,
        true,
        () => {
          window.location.reload();
        }
      );

      // Add "Main Menu" button
      this.mainMenuButton = createButton(
        this,
        this.sys.game.config.width / 2 + 100,
        this.sys.game.config.height - 80,
        "Main Menu",
        true,
        true,
        () => {
          window.location.href = "/main";
        }
      );
    }

    if (this.allPlayersReady()) {
      this.time.delayedCall(1000, () => {
        this.scene.stop();
      });
    }

    // Listen for timer updates
    this.socket.on("timerUpdate", (timerData) => {
      if (timerData.timerName === "summary") {
        this.updateTimer(timerData.remainingTime);
      }
    });

    // Check if any player is ready to continue
    const anyPlayerReady = Object.values(
      this.gameState.playersReadyToContinue
    ).some((ready) => ready);

    // Play the appropriate sound effect
    if (!anyPlayerReady) {
      if (this.gameState.bustedPlayers.includes(this.playerId)) {
        this.sound.play("gameOver");
      } else {
        this.sound.play("summary");
      }
    }
  }

  createHandSummary() {
    const columnWidth = this.sys.game.config.width / (MAX_NUM_COLUMNS + 1);
    const startY = 100;

    // Column headers
    this.add
      .text(columnWidth, startY, "Player", {
        fontFamily: "regular-font",
        fontSize: "18px",
        fill: "#ffffff",
        fontWeight: "bold",
      })
      .setOrigin(0.5);

    this.add
      .text(columnWidth * 2, startY, "Stack", {
        fontFamily: "regular-font",
        fontSize: "18px",
        fill: "#ffffff",
        fontWeight: "bold",
      })
      .setOrigin(0.5);

    this.add
      .text(columnWidth * 3, startY, "Hand", {
        fontFamily: "regular-font",
        fontSize: "18px",
        fill: "#ffffff",
        fontWeight: "bold",
      })
      .setOrigin(0.5);

    this.add
      .text(columnWidth * 4, startY, "Result", {
        fontFamily: "regular-font",
        fontSize: "18px",
        fill: "#ffffff",
        fontWeight: "bold",
      })
      .setOrigin(0.5);

    // Player rows
    let rowIndex = 1;

    // Determine the winner if there was no showdown
    const winner = this.determineWinner();

    // First, add the winner (if there was no showdown)
    if (winner && !this.gameState.showdownResult.includes(winner)) {
      this.addPlayerRow(
        winner,
        columnWidth,
        startY + ROW_HEIGHT * rowIndex,
        true
      );
      rowIndex++;
    }

    // Then, add players who showed their cards (in showdownResult order)
    this.gameState.showdownResult.forEach((playerId) => {
      this.addPlayerRow(playerId, columnWidth, startY + ROW_HEIGHT * rowIndex);
      rowIndex++;
    });

    // Finally, add players who folded (in distributionOfSeats order)
    this.gameState.distributionOfSeats.forEach((playerId) => {
      if (
        !this.gameState.showdownResult.includes(playerId) &&
        playerId !== winner
      ) {
        this.addPlayerRow(
          playerId,
          columnWidth,
          startY + ROW_HEIGHT * rowIndex
        );
        rowIndex++;
      }
    });
  }

  addPlayerRow(playerId, columnWidth, y, isWinnerByDefault = false) {
    const player = this.gameState.lobby.players.find((p) => p.id === playerId);
    const playerName = player.username;
    const isReady = this.gameState.playersReadyToContinue[playerId];
    const readyStatus = isReady ? " ✅" : "";
    const isBusted = this.gameState.bustedPlayers.includes(playerId);
    const bustedStatus = isBusted ? " 👋" : "";

    // Player name, ready status, and busted status
    this.add
      .text(columnWidth, y, `${playerName}${readyStatus}${bustedStatus}`, {
        fontFamily: "regular-font",
        fontSize: "18px",
        fill: "#ffffff",
      })
      .setOrigin(0.5);

    // Stack
    const stackValue = this.calculateStackValue(
      this.gameState.playersStacks[playerId]
    );
    const stackColor = stackValue === 0 ? "#ff0000" : "#ffffff";
    this.add.image(columnWidth * 2 - 20, y, "chipWhiteBlueFront").setScale(0.3);
    this.add
      .text(columnWidth * 2, y, stackValue.toString(), {
        fontFamily: "regular-font",
        fontSize: "18px",
        fill: stackColor,
      })
      .setOrigin(0, 0.5);

    // Hand
    if (isWinnerByDefault) {
      this.add
        .text(columnWidth * 3, y, "Winner (others folded)", {
          fontFamily: "regular-font",
          fontSize: "18px",
          fill: "#ffffff",
        })
        .setOrigin(0.5);
    } else if (
      this.gameState.playersActions[playerId] ===
      POKER_TEXAS_HOLDEM_ACTIONS.fold
    ) {
      this.add
        .text(columnWidth * 3, y, "Fold", {
          fontFamily: "regular-font",
          fontSize: "18px",
          fill: "#ffffff",
        })
        .setOrigin(0.5);
    } else {
      const bestHand = this.gameState.playersBestHands[playerId];
      if (bestHand && bestHand.length === 5) {
        bestHand.forEach((card, index) => {
          const cardSprite = this.add
            .image(
              columnWidth * 3 - 70 + index * 35,
              y,
              this.getCardTexture(card.card)
            )
            .setScale(0.2);
          this.setCardFrame(cardSprite, card.card);
        });
      }
    }

    // Result
    const handResult = this.gameState.playersHandResults[playerId];
    if (handResult && handResult.description) {
      this.add
        .text(columnWidth * 4, y, handResult.description, {
          fontFamily: "regular-font",
          fontSize: "18px",
          fill: "#ffffff",
        })
        .setOrigin(0.5);
    } else if (isWinnerByDefault) {
      this.add
        .text(columnWidth * 4, y, "Winner by default", {
          fontFamily: "regular-font",
          fontSize: "18px",
          fill: "#ffffff",
        })
        .setOrigin(0.5);
    }
  }

  determineWinner() {
    const activePlayers = this.gameState.distributionOfSeats.filter(
      (playerId) =>
        this.gameState.playersActions[playerId] !==
        POKER_TEXAS_HOLDEM_ACTIONS.fold
    );

    if (activePlayers.length === 1) {
      return activePlayers[0];
    }

    return null;
  }

  calculateStackValue(stack) {
    return Object.entries(stack).reduce((total, [color, chips]) => {
      return total + chips.length * POKER_TEXAS_HOLDEM_CHIP_VALUES[color];
    }, 0);
  }

  getCardTexture(card) {
    const [rank, _] = card.split("_");
    return rank === "joker" ? "jokers" : "playingCards";
  }

  setCardFrame(cardSprite, card) {
    const [rank, suit] = card.split("_");
    if (rank !== "joker") {
      const suitIndex = ["clubs", "hearts", "spades", "diamonds"].indexOf(suit);
      const rankIndex = [
        "ace",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "jack",
        "queen",
        "king",
      ].indexOf(rank);
      cardSprite.setFrame(suitIndex * 13 + rankIndex);
    } else {
      cardSprite.setFrame(suit === "red" ? 1 : 0);
    }
  }

  getPlayersReadyText() {
    const readyCount = Object.values(
      this.gameState.playersReadyToContinue
    ).filter((ready) => ready).length;
    const totalPlayers = this.gameState.distributionOfSeats.length;
    return `Players ready: ${readyCount}/${totalPlayers}`;
  }

  allPlayersReady() {
    return Object.values(this.gameState.playersReadyToContinue).every(
      (ready) => ready
    );
  }

  updateTimer(remainingTime) {
    this.summaryTimer = remainingTime;
    if (this.timerText) {
      this.timerText.setText(`Time remaining: ${this.summaryTimer}s`);
    }

    if (
      this.summaryTimer <= 0 &&
      !this.gameState.bustedPlayers.includes(this.playerId)
    ) {
      this.scene.stop();
    }
  }
}

export default HandSummaryScene;
