export function getPlayerLabelPosition(
  scene,
  index,
  playerIndex,
  totalPlayers
) {
  const centerX = scene.sys.game.config.width / 2;
  const centerY = scene.sys.game.config.height / 2;
  const ellipseWidth = scene.sys.game.config.width * 0.9; // Use 90% of screen width
  const ellipseHeight = scene.sys.game.config.height * 0.95; // Use 90% of screen height

  // Ratio between width and height of the ellipse
  const ratio = ellipseWidth / ellipseHeight;

  // Remap player indices from client player's perspective
  const relativeIndex = (index - playerIndex + totalPlayers) % totalPlayers;

  // Start from bottom (π/2) and go anticlockwise (-2π)
  const angle = Math.PI / 2 - (2 * Math.PI * relativeIndex) / totalPlayers;

  // Calculate positions in an elliptical arrangement
  const x = centerX + (ellipseWidth / 2) * Math.cos(angle);
  const y = centerY + (ellipseHeight / 2) * Math.sin(angle);

  // Calculate rotation. We need to adjust it according to the ellipse's curvature
  const rotationAngle = Math.atan2(Math.sin(angle) * ratio, Math.cos(angle));
  const rotation = rotationAngle - Math.PI / 2;

  return { x, y, rotation };
}

function createPlayerLabel(scene, player, index, playerIndex, totalPlayers) {
  const labelPosition = getPlayerLabelPosition(
    scene,
    index,
    playerIndex,
    totalPlayers
  );
  const { x, y, rotation } = labelPosition;
  const centerX = scene.sys.game.config.width / 2;
  const centerY = scene.sys.game.config.height / 2;

  let labelRotation = shouldFlipLabel(x, y, centerX, centerY)
    ? rotation + Math.PI
    : rotation;

  const playerLabel = scene.add
    .text(x, y, player.username, {
      font: "18px regular-font",
      fill: "#ffffff",
    })
    .setOrigin(0.5)
    .setRotation(labelRotation);

  playerLabel.playerName = player.username;

  return playerLabel;
}

export function updatePlayerLabels(scene, gameState, chatMessages) {
  const totalPlayers = gameState.distributionOfSeats.length;

  gameState.distributionOfSeats.forEach((playerId, index) => {
    const player = gameState.lobby.players.find((p) => p.id === playerId);
    const message = chatMessages[playerId];

    if (scene.playerLabels[playerId]) {
      const label = scene.playerLabels[playerId];
      const labelPosition = getPlayerLabelPosition(
        scene,
        index,
        scene.playerIndex,
        totalPlayers
      );
      const { x, y, rotation } = labelPosition;
      const centerX = scene.sys.game.config.width / 2;
      const centerY = scene.sys.game.config.height / 2;

      let labelRotation = shouldFlipLabel(x, y, centerX, centerY)
        ? rotation + Math.PI
        : rotation;

      label.setPosition(x, y);
      label.setRotation(labelRotation);

      if (message) {
        showChatMessageAsLabel(scene, label, message);
      } else {
        showPlayerLabel(label);
      }
    } else {
      // Create new label
      const newLabel = createPlayerLabel(
        scene,
        player,
        index,
        scene.playerIndex,
        totalPlayers
      );
      scene.playerLabels[playerId] = newLabel;

      if (message) {
        showChatMessageAsLabel(scene, newLabel, message);
      }
    }
  });

  // Remove labels for players who are no longer in the game
  Object.keys(scene.playerLabels).forEach((playerId) => {
    if (!gameState.distributionOfSeats.includes(playerId)) {
      scene.playerLabels[playerId].destroy();
      delete scene.playerLabels[playerId];
    }
  });
}

function showPlayerLabel(label) {
  label.setText(label.playerName);
  label.setColor("#ffffff");
  label.setStroke("#000000", 0);
}

function showChatMessageAsLabel(scene, label, message) {
  // Store the original player name on the label object
  label.playerName = label.playerName || label.text;

  // Set the chat message
  label.setText(message);
  label.setColor("#ffff00");
  label.setStroke("#000000", 4);

  // Add a scale animation
  scene.tweens.add({
    targets: label,
    scaleX: 1.2,
    scaleY: 1.2,
    duration: 200,
    yoyo: true,
    repeat: 0,
  });

  // Reset back to player label after a delay
  scene.time.delayedCall(3000, () => {
    scene.tweens.add({
      targets: label,
      alpha: 0,
      duration: 200,
      onComplete: () => {
        showPlayerLabel(label);
        scene.tweens.add({
          targets: label,
          alpha: 1,
          duration: 200,
        });
      },
    });
  });
}

function shouldFlipLabel(x, y, centerX, centerY) {
  // If the label is in the top half of the screen (y < centerY), flip it
  return y < centerY;
}
