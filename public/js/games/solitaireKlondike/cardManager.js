import * as AudioManager from "./audioManager.js";
import {
  SOLITAIRE_KLONDIKE_GAME_STATUS,
  SOLITAIRE_KLONDIKE_PILES,
} from "./solitaireKlondikeConstants.js";

export const CARD_WIDTH = 256;
export const CARD_HEIGHT = 356;
export const CARD_SCALE = 0.3;
const LINE_STYLE = {
  width: 2,
  color: 0xffffff,
  alpha: 0.5,
};
const PLACEHOLDER_RADIUS = 10;

export function getCardTexture(card) {
  const [rank, _] = card.split("_");
  return rank === "joker" ? "jokers" : "playingCards";
}

export function setCardFrame(cardSprite, card) {
  const [rank, suit] = card.split("_");
  if (rank !== "joker") {
    const suitIndex = ["clubs", "hearts", "spades", "diamonds"].indexOf(suit);
    const rankIndex = [
      "ace",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "jack",
      "queen",
      "king",
    ].indexOf(rank);
    cardSprite.setFrame(suitIndex * 13 + rankIndex);
  } else {
    cardSprite.setFrame(suit === "red" ? 1 : 0);
  }
}

export function createCardSprite(scene, card, isVisible) {
  const texture = getCardTexture(card);
  const cardSprite = scene.add
    .sprite(0, 0, texture, 0)
    .setOrigin(0.5)
    .setScale(CARD_SCALE)
    .setInteractive();

  if (isVisible) {
    setCardFrame(cardSprite, card);
  } else {
    cardSprite.setTexture("cardBacks");
    cardSprite.setFrame(0);
  }

  return cardSprite;
}

export function setupDraggableCard(scene, cardSprite, id, gameState, pileInfo) {
  scene.input.setDraggable(cardSprite);
  cardSprite.cardId = id;

  cardSprite.on("dragstart", (pointer) => {
    if (gameState.paused) return;

    let cardsToMove = [];
    if (pileInfo.type === SOLITAIRE_KLONDIKE_PILES.tableau) {
      const pile = gameState.tableau[pileInfo.index];
      const cardIndex = pile.findIndex(card => card.id === id);
      if (cardIndex !== -1) {
        cardsToMove = scene.tableau[pileInfo.index].getChildren().slice(cardIndex);
      }
    } else {
      cardsToMove = [cardSprite];
    }

    cardsToMove.forEach(sprite => {
      scene.cardOriginalDepths[sprite.cardId] = sprite.depth;
      sprite.originalX = sprite.x;
      sprite.originalY = sprite.y;
      sprite.setDepth(1000 + sprite.depth);
    });

    cardSprite.cardsToMove = cardsToMove;
    AudioManager.playRandomSound(scene.cardSlideSounds);
  });

  cardSprite.on("drag", (pointer) => {
    if (gameState.paused) return;

    const dx = pointer.x - cardSprite.originalX;
    const dy = pointer.y - cardSprite.originalY;

    cardSprite.cardsToMove.forEach((sprite) => {
      sprite.x = sprite.originalX + dx;
      sprite.y = sprite.originalY + dy;
    });
  });

  cardSprite.on("dragend", (pointer) => {
    if (gameState.paused) {
      returnCardsToOriginalPosition(cardSprite.cardsToMove, scene);
      return;
    }

    const targetPile = determineTargetPile(scene, pointer, gameState, id, pileInfo.type, pileInfo.index);

    if (targetPile) {
      let cardData;
      if (pileInfo.type === SOLITAIRE_KLONDIKE_PILES.tableau) {
        cardData = gameState.tableau[pileInfo.index].find(
          card => card.id === id
        );
      } else if (pileInfo.type === SOLITAIRE_KLONDIKE_PILES.foundation) {
        cardData = gameState.foundationPiles[pileInfo.index].find(
          card => card.id === id
        );
      } else if (pileInfo.type === SOLITAIRE_KLONDIKE_PILES.waste) {
        cardData = gameState.wastePile.find(card => card.id === id);
      }

      if (!cardData) {
        returnCardsToOriginalPosition(cardSprite.cardsToMove, scene);
        return;
      }

      const move = {
        card: cardData,
        originPile: pileInfo.type,
        originIndex: pileInfo.index,
        targetPile: targetPile.type,
        targetIndex: targetPile.index,
      };
      
      scene.socket.emit("makeAMove", move);
    } else {
      returnCardsToOriginalPosition(cardSprite.cardsToMove, scene);
    }

    AudioManager.playRandomSound(scene.cardSlideSounds);
  });
}

function determineTargetPile(scene, pointer, gameState, draggedCardId, originPile, originIndex) {
  const tableauPiles = scene.tableau;
  const foundationPiles = scene.foundationPiles;

  function isPointInRect(x, y, rect) {
    return x >= rect.x &&
      x <= rect.x + rect.width &&
      y >= rect.y &&
      y <= rect.y + rect.height;
  }

  // Check tableau piles
  for (let i = 0; i < tableauPiles.length; i++) {
    // Skip the origin pile
    if (originPile === SOLITAIRE_KLONDIKE_PILES.tableau && i === originIndex) {
      continue;
    }

    const pile = tableauPiles[i];
    const pileCards = pile.getChildren();

    // Check for empty tableau pile
    if (pileCards.length === 0) {
      const { x, y } = getTableauPilePosition(scene, i);
      const emptyPileRect = new Phaser.Geom.Rectangle(
        x - (CARD_WIDTH * CARD_SCALE) / 2,
        y - (CARD_HEIGHT * CARD_SCALE) / 2,
        CARD_WIDTH * CARD_SCALE,
        CARD_HEIGHT * CARD_SCALE
      );
      
      if (isPointInRect(pointer.x, pointer.y, emptyPileRect)) {
        return { type: SOLITAIRE_KLONDIKE_PILES.tableau, index: i };
      }
    } else {
      // Check last card of non-empty pile
      const lastCard = pileCards[pileCards.length - 1];
      if (isPointInRect(pointer.x, pointer.y, lastCard.getBounds())) {
        return { type: SOLITAIRE_KLONDIKE_PILES.tableau, index: i };
      }
    }
  }

  // Check foundation piles
  for (let i = 0; i < foundationPiles.length; i++) {
    // Skip the origin pile
    if (originPile === SOLITAIRE_KLONDIKE_PILES.foundation && i === originIndex) {
      continue;
    }

    const pile = foundationPiles[i];
    const pileCards = pile.getChildren();

    // Check for empty foundation pile
    if (pileCards.length === 0) {
      const { x, y } = getFoundationPilePosition(scene, i);
      const emptyPileRect = new Phaser.Geom.Rectangle(
        x - (CARD_WIDTH * CARD_SCALE) / 2,
        y - (CARD_HEIGHT * CARD_SCALE) / 2,
        CARD_WIDTH * CARD_SCALE,
        CARD_HEIGHT * CARD_SCALE
      );
      
      if (isPointInRect(pointer.x, pointer.y, emptyPileRect)) {
        return { type: SOLITAIRE_KLONDIKE_PILES.foundation, index: i };
      }
    } else {
      // Check last card of non-empty pile
      const lastCard = pileCards[pileCards.length - 1];
      if (isPointInRect(pointer.x, pointer.y, lastCard.getBounds())) {
        return { type: SOLITAIRE_KLONDIKE_PILES.foundation, index: i };
      }
    }
  }

  return null;
}

function returnCardsToOriginalPosition(cardSprites, scene) {
  cardSprites.forEach(sprite => {
    sprite.x = sprite.originalX;
    sprite.y = sprite.originalY;
    sprite.setDepth(scene.cardOriginalDepths[sprite.cardId]);
  });
}

export function getStockPilePosition(scene) {
  let x = scene.sys.game.config.width / 2 - 290;
  let y = scene.sys.game.config.height / 2 - 280;
  let rotation = 0;

  return { x, y, rotation };
}

export function getTableauPilePosition(scene, pileIndex) {
  const { x: stockX, y: stockY } = getStockPilePosition(scene);
  const offsetX = CARD_WIDTH * CARD_SCALE + 20; // Add spacing between piles
  const offsetY = CARD_HEIGHT * CARD_SCALE + 40; // Vertical offset from stock pile to tableau

  const pileX = stockX + pileIndex * offsetX;
  const pileY = stockY + offsetY;

  return { x: pileX, y: pileY };
}

export function getFoundationPilePosition(scene, pileIndex) {
  const { x: stockX, y: stockY } = getStockPilePosition(scene);
  const offsetX = CARD_WIDTH * CARD_SCALE + 20;
  const foundationX = stockX + (pileIndex + 3) * offsetX;
  const foundationY = stockY;

  return { x: foundationX, y: foundationY };
}

export function getWastePilePosition(scene) {
  const { x: stockX, y: stockY } = getStockPilePosition(scene);
  const wasteX = stockX + CARD_WIDTH * CARD_SCALE + 20;
  const wasteY = stockY;

  return { x: wasteX, y: wasteY };
}

export function updateTableau(scene, gameState) {
  if (!scene.tableau) {
    scene.tableau = [];
  }

  gameState.tableau.forEach((pile, pileIndex) => {
    // Initialize or clear existing pile group
    if (!scene.tableau[pileIndex]) {
      scene.tableau[pileIndex] = scene.add.group();
    } else {
      scene.tableau[pileIndex].clear(true, true);
    }

    // Clear existing placeholder if it exists
    if (scene.tableauPlaceholders && scene.tableauPlaceholders[pileIndex]) {
      scene.tableauPlaceholders[pileIndex].destroy();
    }

    const { x, y } = getTableauPilePosition(scene, pileIndex);

    // Create placeholder if pile is empty
    if (pile.length === 0) {
      if (!scene.tableauPlaceholders) {
        scene.tableauPlaceholders = [];
      }
      scene.tableauPlaceholders[pileIndex] = createEmptyPilePlaceholder(
        scene,
        x,
        y
      );
      return;
    }

    let currentY = y;
    const scaledCardHeight = CARD_HEIGHT * CARD_SCALE;

    pile.forEach((card, cardIndex) => {
      // Skip the moving card to avoid showing it twice
      if (gameState.movingCard && gameState.movingCard.card.id === card.id) {
        return;
      }

      // Determine the vertical offset based on the previous card's state
      if (cardIndex > 0) {
        const previousCard = pile[cardIndex - 1];
        currentY += scaledCardHeight * (previousCard.isFaceUp ? 0.2 : 0.1);
      }

      const cardSprite = createCardSprite(scene, card.card, card.isFaceUp);
      cardSprite.setPosition(x, currentY);
      cardSprite.setDepth(cardIndex);

      if (card.isFaceUp) {
        setupDraggableCard(scene, cardSprite, card.id, gameState, {
          type: SOLITAIRE_KLONDIKE_PILES.tableau,
          index: pileIndex,
        });
      }

      scene.tableau[pileIndex].add(cardSprite);
    });
  });
}

export function updateFoundationPiles(scene, gameState) {
  if (!scene.foundationPiles) {
    scene.foundationPiles = [];
  }

  gameState.foundationPiles.forEach((pile, pileIndex) => {
    // Initialize or clear existing pile group
    if (!scene.foundationPiles[pileIndex]) {
      scene.foundationPiles[pileIndex] = scene.add.group();
    } else {
      scene.foundationPiles[pileIndex].clear(true, true);
    }

    // Clear existing placeholder if it exists
    if (
      scene.foundationPlaceholders &&
      scene.foundationPlaceholders[pileIndex]
    ) {
      scene.foundationPlaceholders[pileIndex].destroy();
    }

    const { x, y } = getFoundationPilePosition(scene, pileIndex);

    // Create placeholder if pile is empty
    if (pile.length === 0) {
      if (!scene.foundationPlaceholders) {
        scene.foundationPlaceholders = [];
      }
      scene.foundationPlaceholders[pileIndex] = createEmptyPilePlaceholder(
        scene,
        x,
        y
      );
      return;
    }

    pile.forEach((card, cardIndex) => {
      const cardSprite = createCardSprite(scene, card.card, true);
      cardSprite.setPosition(x, y);
      cardSprite.setDepth(cardIndex);

      setupDraggableCard(scene, cardSprite, card.id, gameState, {
        type: SOLITAIRE_KLONDIKE_PILES.foundation,
        index: pileIndex,
      });

      scene.foundationPiles[pileIndex].add(cardSprite);
    });
  });
}

export function updateStockPile(scene, gameState) {
  // Clear existing graphics
  if (scene.stockPile) {
    scene.stockPile.clear(true, true);
  }
  if (scene.stockPlaceholder) {
    scene.stockPlaceholder.destroy();
  }

  scene.stockPile = scene.add.group();
  const { x, y, rotation } = getStockPilePosition(scene);

  // Create placeholder if stock is empty
  if (gameState.stockPile.length === 0) {
    scene.stockPlaceholder = createEmptyPilePlaceholder(scene, x, y);

    scene.stockPlaceholder.on("pointerdown", () => {
      const canDrawCard =
        !gameState.paused &&
        gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.player_turn &&
        gameState.wastePile.length > 0;

      if (canDrawCard) {
        scene.socket.emit("drawCard");
        AudioManager.playRandomSound(scene.cardPlaceSounds);
      }
    });
    return;
  }

  // Handle non-empty stock pile
  gameState.stockPile.forEach((card, index) => {
    const cardSprite = scene.add
      .sprite(x, y, "cardBacks", 0)
      .setOrigin(0.5)
      .setScale(CARD_SCALE)
      .setRotation(rotation)
      .setDepth(index)
      .setInteractive();

    cardSprite.on("pointerdown", () => {
      if (gameState.paused) return;

      if (
        gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.ready_to_deal
      ) {
        scene.socket.emit("dealCards");
        AudioManager.playRandomSound(scene.cardSlideSounds);
      } else if (
        gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.player_turn
      ) {
        scene.socket.emit("drawCard");
        AudioManager.playRandomSound(scene.cardSlideSounds);
      }
    });

    scene.stockPile.add(cardSprite);
  });
}

export function updateWastePile(scene, gameState) {
  // Initialize or clear existing pile group and placeholder
  if (!scene.wastePile) {
    scene.wastePile = scene.add.group();
  } else {
    scene.wastePile.clear(true, true);
  }
  if (scene.wastePlaceholder) {
    scene.wastePlaceholder.destroy();
  }

  const { x, y } = getWastePilePosition(scene);

  // Create placeholder if pile is empty
  if (gameState.wastePile.length === 0) {
    scene.wastePlaceholder = createEmptyPilePlaceholder(scene, x, y);
    return;
  }

  gameState.wastePile.forEach((card, index) => {
    // Skip the moving card to avoid showing it twice
    if (gameState.movingCard && gameState.movingCard.card.id === card.id) {
      return;
    }

    const cardSprite = createCardSprite(scene, card.card, true);
    cardSprite.setPosition(x, y);
    cardSprite.setDepth(index);

    setupDraggableCard(scene, cardSprite, card.id, gameState, {
      type: SOLITAIRE_KLONDIKE_PILES.waste,
      index: 0,
    });

    scene.wastePile.add(cardSprite);
  });
}

function createEmptyPilePlaceholder(scene, x, y) {
  const graphics = scene.add.graphics();
  const width = CARD_WIDTH * CARD_SCALE;
  const height = CARD_HEIGHT * CARD_SCALE;

  graphics.lineStyle(LINE_STYLE.width, LINE_STYLE.color, LINE_STYLE.alpha);
  graphics.strokeRoundedRect(
    x - width / 2,
    y - height / 2,
    width,
    height,
    PLACEHOLDER_RADIUS
  );

  const hitArea = new Phaser.Geom.Rectangle(
    x - width / 2,
    y - height / 2,
    width,
    height
  );

  graphics.setInteractive(hitArea, Phaser.Geom.Rectangle.Contains);

  return graphics;
}
