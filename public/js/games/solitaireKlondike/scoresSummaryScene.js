import { SOLITAIRE_KLONDIKE_GAME_STATUS } from "./solitaireKlondikeConstants.js";
import { createButton } from "./uiElements.js";

class ScoresSummaryScene extends Phaser.Scene {
  constructor() {
    super({ key: "ScoresSummaryScene" });
  }

  init(data) {
    this.socket = data.socket;
    this.gameState = data.gameState;
    this.gameStats = data.gameStats;
    this.playerId = this.socket.id;
  }

  create() {
    this.add
      .rectangle(
        0,
        0,
        this.sys.game.config.width,
        this.sys.game.config.height,
        0x000000,
        0.9
      )
      .setOrigin(0);

    const centerX = this.sys.game.config.width / 2;
    const centerY = this.sys.game.config.height / 2;

    if (
      this.gameState.gameStatus ===
      SOLITAIRE_KLONDIKE_GAME_STATUS.end_of_the_game
    ) {
      const winner = this.gameState.lobby.players.find(
        (p) => p.id === this.gameState.winnerPlayer
      );
      const username = winner ? winner.username : "Player";

      // Congratulatory header
      this.add
        .text(centerX, centerY - 100, `Congratulations, ${username}!`, {
          fontFamily: "bold-font",
          fontSize: "36px",
          fill: "#ffffff",
        })
        .setOrigin(0.5);

      // Number of moves label
      this.add
        .text(
          centerX,
          centerY,
          `You completed the game in ${this.gameState.numberOfMoves} moves`,
          {
            fontFamily: "regular-font",
            fontSize: "24px",
            fill: "#ffffff",
          }
        )
        .setOrigin(0.5);

      // New record label (if applicable)
      const isNewRecord =
        !this.gameStats ||
        (this.gameStats.keyStats.min &&
          this.gameState.numberOfMoves < this.gameStats.keyStats.min);

      if (isNewRecord) {
        this.add
          .text(centerX, centerY + 50, "NEW RECORD!", {
            fontFamily: "bold-font",
            fontSize: "32px",
            fill: "#ffff00",
          })
          .setOrigin(0.5);
      }

      // Add "Play Again" button
      this.playAgainButton = createButton(
        this,
        centerX - 100,
        this.sys.game.config.height - 80,
        "Play Again",
        true,
        true,
        () => {
          window.location.reload();
        }
      );

      // Add "Main Menu" button
      this.mainMenuButton = createButton(
        this,
        centerX + 100,
        this.sys.game.config.height - 80,
        "Main Menu",
        true,
        true,
        () => {
          window.location.href = "/main";
        }
      );
    }

    this.sound.play("summary");
  }
}

export default ScoresSummaryScene;
