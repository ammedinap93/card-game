import * as CardManager from "./cardManager.js";
import * as AudioManager from "./audioManager.js";
import * as UIElements from "./uiElements.js";
import * as AnimationManager from "./animationManager.js";
import { SOLITAIRE_KLONDIKE_GAME_STATUS } from "./solitaireKlondikeConstants.js";
import ScoresSummaryScene from "./scoresSummaryScene.js";

class SolitaireKlondikeScene extends Phaser.Scene {
  constructor() {
    super({ key: "SolitaireKlondikeScene" });
  }

  preload() {}

  create() {
    this.scene.add("ScoresSummaryScene", ScoresSummaryScene, false);

    UIElements.createBackgroundImage(this);

    this.statusLabel = UIElements.createStatusLabel(this, "Loading...");
    this.pauseOverlay = UIElements.createPauseOverlay(this);
    this.errorLabel = UIElements.createErrorLabel(this);

    this.mainMenuButton = UIElements.createMainMenuButton(this);
    this.newGameButton = UIElements.createNewGameButton(this);
    this.showStatsButton = UIElements.createShowStatsButton(this);
    this.movesCounter = UIElements.createMovesCounter(this);

    this.cardOriginalDepths = {};
    this.gameState = {};
    this.gameStats = null;

    // Initialize placeholder properties
    this.stockPlaceholder = null;
    this.wastePlaceholder = null;
    this.foundationPlaceholders = [];
    this.tableauPlaceholders = [];

    const { cardPlaceSounds, cardSlideSounds } =
      AudioManager.createSoundEffects(this);
    this.cardPlaceSounds = cardPlaceSounds;
    this.cardSlideSounds = cardSlideSounds;

    // Track previous game state
    this.previousGameState = {};

    this.socket = this.scene.settings.data.socket;
    this.socket.on("gameState", (gameState) => {
      console.log("Game state received:", gameState);
      this.updateGameState(gameState);
    });
    this.socket.on("gameStats", (gameStats) => {
      console.log("Game stats received:", gameStats);
      this.gameStats = gameStats;
    });
  }

  updateGameState(gameState) {
    this.hideStatsOverlay();

    // Store current game state as previous for next comparison
    this.previousGameState = JSON.parse(JSON.stringify(gameState));

    this.gameState = gameState;
    UIElements.updateStatusLabel(this, gameState);
    CardManager.updateStockPile(this, gameState);
    CardManager.updateTableau(this, gameState);
    CardManager.updateFoundationPiles(this, gameState);
    CardManager.updateWastePile(this, gameState);
    AnimationManager.animateDealingCard(this, gameState);
    AnimationManager.animateDrawingCard(this, gameState);
    UIElements.updatePauseOverlay(this, gameState);

    this.updateMovesCounter();

    if (
      gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.end_of_the_game
    ) {
      this.time.delayedCall(1000, () => {
        this.scene.launch("ScoresSummaryScene", {
          socket: this.socket,
          gameState: this.gameState,
          gameStats: this.gameStats,
        });
      });
    }
  }

  showErrorMessage(message) {
    this.errorLabel.setText(message);
    this.time.delayedCall(5000, () => {
      this.errorLabel.setText("");
    });
  }

  updateMovesCounter() {
    if (this.gameState && this.movesCounter) {
      this.movesCounter.setText(`Moves: ${this.gameState.numberOfMoves}`);
    }
  }

  showStatsOverlay() {
    if (this.statsOverlay) {
      this.statsOverlay.forEach((element) => element.destroy());
    }
    this.statsOverlay = UIElements.createStatsOverlay(this);
  }

  hideStatsOverlay() {
    if (this.statsOverlay) {
      this.statsOverlay.forEach((element) => element.destroy());
      this.statsOverlay = null;
    }
  }
}

export default SolitaireKlondikeScene;
