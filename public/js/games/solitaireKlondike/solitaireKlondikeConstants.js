export const SOLITAIRE_KLONDIKE_GAME_STATUS = {
  ready_to_deal: "ready_to_deal",
  dealing_cards: "dealing_cards",
  player_turn: "player_turn",
  drawing_card: "drawing_card",
  end_of_the_game: "end_of_the_game",
};

export const SOLITAIRE_KLONDIKE_PILES = {
  tableau: "tableau",
  foundation: "foundation",
  stock: "stock",
  waste: "waste",
};

export const SOLITAIRE_KLONDIKE_CARD_RANKS = [
  "ace",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "jack",
  "queen",
  "king",
];
