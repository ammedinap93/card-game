export function createBackgroundImage(scene) {
  const backgroundImage = scene.add.image(0, 0, "background").setOrigin(0);
  backgroundImage.setDisplaySize(
    scene.sys.game.config.width,
    scene.sys.game.config.height
  );
  return backgroundImage;
}

export function createButton(scene, x, y, text, visible, centered, callback) {
  const button = scene.add.text(x, y, text, {
    font: "18px bold-font",
    fill: "#ffff00",
  });
  if (centered) {
    button.setOrigin(0.5);
  } else {
    button.setOrigin(0, 0.5);
  }
  button
    .setInteractive({ useHandCursor: true })
    .setVisible(visible)
    .on("pointerdown", callback)
    .on("pointerover", () => button.setStyle({ fill: "#ffffff" }))
    .on("pointerout", () => button.setStyle({ fill: "#ffff00" }));

  return button;
}

export function createStatusLabel(scene, message) {
  return scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2 + 68,
      message,
      {
        font: "18px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
}

export function createErrorLabel(scene) {
  return scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height - 160,
      "",
      { font: "18px regular-font", fill: "#ff0000" }
    )
    .setOrigin(0.5);
}

export function createPauseOverlay(scene) {
  const pauseOverlay = scene.add
    .rectangle(
      0,
      0,
      scene.sys.game.config.width,
      scene.sys.game.config.height,
      0x000000,
      0.5
    )
    .setOrigin(0);
  pauseOverlay.setDepth(15000);
  pauseOverlay.setVisible(false);

  const pauseText = scene.add
    .text(
      scene.sys.game.config.width / 2,
      scene.sys.game.config.height / 2,
      "Game Paused",
      {
        font: "32px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(0.5);
  pauseText.setDepth(15010);
  pauseText.setVisible(false);

  return { pauseOverlay, pauseText };
}

export function updatePauseOverlay(scene, gameState) {
  const { pauseOverlay, pauseText } = scene.pauseOverlay;
  pauseOverlay.setVisible(gameState.paused);
  pauseText.setVisible(gameState.paused);
}

export function updateStatusLabel(scene, gameState) {
  let turnMessage = "";
  scene.statusLabel.setText(turnMessage);
}

export function createNewGameButton(scene) {
  return createButton(
    scene,
    20,
    scene.sys.game.config.height - 20,
    "New Game",
    true,
    false,
    () => {
      scene.socket.emit("newGame");
    }
  );
}

export function createMainMenuButton(scene) {
  return createButton(
    scene,
    20,
    scene.sys.game.config.height - 50,
    "Main Menu",
    true,
    false,
    () => {
      window.location.href = "/main";
    }
  );
}

export function createShowStatsButton(scene) {
  return createButton(
    scene,
    scene.sys.game.config.width - 20,
    scene.sys.game.config.height - 20,
    "Show Stats",
    true,
    false,
    () => {
      scene.showStatsOverlay();
    }
  ).setOrigin(1, 0.5);
}

export function createMovesCounter(scene) {
  return scene.add
    .text(
      scene.sys.game.config.width - 20,
      scene.sys.game.config.height - 50,
      "Moves: 0",
      {
        font: "18px regular-font",
        fill: "#ffffff",
      }
    )
    .setOrigin(1, 0.5);
}

export function createStatsOverlay(scene) {
  const overlay = scene.add
    .rectangle(
      0,
      0,
      scene.sys.game.config.width,
      scene.sys.game.config.height,
      0x000000,
      0.7
    )
    .setOrigin(0)
    .setDepth(20000)
    .setInteractive()
    .on("pointerdown", () => scene.hideStatsOverlay());

  const centerX = scene.sys.game.config.width / 2;
  const startY = scene.sys.game.config.height / 2 - 100;
  const lineSpacing = 40;

  const header = scene.add
    .text(centerX, startY, "Game Stats", {
      font: "32px bold-font",
      fill: "#ffffff",
    })
    .setOrigin(0.5)
    .setDepth(20001);

  const stats = [
    `Wins: ${scene.gameStats ? Object.values(scene.gameStats.wins)[0] : 0}`,
    `Max Moves: ${scene.gameStats ? scene.gameStats.keyStats.max : 0}`,
    `Min Moves: ${scene.gameStats ? scene.gameStats.keyStats.min : 0}`,
    `Avg Moves: ${scene.gameStats ? scene.gameStats.keyStats.avg : 0}`,
  ];

  const statTexts = stats.map((stat, index) => {
    return scene.add
      .text(centerX, startY + (index + 1) * lineSpacing, stat, {
        font: "24px regular-font",
        fill: "#ffffff",
      })
      .setOrigin(0.5)
      .setDepth(20001);
  });

  return [overlay, header, ...statTexts];
}
