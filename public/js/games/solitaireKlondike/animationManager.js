import * as CardManager from "./cardManager.js";
import * as AudioManager from "./audioManager.js";
import {
  SOLITAIRE_KLONDIKE_GAME_STATUS,
  SOLITAIRE_KLONDIKE_PILES,
} from "./solitaireKlondikeConstants.js";
import { getCopyOfAnObject } from "../../helpers.js";

export function animateDealingCard(scene, gameState) {
  if (
    gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.dealing_cards &&
    gameState.movingCard
  ) {
    const { card, targetPile, targetIndex } = gameState.movingCard;
    const { x: startX, y: startY } = CardManager.getStockPilePosition(scene);

    let endX, endY;
    if (targetPile === SOLITAIRE_KLONDIKE_PILES.tableau) {
      const { x, y } = CardManager.getTableauPilePosition(scene, targetIndex);
      endX = x;

      // Calculate endY based on the new positioning logic
      const scaledCardHeight = CardManager.CARD_HEIGHT * CardManager.CARD_SCALE;
      endY = y; // Start with the base y position

      const targetPileCards = gameState.tableau[targetIndex];
      for (let i = 0; i < targetPileCards.length; i++) {
        if (i === 0) continue; // Skip the first card as it doesn't add offset

        const previousCard = targetPileCards[i - 1];
        if (previousCard.isFaceUp) {
          endY += scaledCardHeight * 0.2; // 20% for face-up previous card
        } else {
          endY += scaledCardHeight * 0.1; // 10% for face-down previous card
        }
      }
    }

    const cardSprite = CardManager.createCardSprite(
      scene,
      card.card,
      card.isFaceUp
    );
    cardSprite.setPosition(startX, startY);
    cardSprite.setDepth(1000); // Ensure it's above other cards during animation

    scene.tweens.add({
      targets: cardSprite,
      x: endX,
      y: endY,
      duration: 500,
      ease: "Power2",
      onComplete: () => {
        cardSprite.destroy();
        AudioManager.playRandomSound(scene.cardPlaceSounds);
        let gameStateCopy = getCopyOfAnObject(gameState);
        gameStateCopy.movingCard = null;
        CardManager.updateTableau(scene, gameStateCopy);
      },
    });
  }
}

export function animateDrawingCard(scene, gameState) {
  if (
    gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.drawing_card &&
    gameState.movingCard
  ) {
    const { card } = gameState.movingCard;
    const { x: startX, y: startY } = CardManager.getStockPilePosition(scene);
    const { x: endX, y: endY } = CardManager.getWastePilePosition(scene);

    const cardSprite = CardManager.createCardSprite(
      scene,
      card.card,
      card.isFaceUp
    );
    cardSprite.setPosition(startX, startY);
    cardSprite.setDepth(1000); // Ensure it's above other cards during animation

    scene.tweens.add({
      targets: cardSprite,
      x: endX,
      y: endY,
      duration: 500,
      ease: "Power2",
      onComplete: () => {
        cardSprite.destroy();
        AudioManager.playRandomSound(scene.cardPlaceSounds);
        let gameStateCopy = getCopyOfAnObject(gameState);
        gameStateCopy.movingCard = null;
        CardManager.updateWastePile(scene, gameStateCopy);
      },
    });
  }
}
