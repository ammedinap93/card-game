const emojis = ["😊", "😂", "😎", "🤔", "😍", "😭", "🙄", "😡", "🤞", "👍"];
const MAX_MESSAGE_LENGTH = 100;

export function createChatUI(scene, socket, x, y, alignment = "center") {
  const emojiButton = scene.add
    .text(x - 35, y, "😊", { font: "24px bold-font" })
    .setOrigin(0, 0.5)
    .setInteractive({ useHandCursor: true })
    .on("pointerdown", toggleEmojiSelector);

  const inputField = scene.add.dom(
    x,
    y,
    "input",
    "width: 200px; padding: 1px; font-size: 18px; font-family: regular-font;"
  );  

  const sendButton = scene.add.text(x, y + 20, "Send", {
    font: "18px bold-font",
    fill: "#ffff00",
  });

  sendButton
    .setInteractive({ useHandCursor: true })
    .on("pointerdown", sendMessage)
    .on("pointerover", () => sendButton.setStyle({ fill: "#ffffff" }))
    .on("pointerout", () => sendButton.setStyle({ fill: "#ffff00" }));

  const emojiSelector = scene.add.container(x, y - 55).setVisible(false).setDepth(10000);

  // Add emojis to the selector
  emojis.forEach((emoji, index) => {
    const emojiText = scene.add
      .text(((index % 5) - 2) * 35, Math.floor(index / 5) * 35 - 15, emoji, {
        font: "24px bold-font",
      })
      .setOrigin(0.5)
      .setInteractive({ useHandCursor: true })
      .on("pointerdown", () => addEmojiToMessage(emoji));
    emojiSelector.add(emojiText);
  });

  function toggleEmojiSelector() {
    emojiSelector.setVisible(!emojiSelector.visible);
  }

  function addEmojiToMessage(emoji) {
    const currentText = inputField.node.value;
    if (currentText.length < MAX_MESSAGE_LENGTH) {
      inputField.node.value = currentText + emoji;
    }
  }

  function sendMessage() {
    const message = inputField.node.value;
    if (message.trim() !== "") {
      socket.emit("chatMessage", message);
      inputField.node.value = "";
    }
  }

  // Add keypress event listener for Enter key
  scene.input.keyboard.on("keydown-ENTER", sendMessage);

  // Add input event listener to limit message length
  inputField.node.addEventListener("input", function () {
    if (this.value.length > MAX_MESSAGE_LENGTH) {
      this.value = this.value.slice(0, MAX_MESSAGE_LENGTH);
    }
  });

  // Adjust alignment
  if (alignment === "right") {
    inputField.x = x - inputField.width / 2;
    sendButton.x = x - sendButton.width;
    emojiButton.x = x - inputField.width - 35;
    emojiSelector.x = x - inputField.width / 2;
  } else if (alignment === "left") {
    inputField.x = x + inputField.width / 2;
    emojiButton.x = x - 35;
    emojiSelector.x = x + inputField.width / 2;
  } else if (alignment === "center") {
    sendButton.x = x - sendButton.width / 2;
    emojiButton.x = x - inputField.width / 2 - 35;
  }

  return { inputField, sendButton, emojiButton, emojiSelector };
}
