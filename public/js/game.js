import {
  CONTINENTAL_ID,
  FIFTY_ONE_ID,
  POKER_TEXAS_HOLDEM_ID,
  SOLITAIRE_KLONDIKE_ID,
} from "./constants.js";
import { verifyUser } from "./verifyUser.js";
import ContinentalScene from "./games/continental/continentalScene.js";
import InitialDroppingCardsScene from "./games/continental/initialDroppingCardsScene.js";
import FiftyOneScene from "./games/fiftyOne/fiftyOneScene.js";
import PokerTexasHoldemScene from "./games/pokerTexasHoldem/pokerTexasHoldemScene.js";
import SolitaireKlondikeScene from "./games/solitaireKlondike/solitaireKlondikeScene.js";

verifyUser();

// Retrieve the username from local storage or set it to "guest" if not found
const username = localStorage.getItem("username") || "guest";

// Retrieve the userId from local storage or set it to null if not found
const userId = localStorage.getItem("userId") || null;

// Include the username in the game options
const gameOptions = {
  ...JSON.parse(localStorage.getItem("gameOptions")),
  username: username,
  userId: userId,
};

var config = {
  type: Phaser.AUTO,
  width: 1024,
  height: 768,
  parent: "game-container",
  dom: {
    createContainer: true,
  },
  scene: [
    {
      preload: preload,
      create: create,
      key: "GameScene",
    },
    ContinentalScene,
    InitialDroppingCardsScene,
    FiftyOneScene,
    PokerTexasHoldemScene,
    SolitaireKlondikeScene,
  ],
};

var game = new Phaser.Game(config);

function preload() {
  this.load.image("background", "/assets/backgrounds/background_1.png");
  this.load.spritesheet("cardBacks", "/assets/cards/1/CardBacks.png", {
    frameWidth: 256,
    frameHeight: 356,
  });
  this.load.spritesheet("jokers", "/assets/cards/1/Jokers.png", {
    frameWidth: 256,
    frameHeight: 356,
  });
  this.load.spritesheet("playingCards", "/assets/cards/1/PlayingCards.png", {
    frameWidth: 256,
    frameHeight: 356,
  });

  // Load chip sprites
  this.load.image(
    "chipBlackWhiteFront",
    "/assets/chips/chipBlackWhite_border.png"
  );
  this.load.image(
    "chipBlueWhiteFront",
    "/assets/chips/chipBlueWhite_border.png"
  );
  this.load.image(
    "chipGreenWhiteFront",
    "/assets/chips/chipGreenWhite_border.png"
  );
  this.load.image("chipRedWhiteFront", "/assets/chips/chipRedWhite_border.png");
  this.load.image(
    "chipWhiteBlueFront",
    "/assets/chips/chipWhiteBlue_border.png"
  );
  this.load.image("chipBlackWhite", "/assets/chips/chipBlackWhite_side.png");
  this.load.image("chipBlueWhite", "/assets/chips/chipBlueWhite_side.png");
  this.load.image("chipGreenWhite", "/assets/chips/chipGreenWhite_side.png");
  this.load.image("chipRedWhite", "/assets/chips/chipRedWhite_side.png");
  this.load.image("chipWhiteBlue", "/assets/chips/chipWhiteBlue_side.png");
  this.load.image(
    "chipBlackWhiteBorder",
    "/assets/chips/chipBlackWhite_sideBorder.png"
  );
  this.load.image(
    "chipBlueWhiteBorder",
    "/assets/chips/chipBlueWhite_sideBorder.png"
  );
  this.load.image(
    "chipGreenWhiteBorder",
    "/assets/chips/chipGreenWhite_sideBorder.png"
  );
  this.load.image(
    "chipRedWhiteBorder",
    "/assets/chips/chipRedWhite_sideBorder.png"
  );
  this.load.image(
    "chipWhiteBlueBorder",
    "/assets/chips/chipWhiteBlue_sideBorder.png"
  );

  this.load.image("dealerButton", "/assets/buttons/dealer.png");

  // Preload the sound effects
  this.load.audio("cardPlace1", "/assets/sounds/cardPlace1.mp3");
  this.load.audio("cardPlace2", "/assets/sounds/cardPlace2.mp3");
  this.load.audio("cardPlace3", "/assets/sounds/cardPlace3.mp3");
  this.load.audio("cardSlide1", "/assets/sounds/cardSlide1.mp3");
  this.load.audio("cardSlide2", "/assets/sounds/cardSlide2.mp3");
  this.load.audio("cardSlide3", "/assets/sounds/cardSlide3.mp3");
  this.load.audio("summary", "/assets/sounds/summary.mp3");
  this.load.audio("gameOver", "/assets/sounds/gameOver.mp3");
  this.load.audio("chipsCollide1", "/assets/sounds/chipsCollide1.mp3");
  this.load.audio("chipsCollide2", "/assets/sounds/chipsCollide2.mp3");
  this.load.audio("chipsCollide3", "/assets/sounds/chipsCollide3.mp3");
  this.load.audio("newMessage", "/assets/sounds/newMessage.mp3");
  this.load.audio("newTurn", "/assets/sounds/newTurn.mp3");
  this.load.audio("texasHoldem", "/assets/sounds/dealer/texas hold em.mp3");
  this.load.audio("fold", "/assets/sounds/dealer/fold.mp3");
  this.load.audio("check", "/assets/sounds/dealer/check.mp3");
  this.load.audio("bet", "/assets/sounds/dealer/bet.mp3");
  this.load.audio("call", "/assets/sounds/dealer/call.mp3");
  this.load.audio("raise", "/assets/sounds/dealer/raise.mp3");
  this.load.audio("smallBlind", "/assets/sounds/dealer/small blind.mp3");
  this.load.audio("bigBlind", "/assets/sounds/dealer/big blind.mp3");
  this.load.audio("allIn", "/assets/sounds/dealer/all in.mp3");
  this.load.audio("flop", "/assets/sounds/dealer/flop.mp3");
  this.load.audio("turn", "/assets/sounds/dealer/turn.mp3");
  this.load.audio("river", "/assets/sounds/dealer/river.mp3");
  this.load.audio("showdown", "/assets/sounds/dealer/showdown.mp3");
}

function create() {
  // Add the background image
  const backgroundImage = this.add.image(0, 0, "background").setOrigin(0);

  // Set the display size of the background image to match the scene size
  backgroundImage.setDisplaySize(
    this.sys.game.config.width,
    this.sys.game.config.height
  );

  this.socket = io();

  // Emit the game options to the server
  this.socket.emit("gameOptions", gameOptions);

  this.playerLabels = {}; // Object to store player labels

  // Create a label for the game name, lobby code, and player count
  this.lobbyInfoLabel = this.add.text(20, 20, "", {
    font: "24px regular-font",
    fill: "#ffffff",
  });

  // Receive the lobby data from the server
  this.socket.on("lobbyData", (lobby) => {
    console.log("Lobby data received:", lobby);
    // Update the game state or UI with the received lobby data

    if (!this.scene.isActive()) {
      return;
    }

    // Update the lobby info label
    this.lobbyInfoLabel.setText(
      `${lobby.game.name} - Lobby Code: ${lobby.code} - (${lobby.players.length}/${lobby.targetNumPlayers})`
    );

    // Clear existing player labels
    Object.values(this.playerLabels).forEach((label) => label.destroy());
    this.playerLabels = {};

    // Create labels for each player
    lobby.players.forEach((player, index) => {
      var playerLabel = this.add.text(20, 60 + index * 25, player.username, {
        font: "18px regular-font",
        fill: "#ffffff",
      });
      this.playerLabels[player.id] = playerLabel;
    });

    if (lobby.players.length === lobby.targetNumPlayers) {
      if (lobby.game.id === CONTINENTAL_ID) {
        this.scene.start("ContinentalScene", { socket: this.socket });
      } else if (lobby.game.id === FIFTY_ONE_ID) {
        this.scene.start("FiftyOneScene", { socket: this.socket });
      } else if (lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        this.scene.start("PokerTexasHoldemScene", { socket: this.socket });
      } else if (lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
        this.scene.start("SolitaireKlondikeScene", { socket: this.socket });
      }
    }
  });

  // Listen for the lobbyError event and show the error message
  this.socket.on("lobbyError", (errorMessage) => {
    const errorLabel = this.add
      .text(
        this.sys.game.config.width / 2,
        this.sys.game.config.height / 2,
        errorMessage,
        {
          font: "24px regular-font",
          fill: "#ffffff",
        }
      )
      .setOrigin(0.5);

    // After 3 seconds, redirect to the /main page
    this.time.delayedCall(3000, () => {
      window.location.href = "/main";
    });
  });
}
