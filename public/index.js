import { logOut } from "./js/logOut.js";

// Get the token from localStorage
const token = localStorage.getItem("token");
// Get the username from localStorage
const username = localStorage.getItem("username");
// Get the user ID from localStorage
const userId = localStorage.getItem("userId");

if (token && username && userId) {
  // Send a POST request to the server to verify the token and user data
  fetch("/verify", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ token, username, userId }),
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.valid) {
        // Token is valid and user data is correct
        // Store the verified flag in sessionStorage
        sessionStorage.setItem("verified", "true");
        // Redirect to the main scene
        window.location.href = "/main";
      } else {
        // Token is invalid or user data is incorrect
        logOut();
      }
    })
    .catch((error) => {
      console.error("Error:", error);
      logOut();
    });
} else {
  logOut();
}
