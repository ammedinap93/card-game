require("dotenv").config();
const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
const cardGamesRouter = require("./routes/cardGamesRouter");
const manageLobbies = require("./games/lobbies");

const port = process.env.PORT || 3000;
const pool = require("./db");

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { BOT_NAMES } = require("./games/botConstants");
const saltRounds = 10; // The higher the number, the more secure, but also the slower

app.use(express.json());
app.use("/card-games", cardGamesRouter);
app.use(express.static(__dirname + "/public"));

manageLobbies(io);

// New endpoint for sign up
app.post("/signup", async (req, res) => {
  const { username, password } = req.body;

  // Validate the username
  if (!username) {
    return res.status(400).json({ error: "Username is required" });
  }

  // Check if the username already exists
  const existingUser = await pool.query(
    "SELECT * FROM users WHERE username = $1",
    [username]
  );
  const nameIndex = BOT_NAMES.findIndex((name) => {
    return name === username;
  });
  if (existingUser.rows.length > 0 || nameIndex >= 0) {
    return res.status(400).json({ error: "Username already exists" });
  }

  // Hash the password
  const hashedPassword = await bcrypt.hash(password, saltRounds);

  // Insert the new user into the database with the hashed password
  const result = await pool.query(
    "INSERT INTO users (username, password) VALUES ($1, $2) RETURNING id",
    [username, hashedPassword]
  );
  const userId = result.rows[0].id;

  // Generate a token
  const token = jwt.sign({ userId }, process.env.JWT_SECRET);

  // Return a success response with the token
  res.json({
    message: "Sign up successful",
    token,
    user: { id: userId, username },
  });
});

// New endpoint for sign in
app.post("/signin", async (req, res) => {
  const { username, password } = req.body;

  // Find the user in the database
  const result = await pool.query("SELECT * FROM users WHERE username = $1", [
    username,
  ]);
  const user = result.rows[0];

  // If user doesn't exist or password doesn't match, return an error
  if (!user || !(await bcrypt.compare(password, user.password))) {
    return res.status(401).json({ error: "Invalid credentials" });
  }

  // User authenticated, generate a token and return it
  const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET);
  res.json({ token, user: { id: user.id, username: user.username } });
});

// New endpoint for token verification
app.post("/verify", async (req, res) => {
  const { token, username, userId } = req.body;

  try {
    // Verify the token
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    // Check if the userId and username match the decoded token
    if (parseInt(decoded.userId) === parseInt(userId)) {
      // Find the user in the database
      const result = await pool.query(
        "SELECT * FROM users WHERE id = $1 AND username = $2",
        [userId, username]
      );
      const user = result.rows[0];
      if (user) {
        // Token is valid and user data is correct
        return res.json({ valid: true });
      }
    }

    // Token is invalid or user data is incorrect
    return res.json({ valid: false });
  } catch (error) {
    // Token verification failed
    return res.json({ valid: false });
  }
});

server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
