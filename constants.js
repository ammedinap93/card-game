const POKER_TEXAS_HOLDEM_ID = 1;
const SOLITAIRE_KLONDIKE_ID = 4;
const CONTINENTAL_ID = 5;
const FIFTY_ONE_ID = 6;

const PUBLIC_PAUSE_TIMER = 5; // 5 seconds timeout to test
const PRIVATE_PAUSE_TIMER = 10; // 10 minutes timeout to test
//const PUBLIC_PAUSE_TIMER = 30; // 30 seconds timeout
//const PRIVATE_PAUSE_TIMER = 30 * 60; // 30 minutes timeout

module.exports = {
  POKER_TEXAS_HOLDEM_ID,
  SOLITAIRE_KLONDIKE_ID,
  CONTINENTAL_ID,
  FIFTY_ONE_ID,
  PUBLIC_PAUSE_TIMER,
  PRIVATE_PAUSE_TIMER,
};
