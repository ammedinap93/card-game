const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  try {
    const result = await pool.query(
      "SELECT * FROM card_games WHERE is_active = TRUE ORDER BY display_order"
    );
    const cardGames = result.rows;
    res.json(cardGames);
  } catch (error) {
    console.error("Error fetching card games:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
