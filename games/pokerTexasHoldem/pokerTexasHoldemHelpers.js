const { shuffle, getCopyOfAnObject, delay } = require("../../helpers");
const statsManager = require("../statsManager");
const { emitProtectedGameState } = require("./pokerTexasHoldemAntiCheat");
const {
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_INITIAL_STACK,
  POKER_TEXAS_HOLDEM_CHIP_VALUES,
  POKER_TEXAS_HOLDEM_BLINDS,
  POKER_TEXAS_HOLDEM_ACTIONS,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS,
  POKER_TEXAS_HOLDEM_HAND_RANKINGS,
  POKER_TEXAS_HOLDEM_CARD_RANKS,
  POKER_TEXAS_HOLDEM_HAND_RESULTS,
} = require("./pokerTexasHoldemConstants");

function generateChip(color, id) {
  return {
    id,
    color,
    value: POKER_TEXAS_HOLDEM_CHIP_VALUES[color],
    offsetX: (Math.random() - 0.5) * 3,
    offsetY: (Math.random() - 0.5) * 3,
    flipX: Math.random() < 0.5,
  };
}

function createNewChip(color, gameState) {
  const newChipId = gameState.lastChipId + 1;
  gameState.lastChipId = newChipId;

  return generateChip(color, newChipId);
}

function createTempChip(color) {
  return { color, value: POKER_TEXAS_HOLDEM_CHIP_VALUES[color] };
}

function generateInitialStacks(lobby, lastChipId = 0) {
  const playersStacks = {};
  let currentChipId = lastChipId + 1;

  lobby.players.forEach((player) => {
    const playerStack = {};
    Object.entries(POKER_TEXAS_HOLDEM_INITIAL_STACK).forEach(
      ([color, count]) => {
        playerStack[color] = Array.from({ length: count }, () => {
          const chip = generateChip(color, currentChipId);
          currentChipId++;
          return chip;
        });
      }
    );
    playersStacks[player.id] = playerStack;
  });

  return { playersStacks, lastChipId: currentChipId - 1 };
}

function createNewGameState(lobby) {
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile without IDs first
  let deckPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
      }))
    ),
  ];

  // Shuffle the deck pile
  deckPile = shuffle(deckPile);

  // Assign IDs after shuffling
  let cardId = 1;
  deckPile.forEach((card) => {
    card.id = cardId++;
  });

  // Randomly distribute the seats
  const distributionOfSeats = shuffle(lobby.players.map((player) => player.id));

  const { playersStacks, lastChipId } = generateInitialStacks(lobby);

  return {
    lock: false,
    paused: false,
    progressId: null,
    numberOfPlayers: distributionOfSeats.length,
    lobby: lobby,
    gameStatus: POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds,
    hand: 1,
    playersReadyToContinue: lobby.players.reduce((ready, player) => {
      ready[player.id] = false;
      return ready;
    }, {}),
    playersStacks,
    lastChipId,
    playersBets: lobby.players.reduce((bets, player) => {
      bets[player.id] = [];
      return bets;
    }, {}),
    playersActions: lobby.players.reduce((actions, player) => {
      actions[player.id] = null;
      return actions;
    }, {}),
    deckPile: deckPile,
    discardPile: [],
    faceUpCards: [],
    pots: [],
    distributionOfSeats: distributionOfSeats,
    playerInTurn:
      distributionOfSeats.length > 2
        ? distributionOfSeats[distributionOfSeats.length - 2]
        : distributionOfSeats[1],
    dealer: distributionOfSeats[distributionOfSeats.length - 1],
    smallBlindPlayer:
      distributionOfSeats.length > 2
        ? distributionOfSeats[distributionOfSeats.length - 2]
        : distributionOfSeats[1],
    bigBlindPlayer:
      distributionOfSeats.length > 2
        ? distributionOfSeats[distributionOfSeats.length - 3]
        : distributionOfSeats[0],
    smallBlind: POKER_TEXAS_HOLDEM_BLINDS.initial_small_blind,
    bigBlind: POKER_TEXAS_HOLDEM_BLINDS.initial_big_blind,
    playersHands: lobby.players.reduce((hands, player) => {
      hands[player.id] = [];
      return hands;
    }, {}),
    playersBestHands: lobby.players.reduce((bestHands, player) => {
      bestHands[player.id] = [];
      return bestHands;
    }, {}),
    showdownResult: [],
    dealingCard: null,
    discardingCard: null,
    discardingCards: [],
    bettingChips: [],
    pushingChips: [],
    awardingChips: [],
    exchangingChips: {
      incomingChips: [],
      outgoingChips: [],
    },
    playersHandResults: lobby.players.reduce((handResults, player) => {
      handResults[player.id] = null;
      return handResults;
    }, {}),
    bustedPlayers: [],
    winnerPlayer: null,
  };
}

function pokerTexasHoldemProcessUpdatedLobby(lobby, gameState) {
  const userIdsMatch =
    lobby.players.length === gameState.lobby.players.length &&
    lobby.players.every(
      (player) =>
        gameState.lobby.players.find((p) => p.userId === player.userId) !==
        undefined
    );

  if (userIdsMatch) {
    // Create a new game state object merging the info from the lobby and the old game state
    const newGameState = {
      ...gameState,
      lobby: lobby,
      playersHands: {},
      playersBestHands: {},
      playersStacks: {},
      playersBets: {},
      playersActions: {},
      playersHandResults: {},
      playersReadyToContinue: {},
    };

    // Create a mapping of old socket IDs to new ones
    const idMapping = {};
    lobby.players.forEach((player) => {
      const oldPlayer = gameState.lobby.players.find(
        (p) => p.userId === player.userId
      );
      if (oldPlayer) {
        idMapping[oldPlayer.id] = player.id;
      }
    });

    // Update player-specific data
    lobby.players.forEach((player) => {
      const oldPlayer = gameState.lobby.players.find(
        (p) => p.userId === player.userId
      );

      if (oldPlayer) {
        newGameState.playersHands[player.id] =
          gameState.playersHands[oldPlayer.id];
        newGameState.playersBestHands[player.id] =
          gameState.playersBestHands[oldPlayer.id];
        newGameState.playersStacks[player.id] =
          gameState.playersStacks[oldPlayer.id];
        newGameState.playersBets[player.id] =
          gameState.playersBets[oldPlayer.id];
        newGameState.playersActions[player.id] =
          gameState.playersActions[oldPlayer.id];
        newGameState.playersHandResults[player.id] =
          gameState.playersHandResults[oldPlayer.id];
        newGameState.playersReadyToContinue[player.id] =
          gameState.playersReadyToContinue[oldPlayer.id];
      }
    });

    // Update distributionOfSeats, showdownResult, and other arrays containing player IDs
    newGameState.distributionOfSeats = newGameState.distributionOfSeats.map(
      (id) => idMapping[id] || id
    );
    newGameState.showdownResult = newGameState.showdownResult.map(
      (id) => idMapping[id] || id
    );

    // Update player in turn and dealer
    newGameState.playerInTurn =
      idMapping[gameState.playerInTurn] || gameState.playerInTurn;
    newGameState.dealer = idMapping[gameState.dealer] || gameState.dealer;
    newGameState.smallBlindPlayer =
      idMapping[gameState.smallBlindPlayer] || gameState.smallBlindPlayer;
    newGameState.bigBlindPlayer =
      idMapping[gameState.bigBlindPlayer] || gameState.bigBlindPlayer;

    if (gameState.winnerPlayer) {
      newGameState.winnerPlayer =
        idMapping[gameState.winnerPlayer] || gameState.winnerPlayer;
    }

    // Update pots
    if (newGameState.pots && newGameState.pots.length > 0) {
      newGameState.pots = newGameState.pots.map((pot) => ({
        ...pot,
        players: pot.players.map((playerId) => idMapping[playerId] || playerId),
      }));
    }

    // Update chips
    const updateChipOwner = (chip) => {
      if (chip.betPlayerId) {
        chip.betPlayerId = idMapping[chip.betPlayerId] || chip.betPlayerId;
      }
      if (chip.winnerPlayerId) {
        chip.winnerPlayerId =
          idMapping[chip.winnerPlayerId] || chip.winnerPlayerId;
      }
      return chip;
    };

    newGameState.bettingChips = newGameState.bettingChips.map(updateChipOwner);
    newGameState.pushingChips = newGameState.pushingChips.map(updateChipOwner);
    newGameState.awardingChips =
      newGameState.awardingChips.map(updateChipOwner);

    return newGameState;
  }
  return null;
}

async function endHand(gameState) {
  gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand;

  // Identify players without chips and add them to bustedPlayers
  gameState.distributionOfSeats.forEach((playerId) => {
    const playerChips = Object.values(gameState.playersStacks[playerId]).flat();
    if (
      playerChips.length === 0 &&
      !gameState.bustedPlayers.includes(playerId)
    ) {
      gameState.bustedPlayers.push(playerId);
    }
  });

  const playersWithChips = gameState.distributionOfSeats.filter((playerId) => {
    const playerChips = Object.values(gameState.playersStacks[playerId]).flat();
    return playerChips.length > 0;
  });

  if (playersWithChips.length === 1) {
    gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_game;
    gameState.winnerPlayer = playersWithChips[0];

    // Save game stats if enabled
    const isStatsEnabled = await statsManager.isStatsEnabled(gameState.lobby);
    if (isStatsEnabled) {
      const statsId = await statsManager.saveStats(gameState);
      if (statsId) {
        console.log(`Game stats saved with ID: ${statsId}`);
      } else {
        console.error("Failed to save game stats");
      }
    }
  } else if (playersWithChips.length > 1) {
    // Check if the current dealer is busted
    if (gameState.bustedPlayers.includes(gameState.dealer)) {
      // Find the previous player in the order who still has chips
      let newDealerIndex = gameState.distributionOfSeats.indexOf(
        gameState.dealer
      );
      do {
        newDealerIndex =
          (newDealerIndex + 1) % gameState.distributionOfSeats.length;
      } while (
        gameState.bustedPlayers.includes(
          gameState.distributionOfSeats[newDealerIndex]
        )
      );

      gameState.dealer = gameState.distributionOfSeats[newDealerIndex];
    }

    // Remove busted players from distributionOfSeats
    gameState.distributionOfSeats = gameState.distributionOfSeats.filter(
      (playerId) => !gameState.bustedPlayers.includes(playerId)
    );
  }

  return gameState;
}

function startNextHand(gameState) {
  // Find the index of the current dealer in the distribution of seats
  const currentDealerIndex = gameState.distributionOfSeats.indexOf(
    gameState.dealer
  );

  // Calculate the index of the next dealer and blinds
  const nextDealerIndex =
    (currentDealerIndex - 1 + gameState.distributionOfSeats.length) %
    gameState.distributionOfSeats.length;
  const nextSmallBlindIndex =
    gameState.distributionOfSeats.length > 2
      ? (nextDealerIndex - 1 + gameState.distributionOfSeats.length) %
        gameState.distributionOfSeats.length
      : nextDealerIndex;
  const nextBigBlindIndex =
    gameState.distributionOfSeats.length > 2
      ? (nextDealerIndex - 2 + gameState.distributionOfSeats.length) %
        gameState.distributionOfSeats.length
      : (nextDealerIndex - 1 + gameState.distributionOfSeats.length) %
        gameState.distributionOfSeats.length;

  // Increment the hand number
  const nextHand = gameState.hand + 1;

  // Check if blinds should be increased
  let newSmallBlind = gameState.smallBlind;
  let newBigBlind = gameState.bigBlind;
  if (nextDealerIndex === gameState.distributionOfSeats.length - 1) {
    [newSmallBlind, newBigBlind] = increaseBlinds(
      gameState.smallBlind,
      gameState.bigBlind
    );
  }

  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile without IDs first
  let deckPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
      }))
    ),
  ];

  // Shuffle the deck pile
  deckPile = shuffle(deckPile);

  // Assign IDs after shuffling
  let cardId = 1;
  deckPile.forEach((card) => {
    card.id = cardId++;
  });

  // Create the new game state
  const newGameState = {
    lock: false,
    paused: false,
    progressId: gameState.progressId,
    numberOfPlayers: gameState.distributionOfSeats.length,
    lobby: gameState.lobby,
    gameStatus: POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds,
    hand: nextHand,
    playersReadyToContinue: gameState.lobby.players.reduce((ready, player) => {
      ready[player.id] = false;
      return ready;
    }, {}),
    playersStacks: gameState.playersStacks,
    lastChipId: gameState.lastChipId,
    playersBets: gameState.lobby.players.reduce((bets, player) => {
      bets[player.id] = [];
      return bets;
    }, {}),
    playersActions: gameState.lobby.players.reduce((actions, player) => {
      actions[player.id] = null;
      return actions;
    }, {}),
    deckPile: deckPile,
    discardPile: [],
    faceUpCards: [],
    pots: [],
    distributionOfSeats: gameState.distributionOfSeats,
    playerInTurn: gameState.distributionOfSeats[nextSmallBlindIndex],
    dealer: gameState.distributionOfSeats[nextDealerIndex],
    smallBlindPlayer: gameState.distributionOfSeats[nextSmallBlindIndex],
    bigBlindPlayer: gameState.distributionOfSeats[nextBigBlindIndex],
    smallBlind: newSmallBlind,
    bigBlind: newBigBlind,
    playersHands: gameState.lobby.players.reduce((hands, player) => {
      hands[player.id] = [];
      return hands;
    }, {}),
    playersBestHands: gameState.lobby.players.reduce((bestHands, player) => {
      bestHands[player.id] = [];
      return bestHands;
    }, {}),
    showdownResult: [],
    dealingCard: null,
    discardingCard: null,
    discardingCards: [],
    bettingChips: [],
    pushingChips: [],
    awardingChips: [],
    exchangingChips: {
      incomingChips: [],
      outgoingChips: [],
    },
    playersHandResults: gameState.lobby.players.reduce(
      (handResults, player) => {
        handResults[player.id] = null;
        return handResults;
      },
      {}
    ),
    bustedPlayers: gameState.bustedPlayers,
    winnerPlayer: null,
  };

  return newGameState;
}

function removeBustedPlayers(gameState, deletePlayersFromLobby) {
  // Remove busted players from the lobby
  if (gameState.bustedPlayers.length > 0 && deletePlayersFromLobby) {
    let updatedLobby = deletePlayersFromLobby(
      gameState.lobby.code,
      gameState.bustedPlayers
    );
    gameState.lobby = updatedLobby;
    gameState.bustedPlayers = [];
  }
  return gameState;
}

function increaseBlinds(currentSmallBlind, currentBigBlind) {
  // Increase big blind by 50%
  const newBigBlind = Math.ceil(currentBigBlind * 1.5);

  // Small blind is typically half of the big blind
  const newSmallBlind = Math.ceil(newBigBlind / 2);

  // Ensure the blinds are whole numbers
  return [Math.floor(newSmallBlind), Math.floor(newBigBlind)];
}

function findChipsForAmountInStack(stack, amount) {
  let remainingAmount = amount;
  const chipsToUse = [];

  const chipColors = Object.keys(stack).sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CHIP_VALUES[b] - POKER_TEXAS_HOLDEM_CHIP_VALUES[a]
  );

  for (const color of chipColors) {
    const chipValue = POKER_TEXAS_HOLDEM_CHIP_VALUES[color];
    const availableChips = [...stack[color]];

    while (remainingAmount >= chipValue && availableChips.length > 0) {
      const chip = availableChips.pop();
      chipsToUse.push(chip);
      remainingAmount -= chipValue;
    }

    if (remainingAmount === 0) break;
  }

  return { remainingAmount, chipsToUse };
}

function findChipsForAmount(chips, amount) {
  let remainingAmount = amount;
  const chipsToUse = [];

  const sortedChips = [...chips].sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CHIP_VALUES[b.color] -
      POKER_TEXAS_HOLDEM_CHIP_VALUES[a.color]
  );

  const availableChips = [...sortedChips];
  let chip = availableChips.pop();
  let chipValue = POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color];

  while (remainingAmount >= chipValue) {
    chipsToUse.push(chip);
    remainingAmount -= chipValue;
    if (availableChips.length > 0) {
      chip = availableChips.pop();
      chipValue = POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color];
    } else {
      break;
    }
  }

  return { remainingAmount, chipsToUse };
}

function getExchangingChips(gameState, availableChips, targetAmount) {
  const totalAvailable = availableChips.reduce(
    (sum, chip) => sum + POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color],
    0
  );

  if (totalAvailable < targetAmount) {
    return null;
  }

  // Check if exact combination exists
  if (findExactCombination(availableChips, targetAmount)) {
    return null;
  }

  const chipValues = Object.entries(POKER_TEXAS_HOLDEM_CHIP_VALUES).sort(
    (a, b) => b[1] - a[1]
  );

  let bestExchange = null;
  let bestScore = Infinity;

  // Try breaking down each chip of higher denomination
  for (let i = 0; i < availableChips.length; i++) {
    const outgoingChip = availableChips[i];
    const outgoingValue = POKER_TEXAS_HOLDEM_CHIP_VALUES[outgoingChip.color];

    // Skip white chips as they are the smallest denomination
    if (outgoingChip.color === "white") continue;

    const possibleExchanges = generateExchanges(
      outgoingValue,
      chipValues.slice(1)
    );

    for (const exchange of possibleExchanges) {
      const tempIncomingChips = exchange
        .map(([color, count]) =>
          Array(count)
            .fill()
            .map(() => createTempChip(color))
        )
        .flat();

      // Check if this exchange allows matching the target amount
      const newChips = [
        ...availableChips.slice(0, i),
        ...availableChips.slice(i + 1),
        ...tempIncomingChips,
      ];
      if (findExactCombination(newChips, targetAmount)) {
        const score = tempIncomingChips.length + 1; // +1 for the outgoing chip
        if (score < bestScore) {
          bestScore = score;
          bestExchange = { outgoingChips: [outgoingChip], tempIncomingChips };
        }
      }
    }
  }

  // If a best exchange was found, create actual chips for the incoming chips
  if (bestExchange) {
    bestExchange.incomingChips = bestExchange.tempIncomingChips.map(
      (tempChip) => createNewChip(tempChip.color, gameState)
    );
    delete bestExchange.tempIncomingChips;
  }

  return bestExchange;
}

function generateExchanges(amount, chipValues, current = []) {
  if (amount === 0) {
    return [current];
  }
  if (chipValues.length === 0 || amount < 0) {
    return [];
  }

  const [color, value] = chipValues[0];
  const maxCount = Math.floor(amount / value);
  let exchanges = [];

  for (let count = 0; count <= maxCount; count++) {
    const newCurrent = [...current, [color, count]].filter(([_, c]) => c > 0);
    const subExchanges = generateExchanges(
      amount - count * value,
      chipValues.slice(1),
      newCurrent
    );
    exchanges = exchanges.concat(subExchanges);
  }

  return exchanges;
}

function findExactCombination(chips, targetAmount) {
  if (targetAmount === 0) return [];
  if (chips.length === 0 || targetAmount < 0) return null;

  const firstChip = chips[0];
  const withFirstChip = findExactCombination(
    chips.slice(1),
    targetAmount - POKER_TEXAS_HOLDEM_CHIP_VALUES[firstChip.color]
  );
  if (withFirstChip !== null) {
    return [firstChip, ...withFirstChip];
  }

  return findExactCombination(chips.slice(1), targetAmount);
}

function removeRedundantExchanges(exchangeResult) {
  const chipCounts = {
    outgoing: { white: 0, red: 0, blue: 0, green: 0, black: 0 },
    incoming: { white: 0, red: 0, blue: 0, green: 0, black: 0 },
  };

  exchangeResult.outgoingChips.forEach(
    (chip) => chipCounts.outgoing[chip.color]++
  );
  exchangeResult.incomingChips.forEach(
    (chip) => chipCounts.incoming[chip.color]++
  );

  ["red", "blue", "green"].forEach((color) => {
    const redundantCount = Math.min(
      chipCounts.outgoing[color],
      chipCounts.incoming[color]
    );
    if (redundantCount > 0) {
      // Remove redundant chips from outgoing
      let removedCount = 0;
      exchangeResult.outgoingChips = exchangeResult.outgoingChips.filter(
        (chip) => {
          if (chip.color === color && removedCount < redundantCount) {
            removedCount++;
            return false;
          }
          return true;
        }
      );

      // Remove redundant chips from incoming
      removedCount = 0;
      exchangeResult.incomingChips = exchangeResult.incomingChips.filter(
        (chip) => {
          if (chip.color === color && removedCount < redundantCount) {
            removedCount++;
            return false;
          }
          return true;
        }
      );

      // Update chip counts
      chipCounts.outgoing[color] -= redundantCount;
      chipCounts.incoming[color] -= redundantCount;
    }
  });

  return exchangeResult;
}

function colorUpChips(gameState, chipsToColorUp) {
  const hasBlackChips = chipsToColorUp.some((chip) => chip.color === "black");
  if (hasBlackChips) return null;

  const totalValue = chipsToColorUp.reduce(
    (total, chip) => total + POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color],
    0
  );

  if (totalValue < POKER_TEXAS_HOLDEM_CHIP_VALUES.red) return null;

  const optimalExchange = findOptimalExchange(totalValue);
  const outgoingChips = selectOutgoingChips(
    chipsToColorUp,
    optimalExchange.outgoingValue
  );

  const tempExchangeResult = {
    outgoingChips,
    incomingChips: optimalExchange.incomingChips,
  };

  const cleanExchangeResult = removeRedundantExchanges(tempExchangeResult);

  // Create actual chips only at the end
  cleanExchangeResult.incomingChips = cleanExchangeResult.incomingChips.map(
    (tempChip) => createNewChip(tempChip.color, gameState)
  );

  return cleanExchangeResult;
}

function findOptimalExchange(totalValue) {
  const chipValues = POKER_TEXAS_HOLDEM_CHIP_VALUES;
  const colors = ["black", "green", "blue", "red"];

  let bestExchange = { outgoingValue: 0, incomingChips: [] };

  colors.forEach((color) => {
    const value = chipValues[color];
    const count = Math.floor(totalValue / value);
    const remaining = totalValue % value;

    if (count > 0) {
      const tempChips = Array.from({ length: count }, () =>
        createTempChip(color)
      );
      const subExchange = findOptimalExchange(remaining);

      const candidateExchange = {
        outgoingValue: count * value + subExchange.outgoingValue,
        incomingChips: [...tempChips, ...subExchange.incomingChips],
      };

      if (
        candidateExchange.outgoingValue > bestExchange.outgoingValue ||
        (candidateExchange.outgoingValue === bestExchange.outgoingValue &&
          candidateExchange.incomingChips.length <
            bestExchange.incomingChips.length)
      ) {
        bestExchange = candidateExchange;
      }
    }
  });

  return bestExchange;
}

function selectOutgoingChips(availableChips, targetValue) {
  const chipValues = POKER_TEXAS_HOLDEM_CHIP_VALUES;

  const sortedChips = availableChips.sort(
    (a, b) => chipValues[b.color] - chipValues[a.color]
  );
  let remainingValue = targetValue;
  const selectedChips = [];

  for (const chip of sortedChips) {
    if (chipValues[chip.color] <= remainingValue) {
      selectedChips.push(chip);
      remainingValue -= chipValues[chip.color];

      if (remainingValue === 0) {
        break;
      }
    }
  }

  return selectedChips;
}

function shouldColorUpChips(chips) {
  const chipCounts = {
    white: chips.filter((chip) => chip.color === "white").length,
    red: chips.filter((chip) => chip.color === "red").length,
    blue: chips.filter((chip) => chip.color === "blue").length,
    green: chips.filter((chip) => chip.color === "green").length,
  };

  return (
    chipCounts.white > 20 ||
    chipCounts.red > 20 ||
    chipCounts.blue > 20 ||
    chipCounts.green > 20
  );
}

function getChipsToColorUp(chips) {
  if (!shouldColorUpChips(chips)) return null;

  const chipsToColorUp = [];

  const chipCounts = {
    white: chips.filter((chip) => chip.color === "white").length,
    red: chips.filter((chip) => chip.color === "red").length,
    blue: chips.filter((chip) => chip.color === "blue").length,
    green: chips.filter((chip) => chip.color === "green").length,
  };

  ["white", "red", "blue", "green"].forEach((color) => {
    if (chipCounts[color] > 20) {
      const chipsToAdd = chipCounts[color] - 5;
      chipsToColorUp.push(
        ...chips.filter((chip) => chip.color === color).slice(0, chipsToAdd)
      );
    }
  });

  return chipsToColorUp.length > 0 ? chipsToColorUp : null;
}

async function colorUpAndExchangeChips(
  gameState,
  chips,
  lobby,
  io,
  betPlayerId = null,
  potIndex = null
) {
  let chipsToColorUp = getChipsToColorUp(chips);
  while (chipsToColorUp) {
    const exchangeResult = colorUpChips(gameState, chipsToColorUp);
    if (exchangeResult) {
      const currentGameStatus = gameState.gameStatus;
      gameState.exchangingChips = exchangeResult;
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

      // Remove outgoing chips and add incoming chips
      exchangeResult.outgoingChips.forEach((chip) => {
        const chipIndex = chips.findIndex((c) => c.id === chip.id);
        if (chipIndex !== -1) {
          chips.splice(chipIndex, 1);
        }
      });
      exchangeResult.incomingChips.forEach((chip) => {
        chip.betPlayerId = betPlayerId;
        chip.potIndex = potIndex;
        chips.push(chip);
      });

      emitProtectedGameState(gameState, io, lobby.players);

      await delay(2000);

      gameState.exchangingChips = { outgoingChips: [], incomingChips: [] };
      gameState.gameStatus = currentGameStatus;
    }

    chipsToColorUp = getChipsToColorUp(chips);
  }

  return chips;
}

async function colorUpAndExchangeStackChips(gameState, playerId, lobby, io) {
  const chips = Object.values(gameState.playersStacks[playerId]).flat();
  let chipsToColorUp = getChipsToColorUp(chips);
  while (chipsToColorUp) {
    const exchangeResult = colorUpChips(gameState, chipsToColorUp);
    if (exchangeResult) {
      const currentGameStatus = gameState.gameStatus;
      gameState.exchangingChips = exchangeResult;
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

      // Remove outgoing chips and add incoming chips
      exchangeResult.outgoingChips.forEach((chip) => {
        const chipIndex = chips.findIndex((c) => c.id === chip.id);
        if (chipIndex !== -1) {
          chips.splice(chipIndex, 1);
        }
      });
      exchangeResult.incomingChips.forEach((chip) => {
        chips.push(chip);
      });

      // Regroup chips by color
      gameState.playersStacks[playerId] = {
        white: chips.filter((chip) => chip.color === "white"),
        red: chips.filter((chip) => chip.color === "red"),
        blue: chips.filter((chip) => chip.color === "blue"),
        green: chips.filter((chip) => chip.color === "green"),
        black: chips.filter((chip) => chip.color === "black"),
      };

      emitProtectedGameState(gameState, io, lobby.players);

      await delay(2000);

      gameState.exchangingChips = { outgoingChips: [], incomingChips: [] };
      gameState.gameStatus = currentGameStatus;
    }

    chipsToColorUp = getChipsToColorUp(chips);
  }

  return gameState;
}

function removePlayerFromPots(gameState, playerId) {
  gameState.pots.forEach((pot) => {
    pot.players = pot.players.filter((player) => player !== playerId);
  });
}

function shouldTheHandEnd(gameState) {
  if (gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown) {
    const hasEveryPlayerAnAction = gameState.distributionOfSeats.every(
      (playerId) => gameState.playersActions[playerId] !== null
    );
    return hasEveryPlayerAnAction;
  }

  const activePlayers = gameState.distributionOfSeats.filter(
    (playerId) =>
      gameState.playersActions[playerId] !== POKER_TEXAS_HOLDEM_ACTIONS.fold
  );

  return activePlayers.length <= 1;
}

function shouldTheBettingRoundEnd(gameState) {
  const hasEveryPlayerAnAction = gameState.distributionOfSeats.every(
    (playerId) => gameState.playersActions[playerId] !== null
  );

  const hasSameChipsOnBets = gameState.distributionOfSeats.every((playerId) => {
    const playerBet = betTotalAmount(gameState.playersBets[playerId]);
    const maxBet = Math.max(
      ...gameState.distributionOfSeats.map((id) =>
        betTotalAmount(gameState.playersBets[id])
      )
    );
    const playerAction = gameState.playersActions[playerId];

    return (
      playerAction === POKER_TEXAS_HOLDEM_ACTIONS.fold ||
      playerAction === POKER_TEXAS_HOLDEM_ACTIONS.all_in ||
      playerBet === maxBet
    );
  });

  return hasEveryPlayerAnAction && hasSameChipsOnBets;
}

function shouldTheBettingRoundStart(gameState) {
  const playersWithNoAction = gameState.distributionOfSeats.filter(
    (playerId) => gameState.playersActions[playerId] === null
  );

  return playersWithNoAction.length > 1;
}

function moveTurnToTheNextPlayer(gameState) {
  const originalPlayerInTurn = gameState.playerInTurn;
  const currentIndex = gameState.distributionOfSeats.indexOf(
    gameState.playerInTurn
  );
  let nextIndex =
    (currentIndex - 1 + gameState.distributionOfSeats.length) %
    gameState.distributionOfSeats.length;

  const skippingActions = [POKER_TEXAS_HOLDEM_ACTIONS.fold];
  if (gameState.gameStatus !== POKER_TEXAS_HOLDEM_GAME_STATUS.showdown) {
    skippingActions.push(POKER_TEXAS_HOLDEM_ACTIONS.all_in);
  } else {
    skippingActions.push(POKER_TEXAS_HOLDEM_ACTIONS.show_cards);
  }

  if (
    gameState.distributionOfSeats.every((playerId) => {
      return skippingActions.includes(gameState.playersActions[playerId]);
    })
  ) {
    gameState.playerInTurn = gameState.dealer;
    return false;
  }

  while (
    skippingActions.includes(
      gameState.playersActions[gameState.distributionOfSeats[nextIndex]]
    )
  ) {
    nextIndex =
      (nextIndex - 1 + gameState.distributionOfSeats.length) %
      gameState.distributionOfSeats.length;
  }

  gameState.playerInTurn = gameState.distributionOfSeats[nextIndex];
  if (gameState.playerInTurn === originalPlayerInTurn) {
    return false;
  }
  return true;
}

function initShowdown(gameState) {
  if (gameState.gameStatus !== POKER_TEXAS_HOLDEM_GAME_STATUS.showdown) {
    return gameState; // Return unchanged if not in showdown
  }

  const activePlayers = gameState.distributionOfSeats.filter(
    (playerId) =>
      gameState.playersActions[playerId] !== POKER_TEXAS_HOLDEM_ACTIONS.fold
  );

  const allInPlayers = activePlayers.filter(
    (playerId) =>
      gameState.playersActions[playerId] === POKER_TEXAS_HOLDEM_ACTIONS.all_in
  );

  if (allInPlayers.length > 0) {
    // If there's at least one all-in player, all non-folded players must show cards
    activePlayers.forEach((playerId) => {
      gameState.playersActions[playerId] =
        POKER_TEXAS_HOLDEM_ACTIONS.show_cards;
    });
    gameState.playerInTurn = gameState.dealer; // The hand will automatically end in this case
  } else {
    // Find the last player who raised or bet
    const lastAggressivePlayer = gameState.distributionOfSeats.find(
      (playerId) =>
        gameState.playersActions[playerId] ===
          POKER_TEXAS_HOLDEM_ACTIONS.raise ||
        gameState.playersActions[playerId] === POKER_TEXAS_HOLDEM_ACTIONS.bet
    );

    if (lastAggressivePlayer) {
      gameState.playerInTurn = lastAggressivePlayer;
    } else {
      // If no aggressive action, start with the player next to the dealer
      gameState.playerInTurn = gameState.dealer;
      moveTurnToTheNextPlayer(gameState);
    }
  }

  return gameState;
}

function betTotalAmount(bet) {
  return bet.reduce(
    (total, chip) => total + POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color],
    0
  );
}

function stackTotalAmount(stack) {
  return Object.values(stack)
    .flat()
    .reduce(
      (total, chip) => total + POKER_TEXAS_HOLDEM_CHIP_VALUES[chip.color],
      0
    );
}

async function awardPots(gameState, lobby, io) {
  const activePlayers = gameState.distributionOfSeats.filter(
    (playerId) =>
      gameState.playersActions[playerId] !== POKER_TEXAS_HOLDEM_ACTIONS.fold
  );

  gameState.awardingChips = [];
  const winningPlayers = [];

  if (activePlayers.length === 1) {
    // When only one player left, award all chips to that player
    const winningPlayer = activePlayers[0];
    winningPlayers.push(winningPlayer);

    // Color up chips in player bets
    for (const [playerId, bet] of Object.entries(gameState.playersBets)) {
      if (bet.length > 0) {
        gameState.playersBets[playerId] = await colorUpAndExchangeChips(
          gameState,
          bet,
          lobby,
          io,
          playerId
        );
      }
    }

    // Color up chips in pots
    for (let i = 0; i < gameState.pots.length; i++) {
      gameState.pots[i].chips = await colorUpAndExchangeChips(
        gameState,
        gameState.pots[i].chips,
        lobby,
        io,
        null,
        i
      );
    }

    const allChips = [
      ...gameState.pots.flatMap((pot) => pot.chips),
      ...Object.values(gameState.playersBets).flat(),
    ];

    allChips.forEach((chip) => {
      gameState.awardingChips.push({
        ...chip,
        winnerPlayerId: winningPlayer,
      });

      // Add chip to winner's stack
      const color = chip.color;
      if (!gameState.playersStacks[winningPlayer][color]) {
        gameState.playersStacks[winningPlayer][color] = [];
      }
      gameState.playersStacks[winningPlayer][color].push(chip);
    });

    // Clear pots and bets
    gameState.pots = [];
    gameState.playersBets = gameState.distributionOfSeats.reduce(
      (bets, player) => {
        bets[player] = [];
        return bets;
      },
      {}
    );
  } else {
    // Showdown scenario
    gameState = processShowdownResult(gameState);

    // Iterate through pots in reverse order (side pots first)
    for (let i = gameState.pots.length - 1; i >= 0; i--) {
      const pot = gameState.pots[i];
      const potPlayers = pot.players.filter((playerId) =>
        gameState.showdownResult.includes(playerId)
      );

      if (potPlayers.length === 1) {
        // Only one player showed cards for this pot
        const winningPlayer = potPlayers[0];
        await awardChipsToPotWinner(gameState, i, winningPlayer, lobby, io);
        winningPlayers.push(winningPlayer);
      } else {
        // Compare hands of players who showed cards
        const potWinners = findPotWinners(gameState, potPlayers);
        await splitPotAmongWinners(gameState, pot, potWinners, lobby, io);
        winningPlayers.push(...potWinners);
      }
    }

    // Clear pots
    gameState.pots = [];
  }

  gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.awarding_pots;
  emitProtectedGameState(gameState, io, lobby.players);

  await delay(2000);
  gameState.awardingChips = [];

  // Color up chips for winning players
  for (const winningPlayer of winningPlayers) {
    await colorUpAndExchangeStackChips(gameState, winningPlayer, lobby, io);
  }

  return gameState;
}

async function awardChipsToPotWinner(
  gameState,
  potIndex,
  winningPlayer,
  lobby,
  io
) {
  gameState.pots[potIndex].chips = await colorUpAndExchangeChips(
    gameState,
    gameState.pots[potIndex].chips,
    lobby,
    io,
    null,
    potIndex
  );
  for (const chip of gameState.pots[potIndex].chips) {
    gameState.awardingChips.push({
      ...chip,
      winnerPlayerId: winningPlayer,
    });

    // Add chip to winner's stack
    const color = chip.color;
    if (!gameState.playersStacks[winningPlayer][color]) {
      gameState.playersStacks[winningPlayer][color] = [];
    }
    gameState.playersStacks[winningPlayer][color].push(chip);
  }
}

function findPotWinners(gameState, potPlayers) {
  return potPlayers.filter((playerId, index, array) =>
    array.every(
      (otherId) =>
        compareHands(
          gameState.playersBestHands[playerId],
          gameState.playersBestHands[otherId]
        ) <= 0
    )
  );
}

async function splitPotAmongWinners(gameState, pot, winners, lobby, io) {
  const potAmount = betTotalAmount(pot.chips);
  const splitAmount = Math.floor(potAmount / winners.length);
  const remainder = potAmount % winners.length;

  // Sort winners based on their position relative to the dealer
  const sortedWinners = sortWinnersRelativeToDealer(gameState, winners);

  for (let i = 0; i < sortedWinners.length; i++) {
    const winner = sortedWinners[i];
    let winnerAmount = splitAmount + (i < remainder ? 1 : 0);
    await awardAmountToPlayer(gameState, pot, winner, winnerAmount, lobby, io);
  }
}

function sortWinnersRelativeToDealer(gameState, winners) {
  const dealerIndex = gameState.distributionOfSeats.indexOf(gameState.dealer);
  const numPlayers = gameState.distributionOfSeats.length;

  return winners.sort((a, b) => {
    const aIndex = gameState.distributionOfSeats.indexOf(a);
    const bIndex = gameState.distributionOfSeats.indexOf(b);

    // Calculate the clockwise distance from the dealer
    let aDistance = (dealerIndex - aIndex + numPlayers) % numPlayers;
    let bDistance = (dealerIndex - bIndex + numPlayers) % numPlayers;

    // Adjust the distance so that the dealer (if a winner) is considered last
    if (aDistance === 0) aDistance = numPlayers;
    if (bDistance === 0) bDistance = numPlayers;

    // Sort based on the adjusted distances (smaller distance comes first)
    return aDistance - bDistance;
  });
}

async function awardAmountToPlayer(gameState, pot, winner, amount, lobby, io) {
  let remainingAmount = amount;
  let chipsToAward = [];

  while (remainingAmount > 0) {
    const { remainingAmount: newRemainingAmount, chipsToUse } =
      findChipsForAmount(pot.chips, remainingAmount);
    chipsToAward.push(...chipsToUse);
    pot.chips = pot.chips.filter((chip) => !chipsToAward.includes(chip));
    remainingAmount = newRemainingAmount;

    if (remainingAmount > 0) {
      // Need to exchange chips
      const exchangeResult = getExchangingChips(
        gameState,
        pot.chips,
        remainingAmount
      );
      if (exchangeResult) {
        // Follow the exchange procedure
        const originalGameStatus = gameState.gameStatus;
        gameState.exchangingChips = exchangeResult;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

        // Remove outgoing chips from the pot
        pot.chips = pot.chips.filter(
          (chip) => !exchangeResult.outgoingChips.includes(chip)
        );

        // Add incoming chips to the pot
        pot.chips.push(...exchangeResult.incomingChips);

        // Emit updated game state
        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for two seconds
        await delay(2000);

        // Restore game status and reset exchangingChips
        gameState.gameStatus = originalGameStatus;
        gameState.exchangingChips = { incomingChips: [], outgoingChips: [] };

        // Try to find chips for the remaining amount again
        const {
          remainingAmount: newRemainingAmount,
          chipsToUse: newChipsToUse,
        } = findChipsForAmount(pot.chips, remainingAmount);
        chipsToAward.push(...newChipsToUse);
        pot.chips = pot.chips.filter((chip) => !chipsToAward.includes(chip));
        remainingAmount = newRemainingAmount;
      } else {
        // If exchange is not possible, break the loop and award the current chips
        break;
      }
    }
  }

  // Award chips to the winner
  for (const chip of chipsToAward) {
    gameState.awardingChips.push({
      ...chip,
      winnerPlayerId: winner,
    });

    const color = chip.color;
    if (!gameState.playersStacks[winner][color]) {
      gameState.playersStacks[winner][color] = [];
    }
    gameState.playersStacks[winner][color].push(chip);

    // Remove awarded chips from the pot
    const chipIndex = pot.chips.findIndex((c) => c.id === chip.id);
    if (chipIndex !== -1) {
      pot.chips.splice(chipIndex, 1);
    }
  }
}

async function endBettingRound(gameState, lobby, io) {
  const originalGameState = getCopyOfAnObject(gameState);

  const currentRoundIndex = POKER_TEXAS_HOLDEM_BETTING_ROUNDS.indexOf(
    gameState.gameStatus
  );

  gameState.pushingChips = [];

  let allInPlayersInCurrentRound = gameState.distributionOfSeats.filter(
    (playerId) =>
      gameState.playersActions[playerId] ===
        POKER_TEXAS_HOLDEM_ACTIONS.all_in &&
      betTotalAmount(gameState.playersBets[playerId]) > 0
  );

  // Create pots as needed
  while (allInPlayersInCurrentRound.length > 0) {
    let smallestAllInBet = Math.min(
      ...allInPlayersInCurrentRound.map((playerId) =>
        betTotalAmount(gameState.playersBets[playerId])
      )
    );
    let potPlayers = gameState.distributionOfSeats.filter(
      (playerId) =>
        gameState.playersActions[playerId] !==
          POKER_TEXAS_HOLDEM_ACTIONS.fold &&
        betTotalAmount(gameState.playersBets[playerId]) >= smallestAllInBet
    );

    // Check if there's already a pot with the same players
    let existingPotIndex = gameState.pots.findIndex(
      (pot) =>
        pot.players.length === potPlayers.length &&
        pot.players.every((player) => potPlayers.includes(player))
    );

    let potChips = [];
    for (const playerId of Object.keys(gameState.playersBets)) {
      if (
        gameState.playersBets[playerId] &&
        gameState.playersBets[playerId].length > 0
      ) {
        let { remainingAmount, chipsToUse: chipsToMove } = findChipsForAmount(
          gameState.playersBets[playerId],
          smallestAllInBet
        );

        if (remainingAmount === 0 && chipsToMove) {
          chipsToMove.forEach((chip) => {
            potChips.push(chip);
            gameState.pushingChips.push({
              ...chip,
              betPlayerId: playerId,
              potIndex:
                existingPotIndex !== -1
                  ? existingPotIndex
                  : gameState.pots.length,
            });
          });
          gameState.playersBets[playerId] = gameState.playersBets[
            playerId
          ].filter((chip) => !chipsToMove.includes(chip));
        } else {
          if (
            remainingAmount > 0 &&
            betTotalAmount(gameState.playersBets[playerId]) < smallestAllInBet
          ) {
            // This should happen only for a folded player.
            gameState.playersBets[playerId].forEach((chip) => {
              potChips.push(chip);
              gameState.pushingChips.push({
                ...chip,
                betPlayerId: playerId,
                potIndex:
                  existingPotIndex !== -1
                    ? existingPotIndex
                    : gameState.pots.length,
              });
            });
            gameState.playersBets[playerId] = [];
          } else if (
            remainingAmount > 0 &&
            betTotalAmount(gameState.playersBets[playerId]) >= smallestAllInBet
          ) {
            // Handle case where the chips don't match the exact amount needed.
            // Attempt to exchange chips to get the correct amount
            const exchangeResult = getExchangingChips(
              gameState,
              gameState.playersBets[playerId],
              smallestAllInBet
            );
            if (exchangeResult) {
              // Chip exchange is possible, update game state and player's bet
              originalGameState.exchangingChips = exchangeResult;
              const currentGameStatus = originalGameState.gameStatus;
              originalGameState.gameStatus =
                POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;
              originalGameState.playersBets[playerId] =
                gameState.playersBets[playerId];

              // Remove outgoing chips from player's bet
              exchangeResult.outgoingChips.forEach((chip) => {
                const chipIndex = originalGameState.playersBets[
                  playerId
                ].findIndex((c) => c.id === chip.id);
                if (chipIndex !== -1) {
                  originalGameState.playersBets[playerId].splice(chipIndex, 1);
                }
              });

              // Add incoming chips to player's bet
              exchangeResult.incomingChips.forEach((chip) => {
                originalGameState.playersBets[playerId].push(chip);
              });

              emitProtectedGameState(originalGameState, io, lobby.players);

              // Wait for the chip exchange animation to complete
              await delay(2000);

              // Reset exchanging chips and restore game status
              originalGameState.exchangingChips = {
                incomingChips: [],
                outgoingChips: [],
              };
              originalGameState.gameStatus = currentGameStatus;

              // Recursive call to end betting round with new chip combination
              return await endBettingRound(originalGameState, lobby, io);
            }
          }
        }
      }
    }

    if (existingPotIndex !== -1) {
      // Add chips to existing pot
      gameState.pots[existingPotIndex].chips.push(...potChips);
    } else {
      // Create new pot
      gameState.pots.push({ chips: potChips, players: potPlayers });
    }

    allInPlayersInCurrentRound = allInPlayersInCurrentRound.filter(
      (playerId) => betTotalAmount(gameState.playersBets[playerId]) > 0
    );
  }

  // Add remaining chips to the main pot or a new side pot if necessary
  let potChips = Object.values(gameState.playersBets).flat();
  if (potChips && potChips.length > 0) {
    let potPlayers = gameState.distributionOfSeats.filter(
      (playerId) =>
        gameState.playersActions[playerId] !==
          POKER_TEXAS_HOLDEM_ACTIONS.fold &&
        betTotalAmount(gameState.playersBets[playerId]) > 0
    );

    // Check if there's already a pot
    let existingPotIndex = gameState.pots.findIndex(
      (pot) =>
        pot.players.length === potPlayers.length &&
        pot.players.every((player) => potPlayers.includes(player))
    );

    potChips.forEach((chip) => {
      gameState.pushingChips.push({
        ...chip,
        betPlayerId: Object.keys(gameState.playersBets).find((playerId) =>
          gameState.playersBets[playerId].includes(chip)
        ),
        potIndex:
          existingPotIndex !== -1 ? existingPotIndex : gameState.pots.length,
      });
    });

    if (existingPotIndex !== -1) {
      // Add chips to existing pot
      gameState.pots[existingPotIndex].chips.push(...potChips);
    } else {
      // Create new pot
      gameState.pots.push({ chips: potChips, players: potPlayers });
    }
  }

  // Clear bets
  gameState.playersBets = gameState.distributionOfSeats.reduce(
    (bets, player) => {
      bets[player] = [];
      return bets;
    },
    {}
  );

  // Determine next game status
  switch (currentRoundIndex) {
    case 0:
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop;
      break;
    case 1:
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn;
      break;
    case 2:
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_river;
      break;
    case 3:
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
      break;
    default:
      throw new Error("Invalid betting round");
  }

  // Set player in turn based on game status
  gameState.playerInTurn = gameState.dealer;
  if (gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown) {
    initShowdown(gameState);
  }

  // Reset players' actions accordingly.
  gameState.distributionOfSeats.forEach((playerId) => {
    if (
      gameState.playersActions[playerId] !==
        POKER_TEXAS_HOLDEM_ACTIONS.all_in &&
      gameState.playersActions[playerId] !==
        POKER_TEXAS_HOLDEM_ACTIONS.show_cards &&
      gameState.playersActions[playerId] !== POKER_TEXAS_HOLDEM_ACTIONS.fold
    ) {
      gameState.playersActions[playerId] = null;
    }
  });

  return gameState;
}

function getRank(card) {
  return card.card.split("_")[0];
}

function getSuit(card) {
  return card.card.split("_")[1];
}

function sortCardsByRank(cards) {
  return cards.sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(getRank(b)) -
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(getRank(a))
  );
}

function sortRanks(ranks) {
  return ranks.sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(b) -
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(a)
  );
}

function getHandRanking(cards) {
  if (isRoyalFlush(cards)) return POKER_TEXAS_HOLDEM_HAND_RANKINGS.ROYAL_FLUSH;
  if (isStraightFlush(cards))
    return POKER_TEXAS_HOLDEM_HAND_RANKINGS.STRAIGHT_FLUSH;
  if (isFourOfAKind(cards))
    return POKER_TEXAS_HOLDEM_HAND_RANKINGS.FOUR_OF_A_KIND;
  if (isFullHouse(cards)) return POKER_TEXAS_HOLDEM_HAND_RANKINGS.FULL_HOUSE;
  if (isFlush(cards)) return POKER_TEXAS_HOLDEM_HAND_RANKINGS.FLUSH;
  if (isStraight(cards)) return POKER_TEXAS_HOLDEM_HAND_RANKINGS.STRAIGHT;
  if (isThreeOfAKind(cards))
    return POKER_TEXAS_HOLDEM_HAND_RANKINGS.THREE_OF_A_KIND;
  if (isTwoPair(cards)) return POKER_TEXAS_HOLDEM_HAND_RANKINGS.TWO_PAIR;
  if (isOnePair(cards)) return POKER_TEXAS_HOLDEM_HAND_RANKINGS.ONE_PAIR;
  return POKER_TEXAS_HOLDEM_HAND_RANKINGS.HIGH_CARD;
}

function getHandResult(ranking) {
  switch (ranking) {
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.ROYAL_FLUSH:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.ROYAL_FLUSH;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.STRAIGHT_FLUSH:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.STRAIGHT_FLUSH;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.FOUR_OF_A_KIND:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.FOUR_OF_A_KIND;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.FULL_HOUSE:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.FULL_HOUSE;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.FLUSH:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.FLUSH;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.STRAIGHT:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.STRAIGHT;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.THREE_OF_A_KIND:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.THREE_OF_A_KIND;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.TWO_PAIR:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.TWO_PAIR;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.ONE_PAIR:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.ONE_PAIR;
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.HIGH_CARD:
      return POKER_TEXAS_HOLDEM_HAND_RESULTS.HIGH_CARD;
    default:
      throw new Error("Invalid hand ranking");
  }
}

function isRoyalFlush(cards) {
  return isStraightFlush(cards) && getRank(cards[0]) === "ace";
}

function isStraightFlush(cards) {
  return isFlush(cards) && isStraight(cards);
}

function isFourOfAKind(cards) {
  const ranks = cards.map(getRank);
  return (
    new Set(ranks).size === 2 &&
    (ranks.filter((r) => r === ranks[0]).length === 1 ||
      ranks.filter((r) => r === ranks[0]).length === 4)
  );
}

function isFullHouse(cards) {
  const ranks = cards.map(getRank);
  return (
    new Set(ranks).size === 2 &&
    (ranks.filter((r) => r === ranks[0]).length === 2 ||
      ranks.filter((r) => r === ranks[0]).length === 3)
  );
}

function isFlush(cards) {
  return new Set(cards.map(getSuit)).size === 1;
}

function isStraight(cards) {
  const ranks = cards.map(getRank);
  const uniqueRanks = [...new Set(ranks)];
  if (uniqueRanks.length < 5) return false;

  const sortedRanks = sortRanks(uniqueRanks);
  const indexDiff =
    POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(sortedRanks[0]) -
    POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(sortedRanks[4]);

  // Check for regular straight
  if (indexDiff === 4) return true;

  // Check for five-high straight (wheel)
  if (sortedRanks[0] === "ace" && sortedRanks[1] === "5") return true;

  return false;
}

function isThreeOfAKind(cards) {
  const ranks = cards.map(getRank);
  return (
    new Set(ranks).size === 3 &&
    ranks.some((r) => ranks.filter((rank) => rank === r).length === 3)
  );
}

function isTwoPair(cards) {
  const ranks = cards.map(getRank);
  return (
    new Set(ranks).size === 3 &&
    ranks.filter((r) => ranks.filter((rank) => rank === r).length === 2)
      .length === 4
  );
}

function isOnePair(cards) {
  const ranks = cards.map(getRank);
  return new Set(ranks).size === 4;
}

function getBestHandForAPlayer(cards) {
  const sortedCards = sortCardsByRank(cards);
  let bestHand = null;
  let bestRanking = 0;

  // Generate all possible 5-card combinations
  for (let i = 0; i < 3; i++) {
    for (let j = i + 1; j < 4; j++) {
      for (let k = j + 1; k < 5; k++) {
        for (let l = k + 1; l < 6; l++) {
          for (let m = l + 1; m < 7; m++) {
            const hand = [
              sortedCards[i],
              sortedCards[j],
              sortedCards[k],
              sortedCards[l],
              sortedCards[m],
            ];
            const ranking = getHandRanking(hand);
            if (ranking > bestRanking) {
              bestRanking = ranking;
              bestHand = hand;
            }
          }
        }
      }
    }
  }

  return bestHand;
}

function compareHands(hand1, hand2) {
  const ranking1 = getHandRanking(hand1);
  const ranking2 = getHandRanking(hand2);

  if (ranking1 !== ranking2) {
    return ranking2 - ranking1;
  }

  // Handle specific cases for each ranking
  switch (ranking1) {
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.ROYAL_FLUSH:
      return 0; // Royal flushes are always equal

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.STRAIGHT_FLUSH:
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.STRAIGHT:
      return compareStraights(hand1, hand2);

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.FOUR_OF_A_KIND:
      return compareFourOfAKind(hand1, hand2);

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.FULL_HOUSE:
      return compareFullHouse(hand1, hand2);

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.FLUSH:
    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.HIGH_CARD:
      return compareHighCards(hand1, hand2);

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.THREE_OF_A_KIND:
      return compareThreeOfAKind(hand1, hand2);

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.TWO_PAIR:
      return compareTwoPair(hand1, hand2);

    case POKER_TEXAS_HOLDEM_HAND_RANKINGS.ONE_PAIR:
      return compareOnePair(hand1, hand2);

    default:
      throw new Error("Invalid hand ranking");
  }
}

function compareStraights(hand1, hand2) {
  const isWheel1 = getRank(hand1[0]) === "ace" && getRank(hand1[1]) === "5";
  const isWheel2 = getRank(hand2[0]) === "ace" && getRank(hand2[1]) === "5";

  if (isWheel1 && !isWheel2) return 1; // Wheel loses to any other straight
  if (!isWheel1 && isWheel2) return -1; // Any other straight beats a wheel
  if (isWheel1 && isWheel2) return 0; // If both are wheels, they're equal

  // Compare the highest card of each straight
  return compareRanks(getRank(hand1[0]), getRank(hand2[0]));
}

function compareFourOfAKind(hand1, hand2) {
  const quads1 = getRank(hand1[1]); // The rank at index 1 is always part of the quads
  const quads2 = getRank(hand2[1]);

  if (quads1 !== quads2) {
    return compareRanks(quads1, quads2);
  }

  // If quads are the same, compare the kicker
  const kicker1 =
    getRank(hand1[0]) === quads1 ? getRank(hand1[4]) : getRank(hand1[0]);
  const kicker2 =
    getRank(hand2[0]) === quads2 ? getRank(hand2[4]) : getRank(hand2[0]);
  return compareRanks(kicker1, kicker2);
}

function compareFullHouse(hand1, hand2) {
  const [trips1, pair1] = getFullHouseRanks(hand1);
  const [trips2, pair2] = getFullHouseRanks(hand2);

  if (trips1 !== trips2) {
    return compareRanks(trips1, trips2);
  }

  return compareRanks(pair1, pair2);
}

function getFullHouseRanks(hand) {
  const rankCounts = getRankCounts(hand);
  const trips = Object.keys(rankCounts).find((rank) => rankCounts[rank] === 3);
  const pair = Object.keys(rankCounts).find((rank) => rankCounts[rank] === 2);
  return [trips, pair];
}

function compareHighCards(hand1, hand2) {
  for (let i = 0; i < 5; i++) {
    const rank1 = getRank(hand1[i]);
    const rank2 = getRank(hand2[i]);
    if (rank1 !== rank2) {
      return compareRanks(rank1, rank2);
    }
  }
  return 0; // All cards are the same
}

function compareThreeOfAKind(hand1, hand2) {
  const trips1 = getRank(hand1[2]); // The rank at index 2 is always part of the trips
  const trips2 = getRank(hand2[2]);

  if (trips1 !== trips2) {
    return compareRanks(trips1, trips2);
  }

  // If trips are the same, compare the kickers
  return compareHighCards(hand1, hand2);
}

function compareTwoPair(hand1, hand2) {
  const [highPair1, lowPair1, kicker1] = getTwoPairRanks(hand1);
  const [highPair2, lowPair2, kicker2] = getTwoPairRanks(hand2);

  if (highPair1 !== highPair2) {
    return compareRanks(highPair1, highPair2);
  }

  if (lowPair1 !== lowPair2) {
    return compareRanks(lowPair1, lowPair2);
  }

  return compareRanks(kicker1, kicker2);
}

function getTwoPairRanks(hand) {
  const rankCounts = getRankCounts(hand);
  const pairs = Object.keys(rankCounts)
    .filter((rank) => rankCounts[rank] === 2)
    .sort((a, b) => compareRanks(a, b));
  const kicker = Object.keys(rankCounts).find((rank) => rankCounts[rank] === 1);
  return [...pairs, kicker];
}

function compareOnePair(hand1, hand2) {
  const [pair1, ...kickers1] = getOnePairRanks(hand1);
  const [pair2, ...kickers2] = getOnePairRanks(hand2);

  if (pair1 !== pair2) {
    return compareRanks(pair1, pair2);
  }

  // Compare kickers
  for (let i = 0; i < kickers1.length; i++) {
    if (kickers1[i] !== kickers2[i]) {
      return compareRanks(kickers1[i], kickers2[i]);
    }
  }

  return 0; // Hands are equal
}

function getOnePairRanks(hand) {
  const rankCounts = getRankCounts(hand);
  const pair = Object.keys(rankCounts).find((rank) => rankCounts[rank] === 2);
  const kickers = Object.keys(rankCounts)
    .filter((rank) => rankCounts[rank] === 1)
    .sort((a, b) => compareRanks(a, b));
  return [pair, ...kickers];
}

function getRankCounts(hand) {
  return hand.reduce((counts, card) => {
    const rank = getRank(card);
    counts[rank] = (counts[rank] || 0) + 1;
    return counts;
  }, {});
}

function compareRanks(rank1, rank2) {
  return (
    POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(rank2) -
    POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(rank1)
  );
}

function processShowdownResult(gameState) {
  gameState.showdownResult = [];
  const playersToEvaluate = gameState.distributionOfSeats.filter(
    (playerId) =>
      gameState.playersActions[playerId] ===
      POKER_TEXAS_HOLDEM_ACTIONS.show_cards
  );

  playersToEvaluate.forEach((playerId) => {
    const playerCards = [
      ...gameState.playersHands[playerId],
      ...gameState.faceUpCards,
    ];
    gameState.playersBestHands[playerId] = getBestHandForAPlayer(playerCards);
    const ranking = getHandRanking(gameState.playersBestHands[playerId]);
    gameState.playersHandResults[playerId] = getHandResult(ranking);
  });

  const sortedPlayers = playersToEvaluate.sort((a, b) =>
    compareHands(gameState.playersBestHands[a], gameState.playersBestHands[b])
  );

  gameState.showdownResult = sortedPlayers;

  return gameState;
}

module.exports = {
  createNewGameState,
  pokerTexasHoldemProcessUpdatedLobby,
  endHand,
  startNextHand,
  removeBustedPlayers,
  findChipsForAmountInStack,
  findChipsForAmount,
  getExchangingChips,
  removePlayerFromPots,
  shouldTheHandEnd,
  shouldTheBettingRoundEnd,
  shouldTheBettingRoundStart,
  moveTurnToTheNextPlayer,
  initShowdown,
  betTotalAmount,
  stackTotalAmount,
  awardPots,
  endBettingRound,
  getHandRanking,
  getBestHandForAPlayer,
  compareHands,
  processShowdownResult,
};
