const { getCopyOfAnObject } = require("../../helpers");
const { POKER_TEXAS_HOLDEM_ACTIONS } = require("./pokerTexasHoldemConstants");

/**
 * Creates a protected copy of the game state where face-down cards have their rank and suit hidden
 * @param {Object} gameState - The current game state
 * @returns {Object} A protected copy of the game state
 */
function protectGameState(gameState) {
  // Create a deep copy of the game state
  let gameStateCopy = getCopyOfAnObject(gameState);

  // Hide other players' hole cards
  Object.keys(gameStateCopy.playersHands).forEach((playerId) => {
    gameStateCopy.playersHands[playerId] = gameStateCopy.playersHands[
      playerId
    ].map((card) => ({
      ...card,
      card: "?_?",
    }));
  });

  // Hide deck pile cards
  gameStateCopy.deckPile = gameStateCopy.deckPile.map((card) => ({
    ...card,
    card: "?_?",
  }));

  // Hide discard pile cards
  gameStateCopy.discardPile = gameStateCopy.discardPile.map((card) => ({
    ...card,
    card: "?_?",
  }));

  // Hide dealing card (always hidden)
  if (gameStateCopy.dealingCard) {
    gameStateCopy.dealingCard = {
      ...gameStateCopy.dealingCard,
      card: "?_?",
    };
  }

  // Hide discarding card (always hidden)
  if (gameStateCopy.discardingCard) {
    gameStateCopy.discardingCard = {
      ...gameStateCopy.discardingCard,
      card: "?_?",
    };
  }

  // Hide discarding cards array (always hidden)
  if (gameStateCopy.discardingCards) {
    gameStateCopy.discardingCards = gameStateCopy.discardingCards.map(
      (card) => ({
        ...card,
        card: "?_?",
      })
    );
  }

  return gameStateCopy;
}

/**
 * Creates a protected copy of the game state for a specific player
 * @param {Object} gameState - The current game state
 * @param {string} playerId - The ID of the player
 * @returns {Object} A protected copy of the game state for the player
 */
function protectGameStateForPlayer(gameState, playerId) {
  let protectedState = protectGameState(gameState);

  // Reveal this player's hole cards
  if (protectedState.playersHands[playerId]) {
    protectedState.playersHands[playerId] = gameState.playersHands[playerId];
  }

  // Reveal cards for players who have chosen to show their cards
  Object.keys(gameState.playersActions).forEach((pid) => {
    if (
      gameState.playersActions[pid] === POKER_TEXAS_HOLDEM_ACTIONS.show_cards
    ) {
      protectedState.playersHands[pid] = gameState.playersHands[pid];
    }
  });

  return protectedState;
}

/**
 * Emits the protected game state to players
 * @param {Object} gameState - The current game state
 * @param {Object} io - Socket.io instance
 * @param {Array} players - Array of players to emit to
 */
function emitProtectedGameState(gameState, io, players) {
  players.forEach((player) => {
    const protectedState = protectGameStateForPlayer(gameState, player.id);
    io.to(player.id).emit("gameState", protectedState);
  });
}

module.exports = {
  protectGameState,
  protectGameStateForPlayer,
  emitProtectedGameState,
};
