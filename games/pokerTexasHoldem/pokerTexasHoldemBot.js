const { delayBetween } = require("../../helpers");
const {
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS,
  POKER_TEXAS_HOLDEM_READY_TO_DEAL,
  POKER_TEXAS_HOLDEM_ACTIONS,
  POKER_TEXAS_HOLDEM_CARD_RANKS,
  POKER_TEXAS_HOLDEM_HAND_RANKINGS,
} = require("./pokerTexasHoldemConstants");
const {
  getBestHandForAPlayer,
  getHandRanking,
  compareHands,
  betTotalAmount,
} = require("./pokerTexasHoldemHelpers");

const botPlayPokerTexasHoldem = async (gameState, botId) => {
  if (
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(3000, 5000);
    return { action: "postBlind" };
  } else if (
    POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus) &&
    gameState.dealer === botId
  ) {
    await delayBetween(3000, 5000);
    return { action: "dealCards" };
  } else if (
    POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(3000, 5000);
    return decideBettingAction(gameState, botId);
  } else if (
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(3000, 5000);
    return { action: "showCards" };
  } else if (
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand &&
    gameState.distributionOfSeats.includes(botId)
  ) {
    await delayBetween(5000, 10000);
    return { action: "readyToContinue" };
  }
  return null;
};

const decideBettingAction = (gameState, botId) => {
  const botHand = gameState.playersHands[botId];
  const communityCards = gameState.faceUpCards;
  const handStrength = evaluateHandStrength(botHand, communityCards);
  const currentBet = Math.max(
    ...Object.values(gameState.playersBets).map(betTotalAmount)
  );
  const botBet = betTotalAmount(gameState.playersBets[botId]);
  const amountToCall = currentBet - botBet;

  if (handStrength > 0.8) {
    // Very strong hand
    if (currentBet === 0) {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.bet };
    } else if (Math.random() < 0.2) {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.raise };
    } else {
      return amountToCall === 0
        ? { action: POKER_TEXAS_HOLDEM_ACTIONS.check }
        : { action: POKER_TEXAS_HOLDEM_ACTIONS.call };
    }
  } else if (handStrength > 0.6) {
    // Strong hand
    if (currentBet === 0) {
      return Math.random() < 0.5
        ? { action: POKER_TEXAS_HOLDEM_ACTIONS.bet }
        : { action: POKER_TEXAS_HOLDEM_ACTIONS.check };
    } else if (Math.random() < 0.2) {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.raise };
    } else {
      return amountToCall === 0
        ? { action: POKER_TEXAS_HOLDEM_ACTIONS.check }
        : { action: POKER_TEXAS_HOLDEM_ACTIONS.call };
    }
  } else if (handStrength > 0.4) {
    // Decent hand: call or check
    if (amountToCall === 0) {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.check };
    } else {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.call };
    }
  } else if (handStrength > 0.2) {
    // Weak hand: check or fold, occasionally call
    if (amountToCall === 0) {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.check };
    } else if (Math.random() > 0.7) {
      // Occasionally call with weak hands
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.call };
    } else {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.fold };
    }
  } else {
    // Very weak hand: check or fold
    if (amountToCall === 0) {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.check };
    } else {
      return { action: POKER_TEXAS_HOLDEM_ACTIONS.fold };
    }
  }
};

const evaluateHandStrength = (hand, communityCards) => {
  const stage =
    communityCards.length === 0
      ? "preflop"
      : communityCards.length === 3
      ? "flop"
      : communityCards.length === 4
      ? "turn"
      : "river";

  const allCards = [...hand, ...communityCards];

  let strength = 0;

  // Evaluate made hands using getBestHand and getHandRanking
  if (allCards.length >= 5) {
    const bestHand = getBestHand(allCards);
    const handRanking = getHandRanking(bestHand);
    // Adjust strength based on hand ranking
    if (handRanking >= POKER_TEXAS_HOLDEM_HAND_RANKINGS.TWO_PAIR) {
      strength += 0.85; // Better than two pairs, very strong hand
    } else if (handRanking === POKER_TEXAS_HOLDEM_HAND_RANKINGS.TWO_PAIR) {
      strength += 0.7; // Two pairs, strong hand
    } else if (handRanking === POKER_TEXAS_HOLDEM_HAND_RANKINGS.ONE_PAIR) {
      strength += 0.5; // One pair, decent hand
    } else {
      strength += handRanking / 10; // Normalize other hand rankings
    }
  }

  // Evaluate drawing hands (more valuable in earlier stages)
  if (stage !== "river") {
    if (hasFlushDraw(hand, communityCards)) strength += 0.4;
    if (hasOutsideStraightDraw(hand, communityCards)) strength += 0.3;
    if (hasInsideStraightDraw(hand, communityCards)) strength += 0.15; // Half the value of outside straight draw
  }

  // Evaluate preflop hands
  if (stage === "preflop") {
    if (hasPair(hand)) strength += 0.5;
    if (hasConsecutiveCards(hand)) strength += 0.2;
    strength += evaluateHighCards(hand) * 1.5; // Increase value of high cards preflop
  } else {
    strength += evaluateHighCards(hand) * (stage === "flop" ? 0.75 : 0.5);
  }

  // Adjust strength based on stage
  if (stage === "preflop") {
    strength *= 1.2; // Increase aggression preflop
  } else if (stage === "river") {
    strength *= 0.9; // Slightly decrease strength on the river if not a made hand
  }

  return Math.min(strength, 1); // Ensure strength is between 0 and 1
};

const getBestHand = (cards) => {
  if (cards.length === 7) {
    return getBestHandForAPlayer(cards);
  }

  const sortedCards = cards.sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(b.card.split("_")[0]) -
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(a.card.split("_")[0])
  );

  if (cards.length === 5) {
    return sortedCards;
  }

  // For 6 cards, find the best 5-card combination
  if (cards.length === 6) {
    let bestHand = sortedCards.slice(0, 5);

    for (let i = 0; i < 6; i++) {
      const hand = [...sortedCards.slice(0, i), ...sortedCards.slice(i + 1)];
      if (compareHands(hand, bestHand) < 0) {
        bestHand = hand;
      }
    }

    return bestHand;
  }

  return sortedCards.slice(0, 5); // Fallback for unexpected number of cards
};

// Helper functions for hand evaluation
const hasFlushDraw = (hand, communityCards) => {
  const allCards = [...hand, ...communityCards];
  const suits = allCards.map((card) => card.card.split("_")[1]);

  // Check for flush draw in all cards
  if (
    suits.filter((s) => suits.filter((x) => x === s).length === 4).length > 0
  ) {
    return true;
  }

  // Check for flush draw in preflop (two cards of the same suit)
  if (
    communityCards.length === 0 &&
    hand[0].card.split("_")[1] === hand[1].card.split("_")[1]
  ) {
    return true;
  }

  return false;
};

const hasOutsideStraightDraw = (hand, communityCards) => {
  const allCards = [...hand, ...communityCards];
  const ranks = allCards.map((card) => card.card.split("_")[0]);
  const uniqueRanks = [...new Set(ranks)].sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(a) -
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(b)
  );

  for (let i = 0; i < uniqueRanks.length - 3; i++) {
    const fourCards = uniqueRanks.slice(i, i + 4);
    if (isConsecutive(fourCards) && !fourCards.includes("ace")) {
      return true;
    }
  }

  return false;
};

const hasInsideStraightDraw = (hand, communityCards) => {
  const allCards = [...hand, ...communityCards];
  const ranks = allCards.map((card) => card.card.split("_")[0]);
  const uniqueRanks = [...new Set(ranks)].sort(
    (a, b) =>
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(a) -
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(b)
  );

  // Check for regular inside straight draw
  for (let i = 0; i < uniqueRanks.length - 3; i++) {
    const fourCards = uniqueRanks.slice(i, i + 4);
    if (isInsideStraightDraw(fourCards)) {
      return true;
    }
  }

  // Check for ace-low straight draw
  if (
    uniqueRanks.includes("ace") &&
    uniqueRanks.includes("2") &&
    uniqueRanks.includes("3") &&
    uniqueRanks.includes("4")
  ) {
    return true;
  }

  // Check for ace-high straight draw
  if (
    uniqueRanks.includes("ace") &&
    uniqueRanks.includes("king") &&
    uniqueRanks.includes("queen") &&
    uniqueRanks.includes("jack")
  ) {
    return true;
  }

  return false;
};

const isInsideStraightDraw = (ranks) => {
  if (ranks.length !== 4) return false;

  const indices = ranks.map((rank) =>
    POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(rank)
  );
  const lowestIndex = Math.min(...indices);
  const highestIndex = Math.max(...indices);

  // Check if the difference between highest and lowest index is 4
  // and if there's exactly one missing rank in between
  return highestIndex - lowestIndex === 4 && new Set(indices).size === 4;
};

const isConsecutive = (ranks) => {
  for (let i = 1; i < ranks.length; i++) {
    if (
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(ranks[i]) !==
      POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(ranks[i - 1]) + 1
    ) {
      return false;
    }
  }
  return true;
};

const hasPair = (hand) => {
  return hand[0].card.split("_")[0] === hand[1].card.split("_")[0];
};

const hasConsecutiveCards = (hand) => {
  const ranks = hand.map((card) => card.card.split("_")[0]);
  const rankIndices = ranks.map((rank) =>
    POKER_TEXAS_HOLDEM_CARD_RANKS.indexOf(rank)
  );
  return Math.abs(rankIndices[0] - rankIndices[1]) === 1;
};

const evaluateHighCards = (hand) => {
  const ranks = hand.map((card) => card.card.split("_")[0]);
  const highCardValues = {};

  POKER_TEXAS_HOLDEM_CARD_RANKS.slice()
    .reverse()
    .forEach((rank, index) => {
      const value = 0.2 - index * 0.02;
      if (value > 0) {
        highCardValues[rank] = Number(value.toFixed(2));
      }
    });

  const highCardValue = ranks.reduce((sum, rank) => {
    return sum + (highCardValues[rank] || 0);
  }, 0);

  return Math.min(highCardValue, 0.3); // Cap high card value at 0.3
};

module.exports = { botPlayPokerTexasHoldem };
