const POKER_TEXAS_HOLDEM_GAME_STATUS = {
  posting_blinds: "posting_blinds",
  ready_to_deal_hole_cards: "ready_to_deal_hole_cards",
  dealing_hole_cards: "dealing_hole_cards",
  preflop_betting_round: "preflop_betting_round",
  ready_to_deal_flop: "ready_to_deal_flop",
  dealing_flop: "dealing_flop",
  flop_betting_round: "flop_betting_round",
  ready_to_deal_turn: "ready_to_deal_turn",
  dealing_turn: "dealing_turn",
  turn_betting_round: "turn_betting_round",
  ready_to_deal_river: "ready_to_deal_river",
  dealing_river: "dealing_river",
  river_betting_round: "river_betting_round",
  initializing_showdown: "initializing_showdown",
  showdown: "showdown",
  processing_action: "processing_action",
  exchanging_chips: "exchanging_chips",
  pushing_bets: "pushing_bets",
  awarding_pots: "awarding_pots",
  end_of_the_hand: "end_of_the_hand",
  end_of_the_game: "end_of_the_game",
};

const POKER_TEXAS_HOLDEM_BETTING_ROUNDS = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.preflop_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.turn_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.river_betting_round,
];

const POKER_TEXAS_HOLDEM_BETTING_ROUNDS_AND_SHOWDOWN = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.preflop_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.turn_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.river_betting_round,
  POKER_TEXAS_HOLDEM_GAME_STATUS.showdown,
];

const POKER_TEXAS_HOLDEM_READY_TO_DEAL = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_hole_cards,
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop,
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn,
  POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_river,
];

const POKER_TEXAS_HOLDEM_DEALING_CARDS = [
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_hole_cards,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_flop,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_turn,
  POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_river,
];

const POKER_TEXAS_HOLDEM_ACTIONS = {
  fold: "fold",
  check: "check",
  bet: "bet",
  call: "call",
  raise: "raise",
  all_in: "all-in",
  show_cards: "show-cards",
};

const POKER_TEXAS_HOLDEM_TIMERS = {
  blinds: 10,
  dealer: 10,
  playerTurn: 15,
  summary: 15,
};

const POKER_TEXAS_HOLDEM_BLINDS = {
  initial_small_blind: 1,
  initial_big_blind: 2,
};

const POKER_TEXAS_HOLDEM_CHIP_VALUES = {
  white: 1,
  red: 5,
  blue: 10,
  green: 25,
  black: 100,
};

const POKER_TEXAS_HOLDEM_INITIAL_STACK = {
  white: 10,
  red: 10,
  blue: 9,
  green: 2,
  black: 0,
};

const POKER_TEXAS_HOLDEM_HAND_RANKINGS = {
  ROYAL_FLUSH: 10,
  STRAIGHT_FLUSH: 9,
  FOUR_OF_A_KIND: 8,
  FULL_HOUSE: 7,
  FLUSH: 6,
  STRAIGHT: 5,
  THREE_OF_A_KIND: 4,
  TWO_PAIR: 3,
  ONE_PAIR: 2,
  HIGH_CARD: 1,
};

const POKER_TEXAS_HOLDEM_CARD_RANKS = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "jack",
  "queen",
  "king",
  "ace",
];

const POKER_TEXAS_HOLDEM_HAND_RESULTS = {
  ROYAL_FLUSH: { code: 10, description: "Royal Flush" },
  STRAIGHT_FLUSH: { code: 9, description: "Straight Flush" },
  FOUR_OF_A_KIND: { code: 8, description: "Four of a Kind" },
  FULL_HOUSE: { code: 7, description: "Full House" },
  FLUSH: { code: 6, description: "Flush" },
  STRAIGHT: { code: 5, description: "Straight" },
  THREE_OF_A_KIND: { code: 4, description: "Three of a Kind" },
  TWO_PAIR: { code: 3, description: "Two Pair" },
  ONE_PAIR: { code: 2, description: "One Pair" },
  HIGH_CARD: { code: 1, description: "High Card" },
};

module.exports = {
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS_AND_SHOWDOWN,
  POKER_TEXAS_HOLDEM_READY_TO_DEAL,
  POKER_TEXAS_HOLDEM_DEALING_CARDS,
  POKER_TEXAS_HOLDEM_ACTIONS,
  POKER_TEXAS_HOLDEM_TIMERS,
  POKER_TEXAS_HOLDEM_BLINDS,
  POKER_TEXAS_HOLDEM_CHIP_VALUES,
  POKER_TEXAS_HOLDEM_INITIAL_STACK,
  POKER_TEXAS_HOLDEM_HAND_RANKINGS,
  POKER_TEXAS_HOLDEM_CARD_RANKS,
  POKER_TEXAS_HOLDEM_HAND_RESULTS,
};
