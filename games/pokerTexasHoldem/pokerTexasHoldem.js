const { delay } = require("../../helpers");
const {
  createNewGameState,
  endHand,
  startNextHand,
  pokerTexasHoldemProcessUpdatedLobby,
  findChipsForAmountInStack,
  shouldTheHandEnd,
  shouldTheBettingRoundEnd,
  moveTurnToTheNextPlayer,
  awardPots,
  endBettingRound,
  betTotalAmount,
  stackTotalAmount,
  removePlayerFromPots,
  getExchangingChips,
  initShowdown,
  removeBustedPlayers,
  shouldTheBettingRoundStart,
} = require("./pokerTexasHoldemHelpers");
const {
  isProgressEnabled,
  findProgress,
  createProgress,
  updateProgress,
  deleteProgress,
} = require("../progressManager");
const {
  startTimer,
  stopTimer,
  pauseTimers,
  resumeTimers,
  stopTimers,
  incrementTimer,
  resumeTimer,
} = require("../timerManager");
const statsManager = require("../statsManager");
const { botNextAction } = require("../botLogic");
const {
  POKER_TEXAS_HOLDEM_GAME_STATUS,
  POKER_TEXAS_HOLDEM_TIMERS,
  POKER_TEXAS_HOLDEM_ACTIONS,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS,
  POKER_TEXAS_HOLDEM_READY_TO_DEAL,
  POKER_TEXAS_HOLDEM_BETTING_ROUNDS_AND_SHOWDOWN,
} = require("./pokerTexasHoldemConstants");
const {
  POKER_TEXAS_HOLDEM_ID,
  PUBLIC_PAUSE_TIMER,
  PRIVATE_PAUSE_TIMER,
} = require("../../constants");
const {
  emitProtectedGameState,
  protectGameStateForPlayer,
} = require("./pokerTexasHoldemAntiCheat");

let gameStateByLobby = {};
let pausedGamesByUser = new Map(); // userId -> lobbyCode
let replacePlayersInLobby = null; // callback to replace disconnected players
let deletePlayersFromLobby = null; // callback to remove busted players or if aborting a private game

async function lockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (!gameState) {
    return false;
  }
  while (gameState.lock) {
    await delay(Math.floor(Math.random() * 401) + 100);
  }
  gameState.lock = true;
  console.log("lock");
  return true;
}

function unlockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (gameState) {
    gameState.lock = false;
    console.log("unlock");
  }
}

function pokerTexasHoldemIsThereAGameInProgress(lobby) {
  return gameStateByLobby[lobby.code] !== undefined;
}

function pokerTexasHoldemAddDisconnectedUser(userId, lobby, io) {
  pausedGamesByUser.set(userId, lobby.code);

  // Set a timeout to replace the player
  startGamePausedTimer(lobby, io);
}

function pokerTexasHoldemRemoveDisconnectedUser(userId) {
  pausedGamesByUser.delete(userId);
}

function pokerTexasHoldemFindPausedGameLobbyCode(userId) {
  return pausedGamesByUser.get(userId);
}

function pokerTexasHoldemIsUserInPausedGame(userId) {
  return pausedGamesByUser.has(userId);
}

async function pokerTexasHoldemUpdateLobby(
  lobby,
  io,
  deletePlayersFromLobbyCallBack,
  replacePlayersInLobbyCallBack,
  removeReplacementsInLobbyCallBack
) {
  deletePlayersFromLobby = deletePlayersFromLobbyCallBack;
  replacePlayersInLobby = replacePlayersInLobbyCallBack;
  if (!pokerTexasHoldemIsThereAGameInProgress(lobby)) {
    const lobbyCopy = JSON.parse(JSON.stringify(lobby));
    pokerTexasHoldemGame(lobbyCopy, io);
  } else {
    const proceed = await lockGameState(lobby);
    if (!proceed) {
      return;
    }
    let gameState = gameStateByLobby[lobby.code];
    try {
      const realPlayers = lobby.players.filter((player) => !player.isBot);
      if (
        lobby.players.length < gameState.numberOfPlayers &&
        lobby.players.length > 0
      ) {
        gameState.paused = true;

        // Pause all timers for this lobby (except the Game Paused timer)
        pauseTimers(lobby);
        resumeTimer(lobby, "gamePaused");

        emitProtectedGameState(gameState, io, lobby.players);
      } else if (realPlayers.length === 0) {
        stopTimers(lobby); // Stop all timers
        delete gameStateByLobby[lobby.code];
      } else if (lobby.players.length === gameState.numberOfPlayers) {
        const lobbyCopy = JSON.parse(JSON.stringify(lobby));
        const newGameState = pokerTexasHoldemProcessUpdatedLobby(
          lobbyCopy,
          gameState
        );
        if (newGameState) {
          // Get the list of successfully replaced user IDs
          const replacedUserIds = [];
          for (const disconnectedUserId in lobbyCopy.replacements) {
            if (lobbyCopy.replacements[disconnectedUserId] !== null) {
              replacedUserIds.push(disconnectedUserId);
            }
          }

          // Remove processed replacements from the lobby
          if (replacedUserIds.length > 0) {
            removeReplacementsInLobbyCallBack(lobby.code, replacedUserIds);
          }

          gameStateByLobby[lobby.code] = newGameState;
          gameState = gameStateByLobby[lobby.code];
          gameState.paused = false;

          await delay(3000); // Wait for the client to set the scene
          emitProtectedGameState(gameState, io, lobbyCopy.players);

          if (
            gameState.gameStatus ===
            POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand
          ) {
            incrementTimer(lobby, "summary", 5); // Increase summary timer by 5 seconds
          }

          // Resume all timers for this lobby
          resumeTimers(lobby);

          // Check if stats feature is enabled
          const statsEnabled = await statsManager.isStatsEnabled(lobby);

          if (statsEnabled) {
            // Get players' wins
            const wins = await statsManager.getWins(lobby);

            if (wins) {
              // Emit gameStats event to all players in the lobby
              lobby.players.forEach((player) => {
                io.to(player.id).emit("gameStats", wins);
              });
            }
          }

          performBotsActions(lobby, io);
        } else {
          unlockGameState(lobby);
          pokerTexasHoldemGame(lobbyCopy, io);
        }
      }
    } finally {
      unlockGameState(lobby);
    }
  }
}

async function pokerTexasHoldemGame(lobby, io) {
  await lockGameState(lobby);
  try {
    let gameState;

    // Check if progress is enabled for this lobby
    const progressEnabled = await isProgressEnabled(lobby);

    if (progressEnabled) {
      // Find game progress
      const savedGameState = await findProgress(lobby);
      if (savedGameState) {
        console.log("Game progress found. Resuming game.");
        gameState = savedGameState;
      } else {
        console.log("No game progress found. Starting a new game.");
        gameState = createNewGameState(lobby);

        // Save the new game state
        const progressId = await createProgress(gameState);
        if (progressId) {
          gameState.progressId = progressId;
        }
      }
    } else {
      console.log("Game progress disabled. Starting a new game.");
      gameState = createNewGameState(lobby);
    }

    gameState.paused = false;

    gameStateByLobby[lobby.code] = gameState;

    await delay(3000); // Wait for the client to set the new scene

    // Emit the game state to all players in the lobby
    emitProtectedGameState(gameState, io, lobby.players);

    // Check if stats feature is enabled
    const statsEnabled = await statsManager.isStatsEnabled(lobby);

    if (statsEnabled) {
      // Get players' wins
      const wins = await statsManager.getWins(lobby);

      if (wins) {
        // Emit gameStats event to all players in the lobby
        lobby.players.forEach((player) => {
          io.to(player.id).emit("gameStats", wins);
        });
      }
    }

    // Check the game status and start the correspondent timer
    if (
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds
    ) {
      startBlindsTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)
    ) {
      // Start the dealer timer
      startDealerTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
    ) {
      // Start the player's turn timer
      startPlayerTurnTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand
    ) {
      // Start the summary timer
      startSummaryTimer(lobby, io);
      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemPostBlind(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      gameState.paused ||
      gameState.gameStatus !== POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds ||
      gameState.playerInTurn !== socket.id ||
      gameState.playersBets[socket.id].length > 0
    ) {
      return;
    }

    stopTimer(lobby, "blinds");

    const playerId = socket.id;
    const isSmallBlind = gameState.smallBlindPlayer === playerId;
    const blindAmount = isSmallBlind
      ? gameState.smallBlind
      : gameState.bigBlind;
    const playerStack = gameState.playersStacks[playerId];

    // Attempt to find chips for the blind amount
    const { remainingAmount, chipsToUse: chipsForBlind } =
      findChipsForAmountInStack(playerStack, blindAmount);
    const remainingStackAmount =
      stackTotalAmount(playerStack) - betTotalAmount(chipsForBlind);

    if (remainingAmount > 0 && remainingStackAmount >= remainingAmount) {
      // Player has enough chips but not the exact combination
      // Attempt to exchange chips to get the correct amount
      const exchangeResult = getExchangingChips(
        gameState,
        Object.values(playerStack).flat(),
        blindAmount
      );
      if (exchangeResult) {
        // Chip exchange is possible, update game state and player's stack
        gameState.exchangingChips = exchangeResult;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

        // Remove outgoing chips from player's stack
        exchangeResult.outgoingChips.forEach((chip) => {
          const chipIndex = playerStack[chip.color].findIndex(
            (c) => c.id === chip.id
          );
          if (chipIndex !== -1) {
            playerStack[chip.color].splice(chipIndex, 1);
          }
        });

        // Add incoming chips to player's stack
        exchangeResult.incomingChips.forEach((chip) => {
          if (!playerStack[chip.color]) {
            playerStack[chip.color] = [];
          }
          playerStack[chip.color].push(chip);
        });

        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for the chip exchange animation to complete
        await delay(2000);

        // Reset exchanging chips and game status
        gameState.exchangingChips = {
          incomingChips: [],
          outgoingChips: [],
        };
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds;

        // Recursive call to post blind with new chip combination
        pokerTexasHoldemPostBlind(lobby, socket, io);
      }
    } else if (chipsForBlind) {
      // Player has the exact combination or is going all-in
      chipsForBlind.forEach((chip) => {
        const chipIndex = playerStack[chip.color].findIndex(
          (c) => c.id === chip.id
        );
        if (chipIndex !== -1) {
          const [removedChip] = playerStack[chip.color].splice(chipIndex, 1);
          gameState.playersBets[playerId].push(removedChip);
          gameState.bettingChips.push({ ...removedChip, playerId });
        }
      });

      // Check if player is all-in
      if (stackTotalAmount(gameState.playersStacks[playerId]) === 0) {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.all_in;
      }

      // Process the blind posting action
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;
      emitProtectedGameState(gameState, io, lobby.players);

      await delay(1000);

      gameState.bettingChips = [];

      // Determine next game state based on whether it's small or big blind
      if (isSmallBlind) {
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds;
        gameState.playerInTurn = gameState.bigBlindPlayer;
        startBlindsTimer(lobby, io);
      } else {
        gameState.gameStatus =
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_hole_cards;
        gameState.playerInTurn = gameState.dealer;
        startDealerTimer(lobby, io);
      }

      // Save game progress if enabled
      if (gameState.progressId) {
        await updateProgress(gameState.progressId, gameState);
      }

      emitProtectedGameState(gameState, io, lobby.players);
      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemDealCards(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      !gameState.paused &&
      POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus) &&
      gameState.dealer === socket.id
    ) {
      // Stop the dealer timer
      stopTimer(lobby, "dealer");

      if (
        gameState.gameStatus ===
        POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_hole_cards
      ) {
        const cardsPerPlayer = 2;
        gameState.gameStatus =
          POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_hole_cards;

        const dealCardsToPlayers = async () => {
          const players = gameState.distributionOfSeats;
          const startIndex =
            (players.indexOf(gameState.dealer) - 1 + players.length) %
            players.length;

          for (let i = 0; i < cardsPerPlayer; i++) {
            for (let j = 0; j < players.length; j++) {
              const playerIndex =
                (startIndex - j + players.length) % players.length;
              const playerId = players[playerIndex];
              const card = gameState.deckPile.pop();
              gameState.playersHands[playerId].push(card);

              // Set the dealingCard property in the game state
              gameState.dealingCard = {
                ...card,
                playerId,
              };

              emitProtectedGameState(gameState, io, lobby.players);
              await delay(500);
            }
          }

          gameState.dealingCard = null;
          gameState.gameStatus =
            POKER_TEXAS_HOLDEM_GAME_STATUS.preflop_betting_round;
          gameState.playerInTurn =
            players.length > 2
              ? players[
                  (players.indexOf(gameState.bigBlindPlayer) -
                    1 +
                    players.length) %
                    players.length
                ]
              : players[players.indexOf(gameState.dealer)];
          const skippingActions = [
            POKER_TEXAS_HOLDEM_ACTIONS.fold,
            POKER_TEXAS_HOLDEM_ACTIONS.all_in,
          ];
          if (
            gameState.distributionOfSeats.every((playerId) => {
              skippingActions.includes(gameState.playersActions[playerId]);
            })
          ) {
            gameState.playerInTurn = gameState.dealer;
            gameState.gameStatus =
              POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop;
            // Start dealer timer
            startDealerTimer(lobby, io);
          } else {
            // Start player's turn timer
            startPlayerTurnTimer(lobby, io);
          }

          // Save progress if enabled
          if (gameState.progressId) {
            await updateProgress(gameState.progressId, gameState);
          }

          emitProtectedGameState(gameState, io, lobby.players);

          performBotsActions(lobby, io);
        };

        await dealCardsToPlayers();
      } else if (
        gameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop ||
        gameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn ||
        gameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_river
      ) {
        const cardsToReveal =
          gameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop
            ? 3
            : 1;

        gameState.gameStatus =
          gameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_flop
            ? POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_flop
            : gameState.gameStatus ===
              POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn
            ? POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_turn
            : POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_river;

        // Burn a card
        gameState.discardingCard = gameState.deckPile.pop();
        gameState.discardPile.push(gameState.discardingCard);

        emitProtectedGameState(gameState, io, lobby.players);

        await delay(500);

        gameState.discardingCard = null;

        for (let i = 0; i < cardsToReveal; i++) {
          const card = gameState.deckPile.pop();
          gameState.dealingCard = {
            ...card,
            playerId: null,
          };
          gameState.faceUpCards.push(card);

          emitProtectedGameState(gameState, io, lobby.players);

          await delay(500);
        }

        gameState.dealingCard = null;

        gameState.gameStatus =
          gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_flop
            ? POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round
            : gameState.gameStatus ===
              POKER_TEXAS_HOLDEM_GAME_STATUS.dealing_turn
            ? POKER_TEXAS_HOLDEM_GAME_STATUS.turn_betting_round
            : POKER_TEXAS_HOLDEM_GAME_STATUS.river_betting_round;

        moveTurnToTheNextPlayer(gameState);
        if (!shouldTheBettingRoundStart(gameState)) {
          gameState.gameStatus =
            gameState.gameStatus ===
            POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round
              ? POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_turn
              : gameState.gameStatus ===
                POKER_TEXAS_HOLDEM_GAME_STATUS.turn_betting_round
              ? POKER_TEXAS_HOLDEM_GAME_STATUS.ready_to_deal_river
              : POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
          if (
            gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
          ) {
            gameState.gameStatus =
              POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown;
            emitProtectedGameState(gameState, io, lobby.players);
            await delay(500);
            gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
            initShowdown(gameState);
            if (shouldTheHandEnd(gameState)) {
              gameState = await awardPots(gameState, lobby, io);
              gameState = await endHand(gameState);
              gameStateByLobby[lobby.code] = gameState;
              processEndHandResult(lobby, io, gameState);
            } else {
              startPlayerTurnTimer(lobby, io);
            }
          } else {
            // Start dealer timer
            startDealerTimer(lobby, io);
          }
        } else {
          // Start player's turn timer
          startPlayerTurnTimer(lobby, io);
        }

        // Save progress if enabled
        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }

        emitProtectedGameState(gameState, io, lobby.players);

        performBotsActions(lobby, io);
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemMoveCard(lobby, socket, io, cardId, newIndex) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (!gameState.paused) {
      const playerId = socket.id;
      const playerHand = gameState.playersHands[playerId];

      // Find the card in the player's hand
      const cardIndex = playerHand.findIndex((card) => card.id === cardId);

      if (cardIndex !== -1) {
        // Remove the card from its current position
        const [card] = playerHand.splice(cardIndex, 1);

        // Insert the card at the new index
        playerHand.splice(newIndex, 0, card);

        // Emit the updated game state to all players
        emitProtectedGameState(gameState, io, lobby.players);
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemFold(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      gameState.paused ||
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS_AND_SHOWDOWN.includes(
        gameState.gameStatus
      ) ||
      gameState.playerInTurn !== socket.id
    ) {
      return;
    }

    stopTimer(lobby, "playerTurn");

    const playerId = socket.id;
    const playerHand = gameState.playersHands[playerId];

    gameState.discardingCards = playerHand.map((card) => ({
      ...card,
      playerId,
    }));
    gameState.discardPile.push(...gameState.discardingCards);
    gameState.playersHands[playerId] = [];

    gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.fold;

    removePlayerFromPots(gameState, playerId);

    const currentGameStatus = gameState.gameStatus;
    gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;
    emitProtectedGameState(gameState, io, lobby.players);

    await delay(500);

    gameState.discardingCards = [];
    gameState.gameStatus = currentGameStatus;

    if (shouldTheHandEnd(gameState)) {
      gameState = await awardPots(gameState, lobby, io);
      gameState = await endHand(gameState);
      gameStateByLobby[lobby.code] = gameState;
      processEndHandResult(lobby, io, gameState);
    } else if (shouldTheBettingRoundEnd(gameState)) {
      let previousGameStatus = gameState.gameStatus;
      gameState = await endBettingRound(gameState, lobby, io);
      gameStateByLobby[lobby.code] = gameState;
      if (gameState.pushingChips.length > 0) {
        const currentGameStatus = gameState.gameStatus;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.pushing_bets;
        emitProtectedGameState(gameState, io, lobby.players);
        await delay(1000);
        gameState.pushingChips = [];
        gameState.gameStatus = currentGameStatus;
      }

      if (POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)) {
        startDealerTimer(lobby, io);
      } else if (
        gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
      ) {
        if (previousGameStatus !== POKER_TEXAS_HOLDEM_GAME_STATUS.showdown) {
          gameState.gameStatus =
            POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
        }
        if (shouldTheHandEnd(gameState)) {
          gameState = await awardPots(gameState, lobby, io);
          gameState = await endHand(gameState);
          gameStateByLobby[lobby.code] = gameState;
          processEndHandResult(lobby, io, gameState);
        } else {
          startPlayerTurnTimer(lobby, io);
        }
      }
    } else {
      moveTurnToTheNextPlayer(gameState);
      startPlayerTurnTimer(lobby, io);
    }

    if (gameState.progressId) {
      await updateProgress(gameState.progressId, gameState);
    }

    emitProtectedGameState(gameState, io, lobby.players);

    performBotsActions(lobby, io);
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemCheck(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      gameState.paused ||
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.playerInTurn !== socket.id
    ) {
      return;
    }

    const playerId = socket.id;
    const playerBet = gameState.playersBets[playerId];
    const maxBet = Math.max(
      ...Object.values(gameState.playersBets).map(betTotalAmount)
    );

    if (betTotalAmount(playerBet) !== maxBet && maxBet !== 0) {
      return; // Player can't check if there's a bet to call
    }

    stopTimer(lobby, "playerTurn");

    gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.check;

    const currentGameStatus = gameState.gameStatus;
    gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;
    emitProtectedGameState(gameState, io, lobby.players);

    await delay(1000);

    gameState.gameStatus = currentGameStatus;

    if (shouldTheBettingRoundEnd(gameState)) {
      gameState = await endBettingRound(gameState, lobby, io);
      gameStateByLobby[lobby.code] = gameState;
      if (gameState.pushingChips.length > 0) {
        const currentGameStatus = gameState.gameStatus;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.pushing_bets;
        emitProtectedGameState(gameState, io, lobby.players);
        await delay(1000);
        gameState.pushingChips = [];
        gameState.gameStatus = currentGameStatus;
      }

      if (POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)) {
        startDealerTimer(lobby, io);
      } else if (
        gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
      ) {
        gameState.gameStatus =
          POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown;
        emitProtectedGameState(gameState, io, lobby.players);
        await delay(500);
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
        if (shouldTheHandEnd(gameState)) {
          gameState = await awardPots(gameState, lobby, io);
          gameState = await endHand(gameState);
          gameStateByLobby[lobby.code] = gameState;
          processEndHandResult(lobby, io, gameState);
        } else {
          startPlayerTurnTimer(lobby, io);
        }
      }
    } else {
      moveTurnToTheNextPlayer(gameState);
      startPlayerTurnTimer(lobby, io);
    }

    if (gameState.progressId) {
      await updateProgress(gameState.progressId, gameState);
    }

    emitProtectedGameState(gameState, io, lobby.players);

    performBotsActions(lobby, io);
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemBet(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      gameState.paused ||
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.playerInTurn !== socket.id ||
      Object.values(gameState.playersBets).some((bets) => bets.length > 0)
    ) {
      return;
    }

    stopTimer(lobby, "playerTurn");

    const playerId = socket.id;
    const playerStack = gameState.playersStacks[playerId];

    // Calculate bet amount based on the current betting round
    const betAmount =
      gameState.gameStatus ===
        POKER_TEXAS_HOLDEM_GAME_STATUS.preflop_betting_round ||
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round
        ? gameState.bigBlind
        : gameState.bigBlind * 2;

    // Attempt to find chips for the bet amount
    const { remainingAmount, chipsToUse: chipsForBet } =
      findChipsForAmountInStack(playerStack, betAmount);
    const remainingStackAmount =
      stackTotalAmount(playerStack) - betTotalAmount(chipsForBet);

    if (remainingAmount > 0 && remainingStackAmount >= remainingAmount) {
      // Player has enough chips but not the exact combination
      // Attempt to exchange chips to get the correct amount
      const exchangeResult = getExchangingChips(
        gameState,
        Object.values(playerStack).flat(),
        betAmount
      );
      if (exchangeResult) {
        // Chip exchange is possible, update game state and player's stack
        gameState.exchangingChips = exchangeResult;
        const currentGameStatus = gameState.gameStatus;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

        // Remove outgoing chips from player's stack
        exchangeResult.outgoingChips.forEach((chip) => {
          const chipIndex = playerStack[chip.color].findIndex(
            (c) => c.id === chip.id
          );
          if (chipIndex !== -1) {
            playerStack[chip.color].splice(chipIndex, 1);
          }
        });

        // Add incoming chips to player's stack
        exchangeResult.incomingChips.forEach((chip) => {
          if (!playerStack[chip.color]) {
            playerStack[chip.color] = [];
          }
          playerStack[chip.color].push(chip);
        });

        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for the chip exchange animation to complete
        await delay(2000);

        // Reset exchanging chips and restore game status
        gameState.exchangingChips = {
          incomingChips: [],
          outgoingChips: [],
        };
        gameState.gameStatus = currentGameStatus;

        // Recursive call to bet with new chip combination
        pokerTexasHoldemBet(lobby, socket, io);
      }
    } else if (chipsForBet) {
      // Player has the exact combination or is going all-in
      chipsForBet.forEach((chip) => {
        const chipIndex = playerStack[chip.color].findIndex(
          (c) => c.id === chip.id
        );
        if (chipIndex !== -1) {
          const [removedChip] = playerStack[chip.color].splice(chipIndex, 1);
          gameState.playersBets[playerId].push(removedChip);
          gameState.bettingChips.push({ ...removedChip, playerId });
        }
      });

      // Determine if player is all-in or just betting
      if (stackTotalAmount(gameState.playersStacks[playerId]) === 0) {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.all_in;
      } else {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.bet;
      }

      // Process the betting action
      const currentGameStatus = gameState.gameStatus;
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;
      emitProtectedGameState(gameState, io, lobby.players);

      await delay(1000);

      gameState.bettingChips = [];
      gameState.gameStatus = currentGameStatus;

      // Check if betting round should end
      if (shouldTheBettingRoundEnd(gameState)) {
        gameState = await endBettingRound(gameState, lobby, io);
        gameStateByLobby[lobby.code] = gameState;
        if (gameState.pushingChips.length > 0) {
          // Process pushing chips to pots if necessary
          const currentGameStatus = gameState.gameStatus;
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.pushing_bets;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(1000);
          gameState.pushingChips = [];
          gameState.gameStatus = currentGameStatus;
        }

        // Start appropriate timer based on new game status
        if (POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)) {
          startDealerTimer(lobby, io);
        } else if (
          gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
        ) {
          gameState.gameStatus =
            POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
          if (shouldTheHandEnd(gameState)) {
            gameState = await awardPots(gameState, lobby, io);
            gameState = await endHand(gameState);
            gameStateByLobby[lobby.code] = gameState;
            processEndHandResult(lobby, io, gameState);
          } else {
            startPlayerTurnTimer(lobby, io);
          }
        }
      } else {
        // Move to next player if betting round continues
        moveTurnToTheNextPlayer(gameState);
        startPlayerTurnTimer(lobby, io);
      }

      // Save game progress if enabled
      if (gameState.progressId) {
        await updateProgress(gameState.progressId, gameState);
      }

      emitProtectedGameState(gameState, io, lobby.players);

      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemCall(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      gameState.paused ||
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.playerInTurn !== socket.id
    ) {
      return;
    }

    const playerId = socket.id;
    const playerStack = gameState.playersStacks[playerId];
    const playerBet = gameState.playersBets[playerId];

    // Calculate the current highest bet
    const maxBet = Math.max(
      ...Object.values(gameState.playersBets).map(betTotalAmount)
    );

    // If player's current bet is already equal to max bet, no need to call
    if (betTotalAmount(playerBet) === maxBet) {
      return;
    }

    stopTimer(lobby, "playerTurn");

    // Calculate the amount needed to call
    const amountToCall = maxBet - betTotalAmount(playerBet);
    const { remainingAmount, chipsToUse: chipsToCall } =
      findChipsForAmountInStack(playerStack, amountToCall);
    const remainingStackAmount =
      stackTotalAmount(playerStack) - betTotalAmount(chipsToCall);

    if (remainingAmount > 0 && remainingStackAmount >= remainingAmount) {
      // Player has enough chips but not the exact combination
      // Attempt to exchange chips to get the correct amount
      const exchangeResult = getExchangingChips(
        gameState,
        Object.values(playerStack).flat(),
        amountToCall
      );
      if (exchangeResult) {
        // Chip exchange is possible, update game state and player's stack
        gameState.exchangingChips = exchangeResult;
        const currentGameStatus = gameState.gameStatus;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

        // Remove outgoing chips from player's stack
        exchangeResult.outgoingChips.forEach((chip) => {
          const chipIndex = playerStack[chip.color].findIndex(
            (c) => c.id === chip.id
          );
          if (chipIndex !== -1) {
            playerStack[chip.color].splice(chipIndex, 1);
          }
        });

        // Add incoming chips to player's stack
        exchangeResult.incomingChips.forEach((chip) => {
          if (!playerStack[chip.color]) {
            playerStack[chip.color] = [];
          }
          playerStack[chip.color].push(chip);
        });

        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for the chip exchange animation to complete
        await delay(2000);

        // Reset exchanging chips and restore game status
        gameState.exchangingChips = {
          incomingChips: [],
          outgoingChips: [],
        };
        gameState.gameStatus = currentGameStatus;

        // Recursive call to call with new chip combination
        pokerTexasHoldemCall(lobby, socket, io);
      }
    } else if (chipsToCall) {
      // Player has the exact combination or is going all-in
      chipsToCall.forEach((chip) => {
        const chipIndex = playerStack[chip.color].findIndex(
          (c) => c.id === chip.id
        );
        if (chipIndex !== -1) {
          const [removedChip] = playerStack[chip.color].splice(chipIndex, 1);
          playerBet.push(removedChip);
          gameState.bettingChips.push({ ...removedChip, playerId });
        }
      });

      // Determine if player is all-in or just calling
      if (stackTotalAmount(gameState.playersStacks[playerId]) === 0) {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.all_in;
      } else {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.call;
      }

      // Process the calling action
      const currentGameStatus = gameState.gameStatus;
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;
      emitProtectedGameState(gameState, io, lobby.players);

      await delay(1000);

      gameState.bettingChips = [];
      gameState.gameStatus = currentGameStatus;

      // Check if betting round should end
      if (shouldTheBettingRoundEnd(gameState)) {
        gameState = await endBettingRound(gameState, lobby, io);
        gameStateByLobby[lobby.code] = gameState;
        if (gameState.pushingChips.length > 0) {
          // Process pushing chips to pots if necessary
          const currentGameStatus = gameState.gameStatus;
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.pushing_bets;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(1000);
          gameState.pushingChips = [];
          gameState.gameStatus = currentGameStatus;
        }

        // Start appropriate timer based on new game status
        if (POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)) {
          startDealerTimer(lobby, io);
        } else if (
          gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
        ) {
          gameState.gameStatus =
            POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
          if (shouldTheHandEnd(gameState)) {
            gameState = await awardPots(gameState, lobby, io);
            gameState = await endHand(gameState);
            gameStateByLobby[lobby.code] = gameState;
            processEndHandResult(lobby, io, gameState);
          } else {
            startPlayerTurnTimer(lobby, io);
          }
        }
      } else {
        // Move to next player if betting round continues
        moveTurnToTheNextPlayer(gameState);
        startPlayerTurnTimer(lobby, io);
      }

      // Save game progress if enabled
      if (gameState.progressId) {
        await updateProgress(gameState.progressId, gameState);
      }

      emitProtectedGameState(gameState, io, lobby.players);

      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemRaise(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      gameState.paused ||
      !POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(gameState.gameStatus) ||
      gameState.playerInTurn !== socket.id ||
      Object.values(gameState.playersBets).every((bets) => bets.length === 0)
    ) {
      return;
    }

    stopTimer(lobby, "playerTurn");

    const playerId = socket.id;
    const playerStack = gameState.playersStacks[playerId];
    const playerBet = gameState.playersBets[playerId];

    // Calculate the current highest bet
    const maxBet = Math.max(
      ...Object.values(gameState.playersBets).map(betTotalAmount)
    );

    // Calculate the raise amount
    const raiseAmount =
      maxBet -
      betTotalAmount(playerBet) +
      (gameState.gameStatus ===
        POKER_TEXAS_HOLDEM_GAME_STATUS.preflop_betting_round ||
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.flop_betting_round
        ? gameState.bigBlind
        : gameState.bigBlind * 2);

    // Attempt to find chips for the raise amount
    const { remainingAmount, chipsToUse: chipsForRaise } =
      findChipsForAmountInStack(playerStack, raiseAmount);
    const remainingStackAmount =
      stackTotalAmount(playerStack) - betTotalAmount(chipsForRaise);

    if (remainingAmount > 0 && remainingStackAmount >= remainingAmount) {
      // Player has enough chips but not the exact combination
      // Attempt to exchange chips to get the correct amount
      const exchangeResult = getExchangingChips(
        gameState,
        Object.values(playerStack).flat(),
        raiseAmount
      );
      if (exchangeResult) {
        // Chip exchange is possible, update game state and player's stack
        gameState.exchangingChips = exchangeResult;
        const currentGameStatus = gameState.gameStatus;
        gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.exchanging_chips;

        // Remove outgoing chips from player's stack
        exchangeResult.outgoingChips.forEach((chip) => {
          const chipIndex = playerStack[chip.color].findIndex(
            (c) => c.id === chip.id
          );
          if (chipIndex !== -1) {
            playerStack[chip.color].splice(chipIndex, 1);
          }
        });

        // Add incoming chips to player's stack
        exchangeResult.incomingChips.forEach((chip) => {
          if (!playerStack[chip.color]) {
            playerStack[chip.color] = [];
          }
          playerStack[chip.color].push(chip);
        });

        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for the chip exchange animation to complete
        await delay(2000);

        // Reset exchanging chips and restore game status
        gameState.exchangingChips = {
          incomingChips: [],
          outgoingChips: [],
        };
        gameState.gameStatus = currentGameStatus;

        // Recursive call to raise with new chip combination
        pokerTexasHoldemRaise(lobby, socket, io);
      }
    } else if (chipsForRaise) {
      // Player has the exact combination or is going all-in
      chipsForRaise.forEach((chip) => {
        const chipIndex = playerStack[chip.color].findIndex(
          (c) => c.id === chip.id
        );
        if (chipIndex !== -1) {
          const [removedChip] = playerStack[chip.color].splice(chipIndex, 1);
          playerBet.push(removedChip);
          gameState.bettingChips.push({ ...removedChip, playerId });
        }
      });

      // Determine if player is all-in or just raising
      if (stackTotalAmount(gameState.playersStacks[playerId]) === 0) {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.all_in;
      } else {
        gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.raise;
      }

      // Process the raising action
      const currentGameStatus = gameState.gameStatus;
      gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;
      emitProtectedGameState(gameState, io, lobby.players);

      await delay(1000);

      gameState.bettingChips = [];
      gameState.gameStatus = currentGameStatus;

      // Check if betting round should end
      if (shouldTheBettingRoundEnd(gameState)) {
        gameState = await endBettingRound(gameState, lobby, io);
        gameStateByLobby[lobby.code] = gameState;
        if (gameState.pushingChips.length > 0) {
          // Process pushing chips to pots if necessary
          const currentGameStatus = gameState.gameStatus;
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.pushing_bets;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(1000);
          gameState.pushingChips = [];
          gameState.gameStatus = currentGameStatus;
        }

        // Start appropriate timer based on new game status
        if (POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(gameState.gameStatus)) {
          startDealerTimer(lobby, io);
        } else if (
          gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.showdown
        ) {
          gameState.gameStatus =
            POKER_TEXAS_HOLDEM_GAME_STATUS.initializing_showdown;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);
          gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.showdown;
          if (shouldTheHandEnd(gameState)) {
            gameState = await awardPots(gameState, lobby, io);
            gameState = await endHand(gameState);
            gameStateByLobby[lobby.code] = gameState;
            processEndHandResult(lobby, io, gameState);
          } else {
            startPlayerTurnTimer(lobby, io);
          }
        }
      } else {
        // Move to next player if betting round continues
        moveTurnToTheNextPlayer(gameState);
        startPlayerTurnTimer(lobby, io);
      }

      // Save game progress if enabled
      if (gameState.progressId) {
        await updateProgress(gameState.progressId, gameState);
      }

      emitProtectedGameState(gameState, io, lobby.players);

      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemShowCards(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;

  try {
    let gameState = gameStateByLobby[lobby.code];

    if (
      gameState.paused ||
      gameState.gameStatus !== POKER_TEXAS_HOLDEM_GAME_STATUS.showdown ||
      gameState.playerInTurn !== socket.id ||
      gameState.playersActions[socket.id] !== null
    ) {
      return;
    }

    stopTimer(lobby, "playerTurn");

    const playerId = socket.id;

    // Set player's action to show_cards
    gameState.playersActions[playerId] = POKER_TEXAS_HOLDEM_ACTIONS.show_cards;

    // Temporarily change game status to processing_action
    const currentGameStatus = gameState.gameStatus;
    gameState.gameStatus = POKER_TEXAS_HOLDEM_GAME_STATUS.processing_action;

    // Emit updated game state
    emitProtectedGameState(gameState, io, lobby.players);

    // Wait for one second
    await delay(1000);

    // Restore previous game status (showdown)
    gameState.gameStatus = currentGameStatus;

    if (shouldTheHandEnd(gameState)) {
      gameState = await awardPots(gameState, lobby, io);
      gameState = await endHand(gameState);
      gameStateByLobby[lobby.code] = gameState;
      processEndHandResult(lobby, io, gameState);
    } else {
      moveTurnToTheNextPlayer(gameState);
      startPlayerTurnTimer(lobby, io);
    }

    // Save game progress if enabled
    if (gameState.progressId) {
      await updateProgress(gameState.progressId, gameState);
    }

    // Emit final game state
    emitProtectedGameState(gameState, io, lobby.players);

    performBotsActions(lobby, io);
  } finally {
    unlockGameState(lobby);
  }
}

async function pokerTexasHoldemReadyToContinue(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;

    if (
      !gameState.paused &&
      gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand
    ) {
      gameState.playersReadyToContinue[playerId] = true;

      // Emit the updated game state to all players
      emitProtectedGameState(gameState, io, lobby.players);

      const allPlayersReady = Object.values(
        gameState.playersReadyToContinue
      ).every((ready) => ready);

      if (allPlayersReady) {
        // Stop the summary timer
        stopTimer(lobby, "summary");

        const newGameState = startNextHand(gameState);
        if (newGameState) {
          gameStateByLobby[lobby.code] = newGameState;
          gameState = newGameState;

          // Save progress if enabled
          if (gameState.progressId) {
            await updateProgress(gameState.progressId, gameState);
          }

          await delay(3000);

          // Emit the updated game state to non-busted players
          let nonBustedPlayers = [];
          lobby.players.forEach((player) => {
            if (!gameState.bustedPlayers.includes(player.id)) {
              nonBustedPlayers.push(player);
            }
          });
          emitProtectedGameState(gameState, io, nonBustedPlayers);

          // Start the blinds timer
          startBlindsTimer(lobby, io);
          performBotsActions(lobby, io);

          gameState = removeBustedPlayers(
            gameStateByLobby[lobby.code],
            deletePlayersFromLobby
          );

          // Save progress if enabled
          if (gameState.progressId) {
            await updateProgress(gameState.progressId, gameState);
          }
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function processEndHandResult(lobby, io, gameState) {
  if (gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand) {
    // Save progress if the feature is enabled
    if (gameState.progressId) {
      await updateProgress(gameState.progressId, gameState);
    }

    // Emit the updated game state to the players
    emitProtectedGameState(gameState, io, lobby.players);

    // Start the summary timer
    startSummaryTimer(lobby, io);
    performBotsActions(lobby, io);
  } else if (
    gameState.gameStatus === POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_game
  ) {
    // Emit the updated game state to the players
    emitProtectedGameState(gameState, io, lobby.players);

    // Check if stats feature is enabled
    const statsEnabled = await statsManager.isStatsEnabled(lobby);
    if (statsEnabled) {
      // Get players' wins
      const wins = await statsManager.getWins(lobby);
      if (wins) {
        // Emit gameStats event to all players in the lobby
        lobby.players.forEach((player) => {
          io.to(player.id).emit("gameStats", wins);
        });
      }
    }

    // Delete the progress save
    if (gameState.progressId) {
      await deleteProgress(gameState.progressId);
    }

    // Stop all timers for the lobby
    stopTimers(lobby);

    // Delete the game state in the gameStateByLobby object
    delete gameStateByLobby[lobby.code];
  }
}

async function startBlindsTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "blinds",
      remainingTime: POKER_TEXAS_HOLDEM_TIMERS.blinds,
    });
  });

  startTimer(
    lobby,
    "blinds",
    POKER_TEXAS_HOLDEM_TIMERS.blinds,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "blinds",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      if (
        currentGameState &&
        currentGameState.gameStatus ===
          POKER_TEXAS_HOLDEM_GAME_STATUS.posting_blinds
      ) {
        pokerTexasHoldemPostBlind(
          currentLobby,
          { id: currentGameState.playerInTurn },
          io
        );
      }
    }
  );
}

async function startDealerTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "dealer",
      remainingTime: POKER_TEXAS_HOLDEM_TIMERS.dealer,
    });
  });
  startTimer(
    lobby,
    "dealer",
    POKER_TEXAS_HOLDEM_TIMERS.dealer,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "dealer",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      // Time's up, start dealing cards automatically
      if (
        currentGameState &&
        POKER_TEXAS_HOLDEM_READY_TO_DEAL.includes(currentGameState.gameStatus)
      ) {
        pokerTexasHoldemDealCards(
          currentLobby,
          { id: currentGameState.dealer },
          io
        );
      }
    }
  );
}

async function startPlayerTurnTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "playerTurn",
      remainingTime: POKER_TEXAS_HOLDEM_TIMERS.playerTurn,
    });
  });
  startTimer(
    lobby,
    "playerTurn",
    POKER_TEXAS_HOLDEM_TIMERS.playerTurn,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "playerTurn",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;

      if (
        currentGameState &&
        POKER_TEXAS_HOLDEM_BETTING_ROUNDS.includes(currentGameState.gameStatus)
      ) {
        pokerTexasHoldemFold(
          currentLobby,
          { id: currentGameState.playerInTurn },
          io
        );
      }
    }
  );
}

async function startSummaryTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "summary",
      remainingTime: POKER_TEXAS_HOLDEM_TIMERS.summary,
    });
  });
  startTimer(
    lobby,
    "summary",
    POKER_TEXAS_HOLDEM_TIMERS.summary,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "summary",
          remainingTime,
        });
      });
    },
    async () => {
      const proceed = await lockGameState(lobby);
      if (!proceed) {
        return;
      }
      try {
        let currentGameState = gameStateByLobby[lobby.code];
        let currentLobby = currentGameState.lobby;
        if (
          !currentGameState.paused &&
          currentGameState.gameStatus ===
            POKER_TEXAS_HOLDEM_GAME_STATUS.end_of_the_hand
        ) {
          const newGameState = startNextHand(currentGameState);
          if (newGameState) {
            gameStateByLobby[lobby.code] = newGameState;
            currentGameState = newGameState;
            currentLobby = currentGameState.lobby;

            // Save progress if enabled
            if (currentGameState.progressId) {
              await updateProgress(
                currentGameState.progressId,
                currentGameState
              );
            }

            // Emit the updated game state to non-busted players
            let nonBustedPlayers = [];
            currentLobby.players.forEach((player) => {
              if (!currentGameState.bustedPlayers.includes(player.id)) {
                nonBustedPlayers.push(player);
              }
            });
            emitProtectedGameState(currentGameState, io, nonBustedPlayers);

            startBlindsTimer(currentLobby, io);
            performBotsActions(currentLobby, io);

            currentGameState = removeBustedPlayers(
              gameStateByLobby[currentLobby.code],
              deletePlayersFromLobby
            );

            // Save progress if enabled
            if (currentGameState.progressId) {
              await updateProgress(
                currentGameState.progressId,
                currentGameState
              );
            }
          }
        }
      } finally {
        unlockGameState(lobby);
      }
    }
  );
}

async function startGamePausedTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "gamePaused",
      remainingTime: lobby.isPublic ? PUBLIC_PAUSE_TIMER : PRIVATE_PAUSE_TIMER,
    });
  });

  startTimer(
    lobby,
    "gamePaused",
    lobby.isPublic ? PUBLIC_PAUSE_TIMER : PRIVATE_PAUSE_TIMER,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "gamePaused",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;

      if (currentGameState && currentGameState.paused) {
        if (currentLobby.isPublic) {
          // Get disconnected user IDs from the pausedGamesByUser map
          const disconnectedUserIds = [];
          for (const [userId, lobbyCode] of pausedGamesByUser.entries()) {
            if (lobbyCode === currentLobby.code) {
              disconnectedUserIds.push(userId);
            }
          }

          // Call replacePlayersInLobby with the disconnected user IDs
          replacePlayersInLobby(currentLobby.code, disconnectedUserIds);
        } else {
          // Abort the game for private lobbies
          const playerIds = currentLobby.players.map((player) => player.id);
          deletePlayersFromLobby(currentLobby.code, playerIds);
        }
      }
    }
  );
}

async function performBotsActions(lobby, io) {
  const botPlayers = lobby.players.filter((player) => player.isBot);
  for (const botPlayer of botPlayers) {
    performBotAction(lobby, io, botPlayer);
  }
}

async function performBotAction(lobby, io, botPlayer) {
  const gameState = gameStateByLobby[lobby.code];
  const protectedGameState = protectGameStateForPlayer(gameState, botPlayer.id);

  const botAction = await botNextAction(
    protectedGameState, // Pass the protected game state
    botPlayer.id,
    POKER_TEXAS_HOLDEM_ID
  );
  if (botAction) {
    switch (botAction.action) {
      case "postBlind":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemPostBlind(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "dealCards":
        if (gameState.dealer === botPlayer.id) {
          pokerTexasHoldemDealCards(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "fold":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemFold(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "check":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemCheck(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "bet":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemBet(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "call":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemCall(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "raise":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemRaise(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "showCards":
        if (gameState.playerInTurn === botPlayer.id) {
          pokerTexasHoldemShowCards(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "readyToContinue":
        if (gameState.distributionOfSeats.includes(botPlayer.id)) {
          pokerTexasHoldemReadyToContinue(lobby, { id: botPlayer.id }, io);
        }
        break;
    }
  }
}

exports.pokerTexasHoldemIsThereAGameInProgress =
  pokerTexasHoldemIsThereAGameInProgress;
exports.pokerTexasHoldemIsUserInPausedGame = pokerTexasHoldemIsUserInPausedGame;
exports.pokerTexasHoldemFindPausedGameLobbyCode =
  pokerTexasHoldemFindPausedGameLobbyCode;
exports.pokerTexasHoldemAddDisconnectedUser =
  pokerTexasHoldemAddDisconnectedUser;
exports.pokerTexasHoldemRemoveDisconnectedUser =
  pokerTexasHoldemRemoveDisconnectedUser;
exports.pokerTexasHoldemUpdateLobby = pokerTexasHoldemUpdateLobby;
exports.pokerTexasHoldemPostBlind = pokerTexasHoldemPostBlind;
exports.pokerTexasHoldemDealCards = pokerTexasHoldemDealCards;
exports.pokerTexasHoldemMoveCard = pokerTexasHoldemMoveCard;
exports.pokerTexasHoldemFold = pokerTexasHoldemFold;
exports.pokerTexasHoldemCheck = pokerTexasHoldemCheck;
exports.pokerTexasHoldemBet = pokerTexasHoldemBet;
exports.pokerTexasHoldemCall = pokerTexasHoldemCall;
exports.pokerTexasHoldemRaise = pokerTexasHoldemRaise;
exports.pokerTexasHoldemShowCards = pokerTexasHoldemShowCards;
exports.pokerTexasHoldemReadyToContinue = pokerTexasHoldemReadyToContinue;
