const BOT_NAMES = [
  "Amy",
  "Charlie",
  "Dana",
  "Jamie",
  "Morgan",
  "Pat",
  "Sam",
  "Taylor",
  "Casey",
  "Jordan",
  "Riley",
  "Avery",
];

module.exports = {
  BOT_NAMES,
};
