const pool = require("../db");
const {
  POKER_TEXAS_HOLDEM_ID,
  SOLITAIRE_KLONDIKE_ID,
  CONTINENTAL_ID,
  FIFTY_ONE_ID,
} = require("../constants");
const {
  pokerTexasHoldemUpdateLobby,
  pokerTexasHoldemDealCards,
  pokerTexasHoldemMoveCard,
  pokerTexasHoldemReadyToContinue,
  pokerTexasHoldemIsThereAGameInProgress,
  pokerTexasHoldemPostBlind,
  pokerTexasHoldemFold,
  pokerTexasHoldemCall,
  pokerTexasHoldemCheck,
  pokerTexasHoldemBet,
  pokerTexasHoldemRaise,
  pokerTexasHoldemShowCards,
  pokerTexasHoldemAddDisconnectedUser,
  pokerTexasHoldemRemoveDisconnectedUser,
  pokerTexasHoldemFindPausedGameLobbyCode,
} = require("./pokerTexasHoldem/pokerTexasHoldem");
const {
  continentalIsThereAGameInProgress,
  continentalUpdateLobby,
  continentalDealCards,
  continentalMoveCard,
  continentalTakeCard,
  continentalPickCardOutOfTurn,
  continentalDiscardCard,
  continentalInitialDroppingCards,
  continentalDropCard,
  continentalReadyToContinue,
  continentalAddDisconnectedUser,
  continentalRemoveDisconnectedUser,
  continentalFindPausedGameLobbyCode,
} = require("./continental/continental");
const { processChatMessage } = require("./chatManager");
const {
  fiftyOneUpdateLobby,
  fiftyOneDealCards,
  fiftyOneMoveCard,
  fiftyOneTakeCardFromTheDeckPile,
  fiftyOneDiscardCard,
  fiftyOneCall,
  fiftyOneChallenge,
  fiftyOneNotChallenge,
  fiftyOneReadyToContinue,
  fiftyOneIsThereAGameInProgress,
  fiftyOneExchangeCards,
  fiftyOneAddDisconnectedUser,
  fiftyOneRemoveDisconnectedUser,
  fiftyOneFindPausedGameLobbyCode,
} = require("./fiftyOne/fiftyOne");
const { createBot } = require("./botLogic");
const {
  solitaireKlondikeDealCards,
  solitaireKlondikeMakeAMove,
  solitaireKlondikeIsThereAGameInProgress,
  solitaireKlondikeUpdateLobby,
  solitaireKlondikeDrawCard,
  solitaireKlondikeStartNewGame,
} = require("./solitaireKlondike/solitaireKlondike");

const ADD_BOT_TIMER = 10000;

const manageLobbies = (io) => {
  const lobbies = {}; // Object to store lobbies
  const lobbyTimers = {}; // Object to store timers for each lobby

  const deletePlayersFromLobby = (lobbyCode, playerIds) => {
    const lobby = lobbies[lobbyCode];
    if (lobby) {
      // Delete the lobby if the player to remove is the only player in the lobby
      if (
        lobby.players.length === 1 &&
        lobby.players[0].id === playerIds[0].id
      ) {
        delete lobbies[lobbyCode];
        return null;
      }

      // Remove the users' IDs from the lobby's userIds set
      playerIds.forEach((id) => {
        const player = lobby.players.find((p) => p.id === id);
        if (player) {
          lobby.userIds.delete(player.userId);
        }
      });

      // Remove the players from the lobby's players array
      lobby.players = lobby.players.filter(
        (player) => !playerIds.includes(player.id)
      );

      // Emit the updated lobby data to all players in the lobby
      lobby.players.forEach((player) => {
        io.to(player.id).emit("lobbyData", lobby);
      });

      // Call the corresponding function to update the lobby based on the game
      if (
        lobby.game.id === CONTINENTAL_ID &&
        continentalIsThereAGameInProgress(lobby)
      ) {
        continentalUpdateLobby(
          lobby,
          io,
          deletePlayersFromLobby,
          replacePlayersInLobby
        );
      } else if (
        lobby.game.id === FIFTY_ONE_ID &&
        fiftyOneIsThereAGameInProgress(lobby)
      ) {
        fiftyOneUpdateLobby(
          lobby,
          io,
          deletePlayersFromLobby,
          replacePlayersInLobby,
          removeReplacementsInLobby
        );
      } else if (
        lobby.game.id === POKER_TEXAS_HOLDEM_ID &&
        pokerTexasHoldemIsThereAGameInProgress(lobby)
      ) {
        pokerTexasHoldemUpdateLobby(
          lobby,
          io,
          deletePlayersFromLobby,
          replacePlayersInLobby
        );
      } else if (
        lobby.game.id === SOLITAIRE_KLONDIKE_ID &&
        solitaireKlondikeIsThereAGameInProgress(lobby)
      ) {
        solitaireKlondikeUpdateLobby(lobby, io, deletePlayersFromLobby);
      }

      lobbies[lobbyCode] = lobby;

      // Check if there are no real players left
      const realPlayers = lobby.players.filter((player) => !player.isBot);
      if (realPlayers.length === 0) {
        // Clear and delete timers
        if (lobbyTimers[lobby.code]) {
          clearTimeout(lobbyTimers[lobby.code]);
          delete lobbyTimers[lobby.code];
        }
        // Delete the lobby
        delete lobbies[lobby.code];
      } else if (
        lobby.isPublic &&
        lobby.players.length < lobby.targetNumPlayers &&
        !isThereAGameInProgress(lobby)
      ) {
        // If the lobby is public and not full after player removal, start a timer to add a bot
        lobbyTimers[lobby.code] = setTimeout(() => {
          addBotToLobby(lobby.code);
        }, ADD_BOT_TIMER);
      }

      return lobby;
    }
    return null;
  };

  const replacePlayersInLobby = async (lobbyCode, disconnectedUserIds) => {
    const lobby = lobbies[lobbyCode];
    if (!lobby) return;

    // Set the lobby in replacement state
    lobby.lookingForReplacements = true;
    disconnectedUserIds.forEach((userId) => {
      lobby.replacements[userId] = null;
    });

    // Emit the updated lobby state to all players
    lobby.players.forEach((player) => {
      io.to(player.id).emit("lobbyData", lobby);
    });

    // Start a timer to add bots if needed
    setTimeout(async () => {
      if (lobby.lookingForReplacements) {
        // Add bots for any remaining disconnected players
        for (const disconnectedUserId in lobby.replacements) {
          if (lobby.replacements[disconnectedUserId] === null) {
            const botPlayer = createBot(lobby);
            lobby.replacements[disconnectedUserId] = botPlayer.userId;
            lobby.players.push(botPlayer);
            lobby.userIds.add(botPlayer.userId);
          }
        }

        // Reset replacement state
        lobby.lookingForReplacements = false;

        // Emit the updated lobby state to all players
        lobby.players.forEach((player) => {
          io.to(player.id).emit("lobbyData", lobby);
        });

        // Update the game state
        if (lobby.game.id === FIFTY_ONE_ID) {
          fiftyOneUpdateLobby(
            lobby,
            io,
            deletePlayersFromLobby,
            replacePlayersInLobby,
            removeReplacementsInLobby
          );
        } else if (lobby.game.id === CONTINENTAL_ID) {
          continentalUpdateLobby(
            lobby,
            io,
            deletePlayersFromLobby,
            replacePlayersInLobby,
            removeReplacementsInLobby
          );
        } else if (lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
          pokerTexasHoldemUpdateLobby(
            lobby,
            io,
            deletePlayersFromLobby,
            replacePlayersInLobby,
            removeReplacementsInLobby
          );
        }
      }
    }, ADD_BOT_TIMER);
  };

  const removeReplacementsInLobby = (lobbyCode, replacedUserIds) => {
    const lobby = lobbies[lobbyCode];
    if (lobby) {
      replacedUserIds.forEach((userId) => {
        if (userId in lobby.replacements) {
          delete lobby.replacements[userId];

          // Remove from game's disconnected users list
          if (lobby.game.id === FIFTY_ONE_ID) {
            fiftyOneRemoveDisconnectedUser(userId);
          } else if (lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
            pokerTexasHoldemRemoveDisconnectedUser(userId);
          } else if (lobby.game.id === CONTINENTAL_ID) {
            continentalRemoveDisconnectedUser(userId);
          }
        }
      });
    }
  };

  const addBotToLobby = (lobbyCode) => {
    const lobby = lobbies[lobbyCode];
    if (
      lobby &&
      lobby.isPublic &&
      lobby.players.length < lobby.targetNumPlayers &&
      !isThereAGameInProgress(lobby)
    ) {
      const botPlayer = createBot(lobby);

      lobby.players.push(botPlayer);
      lobby.userIds.add(botPlayer.userId);

      // Emit the updated lobby data to all players
      lobby.players.forEach((player) => {
        io.to(player.id).emit("lobbyData", lobby);
      });

      // If the lobby is now full, update the game
      if (lobby.players.length === lobby.targetNumPlayers) {
        if (lobby.game.id === CONTINENTAL_ID) {
          continentalUpdateLobby(
            lobby,
            io,
            deletePlayersFromLobby,
            replacePlayersInLobby,
            removeReplacementsInLobby
          );
        } else if (lobby.game.id === FIFTY_ONE_ID) {
          fiftyOneUpdateLobby(
            lobby,
            io,
            deletePlayersFromLobby,
            replacePlayersInLobby,
            removeReplacementsInLobby
          );
        } else if (lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
          pokerTexasHoldemUpdateLobby(
            lobby,
            io,
            deletePlayersFromLobby,
            replacePlayersInLobby,
            removeReplacementsInLobby
          );
        } else if (lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
          solitaireKlondikeUpdateLobby(lobby, io, deletePlayersFromLobby);
        }
      } else {
        // Clear any existing timer for this lobby
        if (lobbyTimers[lobby.code]) {
          clearTimeout(lobbyTimers[lobby.code]);
        }
        // Set a new timer if the lobby is not full
        if (
          lobby.isPublic &&
          lobby.players.length < lobby.targetNumPlayers &&
          !isThereAGameInProgress(lobby)
        ) {
          lobbyTimers[lobby.code] = setTimeout(() => {
            addBotToLobby(lobby.code);
          }, ADD_BOT_TIMER);
        }
      }
    }
  };

  io.on("connection", (socket) => {
    console.log("A user connected");

    // Receive game options from the client
    socket.on("gameOptions", async (options) => {
      const {
        cardGameId,
        matchType,
        lobbyCode,
        username,
        userId,
        preferredNumPlayers,
      } = options;

      const cardGame = await getCardGameFromDB(cardGameId);

      if (!cardGame) return; // Invalid cardGameId

      // Determine the target number of players
      const targetNumPlayers = getTargetNumPlayers(
        cardGame,
        preferredNumPlayers
      );

      // Create a player object with the socket ID, username and user ID
      const player = {
        id: socket.id,
        username: username,
        userId: userId,
      };

      let lobby;
      let joinSuccessful = false;

      // Check if this user was in a paused game
      let pausedLobbyCode;
      if (cardGame.id === FIFTY_ONE_ID) {
        pausedLobbyCode = fiftyOneFindPausedGameLobbyCode(userId);
      } else if (cardGame.id === POKER_TEXAS_HOLDEM_ID) {
        pausedLobbyCode = pokerTexasHoldemFindPausedGameLobbyCode(userId);
      } else if (cardGame.id === CONTINENTAL_ID) {
        pausedLobbyCode = continentalFindPausedGameLobbyCode(userId);
      }

      // Try to rejoin paused game first
      if (pausedLobbyCode && lobbies[pausedLobbyCode]) {
        const pausedLobby = lobbies[pausedLobbyCode];

        // Check if this user is in the replacements object
        if (
          pausedLobby.lookingForReplacements &&
          userId in pausedLobby.replacements
        ) {
          // Remove from replacements object as the original player is back
          delete pausedLobby.replacements[userId];
          pausedLobby.players.push(player);
          pausedLobby.userIds.add(userId);

          // Count remaining empty replacement spots
          let emptyReplacementSpots = 0;
          for (const replacementUserId in pausedLobby.replacements) {
            if (pausedLobby.replacements[replacementUserId] === null) {
              emptyReplacementSpots++;
            }
          }

          // Update replacement flag only if all spots are filled
          if (emptyReplacementSpots === 0) {
            pausedLobby.lookingForReplacements = false;
          }

          lobby = pausedLobby;
          joinSuccessful = true;
        } else {
          // Normal rejoin if the lobby is not yet looking for replacements
          // or the user is not in the replacements object
          pausedLobby.players.push(player);
          pausedLobby.userIds.add(userId);

          // Remove from disconnected users
          if (cardGame.id === FIFTY_ONE_ID) {
            fiftyOneRemoveDisconnectedUser(userId);
          } else if (cardGame.id === POKER_TEXAS_HOLDEM_ID) {
            pokerTexasHoldemRemoveDisconnectedUser(userId);
          } else if (cardGame.id === CONTINENTAL_ID) {
            continentalRemoveDisconnectedUser(userId);
          }

          lobby = pausedLobby;
          joinSuccessful = true;
        }
      }

      // If not rejoining a paused game, try to join a lobby looking for replacements
      if (!joinSuccessful && matchType === "public") {
        const lobbiesNeedingReplacements = Object.values(lobbies).filter(
          (l) =>
            l.game.id === cardGame.id &&
            l.isPublic &&
            l.lookingForReplacements &&
            !l.userIds.has(userId) &&
            l.targetNumPlayers === targetNumPlayers
        );

        if (lobbiesNeedingReplacements.length > 0) {
          lobby = lobbiesNeedingReplacements[0];

          // Find first disconnected user that needs a replacement
          for (const disconnectedUserId in lobby.replacements) {
            if (lobby.replacements[disconnectedUserId] === null) {
              // Store the replacement
              lobby.replacements[disconnectedUserId] = userId;
              lobby.players.push(player);
              lobby.userIds.add(userId);

              // Count remaining empty replacement spots
              let emptyReplacementSpots = 0;
              for (const replacementUserId in lobby.replacements) {
                if (lobby.replacements[replacementUserId] === null) {
                  emptyReplacementSpots++;
                }
              }

              // Update replacement flag only if all spots are filled
              if (emptyReplacementSpots === 0) {
                lobby.lookingForReplacements = false;
              }

              joinSuccessful = true;
              break;
            }
          }
        }
      }

      // If still not joined, try normal lobby joining logic
      if (!joinSuccessful) {
        // Join specific lobby if code provided
        if (lobbyCode && lobbies[lobbyCode]) {
          lobby = lobbies[lobbyCode];
          if (
            lobby.players.length < lobby.targetNumPlayers &&
            !lobby.userIds.has(userId) &&
            !lobby.lookingForReplacements
          ) {
            lobby.players.push(player);
            lobby.userIds.add(userId);
            joinSuccessful = true;
          }
        }
        // Otherwise, find or create appropriate lobby
        else {
          const availableLobbies = Object.values(lobbies).filter(
            (l) =>
              l.game.id === cardGame.id &&
              l.isPublic &&
              l.players.length < l.targetNumPlayers &&
              !l.userIds.has(userId) &&
              !isThereAGameInProgress(l) &&
              !l.lookingForReplacements &&
              l.targetNumPlayers === targetNumPlayers
          );

          if (availableLobbies.length === 0 || matchType === "private") {
            lobby = createLobby(
              cardGame,
              matchType,
              player,
              lobbies,
              targetNumPlayers
            );
            joinSuccessful = true;
          } else if (matchType === "public") {
            lobby = availableLobbies[0];
            lobby.players.push(player);
            lobby.userIds.add(userId);
            joinSuccessful = true;
          }
        }
      }

      if (joinSuccessful) {
        lobbies[lobby.code] = lobby;

        // Clear any existing timer for this lobby
        if (lobbyTimers[lobby.code]) {
          clearTimeout(lobbyTimers[lobby.code]);
        }

        // Set a new timer if the lobby is not full and not looking for replacements
        if (
          lobby.isPublic &&
          lobby.players.length < lobby.targetNumPlayers &&
          !isThereAGameInProgress(lobby) &&
          !lobby.lookingForReplacements
        ) {
          lobbyTimers[lobby.code] = setTimeout(() => {
            addBotToLobby(lobby.code);
          }, ADD_BOT_TIMER);
        }

        // Emit the lobby data to all players in the lobby
        lobby.players.forEach((p) => {
          io.to(p.id).emit("lobbyData", lobby);
        });

        // Update lobby if it's full or all replacements are found
        if (
          lobby.players.length === lobby.targetNumPlayers &&
          !lobby.lookingForReplacements
        ) {
          if (lobby.game.id === CONTINENTAL_ID) {
            continentalUpdateLobby(
              lobby,
              io,
              deletePlayersFromLobby,
              replacePlayersInLobby,
              removeReplacementsInLobby
            );
          } else if (lobby.game.id === FIFTY_ONE_ID) {
            fiftyOneUpdateLobby(
              lobby,
              io,
              deletePlayersFromLobby,
              replacePlayersInLobby,
              removeReplacementsInLobby
            );
          } else if (lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
            pokerTexasHoldemUpdateLobby(
              lobby,
              io,
              deletePlayersFromLobby,
              replacePlayersInLobby,
              removeReplacementsInLobby
            );
          } else if (lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
            solitaireKlondikeUpdateLobby(lobby, io, deletePlayersFromLobby);
          }
        }
      } else {
        socket.emit(
          "lobbyError",
          "Unable to join lobby. It might be full, in progress, or you're already in it."
        );
      }
    });

    socket.on("chatMessage", (message) => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby) {
        processChatMessage(lobby, socket, io, message);
      }
    });

    socket.on("newGame", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
        solitaireKlondikeStartNewGame(lobby, socket, io);
      }
    });

    socket.on("postBlind", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemPostBlind(lobby, socket, io);
      }
    });

    socket.on("fold", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemFold(lobby, socket, io);
      }
    });

    socket.on("dealCards", () => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalDealCards(lobby, socket, io);
      } else if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneDealCards(lobby, socket, io);
      } else if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemDealCards(lobby, socket, io);
      } else if (lobby && lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
        solitaireKlondikeDealCards(lobby, socket, io);
      }
    });

    socket.on("drawCard", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
        solitaireKlondikeDrawCard(lobby, socket, io);
      }
    });

    socket.on("moveCard", ({ cardId, newIndex }) => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalMoveCard(lobby, socket, io, cardId, newIndex);
      } else if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneMoveCard(lobby, socket, io, cardId, newIndex);
      } else if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemMoveCard(lobby, socket, io, cardId, newIndex);
      }
    });

    socket.on("makeAMove", (move) => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
        solitaireKlondikeMakeAMove(lobby, socket, io, move);
      }
    });

    socket.on("takeCard", ({ fromDeckPile }) => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalTakeCard(lobby, socket, io, fromDeckPile);
      } else if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneTakeCardFromTheDeckPile(lobby, socket, io);
      }
    });

    socket.on("pickCardOutOfTurn", () => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalPickCardOutOfTurn(lobby, socket, io);
      }
    });

    socket.on("discardCard", ({ cardId }) => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalDiscardCard(lobby, socket, io, cardId);
      } else if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneDiscardCard(lobby, socket, io, cardId);
      }
    });

    socket.on("exchangeCards", ({ takingCards, discardingCards }) => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneExchangeCards(lobby, socket, io, takingCards, discardingCards);
      }
    });

    socket.on("check", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemCheck(lobby, socket, io);
      }
    });

    socket.on("bet", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemBet(lobby, socket, io);
      }
    });

    socket.on("call", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneCall(lobby, socket, io);
      } else if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemCall(lobby, socket, io);
      }
    });

    socket.on("raise", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemRaise(lobby, socket, io);
      }
    });

    socket.on("showCards", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemShowCards(lobby, socket, io);
      }
    });

    socket.on("challenge", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneChallenge(lobby, socket, io);
      }
    });

    socket.on("notChallenge", () => {
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);
      if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneNotChallenge(lobby, socket, io);
      }
    });

    socket.on("initialDroppingCards", ({ combinations }) => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalInitialDroppingCards(lobby, socket, io, combinations);
      }
    });

    socket.on(
      "dropCard",
      ({
        cardToDrop,
        targetCombinationKey,
        targetCombinationIndex,
        addToStart,
      }) => {
        // Find the lobby that the player belongs to
        const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

        if (lobby && lobby.game.id === CONTINENTAL_ID) {
          continentalDropCard(
            lobby,
            socket,
            io,
            cardToDrop,
            targetCombinationKey,
            targetCombinationIndex,
            addToStart
          );
        }
      }
    );

    socket.on("readyToContinue", () => {
      // Find the lobby that the player belongs to
      const lobby = getCopyOfTheLobbyOfAPlayer(lobbies, socket);

      if (lobby && lobby.game.id === CONTINENTAL_ID) {
        continentalReadyToContinue(lobby, socket, io);
      } else if (lobby && lobby.game.id === FIFTY_ONE_ID) {
        fiftyOneReadyToContinue(lobby, socket, io);
      } else if (lobby && lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        pokerTexasHoldemReadyToContinue(lobby, socket, io);
      }
    });

    // Handle player disconnection
    socket.on("disconnect", () => {
      console.log("A user disconnected");

      // Find the lobby that the disconnecting player belongs to
      const lobby = Object.values(lobbies).find((lobby) =>
        lobby.players.some((player) => player.id === socket.id)
      );

      if (lobby) {
        // Find the disconnecting player
        const disconnectingPlayer = lobby.players.find(
          (player) => player.id === socket.id
        );

        if (disconnectingPlayer) {
          // Add to disconnected users if game is in progress
          if (
            lobby.game.id === FIFTY_ONE_ID &&
            fiftyOneIsThereAGameInProgress(lobby)
          ) {
            fiftyOneAddDisconnectedUser(disconnectingPlayer.userId, lobby, io);
          } else if (
            lobby.game.id === POKER_TEXAS_HOLDEM_ID &&
            pokerTexasHoldemIsThereAGameInProgress(lobby)
          ) {
            pokerTexasHoldemAddDisconnectedUser(
              disconnectingPlayer.userId,
              lobby,
              io
            );
          } else if (
            lobby.game.id === CONTINENTAL_ID &&
            continentalIsThereAGameInProgress(lobby)
          ) {
            continentalAddDisconnectedUser(
              disconnectingPlayer.userId,
              lobby,
              io
            );
          }
          deletePlayersFromLobby(lobby.code, [disconnectingPlayer.id]);
        }
      }
    });
  });
};

const isThereAGameInProgress = (lobby) => {
  if (
    (lobby.game.id === CONTINENTAL_ID &&
      continentalIsThereAGameInProgress(lobby)) ||
    (lobby.game.id === FIFTY_ONE_ID && fiftyOneIsThereAGameInProgress(lobby)) ||
    (lobby.game.id === POKER_TEXAS_HOLDEM_ID &&
      pokerTexasHoldemIsThereAGameInProgress(lobby)) ||
    (lobby.game.id === SOLITAIRE_KLONDIKE_ID &&
      solitaireKlondikeIsThereAGameInProgress(lobby))
  ) {
    return true;
  }
  return false;
};

const generateUniqueCode = (lobbies) => {
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  let code;

  do {
    code = "";
    for (let i = 0; i < 5; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      code += characters[randomIndex];
    }
  } while (lobbies[code]);

  return code;
};

const getCardGameFromDB = async (cardGameId) => {
  // Get the card game details from the database
  const result = await pool.query("SELECT * FROM card_games WHERE id = $1", [
    cardGameId,
  ]);

  return result.rows[0];
};

const getTargetNumPlayers = (cardGame, preferredNumPlayers) => {
  if (
    preferredNumPlayers &&
    preferredNumPlayers >= cardGame.min_num_players &&
    preferredNumPlayers <= cardGame.max_num_players
  ) {
    return preferredNumPlayers;
  }
  return cardGame.max_num_players;
};

const createLobby = (
  cardGame,
  matchType,
  player,
  lobbies,
  targetNumPlayers
) => {
  const newLobbyCode = generateUniqueCode(Object.keys(lobbies));
  return {
    code: newLobbyCode,
    game: cardGame,
    players: [player],
    isPublic: matchType === "public",
    userIds: new Set([player.userId]),
    targetNumPlayers: targetNumPlayers,
    replacements: {},
    lookingForReplacements: false,
  };
};

const getCopyOfTheLobbyOfAPlayer = (lobbies, socket) => {
  try {
    const lobby = JSON.parse(
      JSON.stringify(
        Object.values(lobbies).find((lobby) =>
          lobby.players.some((player) => player.id === socket.id)
        )
      )
    );
    return lobby;
  } catch (error) {
    console.error("Error copying the lobby of a player:", error);
    return null;
  }
};

module.exports = manageLobbies;
