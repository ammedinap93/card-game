const messageQueues = {};
const processingQueues = {};

const MAX_CHARS_PER_MESSAGE = 13;

module.exports = {
  processChatMessage: (lobby, socket, io, message) => {
    const player = lobby.players.find((p) => p.id === socket.id);
    if (!player) return;

    const splitMessages = splitMessage(message);

    if (!messageQueues[player.id]) {
      messageQueues[player.id] = [];
    }

    splitMessages.forEach((msg) => {
      messageQueues[player.id].push({
        message: msg,
        player: player,
      });
    });

    if (!processingQueues[player.id]) {
      processingQueues[player.id] = true;
      processQueuedMessages(lobby, io, player.id);
    }
  },
};

function splitMessage(message) {
  const words = message.split(" ");
  const result = [];
  let currentMessage = "";

  for (const word of words) {
    if (word.length > MAX_CHARS_PER_MESSAGE) {
      if (currentMessage) {
        result.push(currentMessage.trim());
      }
      break; // Skip the rest of the message
    }

    if ((currentMessage + " " + word).length <= MAX_CHARS_PER_MESSAGE) {
      currentMessage += (currentMessage ? " " : "") + word;
    } else {
      result.push(currentMessage.trim());
      currentMessage = word;
    }
  }

  if (currentMessage) {
    result.push(currentMessage.trim());
  }

  return result;
}

function emitMessage(lobby, io, chatMessage) {
  lobby.players.forEach((p) => {
    io.to(p.id).emit("chatMessage", chatMessage);
  });
}

function processQueuedMessages(lobby, io, playerId) {
  if (messageQueues[playerId] && messageQueues[playerId].length > 0) {
    const chatMessage = messageQueues[playerId].shift();
    emitMessage(lobby, io, chatMessage);

    setTimeout(() => {
      if (messageQueues[playerId].length > 0) {
        processQueuedMessages(lobby, io, playerId);
      } else {
        processingQueues[playerId] = false;
      }
    }, 4000);
  } else {
    processingQueues[playerId] = false;
  }
}
