let timers = {};

function startTimer(lobby, timerName, duration, onTick, onComplete) {
  stopTimer(lobby, timerName);

  const timer = {
    remainingTime: duration,
    isPaused: false,
    interval: setInterval(() => {
      if (!timer.isPaused) {
        timer.remainingTime--;
        onTick(timer.remainingTime);

        if (timer.remainingTime <= 0) {
          clearInterval(timer.interval);
          onComplete();
        }
      }
    }, 1000),
  };

  timers[lobby.code] = timers[lobby.code] || {};
  timers[lobby.code][timerName] = timer;
}

function stopTimer(lobby, timerName) {
  if (timers[lobby.code] && timers[lobby.code][timerName]) {
    clearInterval(timers[lobby.code][timerName].interval);
    delete timers[lobby.code][timerName];
  }
}

function pauseTimer(lobby, timerName) {
  if (timers[lobby.code] && timers[lobby.code][timerName]) {
    timers[lobby.code][timerName].isPaused = true;
  }
}

function resumeTimer(lobby, timerName) {
  if (timers[lobby.code] && timers[lobby.code][timerName]) {
    timers[lobby.code][timerName].isPaused = false;
  }
}

function getTimerValue(lobby, timerName) {
  if (timers[lobby.code] && timers[lobby.code][timerName]) {
    return timers[lobby.code][timerName].remainingTime;
  }
  return null;
}

function pauseTimers(lobby) {
  if (timers[lobby.code]) {
    Object.keys(timers[lobby.code]).forEach((timerName) => {
      pauseTimer(lobby, timerName);
    });
  }
}

function resumeTimers(lobby) {
  if (timers[lobby.code]) {
    Object.keys(timers[lobby.code]).forEach((timerName) => {
      resumeTimer(lobby, timerName);
    });
  }
}

function stopTimers(lobby) {
  if (timers[lobby.code]) {
    Object.keys(timers[lobby.code]).forEach((timerName) => {
      stopTimer(lobby, timerName);
    });
    delete timers[lobby.code];
  }
}

function incrementTimer(lobby, timerName, increment) {
  if (timers[lobby.code] && timers[lobby.code][timerName]) {
    timers[lobby.code][timerName].remainingTime += increment;
  }
}

module.exports = {
  startTimer,
  stopTimer,
  pauseTimer,
  resumeTimer,
  getTimerValue,
  pauseTimers,
  resumeTimers,
  stopTimers,
  incrementTimer,
};
