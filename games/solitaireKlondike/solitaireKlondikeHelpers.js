const { shuffle } = require("../../helpers");
const statsManager = require("../statsManager");
const {
  SOLITAIRE_KLONDIKE_GAME_STATUS,
  SOLITAIRE_KLONDIKE_CARD_RANKS,
  SOLITAIRE_KLONDIKE_PILES,
} = require("./solitaireKlondikeConstants");

function createNewGameState(lobby) {
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile without IDs first
  let stockPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
        isFaceUp: false,
      }))
    ),
  ];

  // Shuffle the deck pile
  stockPile = shuffle(stockPile);

  // Assign IDs after shuffling
  let cardId = 1;
  stockPile.forEach(card => {
    card.id = cardId++;
  });

  return {
    lock: false,
    paused: false,
    progressId: null,
    numberOfPlayers: 1,
    lobby: lobby,
    gameStatus: SOLITAIRE_KLONDIKE_GAME_STATUS.ready_to_deal,
    stockPile: stockPile,
    wastePile: [],
    foundationPiles: Array(4)
      .fill()
      .map(() => []),
    tableau: Array(7)
      .fill()
      .map(() => []),
    movingCard: null,
    numberOfMoves: 0,
    winnerPlayer: null,
  };
}

function solitaireKlondikeProcessUpdatedLobby(lobby, gameState) {
  const userIdsMatch =
    lobby.players.length === gameState.lobby.players.length &&
    lobby.players.every(
      (player) =>
        gameState.lobby.players.find((p) => p.userId === player.userId) !==
        undefined
    );

  if (userIdsMatch) {
    // Create a new game state object merging the info from the lobby and the old game state
    const newGameState = {
      ...gameState,
      lobby: lobby,
    };

    // Create a mapping of old socket IDs to new ones
    const idMapping = {};
    lobby.players.forEach((player) => {
      const oldPlayer = gameState.lobby.players.find(
        (p) => p.userId === player.userId
      );
      if (oldPlayer) {
        idMapping[oldPlayer.id] = player.id;
      }
    });

    if (gameState.winnerPlayer) {
      newGameState.winnerPlayer =
        idMapping[gameState.winnerPlayer] || gameState.winnerPlayer;
    }

    return newGameState;
  }
  return null;
}

async function endGame(gameState) {
  gameState.gameStatus = SOLITAIRE_KLONDIKE_GAME_STATUS.end_of_the_game;
  gameState.winnerPlayer = gameState.lobby.players[0].id;

  // Save game stats if enabled
  const isStatsEnabled = await statsManager.isStatsEnabled(gameState.lobby);
  if (isStatsEnabled) {
    const statsId = await statsManager.saveStats(gameState, gameState.numberOfMoves);
    if (statsId) {
      console.log(`Game stats saved with ID: ${statsId}`);
    } else {
      console.error("Failed to save game stats");
    }
  }

  return gameState;
}

function getRank(card) {
  return card.card.split("_")[0];
}

function getSuit(card) {
  return card.card.split("_")[1];
}

function isValidMove(gameState, move) {
  const { card, originPile, originIndex, targetPile, targetIndex } = move;

  if (targetPile === SOLITAIRE_KLONDIKE_PILES.tableau) {
    const targetTableau = gameState.tableau[targetIndex];
    if (targetTableau.length === 0) {
      // Only kings can be placed on empty tableau piles
      return getRank(card) === "king";
    } else {
      const targetCard = targetTableau[targetTableau.length - 1];
      return (
        isOppositeColor(card, targetCard) &&
        SOLITAIRE_KLONDIKE_CARD_RANKS.indexOf(getRank(card)) ===
          SOLITAIRE_KLONDIKE_CARD_RANKS.indexOf(getRank(targetCard)) - 1
      );
    }
  } else if (targetPile === SOLITAIRE_KLONDIKE_PILES.foundation) {
    const targetFoundation = gameState.foundationPiles[targetIndex];
    if (targetFoundation.length === 0) {
      // Only aces can be placed on empty foundation piles
      return getRank(card) === "ace";
    } else {
      const targetCard = targetFoundation[targetFoundation.length - 1];
      return (
        getSuit(card) === getSuit(targetCard) &&
        SOLITAIRE_KLONDIKE_CARD_RANKS.indexOf(getRank(card)) ===
          SOLITAIRE_KLONDIKE_CARD_RANKS.indexOf(getRank(targetCard)) + 1
      );
    }
  }

  return false;
}

function isOppositeColor(card1, card2) {
  const suit1 = getSuit(card1);
  const suit2 = getSuit(card2);
  return (
    (suit1 === "hearts" || suit1 === "diamonds") !==
    (suit2 === "hearts" || suit2 === "diamonds")
  );
}

function isGameWon(gameState) {
  return gameState.foundationPiles.every((pile) => pile.length === 13);
}

module.exports = {
  createNewGameState,
  solitaireKlondikeProcessUpdatedLobby,
  endGame,
  getRank,
  getSuit,
  isValidMove,
  isOppositeColor,
  isGameWon,
};
