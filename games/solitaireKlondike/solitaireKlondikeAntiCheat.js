const { getCopyOfAnObject } = require("../../helpers");

/**
 * Creates a protected copy of the game state where face-down cards have their rank and suit hidden
 * @param {Object} gameState - The current game state
 * @returns {Object} A protected copy of the game state
 */
function protectGameState(gameState) {
  // Create a deep copy of the game state
  let gameStateCopy = getCopyOfAnObject(gameState);

  // Helper function to protect a card's value if it's face down
  const protectCard = (card) => {
    if (!card.isFaceUp) {
      card.card = "?_?";
    }
    return card;
  };

  // Protect face-down cards in the stock pile
  gameStateCopy.stockPile = gameStateCopy.stockPile.map(protectCard);

  // Protect face-down cards in the tableau
  gameStateCopy.tableau = gameStateCopy.tableau.map(pile => 
    pile.map(protectCard)
  );

  // Note: We don't need to protect cards in waste pile or foundation piles
  // as they are always face up

  // If there's a moving card and it's face down, protect it
  if (gameStateCopy.movingCard && gameStateCopy.movingCard.card) {
    gameStateCopy.movingCard.card = protectCard(gameStateCopy.movingCard.card);
  }

  return gameStateCopy;
}

/**
 * Helper function to safely emit the protected game state to players
 * @param {Object} gameState - The current game state
 * @param {Object} io - Socket.io instance
 * @param {Array} players - Array of players to emit to
 */
function emitProtectedGameState(gameState, io, players) {
  const protectedGameState = protectGameState(gameState);
  players.forEach((player) => {
    io.to(player.id).emit("gameState", protectedGameState);
  });
}

module.exports = {
  protectGameState,
  emitProtectedGameState,
};
