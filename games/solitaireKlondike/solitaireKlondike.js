const { delay } = require("../../helpers");
const {
  createNewGameState,
  endGame,
  isValidMove,
  isGameWon,
  solitaireKlondikeProcessUpdatedLobby,
} = require("./solitaireKlondikeHelpers");
const {
  isProgressEnabled,
  findProgress,
  createProgress,
  updateProgress,
  deleteProgress,
} = require("../progressManager");
const statsManager = require("../statsManager");
const { emitProtectedGameState } = require("./solitaireKlondikeAntiCheat");
const {
  SOLITAIRE_KLONDIKE_GAME_STATUS,
  SOLITAIRE_KLONDIKE_PILES,
} = require("./solitaireKlondikeConstants");

let gameStateByLobby = {};
let deletePlayersFromLobby = null; // callback to remove the player from the lobby when the player wins.

async function lockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (!gameState) {
    return false;
  }
  while (gameState.lock) {
    await delay(Math.floor(Math.random() * 401) + 100);
  }
  gameState.lock = true;
  console.log("lock");
  return true;
}

function unlockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (gameState) {
    gameState.lock = false;
    console.log("unlock");
  }
}

function solitaireKlondikeIsThereAGameInProgress(lobby) {
  return gameStateByLobby[lobby.code] !== undefined;
}

async function solitaireKlondikeUpdateLobby(
  lobby,
  io,
  deletePlayersFromLobbyCallBack
) {
  deletePlayersFromLobby = deletePlayersFromLobbyCallBack;
  if (!solitaireKlondikeIsThereAGameInProgress(lobby)) {
    const lobbyCopy = JSON.parse(JSON.stringify(lobby));
    solitaireKlondikeGame(lobbyCopy, io);
  } else {
    const proceed = await lockGameState(lobby);
    if (!proceed) {
      return;
    }
    let gameState = gameStateByLobby[lobby.code];
    try {
      const realPlayers = lobby.players.filter((player) => !player.isBot);
      if (
        lobby.players.length < gameState.numberOfPlayers &&
        lobby.players.length > 0
      ) {
        gameState.paused = true;
        emitProtectedGameState(gameState, io, lobby.players);
      } else if (realPlayers.length === 0) {
        delete gameStateByLobby[lobby.code];
      } else if (lobby.players.length === gameState.numberOfPlayers) {
        const lobbyCopy = JSON.parse(JSON.stringify(lobby));
        const newGameState = solitaireKlondikeProcessUpdatedLobby(
          lobbyCopy,
          gameState
        );
        gameStateByLobby[lobby.code] = newGameState;
        gameState = gameStateByLobby[lobby.code];
        if (newGameState) {
          gameState.paused = false;
          await delay(3000);
          emitProtectedGameState(gameState, io, lobby.players);

          // Check if stats feature is enabled
          const statsEnabled = await statsManager.isStatsEnabled(lobby);
          if (statsEnabled) {
            const stats = await statsManager.getStats(lobby);
            if (stats) {
              lobby.players.forEach((player) => {
                io.to(player.id).emit("gameStats", stats);
              });
            }
          }
        } else {
          unlockGameState(lobby);
          await solitaireKlondikeGame(lobbyCopy, io);
        }
      }
    } finally {
      unlockGameState(lobby);
    }
  }
}

async function solitaireKlondikeGame(lobby, io) {
  await lockGameState(lobby);
  try {
    let gameState;
    const progressEnabled = await isProgressEnabled(lobby);

    if (progressEnabled) {
      const savedGameState = await findProgress(lobby);
      if (savedGameState) {
        console.log("Game progress found. Resuming game.");
        gameState = savedGameState;
      } else {
        console.log("No game progress found. Starting a new game.");
        gameState = createNewGameState(lobby);
        const progressId = await createProgress(gameState);
        if (progressId) {
          gameState.progressId = progressId;
        }
      }
    } else {
      console.log("Game progress disabled. Starting a new game.");
      gameState = createNewGameState(lobby);
    }

    gameState.paused = false;
    gameStateByLobby[lobby.code] = gameState;

    await delay(3000);
    emitProtectedGameState(gameState, io, lobby.players);

    // Check if stats feature is enabled
    const statsEnabled = await statsManager.isStatsEnabled(lobby);
    if (statsEnabled) {
      const stats = await statsManager.getStats(lobby);
      if (stats) {
        lobby.players.forEach((player) => {
          io.to(player.id).emit("gameStats", stats);
        });
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function solitaireKlondikeStartNewGame(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }

  try {
    // Check if there's an existing game in progress
    if (solitaireKlondikeIsThereAGameInProgress(lobby)) {
      // Delete the saved progress and the game state
      const gameState = gameStateByLobby[lobby.code];
      if (gameState.progressId) {
        await deleteProgress(gameState.progressId);
        console.log(`Deleted progress with ID: ${gameState.progressId}`);
        gameState.progressId = null;
      }
      delete gameStateByLobby[lobby.code];
    }

    // Start a new game
    await solitaireKlondikeGame(lobby, io);
  } catch (error) {
    console.error("Error in solitaireKlondikeStartNewGame:", error);
  } finally {
    unlockGameState(lobby);
  }
}

async function solitaireKlondikeDealCards(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.ready_to_deal
    ) {
      gameState.gameStatus = SOLITAIRE_KLONDIKE_GAME_STATUS.dealing_cards;
      gameState.tableau = Array(7)
        .fill()
        .map(() => []);

      for (let i = 0; i < 7; i++) {
        for (let j = i; j < 7; j++) {
          const card = gameState.stockPile.pop();
          card.isFaceUp = i === j;

          gameState.movingCard = {
            card: card,
            targetPile: SOLITAIRE_KLONDIKE_PILES.tableau,
            targetIndex: j,
          };

          gameState.tableau[j].push(card);
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);
        }
      }

      gameState.movingCard = null;
      gameState.gameStatus = SOLITAIRE_KLONDIKE_GAME_STATUS.player_turn;
      emitProtectedGameState(gameState, io, lobby.players);

      if (gameState.progressId) {
        await updateProgress(gameState.progressId, gameState);
      }
    }
  } catch (error) {
    console.error("Error in solitaireKlondikeDealCards:", error);
  } finally {
    unlockGameState(lobby);
  }
}

async function solitaireKlondikeDrawCard(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.player_turn
    ) {
      if (gameState.stockPile.length > 0) {
        const card = gameState.stockPile.pop();
        card.isFaceUp = true;
        gameState.wastePile.push(card);

        gameState.gameStatus = SOLITAIRE_KLONDIKE_GAME_STATUS.drawing_card;
        gameState.movingCard = {
          card: card,
          targetPile: SOLITAIRE_KLONDIKE_PILES.waste,
          targetIndex: null,
        };

        emitProtectedGameState(gameState, io, lobby.players);
        await delay(500);

        gameState.movingCard = null;
        gameState.gameStatus = SOLITAIRE_KLONDIKE_GAME_STATUS.player_turn;
        gameState.numberOfMoves++;

        emitProtectedGameState(gameState, io, lobby.players);

        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }
      } else if (
        gameState.stockPile.length === 0 &&
        gameState.wastePile.length > 0
      ) {
        gameState.stockPile = gameState.wastePile.reverse();
        gameState.wastePile = [];
        gameState.stockPile.forEach((card) => (card.isFaceUp = false));
        gameState.numberOfMoves++;

        emitProtectedGameState(gameState, io, lobby.players);

        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }
      }
    }
  } catch (error) {
    console.error("Error in solitaireKlondikeDrawCard:", error);
  } finally {
    unlockGameState(lobby);
  }
}

async function solitaireKlondikeMakeAMove(lobby, socket, io, move) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === SOLITAIRE_KLONDIKE_GAME_STATUS.player_turn
    ) {
      if (!isValidMove(gameState, move)) {
        console.log("Invalid move:", move);
        emitProtectedGameState(gameState, io, [socket]);
        return;
      }

      const { card, originPile, originIndex, targetPile, targetIndex } = move;

      // Perform the move
      let cardsToMove;
      if (originPile === SOLITAIRE_KLONDIKE_PILES.tableau) {
        const originTableau = gameState.tableau[originIndex];
        const cardIndex = originTableau.findIndex((c) => c.id === card.id);
        cardsToMove = originTableau.splice(cardIndex);
      } else if (originPile === SOLITAIRE_KLONDIKE_PILES.waste) {
        cardsToMove = [gameState.wastePile.pop()];
      } else if (originPile === SOLITAIRE_KLONDIKE_PILES.foundation) {
        cardsToMove = [gameState.foundationPiles[originIndex].pop()];
      }

      if (targetPile === SOLITAIRE_KLONDIKE_PILES.tableau) {
        gameState.tableau[targetIndex].push(...cardsToMove);
      } else if (targetPile === SOLITAIRE_KLONDIKE_PILES.foundation) {
        gameState.foundationPiles[targetIndex].push(...cardsToMove);
      }

      // If a card was moved from the tableau, flip the next card if it's face down
      if (
        originPile === SOLITAIRE_KLONDIKE_PILES.tableau &&
        gameState.tableau[originIndex].length > 0
      ) {
        const lastCard =
          gameState.tableau[originIndex][
            gameState.tableau[originIndex].length - 1
          ];
        if (!lastCard.isFaceUp) {
          lastCard.isFaceUp = true;
        }
      }

      // Increment the move counter
      gameState.numberOfMoves++;

      if (isGameWon(gameState)) {
        gameState = await endGame(gameState);
        emitProtectedGameState(gameState, io, lobby.players);

        deletePlayersFromLobby(lobby.code, [lobby.players[0].id]);

        if (gameState.progressId) {
          await deleteProgress(gameState.progressId);
          console.log(`Deleted progress with ID: ${gameState.progressId}`);
          gameState.progressId = null;
        }
        delete gameStateByLobby[lobby.code];
      } else {
        emitProtectedGameState(gameState, io, lobby.players);
        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }
      }
    } else {
      emitProtectedGameState(gameState, io, [socket]);
    }
  } catch (error) {
    console.error("Error in solitaireKlondikeMakeAMove:", error);
    console.log("Move:", move);
    emitProtectedGameState(gameStateByLobby[lobby.code], io, [socket]);
  } finally {
    unlockGameState(lobby);
  }
}

exports.solitaireKlondikeIsThereAGameInProgress =
  solitaireKlondikeIsThereAGameInProgress;
exports.solitaireKlondikeUpdateLobby = solitaireKlondikeUpdateLobby;
exports.solitaireKlondikeStartNewGame = solitaireKlondikeStartNewGame;
exports.solitaireKlondikeDealCards = solitaireKlondikeDealCards;
exports.solitaireKlondikeDrawCard = solitaireKlondikeDrawCard;
exports.solitaireKlondikeMakeAMove = solitaireKlondikeMakeAMove;
