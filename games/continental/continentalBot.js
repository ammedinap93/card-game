const { delayBetween } = require("../../helpers");
const {
  evaluateHand,
  findWorstCard,
  findValidCombinations,
  canDropOnExistingCombination,
  findCardToDropOnExistingCombination,
  findHighestScoringCard,
} = require("./continentalBotHelpers");
const { CONTINENTAL_GAME_STATUS } = require("./continentalConstants");

const botPlayContinental = async (gameState, botId) => {
  const botHasInitiallyDropped =
    gameState.playersCombinations[botId].length > 0;

  if (
    gameState.gameStatus === CONTINENTAL_GAME_STATUS.ready_to_deal &&
    gameState.dealer === botId
  ) {
    await delayBetween(3000, 5000);
    return { action: "dealCards" };
  } else if (
    gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(5000, 10000);
    const discardPileTopCard =
      gameState.discardPile[gameState.discardPile.length - 1];

    if (botHasInitiallyDropped) {
      const dropInfo = canDropOnExistingCombination(
        discardPileTopCard,
        gameState.playersCombinations
      );
      if (dropInfo) {
        return {
          action: "takeCard",
          fromDeckPile: false,
        };
      }
      // If can't drop, take from deck pile
      return {
        action: "takeCard",
        fromDeckPile: true,
      };
    }
    // If not initially dropped, use existing logic
    const botHand = gameState.playersHands[botId];
    const currentHandScore = evaluateHand(botHand, gameState.hand.goal);
    const potentialHandScore = evaluateHand(
      [...botHand, discardPileTopCard],
      gameState.hand.goal
    );
    const potentialCardToDiscard = findWorstCard(
      [...botHand, discardPileTopCard],
      gameState.hand.goal,
      gameState,
      botId
    );

    return {
      action: "takeCard",
      fromDeckPile:
        potentialHandScore <= currentHandScore ||
        potentialCardToDiscard.id === discardPileTopCard.id,
    };
  } else if (
    gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(8000, 12000);
    const botHand = gameState.playersHands[botId];

    if (botHasInitiallyDropped) {
      const dropInfo = findCardToDropOnExistingCombination(
        botHand,
        gameState.playersCombinations
      );
      if (dropInfo) {
        return {
          action: "dropCard",
          cardToDrop: dropInfo.card,
          targetCombinationKey: dropInfo.playerId,
          targetCombinationIndex: dropInfo.combinationIndex,
          addToStart: dropInfo.addToStart,
        };
      }
      // If no card can be dropped, discard the highest scoring card
      const cardToDiscard = findHighestScoringCard(botHand);
      return {
        action: "discardCard",
        cardId: cardToDiscard.id,
      };
    }
    // If not initially dropped, use existing logic
    if (gameState.fromDeckPile && gameState.roundNumber > 1) {
      const combinations = findValidCombinations(botHand, gameState.hand.goal);
      if (combinations) {
        return {
          action: "initialDroppingCards",
          combinations: combinations,
        };
      }
    }
    const cardToDiscard = findWorstCard(
      botHand,
      gameState.hand.goal,
      gameState,
      botId
    );
    return {
      action: "discardCard",
      cardId: cardToDiscard.id,
    };
  } else if (
    gameState.gameStatus ===
      CONTINENTAL_GAME_STATUS.waiting_for_picking_card_out_of_turn &&
    gameState.playerInTurn !== botId
  ) {
    await delayBetween(1000, 2000);
    if (!botHasInitiallyDropped) {
      const botHand = gameState.playersHands[botId];
      const discardPileTopCard =
        gameState.discardPile[gameState.discardPile.length - 1];
      const currentHandScore = evaluateHand(botHand, gameState.hand.goal);
      const potentialHandScore = evaluateHand(
        [...botHand, discardPileTopCard],
        gameState.hand.goal
      );

      if (potentialHandScore > currentHandScore) {
        return { action: "pickCardOutOfTurn" };
      }
    }
  } else if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand) {
    await delayBetween(8000, 15000);
    return { action: "readyToContinue" };
  }

  return null;
};

module.exports = { botPlayContinental };
