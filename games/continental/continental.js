const {
  CONTINENTAL_ID,
  PUBLIC_PAUSE_TIMER,
  PRIVATE_PAUSE_TIMER,
} = require("../../constants");
const { delay } = require("../../helpers");
const {
  createNewGameState,
  endHand,
  startNextHand,
  continentalProcessUpdatedLobby,
} = require("./continentalHelpers");
const { initialDroppingCards, dropCard } = require("./droppingCards");
const {
  isProgressEnabled,
  findProgress,
  createProgress,
  updateProgress,
  deleteProgress,
} = require("../progressManager");
const {
  startTimer,
  stopTimer,
  pauseTimers,
  resumeTimers,
  stopTimers,
  incrementTimer,
  resumeTimer,
} = require("../timerManager");
const statsManager = require("../statsManager");
const { botNextAction } = require("../botLogic");
const {
  CONTINENTAL_GAME_STATUS,
  CONTINENTAL_TIMERS,
} = require("./continentalConstants");
const {
  emitProtectedGameState,
  protectGameStateForPlayer,
} = require("./continentalAntiCheat");

let gameStateByLobby = {};
let pausedGamesByUser = new Map(); // userId -> lobbyCode
let replacePlayersInLobby = null; // callback to replace disconnected players
let deletePlayersFromLobby = null; // callback to remove players when aborting a private game

async function lockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (!gameState) {
    return false;
  }
  while (gameState.lock) {
    await delay(Math.floor(Math.random() * 401) + 100);
  }
  gameState.lock = true;
  console.log("lock");
  return true;
}

function unlockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (gameState) {
    gameState.lock = false;
    console.log("unlock");
  }
}

function continentalIsThereAGameInProgress(lobby) {
  return gameStateByLobby[lobby.code] !== undefined;
}

function continentalAddDisconnectedUser(userId, lobby, io) {
  pausedGamesByUser.set(userId, lobby.code);

  // Set a timeout to replace the player
  startGamePausedTimer(lobby, io);
}

function continentalRemoveDisconnectedUser(userId) {
  pausedGamesByUser.delete(userId);
}

function continentalFindPausedGameLobbyCode(userId) {
  return pausedGamesByUser.get(userId);
}

function continentalIsUserInPausedGame(userId) {
  return pausedGamesByUser.has(userId);
}

async function continentalUpdateLobby(
  lobby,
  io,
  deletePlayersFromLobbyCallBack,
  replacePlayersInLobbyCallBack,
  removeReplacementsInLobbyCallBack
) {
  deletePlayersFromLobby = deletePlayersFromLobbyCallBack;
  replacePlayersInLobby = replacePlayersInLobbyCallBack;
  if (!continentalIsThereAGameInProgress(lobby)) {
    const lobbyCopy = JSON.parse(JSON.stringify(lobby));
    continentalGame(lobbyCopy, io);
  } else {
    const proceed = await lockGameState(lobby);
    if (!proceed) {
      return;
    }
    let gameState = gameStateByLobby[lobby.code];
    try {
      const realPlayers = lobby.players.filter((player) => !player.isBot);
      if (
        lobby.players.length < gameState.initialNumberOfPlayers &&
        lobby.players.length > 0
      ) {
        gameState.paused = true;

        // Pause all timers for this lobby (except the Game Paused timer)
        pauseTimers(lobby);
        resumeTimer(lobby, "gamePaused");

        emitProtectedGameState(gameState, io, lobby.players);
      } else if (realPlayers.length === 0) {
        stopTimers(lobby); // Stop all timers
        delete gameStateByLobby[lobby.code];
      } else if (lobby.players.length === gameState.initialNumberOfPlayers) {
        const lobbyCopy = JSON.parse(JSON.stringify(lobby));
        const newGameState = continentalProcessUpdatedLobby(
          lobbyCopy,
          gameState
        );
        if (newGameState) {
          // Get the list of successfully replaced user IDs
          const replacedUserIds = [];
          for (const disconnectedUserId in lobbyCopy.replacements) {
            if (lobbyCopy.replacements[disconnectedUserId] !== null) {
              replacedUserIds.push(disconnectedUserId);
            }
          }

          // Remove processed replacements from the lobby
          if (replacedUserIds.length > 0) {
            removeReplacementsInLobbyCallBack(lobby.code, replacedUserIds);
          }

          gameStateByLobby[lobby.code] = newGameState;
          gameState = gameStateByLobby[lobby.code];
          gameState.paused = false;

          await delay(3000); // Wait for the client to set the scene
          emitProtectedGameState(gameState, io, lobbyCopy.players);

          if (
            gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand
          ) {
            incrementTimer(lobby, "summary", 5); // Increase summary timer by 5 seconds
          }

          // Resume all timers for this lobby
          resumeTimers(lobby);

          // Check if stats feature is enabled
          const statsEnabled = await statsManager.isStatsEnabled(lobby);

          if (statsEnabled) {
            // Get players' wins
            const wins = await statsManager.getWins(lobby);

            if (wins) {
              // Emit gameStats event to all players in the lobby
              lobby.players.forEach((player) => {
                io.to(player.id).emit("gameStats", wins);
              });
            }
          }

          performBotsActions(lobby, io);
        } else {
          unlockGameState(lobby);
          continentalGame(lobbyCopy, io);
        }
      }
    } finally {
      unlockGameState(lobby);
    }
  }
}

async function continentalGame(lobby, io) {
  await lockGameState(lobby);
  try {
    let gameState;

    // Check if progress is enabled for this lobby
    const progressEnabled = await isProgressEnabled(lobby);

    if (progressEnabled) {
      // Find game progress
      const savedGameState = await findProgress(lobby);
      if (savedGameState) {
        console.log("Game progress found. Resuming game.");
        gameState = savedGameState;
      } else {
        console.log("No game progress found. Starting a new game.");
        gameState = createNewGameState(lobby);

        // Save the new game state
        const progressId = await createProgress(gameState);
        if (progressId) {
          gameState.progressId = progressId;
        }
      }
    } else {
      console.log("Game progress disabled. Starting a new game.");
      gameState = createNewGameState(lobby);
    }

    gameState.paused = false;

    gameStateByLobby[lobby.code] = gameState;

    await delay(3000); // Wait for the client to set the new scene

    // Emit the game state to all players in the lobby
    emitProtectedGameState(gameState, io, lobby.players);

    // Check if stats feature is enabled
    const statsEnabled = await statsManager.isStatsEnabled(lobby);

    if (statsEnabled) {
      // Get players' wins
      const wins = await statsManager.getWins(lobby);

      if (wins) {
        // Emit gameStats event to all players in the lobby
        lobby.players.forEach((player) => {
          io.to(player.id).emit("gameStats", wins);
        });
      }
    }

    // Check the game status and start the correspondent timer
    if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.ready_to_deal) {
      // Start the dealer timer
      startDealerTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn) {
      // Start the player's turn timer
      startPlayerTurnTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn_in_progress
    ) {
      // Start the player's turn in progress timer
      startPlayerTurnInProgressTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand
    ) {
      // Start the summary timer
      startSummaryTimer(lobby, io);
      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function continentalDealCards(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.ready_to_deal &&
      gameState.dealer === socket.id
    ) {
      // Stop the dealer timer
      stopTimer(lobby, "dealer");

      const cardsPerPlayer = gameState.hand.cardsToBeDealt;
      gameState.gameStatus = CONTINENTAL_GAME_STATUS.dealing_cards;

      const dealCardsToPlayers = async () => {
        const players = gameState.distributionOfSeats;
        const startIndex =
          (players.indexOf(gameState.dealer) + 1) % players.length;

        for (let i = 0; i < cardsPerPlayer; i++) {
          for (let j = 0; j < players.length; j++) {
            const playerIndex = (startIndex + j) % players.length;
            const playerId = players[playerIndex];
            const card = gameState.deckPile.pop();
            gameState.playersHands[playerId].push(card);

            // Set the dealingCard property in the game state
            gameState.dealingCard = {
              ...card,
              playerId,
            };

            emitProtectedGameState(gameState, io, lobby.players);
            await delay(500);
          }
        }

        // Move the top card from the deck pile to the discard pile
        const topCard = gameState.deckPile.pop();
        gameState.discardPile.push(topCard);
        gameState.discardingCard = {
          ...topCard,
          playerId: null,
        };
        gameState.dealingCard = null;
        gameState.gameStatus =
          CONTINENTAL_GAME_STATUS.discarding_from_deck_pile;
        emitProtectedGameState(gameState, io, lobby.players);
        await delay(500);

        gameState.gameStatus = CONTINENTAL_GAME_STATUS.player_turn;
        gameState.playerInTurn = players[startIndex];
        gameState.roundNumber++;
        gameState.discardingCard = null;

        // Save progress if enabled
        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }

        emitProtectedGameState(gameState, io, lobby.players);

        // Start the next player's turn
        startPlayerTurnTimer(lobby, io);
        performBotsActions(lobby, io);
      };

      await dealCardsToPlayers();
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function continentalMoveCard(lobby, socket, io, cardId, newIndex) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (!gameState.paused) {
      const playerId = socket.id;
      const playerHand = gameState.playersHands[playerId];

      // Find the card in the player's hand
      const cardIndex = playerHand.findIndex((card) => card.id === cardId);

      if (cardIndex !== -1) {
        // Remove the card from its current position
        const [card] = playerHand.splice(cardIndex, 1);

        // Insert the card at the new index
        playerHand.splice(newIndex, 0, card);

        // Emit the updated game state to all players
        emitProtectedGameState(gameState, io, lobby.players);
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function continentalTakeCard(lobby, socket, io, fromDeckPile) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;
    if (
      !gameState.paused &&
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn &&
      gameState.playerInTurn === playerId
    ) {
      // Stop the player turn timer
      stopTimer(lobby, "playerTurn");

      const pile = fromDeckPile ? gameState.deckPile : gameState.discardPile;
      const status = fromDeckPile
        ? CONTINENTAL_GAME_STATUS.taking_from_deck_pile
        : CONTINENTAL_GAME_STATUS.taking_from_discard_pile;

      if (pile.length > 0) {
        gameState.gameStatus = status;
        const card = pile.pop();
        gameState.playersHands[playerId].push(card);
        gameState.fromDeckPile = fromDeckPile;
        gameState.lastPickedCard = card; // Update the last picked card

        // Save progress if enabled
        if (gameState.progressId) {
          gameState.gameStatus =
            CONTINENTAL_GAME_STATUS.player_turn_in_progress;
          await updateProgress(gameState.progressId, gameState);
          gameState.gameStatus = status;
        }

        gameState.dealingCard = {
          ...card,
          playerId,
        };
        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for a brief moment to show the animation
        setTimeout(async () => {
          try {
            gameState.dealingCard = null;
            if (fromDeckPile) {
              gameState.gameStatus =
                CONTINENTAL_GAME_STATUS.waiting_for_picking_card_out_of_turn;
              gameState.waitingForPickingCardOutOfTurn = [];

              startPickCardOutOfTurnTimer(lobby, io);
              performBotsActions(lobby, io);

              unlockGameState(lobby);
            } else {
              gameState.gameStatus =
                CONTINENTAL_GAME_STATUS.player_turn_in_progress;

              // Save progress if enabled
              if (gameState.progressId) {
                await updateProgress(gameState.progressId, gameState);
              }

              // Start the player's turn in progress timer
              startPlayerTurnInProgressTimer(lobby, io);
              performBotsActions(lobby, io);

              unlockGameState(lobby);
            }
            emitProtectedGameState(gameState, io, lobby.players);
          } catch (error) {
            unlockGameState(lobby);
          }
        }, 500);
      } else {
        unlockGameState(lobby);
      }
    } else {
      unlockGameState(lobby);
    }
  } catch (error) {
    unlockGameState(lobby);
  }
}

async function continentalPickCardOutOfTurn(
  lobby,
  socket,
  io,
  selectedPlayerId
) {
  let gameState = gameStateByLobby[lobby.code];
  if (gameState && (!gameState.paused || selectedPlayerId)) {
    const playerId = selectedPlayerId || socket.id;

    if (
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.waiting_for_picking_card_out_of_turn &&
      gameState.playerInTurn !== playerId &&
      gameState.discardPile.length > 0 &&
      !gameState.discardPile[gameState.discardPile.length - 1].isDead &&
      !gameState.waitingForPickingCardOutOfTurn.includes(playerId)
    ) {
      gameState.waitingForPickingCardOutOfTurn.push(playerId);
      emitProtectedGameState(gameState, io, lobby.players);
    } else if (
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn !== playerId &&
      gameState.discardPile.length > 0 &&
      !gameState.discardPile[gameState.discardPile.length - 1].isDead
    ) {
      const proceed = await lockGameState(lobby);
      if (!proceed) {
        return;
      }
      try {
        gameState = gameStateByLobby[lobby.code];
        gameState.gameStatus = CONTINENTAL_GAME_STATUS.taking_from_discard_pile;
        const topCard = gameState.discardPile.pop();
        gameState.playersHands[playerId].push(topCard);
        gameState.dealingCard = {
          ...topCard,
          playerId,
        };
        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for 500 milliseconds to show the animation
        setTimeout(async () => {
          gameState = gameStateByLobby[lobby.code];
          gameState.dealingCard = null;
          gameState.gameStatus =
            CONTINENTAL_GAME_STATUS.player_turn_in_progress;

          // Save progress if enabled
          if (gameState.progressId) {
            await updateProgress(gameState.progressId, gameState);
          }

          emitProtectedGameState(gameState, io, lobby.players);

          unlockGameState(lobby);
        }, 500);
      } catch (error) {
        unlockGameState(lobby);
      }
    }
  }
}

async function continentalDiscardCard(lobby, socket, io, cardId) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;
    if (
      !gameState.paused &&
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === playerId
    ) {
      const playerHand = gameState.playersHands[playerId];
      const cardIndex = playerHand.findIndex((card) => card.id === cardId);

      if (cardIndex !== -1) {
        // Stop the player turn in progress timer
        stopTimer(lobby, "playerTurnInProgress");

        const discardCard = async () => {
          const [discardedCard] = playerHand.splice(cardIndex, 1);

          // Mark the top card of the discard pile as dead
          if (gameState.discardPile.length > 0) {
            gameState.discardPile[
              gameState.discardPile.length - 1
            ].isDead = true;
          }

          // Add the discarded card to the discard pile
          gameState.discardPile.push(discardedCard);

          gameState.discardingCard = {
            ...discardedCard,
            playerId,
          };
          gameState.gameStatus = CONTINENTAL_GAME_STATUS.discarding_from_player;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);

          gameState.discardingCard = null;

          if (playerHand.length === 0 || gameState.deckPile.length === 0) {
            const updatedGameState = await endHand(gameState);
            if (updatedGameState) {
              // The hand has ended, use the updated game state
              gameState = updatedGameState;

              if (
                gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand
              ) {
                // Start the summary timer
                startSummaryTimer(lobby, io);
                performBotsActions(lobby, io);
              }
            }
          } else {
            // Move the turn to the next player
            const currentPlayerIndex =
              gameState.distributionOfSeats.indexOf(playerId);
            const nextPlayerIndex =
              (currentPlayerIndex + 1) % gameState.distributionOfSeats.length;
            gameState.playerInTurn =
              gameState.distributionOfSeats[nextPlayerIndex];

            const startIndex =
              (gameState.distributionOfSeats.indexOf(gameState.dealer) + 1) %
              gameState.distributionOfSeats.length;
            if (nextPlayerIndex === startIndex) {
              gameState.roundNumber++;
            }

            // Update the game status back to player_turn
            gameState.gameStatus = CONTINENTAL_GAME_STATUS.player_turn;

            // Start the next player's turn
            startPlayerTurnTimer(lobby, io);
            performBotsActions(lobby, io);
          }

          // Save progress if enabled and game status is not end_of_the_game
          if (
            gameState.progressId &&
            gameState.gameStatus !== CONTINENTAL_GAME_STATUS.end_of_the_game
          ) {
            await updateProgress(gameState.progressId, gameState);
          }

          // Emit the updated game state to all players
          emitProtectedGameState(gameState, io, lobby.players);

          if (
            gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_game
          ) {
            // Check if stats feature is enabled
            const statsEnabled = await statsManager.isStatsEnabled(lobby);

            if (statsEnabled) {
              // Get players' wins
              const wins = await statsManager.getWins(lobby);

              if (wins) {
                // Emit gameStats event to all players in the lobby
                lobby.players.forEach((player) => {
                  io.to(player.id).emit("gameStats", wins);
                });
              }
            }

            // Delete the saved progress and the game state if the game has ended
            if (gameState.progressId) {
              await deleteProgress(gameState.progressId);
              console.log(`Deleted progress with ID: ${gameState.progressId}`);
              gameState.progressId = null;
            }
            stopTimers(lobby); // Stop all timers
            delete gameStateByLobby[lobby.code];
          }
        };

        discardCard();
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function continentalInitialDroppingCards(
  lobby,
  socket,
  io,
  combinations
) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;
    if (
      !gameState.paused &&
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === playerId &&
      gameState.fromDeckPile &&
      gameState.roundNumber > 1
    ) {
      const playerHand = gameState.playersHands[playerId];
      const goal = gameState.hand.goal;

      const updatedHand = initialDroppingCards(combinations, goal, playerHand);

      if (updatedHand !== null) {
        // Track the initial drop round if it hasn't been set yet
        if (gameState.playersInitialDropRound[playerId] === null) {
          gameState.playersInitialDropRound[playerId] = gameState.roundNumber;
        }

        gameState.playersHands[playerId] = updatedHand;
        gameState.playersCombinations[playerId] = combinations;

        if (updatedHand.length === 0) {
          const updatedGameState = await endHand(gameState);
          if (updatedGameState) {
            // The hand has ended, use the updated game state
            gameState = updatedGameState;

            // Stop the player turn in progress timer
            stopTimer(lobby, "playerTurnInProgress");

            if (
              gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand
            ) {
              // Start the summary timer
              startSummaryTimer(lobby, io);
              performBotsActions(lobby, io);
            }
          }
        } else {
          // Start the player's turn in progress timer
          startPlayerTurnInProgressTimer(lobby, io);
          performBotsActions(lobby, io);
        }

        // Save progress if enabled and game status is not end_of_the_game
        if (
          gameState.progressId &&
          gameState.gameStatus !== CONTINENTAL_GAME_STATUS.end_of_the_game
        ) {
          await updateProgress(gameState.progressId, gameState);
        }

        // Emit the updated game state to all players
        emitProtectedGameState(gameState, io, lobby.players);

        if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_game) {
          // Check if stats feature is enabled
          const statsEnabled = await statsManager.isStatsEnabled(lobby);

          if (statsEnabled) {
            // Get players' wins
            const wins = await statsManager.getWins(lobby);

            if (wins) {
              // Emit gameStats event to all players in the lobby
              lobby.players.forEach((player) => {
                io.to(player.id).emit("gameStats", wins);
              });
            }
          }

          // Delete the saved progress and the game state if the game has ended
          if (gameState.progressId) {
            await deleteProgress(gameState.progressId);
            console.log(`Deleted progress with ID: ${gameState.progressId}`);
            gameState.progressId = null;
          }
          stopTimers(lobby); // Stop all timers
          delete gameStateByLobby[lobby.code];
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function continentalDropCard(
  lobby,
  socket,
  io,
  cardToDrop,
  targetCombinationKey,
  targetCombinationIndex,
  addToStart
) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;
    if (
      !gameState.paused &&
      gameState.gameStatus ===
        CONTINENTAL_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === playerId &&
      gameState.playersCombinations[playerId] &&
      gameState.playersCombinations[playerId].length > 0
    ) {
      const playerHand = gameState.playersHands[playerId];
      const targetCombination =
        gameState.playersCombinations[targetCombinationKey][
          targetCombinationIndex
        ];

      const result = dropCard(
        cardToDrop,
        targetCombination,
        playerHand,
        addToStart
      );

      if (result) {
        gameState.playersHands[playerId] = result.newPlayerHand;
        gameState.playersCombinations[targetCombinationKey][
          targetCombinationIndex
        ] = result.newCombination;

        // Store the dropped card information in the droppingCard property
        gameState.droppingCard = {
          card: cardToDrop,
          targetCombinationKey,
          targetCombinationIndex,
          addToStart,
        };
        gameState.gameStatus = CONTINENTAL_GAME_STATUS.dropping_card;
        // Emit the updated game state to all players
        emitProtectedGameState(gameState, io, lobby.players);
        await delay(500);

        gameState.droppingCard = null;
        gameState.gameStatus = CONTINENTAL_GAME_STATUS.player_turn_in_progress;

        if (gameState.playersHands[playerId].length === 0) {
          const updatedGameState = await endHand(gameState);
          if (updatedGameState) {
            // The hand has ended, use the updated game state
            gameState = updatedGameState;

            // Stop the player turn in progress timer
            stopTimer(lobby, "playerTurnInProgress");

            if (
              gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand
            ) {
              // Start the summary timer
              startSummaryTimer(lobby, io);
              performBotsActions(lobby, io);
            }
          }
        } else {
          // Start the player's turn in progress timer
          startPlayerTurnInProgressTimer(lobby, io);
          performBotsActions(lobby, io);
        }

        // Save progress if enabled and game status is not end_of_the_game
        if (
          gameState.progressId &&
          gameState.gameStatus !== CONTINENTAL_GAME_STATUS.end_of_the_game
        ) {
          await updateProgress(gameState.progressId, gameState);
        }

        // Emit the updated game state to all players
        emitProtectedGameState(gameState, io, lobby.players);

        if (gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_game) {
          // Check if stats feature is enabled
          const statsEnabled = await statsManager.isStatsEnabled(lobby);

          if (statsEnabled) {
            // Get players' wins
            const wins = await statsManager.getWins(lobby);

            if (wins) {
              // Emit gameStats event to all players in the lobby
              lobby.players.forEach((player) => {
                io.to(player.id).emit("gameStats", wins);
              });
            }
          }

          // Delete the saved progress and the game state if the game has ended
          if (gameState.progressId) {
            await deleteProgress(gameState.progressId);
            console.log(`Deleted progress with ID: ${gameState.progressId}`);
            gameState.progressId = null;
          }
          stopTimers(lobby); // Stop all timers
          delete gameStateByLobby[lobby.code];
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function continentalReadyToContinue(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;

    if (
      !gameState.paused &&
      gameState.gameStatus === CONTINENTAL_GAME_STATUS.end_of_the_hand
    ) {
      gameState.playersReadyToContinue[playerId] = true;

      // Emit the updated game state to all players
      emitProtectedGameState(gameState, io, lobby.players);

      const allPlayersReady = Object.values(
        gameState.playersReadyToContinue
      ).every((ready) => ready);

      if (allPlayersReady) {
        // Stop the summary timer
        stopTimer(lobby, "summary");

        const newGameState = startNextHand(gameState);
        if (newGameState) {
          gameStateByLobby[lobby.code] = newGameState;
          gameState = newGameState;

          // Save progress if enabled
          if (gameState.progressId) {
            await updateProgress(gameState.progressId, gameState);
          }

          await delay(3000);

          // Emit the updated game state to all players
          emitProtectedGameState(gameState, io, lobby.players);

          // Start the dealer timer
          startDealerTimer(lobby, io);
          performBotsActions(lobby, io);
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function processPickingCardOutOfTurn(lobby, io, gameState) {
  try {
    let selectedPlayerId;
    if (gameState.waitingForPickingCardOutOfTurn.length === 1) {
      selectedPlayerId = gameState.waitingForPickingCardOutOfTurn[0];
    } else {
      // Find the player whose turn is closest to the current player
      const playerTurnIndex = gameState.distributionOfSeats.indexOf(
        gameState.playerInTurn
      );
      const nextTurnIndices = gameState.distributionOfSeats
        .slice(playerTurnIndex + 1)
        .concat(gameState.distributionOfSeats.slice(0, playerTurnIndex));

      selectedPlayerId = nextTurnIndices.find((playerId) =>
        gameState.waitingForPickingCardOutOfTurn.includes(playerId)
      );
    }
    gameState.waitingForPickingCardOutOfTurn = [];
    gameState.gameStatus = CONTINENTAL_GAME_STATUS.player_turn_in_progress;

    // Start the player's turn in progress timer
    startPlayerTurnInProgressTimer(lobby, io);
    performBotsActions(lobby, io);

    unlockGameState(lobby);
    continentalPickCardOutOfTurn(
      lobby,
      { id: selectedPlayerId },
      io,
      selectedPlayerId
    );
  } catch {
    unlockGameState(lobby);
  }
}

async function startDealerTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "dealer",
      remainingTime: CONTINENTAL_TIMERS.dealer,
    });
  });
  startTimer(
    lobby,
    "dealer",
    CONTINENTAL_TIMERS.dealer,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "dealer",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      // Time's up, start dealing cards automatically
      if (
        currentGameState &&
        currentGameState.gameStatus === CONTINENTAL_GAME_STATUS.ready_to_deal
      ) {
        continentalDealCards(currentLobby, { id: currentGameState.dealer }, io);
      }
    }
  );
}

async function startPlayerTurnTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "playerTurn",
      remainingTime: CONTINENTAL_TIMERS.playerTurn,
    });
  });
  startTimer(
    lobby,
    "playerTurn",
    CONTINENTAL_TIMERS.playerTurn,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "playerTurn",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;

      // Time's up, pick from deck pile by default
      if (
        currentGameState &&
        currentGameState.gameStatus === CONTINENTAL_GAME_STATUS.player_turn
      ) {
        continentalTakeCard(
          currentLobby,
          { id: currentGameState.playerInTurn },
          io,
          true
        );
      }
    }
  );
}

async function startPlayerTurnInProgressTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "playerTurnInProgress",
      remainingTime: CONTINENTAL_TIMERS.playerTurnInProgress,
    });
  });
  startTimer(
    lobby,
    "playerTurnInProgress",
    CONTINENTAL_TIMERS.playerTurnInProgress,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "playerTurnInProgress",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      // Time's up, discard the last picked card by default
      if (
        currentGameState &&
        currentGameState.gameStatus ===
          CONTINENTAL_GAME_STATUS.player_turn_in_progress
      ) {
        const playerId = currentGameState.playerInTurn;
        let lastPickedCard = currentGameState.lastPickedCard;

        if (lastPickedCard) {
          const playerHand = currentGameState.playersHands[playerId];
          const cardIndex = playerHand.findIndex(
            (card) => card.id === lastPickedCard.id
          );

          if (cardIndex !== -1) {
            continentalDiscardCard(
              currentLobby,
              { id: playerId },
              io,
              lastPickedCard.id
            );
          } else {
            lastPickedCard = null;
          }
        }
        if (!lastPickedCard) {
          // If no card was picked (shouldn't happen normally) or the card is not in the player's hand, discard the last card in hand
          const playerHand = currentGameState.playersHands[playerId];
          if (playerHand.length > 0) {
            continentalDiscardCard(
              currentLobby,
              { id: playerId },
              io,
              playerHand[playerHand.length - 1].id
            );
          }
        }
      }
    }
  );
}

async function startPickCardOutOfTurnTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "pickCardOutOfTurn",
      remainingTime: CONTINENTAL_TIMERS.pickCardOutOfTurn,
    });
  });
  startTimer(
    lobby,
    "pickCardOutOfTurn",
    CONTINENTAL_TIMERS.pickCardOutOfTurn,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "pickCardOutOfTurn",
          remainingTime,
        });
      });
    },
    async () => {
      const proceed = await lockGameState(lobby);
      if (!proceed) {
        return;
      }
      try {
        let currentGameState = gameStateByLobby[lobby.code];
        let currentLobby = currentGameState.lobby;
        if (currentGameState.waitingForPickingCardOutOfTurn.length > 0) {
          currentGameState.gameStatus =
            CONTINENTAL_GAME_STATUS.processing_picking_card_out_of_turn;

          emitProtectedGameState(currentGameState, io, currentLobby.players);

          processPickingCardOutOfTurn(currentLobby, io, currentGameState);
        } else {
          currentGameState.gameStatus =
            CONTINENTAL_GAME_STATUS.player_turn_in_progress;

          // Save progress if enabled
          if (currentGameState.progressId) {
            await updateProgress(currentGameState.progressId, currentGameState);
          }

          emitProtectedGameState(currentGameState, io, currentLobby.players);

          // Start the player's turn in progress timer
          startPlayerTurnInProgressTimer(currentLobby, io);
          performBotsActions(currentLobby, io);

          unlockGameState(currentLobby);
        }
      } catch (error) {
        unlockGameState(lobby);
      }
    }
  );
}

async function startSummaryTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "summary",
      remainingTime: CONTINENTAL_TIMERS.summary,
    });
  });
  startTimer(
    lobby,
    "summary",
    CONTINENTAL_TIMERS.summary,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "summary",
          remainingTime,
        });
      });
    },
    async () => {
      const proceed = await lockGameState(lobby);
      if (!proceed) {
        return;
      }
      try {
        let currentGameState = gameStateByLobby[lobby.code];
        let currentLobby = currentGameState.lobby;
        if (
          !currentGameState.paused &&
          currentGameState.gameStatus ===
            CONTINENTAL_GAME_STATUS.end_of_the_hand
        ) {
          const newGameState = startNextHand(currentGameState);
          if (newGameState) {
            gameStateByLobby[lobby.code] = newGameState;
            currentGameState = newGameState;
            currentLobby = currentGameState.lobby;

            // Save progress if enabled
            if (currentGameState.progressId) {
              await updateProgress(
                currentGameState.progressId,
                currentGameState
              );
            }

            emitProtectedGameState(currentGameState, io, currentLobby.players);

            // Start the dealer timer
            startDealerTimer(currentLobby, io);
            performBotsActions(currentLobby, io);
          }
        }
      } finally {
        unlockGameState(lobby);
      }
    }
  );
}

async function startGamePausedTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "gamePaused",
      remainingTime: lobby.isPublic ? PUBLIC_PAUSE_TIMER : PRIVATE_PAUSE_TIMER,
    });
  });

  startTimer(
    lobby,
    "gamePaused",
    lobby.isPublic ? PUBLIC_PAUSE_TIMER : PRIVATE_PAUSE_TIMER,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "gamePaused",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;

      if (currentGameState && currentGameState.paused) {
        if (currentLobby.isPublic) {
          // Get disconnected user IDs from the pausedGamesByUser map
          const disconnectedUserIds = [];
          for (const [userId, lobbyCode] of pausedGamesByUser.entries()) {
            if (lobbyCode === currentLobby.code) {
              disconnectedUserIds.push(userId);
            }
          }

          // Call replacePlayersInLobby with the disconnected user IDs
          replacePlayersInLobby(currentLobby.code, disconnectedUserIds);
        } else {
          // Abort the game for private lobbies
          const playerIds = currentLobby.players.map((player) => player.id);
          deletePlayersFromLobby(currentLobby.code, playerIds);
        }
      }
    }
  );
}

async function performBotsActions(lobby, io) {
  const botPlayers = lobby.players.filter((player) => player.isBot);
  for (const botPlayer of botPlayers) {
    performBotAction(lobby, io, botPlayer);
  }
}

async function performBotAction(lobby, io, botPlayer) {
  const gameState = gameStateByLobby[lobby.code];
  const protectedGameState = protectGameStateForPlayer(gameState, botPlayer.id);

  const botAction = await botNextAction(
    protectedGameState, // Pass the protected game state
    botPlayer.id,
    CONTINENTAL_ID
  );
  if (botAction) {
    switch (botAction.action) {
      case "dealCards":
        if (gameState.dealer === botPlayer.id) {
          continentalDealCards(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "takeCard":
        if (gameState.playerInTurn === botPlayer.id) {
          continentalTakeCard(
            lobby,
            { id: botPlayer.id },
            io,
            botAction.fromDeckPile
          );
        }
        break;
      case "discardCard":
        if (gameState.playerInTurn === botPlayer.id) {
          continentalDiscardCard(
            lobby,
            { id: botPlayer.id },
            io,
            botAction.cardId
          );
        }
        break;
      case "pickCardOutOfTurn":
        if (gameState.playerInTurn !== botPlayer.id) {
          continentalPickCardOutOfTurn(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "initialDroppingCards":
        if (gameState.playerInTurn === botPlayer.id) {
          continentalInitialDroppingCards(
            lobby,
            { id: botPlayer.id },
            io,
            botAction.combinations
          );
        }
        break;
      case "dropCard":
        if (gameState.playerInTurn === botPlayer.id) {
          continentalDropCard(
            lobby,
            { id: botPlayer.id },
            io,
            botAction.cardToDrop,
            botAction.targetCombinationKey,
            botAction.targetCombinationIndex,
            botAction.addToStart
          );
        }
        break;
      case "readyToContinue":
        continentalReadyToContinue(lobby, { id: botPlayer.id }, io);
        break;
    }
  }
}

exports.continentalIsThereAGameInProgress = continentalIsThereAGameInProgress;
exports.continentalIsUserInPausedGame = continentalIsUserInPausedGame;
exports.continentalFindPausedGameLobbyCode = continentalFindPausedGameLobbyCode;
exports.continentalAddDisconnectedUser = continentalAddDisconnectedUser;
exports.continentalRemoveDisconnectedUser = continentalRemoveDisconnectedUser;
exports.continentalUpdateLobby = continentalUpdateLobby;
exports.continentalDealCards = continentalDealCards;
exports.continentalMoveCard = continentalMoveCard;
exports.continentalTakeCard = continentalTakeCard;
exports.continentalPickCardOutOfTurn = continentalPickCardOutOfTurn;
exports.continentalDiscardCard = continentalDiscardCard;
exports.continentalInitialDroppingCards = continentalInitialDroppingCards;
exports.continentalDropCard = continentalDropCard;
exports.continentalReadyToContinue = continentalReadyToContinue;
