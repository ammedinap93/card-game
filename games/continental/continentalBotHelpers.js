const { CONTINENTAL_GOALS } = require("./continentalConstants");
const {
  isATrio,
  isARun,
  isAValidInitialDrop,
  isAValidDrop,
} = require("./droppingCards");

function evaluateHand(cards, goal) {
  const { requiredRuns, requiredTrios } = getRequirementsForGoal(goal);
  let score = 0;

  // Check if hand can be initially dropped (highest priority)
  const validCombinations = findValidCombinations(cards, goal);
  if (validCombinations && isAValidInitialDrop(validCombinations, goal)) {
    return 10000; // Guaranteed higher than any non-droppable hand
  }

  // Count wildcards
  const wildcards = cards.filter(isWildCard).length;
  score += wildcards * 5; // Base points for wildcards

  if (requiredRuns > 0) {
    const { uniqueRuns, totalRuns, partialRuns, sameRangeSuits } =
      analyzeRuns(cards);
    score += Math.min(requiredRuns, uniqueRuns) * 500; // Highest reward for unique runs, limited by requirement
    score += Math.min(requiredRuns + 3, totalRuns) * 100; // Reward for any valid run, limited by requirement + 3
    score += partialRuns * 50; // Reward for partial runs
    score += sameRangeSuits * 20; // Increased reward for potential runs
  }

  if (requiredTrios > 0) {
    const { uniqueTrios, totalTrios, partialTrios } = analyzeTrios(cards);
    score += Math.min(requiredTrios, uniqueTrios) * 400; // High reward, limited by requirement
    score += Math.min(requiredTrios + 3, totalTrios) * 80; // Reward for any valid trio, limited by requirement + 3
    score += partialTrios * 40; // Reward for partial trios
  }

  return score;
}
function analyzeRuns(cards) {
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];
  let uniqueRuns = 0,
    totalRuns = 0,
    partialRuns = 0,
    sameRangeSuits = 0;
  const usedCards = new Set();

  // Helper function to calculate circular distance between ranks
  const circularDistance = (r1, r2) => {
    const pos1 = ranks.indexOf(r1),
      pos2 = ranks.indexOf(r2);
    return Math.min((pos2 - pos1 + 13) % 13, (pos1 - pos2 + 13) % 13);
  };

  for (let suit of suits) {
    const suitCards = cards.filter(
      (card) => card.card.split("_")[1] === suit && !isWildCard(card)
    );
    const wildCards = cards.filter(isWildCard);

    // Find valid runs
    for (
      let startRankIndex = 0;
      startRankIndex < ranks.length;
      startRankIndex++
    ) {
      for (let length = 4; length <= ranks.length; length++) {
        let run = [],
          wildcardCount = 0;
        for (let i = 0; i < length; i++) {
          const rankIndex = (startRankIndex + i) % ranks.length;
          const card = suitCards.find(
            (card) => card.card.split("_")[0] === ranks[rankIndex]
          );
          if (card) run.push(card);
          else if (wildCards.length > wildcardCount) {
            run.push(wildCards[wildcardCount]);
            wildcardCount++;
          } else break;
        }
        if (isARun(run)) {
          totalRuns++;
          if (run.every((card) => !usedCards.has(card.id))) {
            uniqueRuns++;
            run.forEach((card) => usedCards.add(card.id));
          }
        } else if (run.length === length - 1) {
          partialRuns++;
        }
      }
    }

    // Count cards in the same four-card range
    const suitCardsByRank = suitCards.map((card) => card.card.split("_")[0]);
    for (let i = 0; i < suitCardsByRank.length; i++) {
      let rangeCards = 0;
      const currentRank = suitCardsByRank[i];
      for (let j = i + 1; j < suitCardsByRank.length; j++) {
        const otherRank = suitCardsByRank[j];
        if (circularDistance(currentRank, otherRank) <= 3) {
          // within 4-card range
          rangeCards++;
        }
      }
      sameRangeSuits += rangeCards; // Each pair within range contributes
    }
  }

  return { uniqueRuns, totalRuns, partialRuns, sameRangeSuits };
}

function analyzeTrios(cards) {
  let uniqueTrios = 0,
    totalTrios = 0,
    partialTrios = 0;
  const usedCards = new Set();
  const groupedCards = {};
  const normalCards = cards.filter((card) => !isWildCard(card));
  const wildCards = cards.filter(isWildCard);

  // Group cards by rank
  normalCards.forEach((card) => {
    const rank = card.card.split("_")[0];
    if (!groupedCards[rank]) groupedCards[rank] = [];
    groupedCards[rank].push(card);
  });

  // Analyze each rank group
  for (let rank in groupedCards) {
    const rankCards = groupedCards[rank];

    // Check for complete trios
    for (let i = 0; i < rankCards.length - 1; i++) {
      for (let j = i + 1; j <= rankCards.length; j++) {
        const group = rankCards.slice(i, j);
        if (isATrio(group)) {
          totalTrios++;
          if (group.every((card) => !usedCards.has(card.id))) {
            uniqueTrios++;
            group.forEach((card) => usedCards.add(card.id));
          }
        }
        for (let k = 0; k < wildCards.length; k++) {
          if (isATrio([...group, wildCards[k]])) {
            totalTrios++;
            if (
              [...group, wildCards[k]].every((card) => !usedCards.has(card.id))
            ) {
              uniqueTrios++;
              [...group, wildCards[k]].forEach((card) =>
                usedCards.add(card.id)
              );
            }
          }
        }
      }
    }

    // Check for partial trios (excluding wildcards)
    if (rankCards.length === 2) partialTrios++;
  }

  return { uniqueTrios, totalTrios, partialTrios };
}
function getRequirementsForGoal(goal) {
  const requirements = {
    [CONTINENTAL_GOALS.TT]: { requiredTrios: 2, requiredRuns: 0 },
    [CONTINENTAL_GOALS.TR]: { requiredTrios: 1, requiredRuns: 1 },
    [CONTINENTAL_GOALS.RR]: { requiredTrios: 0, requiredRuns: 2 },
    [CONTINENTAL_GOALS.TTT]: { requiredTrios: 3, requiredRuns: 0 },
    [CONTINENTAL_GOALS.TRT]: { requiredTrios: 2, requiredRuns: 1 },
    [CONTINENTAL_GOALS.RTR]: { requiredTrios: 1, requiredRuns: 2 },
    [CONTINENTAL_GOALS.TTTT]: { requiredTrios: 4, requiredRuns: 0 },
    [CONTINENTAL_GOALS.RRR]: { requiredTrios: 0, requiredRuns: 3 },
  };
  return requirements[goal];
}

function isWildCard(card) {
  const [rank, suit] = card.card.split("_");
  return (
    rank === "joker" ||
    (rank === "ace" && (suit === "hearts" || suit === "diamonds"))
  );
}

function isNotAJoker(card) {
  const [rank, suit] = card.card.split("_");
  return rank !== "joker";
}

function findWorstCard(cards, goal, gameState, botId) {
  // Sort cards based on how much they contribute to hand score
  const sortedCards = cards
    .map((card) => {
      const remainingCards = cards.filter((c) => c.id !== card.id);
      const score = evaluateHand(remainingCards, goal);
      return { card, score };
    })
    .sort((a, b) => b.score - a.score); // Sort in descending order of score (worst cards first)

  // Determine if defensive play is needed
  const currentPlayerIndex = gameState.distributionOfSeats.indexOf(botId);
  const nextPlayerIndex =
    (currentPlayerIndex + 1) % gameState.distributionOfSeats.length;
  const nextPlayerId = gameState.distributionOfSeats[nextPlayerIndex];
  const nextPlayerHasInitiallyDropped =
    gameState.playersCombinations[nextPlayerId].length > 0;
  const nextPlayerHandSize = gameState.playersHands[nextPlayerId].length;
  const playDefensively =
    nextPlayerHasInitiallyDropped && nextPlayerHandSize <= 3;

  if (playDefensively) {
    // Check cards from worst to least-worst
    for (let { card } of sortedCards) {
      const dropInfo = canDropOnExistingCombination(
        card,
        gameState.playersCombinations
      );
      if (!dropInfo) {
        // Found a card that can't be dropped on any combination
        return card;
      }
    }
  }

  // If not playing defensively or all cards can be dropped, return the worst card
  return sortedCards[0].card; // First card is the worst
}

function findValidCombinations(cards, goal) {
  const { requiredTrios, requiredRuns } = getRequirementsForGoal(goal);

  // Step 1: Find all possible runs and trios
  let allRuns = [];
  let allTrios = [];
  if (requiredRuns > 0) {
    allRuns = findAllRuns(cards);
  }
  if (requiredTrios > 0) {
    allTrios = findAllTrios(cards);
  }

  // Step 2: Find a valid set of combinations
  const validCombinations = findValidCombinationSet(
    allRuns,
    allTrios,
    requiredRuns,
    requiredTrios
  );

  // Step 3: Check if the combinations are valid for the initial drop
  if (validCombinations && isAValidInitialDrop(validCombinations, goal)) {
    return validCombinations;
  }

  return null;
}

function findAllRuns(cards) {
  const runs = [];
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  for (const suit of suits) {
    const suitCards = cards.filter((card) => card.card.split("_")[1] === suit);
    const wildCards = cards.filter((card) => isWildCard(card));

    for (let startIndex = 0; startIndex < ranks.length; startIndex++) {
      for (let length = 4; length <= ranks.length; length++) {
        const potentialRun = [];
        let wildCardsUsed = 0;

        for (let i = 0; i < length; i++) {
          const rankIndex = (startIndex + i) % ranks.length;
          const card = suitCards.find(
            (card) => card.card.split("_")[0] === ranks[rankIndex]
          );

          if (card) {
            potentialRun.push(card);
          } else if (wildCardsUsed < wildCards.length) {
            potentialRun.push(wildCards[wildCardsUsed]);
            wildCardsUsed++;
          } else {
            break;
          }
        }
        if (isARun(potentialRun)) {
          runs.push(potentialRun);
        }
      }
    }
  }

  return runs;
}

function findAllTrios(cards) {
  const trios = [];
  const rankGroups = {};

  const nonJokerCards = cards.filter(isNotAJoker);
  nonJokerCards.forEach((card) => {
    const rank = card.card.split("_")[0];
    if (!rankGroups[rank]) {
      rankGroups[rank] = [];
    }
    rankGroups[rank].push(card);
  });

  const wildCards = cards.filter(isWildCard);

  for (const rank in rankGroups) {
    const group = rankGroups[rank];
    for (let i = 0; i < group.length - 1; i++) {
      for (let j = i + 1; j <= group.length; j++) {
        if (wildCards.length > 0) {
          for (let k = 0; k < wildCards.length; k++) {
            for (let l = k + 1; l <= wildCards.length; l++) {
              const potentialTrio = [
                ...group.slice(i, j),
                ...wildCards.slice(k, l),
              ];
              if (isATrio(potentialTrio)) {
                trios.push(potentialTrio);
              }
            }
          }
        }
        const potentialTrio = [...group.slice(i, j)];
        if (isATrio(potentialTrio)) {
          trios.push(potentialTrio);
        }
      }
    }
  }

  return trios;
}

function findValidCombinationSet(
  allRuns,
  allTrios,
  requiredRuns,
  requiredTrios
) {
  const combinations = [];
  const usedCards = new Set();

  function backtrack(runCount, trioCount) {
    if (runCount === requiredRuns && trioCount === requiredTrios) {
      return true;
    }

    if (runCount < requiredRuns) {
      for (const run of allRuns) {
        if (run.every((card) => !usedCards.has(card.id))) {
          run.forEach((card) => usedCards.add(card.id));
          combinations.push(run);

          if (backtrack(runCount + 1, trioCount)) {
            return true;
          }

          run.forEach((card) => usedCards.delete(card.id));
          combinations.pop();
        }
      }
    }

    if (trioCount < requiredTrios) {
      for (const trio of allTrios) {
        if (trio.every((card) => !usedCards.has(card.id))) {
          trio.forEach((card) => usedCards.add(card.id));
          combinations.push(trio);

          if (backtrack(runCount, trioCount + 1)) {
            return true;
          }

          trio.forEach((card) => usedCards.delete(card.id));
          combinations.pop();
        }
      }
    }

    return false;
  }

  if (backtrack(0, 0)) {
    return combinations;
  }

  return null;
}

function canDropOnExistingCombination(card, playersCombinations) {
  for (const [playerId, combinations] of Object.entries(playersCombinations)) {
    for (let i = 0; i < combinations.length; i++) {
      const combination = combinations[i];
      if (isAValidDrop(card, combination)) {
        return { playerId, combinationIndex: i, addToStart: false };
      }
      if (isAValidDrop(card, combination, true)) {
        return { playerId, combinationIndex: i, addToStart: true };
      }
    }
  }
  return null;
}

function findCardToDropOnExistingCombination(hand, playersCombinations) {
  for (const card of hand) {
    const dropInfo = canDropOnExistingCombination(card, playersCombinations);
    if (dropInfo) {
      return { card, ...dropInfo };
    }
  }
  return null;
}

function findHighestScoringCard(hand) {
  const getCardScore = (card) => {
    const [rank, _] = card.card.split("_");
    if (rank === "joker") return 50;
    if (rank === "ace") return 20;
    if (["8", "9", "10", "jack", "queen", "king"].includes(rank)) return 10;
    return 5;
  };

  return hand.reduce((highestCard, currentCard) =>
    getCardScore(currentCard) > getCardScore(highestCard)
      ? currentCard
      : highestCard
  );
}

module.exports = {
  evaluateHand,
  findWorstCard,
  findValidCombinations,
  canDropOnExistingCombination,
  findCardToDropOnExistingCombination,
  findHighestScoringCard,
};
