const CONTINENTAL_GAME_STATUS = {
  ready_to_deal: "ready_to_deal",
  dealing_cards: "dealing_cards",
  player_turn: "player_turn",
  taking_from_deck_pile: "taking_from_deck_pile",
  taking_from_discard_pile: "taking_from_discard_pile",
  discarding_from_deck_pile: "discarding_from_deck_pile",
  discarding_from_player: "discarding_from_player",
  player_turn_in_progress: "player_turn_in_progress",
  waiting_for_picking_card_out_of_turn: "waiting_for_picking_card_out_of_turn",
  processing_picking_card_out_of_turn: "processing_picking_card_out_of_turn",
  dropping_card: "dropping_card",
  end_of_the_hand: "end_of_the_hand",
  end_of_the_game: "end_of_the_game",
};

const CONTINENTAL_GOALS = {
  TT: "TT",
  TR: "TR",
  RR: "RR",
  TTT: "TTT",
  TRT: "TRT",
  RTR: "RTR",
  TTTT: "TTTT",
  RRR: "RRR",
};

const CONTINENTAL_HANDS = [
  {
    number: 1,
    goal: CONTINENTAL_GOALS.TT,
    goalDescription: "Two trios",
    cardsToBeDealt: 7,
  },
  {
    number: 2,
    goal: CONTINENTAL_GOALS.TR,
    goalDescription: "One trio and one run",
    cardsToBeDealt: 8,
  },
  {
    number: 3,
    goal: CONTINENTAL_GOALS.RR,
    goalDescription: "Two runs",
    cardsToBeDealt: 9,
  },
  {
    number: 4,
    goal: CONTINENTAL_GOALS.TTT,
    goalDescription: "Three trios",
    cardsToBeDealt: 10,
  },
  {
    number: 5,
    goal: CONTINENTAL_GOALS.TRT,
    goalDescription: "Two trios and one run",
    cardsToBeDealt: 11,
  },
  {
    number: 6,
    goal: CONTINENTAL_GOALS.RTR,
    goalDescription: "One trio and two runs",
    cardsToBeDealt: 12,
  },
  {
    number: 7,
    goal: CONTINENTAL_GOALS.TTTT,
    goalDescription: "Four trios",
    cardsToBeDealt: 13,
  },
  {
    number: 8,
    goal: CONTINENTAL_GOALS.RRR,
    goalDescription: "Three runs",
    cardsToBeDealt: 13,
  },
];

const CONTINENTAL_TIMERS = {
  dealer: 10,
  playerTurn: 30,
  playerTurnInProgress: 60,
  pickCardOutOfTurn: 5,
  summary: 30,
};

module.exports = {
  CONTINENTAL_GAME_STATUS,
  CONTINENTAL_GOALS,
  CONTINENTAL_HANDS,
  CONTINENTAL_TIMERS,
};
