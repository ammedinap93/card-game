function isATrio(cards) {
  if (cards.length < 3 || !checkUniqueCardIds(cards)) {
    return false;
  }

  const counts = {};
  let redAces = 0;
  let jokers = 0;

  cards.forEach((card) => {
    const [rank, suit] = card.card.split("_");
    if (rank === "ace" && (suit === "hearts" || suit === "diamonds")) {
      redAces++;
    } else if (rank === "joker") {
      jokers++;
    } else {
      counts[rank] = (counts[rank] || 0) + 1;
    }
  });

  const regularRanks = Object.keys(counts);
  const regularCards = regularRanks.reduce(
    (sum, rank) => sum + counts[rank],
    0
  );

  if (
    regularRanks.length === 0 ||
    (regularRanks.length === 1 && regularRanks[0] === "ace")
  ) {
    // If there are no regular cards or the regular cards are black aces,
    // the red aces count as regular cards
    return (
      regularCards + redAces + jokers === cards.length &&
      regularCards + redAces > jokers
    );
  } else {
    // If there are regular cards and they aren't black aces,
    // the red aces count as wild cards
    return (
      regularRanks.length === 1 &&
      regularCards + redAces + jokers === cards.length &&
      regularCards > redAces + jokers
    );
  }
}

function isARun(cards) {
  const reversedCards = [...cards].reverse();
  return isAValidRun(cards) || isAValidRun(reversedCards);
}

function isAValidRun(cards) {
  if (cards.length < 4 || !checkUniqueCardIds(cards)) {
    return false;
  }

  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Determine the suit of the run
  let runSuit;
  for (const card of cards) {
    const [rank, suit] = card.card.split("_");
    if (
      rank !== "joker" &&
      !(rank === "ace" && (suit === "hearts" || suit === "diamonds"))
    ) {
      runSuit = suit;
      break;
    }
  }

  if (!runSuit) {
    return false; // No regular cards found
  }

  // Check if all regular cards have the same suit
  if (
    !cards.every((card) => {
      const [rank, suit] = card.card.split("_");
      return (
        rank === "joker" ||
        (rank === "ace" && (suit === "hearts" || suit === "diamonds")) ||
        suit === runSuit
      );
    })
  ) {
    return false;
  }

  const convertedRanks = new Array(cards.length).fill(null);

  // Fill convertedRanks array
  while (convertedRanks.includes(null)) {
    for (let i = 0; i < convertedRanks.length; i++) {
      if (convertedRanks[i] === null) {
        const [rank, suit] = cards[i].card.split("_");
        if (
          rank !== "joker" &&
          !(rank === "ace" && (suit === "hearts" || suit === "diamonds"))
        ) {
          convertedRanks[i] = ranks.indexOf(rank);
        } else {
          const nextRank = convertedRanks[i + 1];
          const prevRank = convertedRanks[i - 1];
          if (nextRank !== null && nextRank !== undefined) {
            convertedRanks[i] = (nextRank - 1 + ranks.length) % ranks.length;
          } else if (prevRank !== null && prevRank !== undefined) {
            convertedRanks[i] = (prevRank + 1) % ranks.length;
          }
        }
      }
    }
  }

  // Verify sequence and count regular/wild cards
  let regularCards = 0;
  let wildCards = 0;
  for (let i = 0; i < convertedRanks.length - 1; i++) {
    if ((convertedRanks[i] + 1) % ranks.length !== convertedRanks[i + 1]) {
      return false; // Invalid sequence
    }

    const [rank, suit] = cards[i].card.split("_");
    if (rank === "joker") {
      wildCards++;
    } else if (rank === "ace" && (suit === "hearts" || suit === "diamonds")) {
      if (suit === runSuit && convertedRanks[i] === 0) {
        regularCards++;
      } else {
        wildCards++;
      }
    } else {
      regularCards++;
    }
  }

  // Count the last card
  const [lastRank, lastSuit] = cards[cards.length - 1].card.split("_");
  if (lastRank === "joker") {
    wildCards++;
  } else if (
    lastRank === "ace" &&
    (lastSuit === "hearts" || lastSuit === "diamonds")
  ) {
    if (lastSuit === runSuit && convertedRanks[cards.length - 1] === 0) {
      regularCards++;
    } else {
      wildCards++;
    }
  } else {
    regularCards++;
  }

  return regularCards > wildCards;
}

function isAValidInitialDrop(combinations, goal) {
  // Check for unique card IDs
  const allCards = combinations.flat();
  if (!checkUniqueCardIds(allCards)) {
    return false;
  }

  const combinationTypes = {
    TT: (combinations) =>
      combinations.length === 2 &&
      combinations.every((combination) => isATrio(combination)),
    TR: (combinations) =>
      combinations.length === 2 &&
      combinations.filter(isATrio).length === 1 &&
      combinations.filter(isARun).length === 1,
    RR: (combinations) =>
      combinations.length === 2 &&
      combinations.every((combination) => isARun(combination)),
    TTT: (combinations) =>
      combinations.length === 3 &&
      combinations.every((combination) => isATrio(combination)),
    TRT: (combinations) =>
      combinations.length === 3 &&
      combinations.filter(isATrio).length === 2 &&
      combinations.filter(isARun).length === 1,
    RTR: (combinations) =>
      combinations.length === 3 &&
      combinations.filter(isATrio).length === 1 &&
      combinations.filter(isARun).length === 2,
    TTTT: (combinations) =>
      combinations.length === 4 &&
      combinations.every((combination) => isATrio(combination)),
    RRR: (combinations) =>
      combinations.length === 3 &&
      combinations.every((combination) => isARun(combination)),
  };

  const isValid = combinationTypes[goal];
  return isValid ? isValid(combinations) : false;
}

function initialDroppingCards(combinations, goal, playerHand) {
  if (!isAValidInitialDrop(combinations, goal)) {
    return null;
  }

  const droppedCards = combinations.flat();
  const droppedCardsIds = droppedCards.map((card) => card.id);
  const updatedHand = playerHand.filter(
    (card) => !droppedCardsIds.includes(card.id)
  );

  if (updatedHand.length + droppedCards.length !== playerHand.length) {
    return null;
  }

  return updatedHand;
}

function isAValidDrop(cardToDrop, targetCombination, addToStart = false) {
  const newCombination = addToStart
    ? [cardToDrop, ...targetCombination]
    : [...targetCombination, cardToDrop];
  return isATrio(newCombination) || isARun(newCombination);
}

function dropCard(
  cardToDrop,
  targetCombination,
  playerHand,
  addToStart = false
) {
  // Validate that the card exists in the player's hand
  const cardIndex = playerHand.findIndex((card) => card.id === cardToDrop.id);
  if (cardIndex === -1) {
    return null;
  }

  // Create the new combination based on the addToStart parameter
  const newCombination = addToStart
    ? [cardToDrop, ...targetCombination]
    : [...targetCombination, cardToDrop];

  // Check if the drop is valid
  if (!isATrio(newCombination) && !isARun(newCombination)) {
    return null;
  }

  // Update the player's hand
  const newPlayerHand = [
    ...playerHand.slice(0, cardIndex),
    ...playerHand.slice(cardIndex + 1),
  ];

  return {
    newCombination,
    newPlayerHand,
  };
}

function checkUniqueCardIds(cards) {
  const uniqueCardIds = new Set(cards.map((card) => card.id));
  if (uniqueCardIds.size !== cards.length) {
    return false;
  }
  return true;
}

module.exports = {
  isATrio,
  isARun,
  isAValidInitialDrop,
  initialDroppingCards,
  isAValidDrop,
  dropCard,
};
