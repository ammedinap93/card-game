const { shuffle } = require("../../helpers");
const statsManager = require("../statsManager");
const {
  CONTINENTAL_GAME_STATUS,
  CONTINENTAL_HANDS,
} = require("./continentalConstants");

function createNewGameState(lobby) {
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile with two English decks and four jokers
  let deckPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
        isDead: false,
      }))
    ),
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
        isDead: false,
      }))
    ),
    { card: "joker_red", isDead: false },
    { card: "joker_black", isDead: false },
    { card: "joker_red", isDead: false },
    { card: "joker_black", isDead: false },
  ];

  // Shuffle the deck pile first
  deckPile = shuffle(deckPile);

  // Assign IDs after shuffling
  let cardId = 1;
  deckPile.forEach((card) => {
    card.id = cardId++;
  });

  // Randomly distribute the seats
  const distributionOfSeats = shuffle(lobby.players.map((player) => player.id));

  return {
    lock: false,
    paused: false,
    fromDeckPile: false,
    progressId: null,
    initialNumberOfPlayers: lobby.players.length,
    lobby: lobby,
    gameStatus: CONTINENTAL_GAME_STATUS.ready_to_deal,
    hand: CONTINENTAL_HANDS[0],
    playersReadyToContinue: lobby.players.reduce((ready, player) => {
      ready[player.id] = false;
      return ready;
    }, {}),
    playersInitialDropRound: lobby.players.reduce((rounds, player) => {
      rounds[player.id] = null;
      return rounds;
    }, {}),
    playersScoresByHand: lobby.players.reduce((scores, player) => {
      scores[player.id] = [];
      return scores;
    }, {}),
    deckPile: deckPile,
    discardPile: [],
    distributionOfSeats: distributionOfSeats,
    roundNumber: 0,
    playerInTurn: distributionOfSeats[distributionOfSeats.length - 1],
    dealer: distributionOfSeats[distributionOfSeats.length - 1],
    playersHands: lobby.players.reduce((hands, player) => {
      hands[player.id] = [];
      return hands;
    }, {}),
    playersCombinations: lobby.players.reduce((combinations, player) => {
      combinations[player.id] = [];
      return combinations;
    }, {}),
    dealingCard: null,
    discardingCard: null,
    droppingCard: null,
    lastPickedCard: null,
    waitingForPickingCardOutOfTurn: [],
    winnerPlayer: null,
  };
}

function continentalProcessUpdatedLobby(lobby, gameState) {
  const userIdsMatch =
    lobby.players.length === gameState.lobby.players.length &&
    lobby.players.every(
      (player) =>
        gameState.lobby.players.find((p) => p.userId === player.userId) !==
        undefined
    );

  if (userIdsMatch) {
    // Create a new game state object merging the info from the lobby and the old game state
    const newGameState = {
      ...gameState,
      lobby: lobby,
      playersHands: {},
      playersCombinations: {},
      playersScoresByHand: {},
      playersInitialDropRound: {},
      playersReadyToContinue: {},
    };

    // Map old socket IDs to new ones using userId
    lobby.players.forEach((player) => {
      const oldPlayer = gameState.lobby.players.find(
        (p) => p.userId === player.userId
      );

      if (oldPlayer) {
        newGameState.playersHands[player.id] =
          gameState.playersHands[oldPlayer.id];
        newGameState.playersCombinations[player.id] =
          gameState.playersCombinations[oldPlayer.id];
        newGameState.playersScoresByHand[player.id] =
          gameState.playersScoresByHand[oldPlayer.id];
        newGameState.playersInitialDropRound[player.id] =
          gameState.playersInitialDropRound[oldPlayer.id];
        newGameState.playersReadyToContinue[player.id] =
          gameState.playersReadyToContinue[oldPlayer.id];
        newGameState.waitingForPickingCardOutOfTurn =
          newGameState.waitingForPickingCardOutOfTurn.map((id) =>
            id === oldPlayer.id ? player.id : id
          );
        newGameState.distributionOfSeats = newGameState.distributionOfSeats.map(
          (id) => (id === oldPlayer.id ? player.id : id)
        );
      }
    });

    // Update player in turn and dealer
    const playerInTurnOld = gameState.lobby.players.find(
      (p) => p.id === gameState.playerInTurn
    );
    const dealerOld = gameState.lobby.players.find(
      (p) => p.id === gameState.dealer
    );

    newGameState.playerInTurn = lobby.players.find(
      (p) => p.userId === playerInTurnOld.userId
    ).id;
    newGameState.dealer = lobby.players.find(
      (p) => p.userId === dealerOld.userId
    ).id;

    if (gameState.winnerPlayer) {
      const winnerPlayerOld = gameState.lobby.players.find(
        (p) => p.id === gameState.winnerPlayer
      );
      newGameState.winnerPlayer = lobby.players.find(
        (p) => p.userId === winnerPlayerOld.userId
      ).id;
    }

    return newGameState;
  }
  return null;
}

async function endHand(gameState) {
  // Validate that at least one player's hand is empty or the deck pile is empty
  const winningPlayerId = Object.entries(gameState.playersHands).find(
    ([_, hand]) => hand.length === 0
  )?.[0];

  if (!winningPlayerId && gameState.deckPile.length > 0) {
    return null;
  }

  // Calculate the players' scores in the hand
  const playersScores = {};
  let doubleScores = false;

  // Check for bonus condition
  if (
    winningPlayerId &&
    gameState.playersInitialDropRound[winningPlayerId] ===
      gameState.roundNumber &&
    Object.values(gameState.playersInitialDropRound).filter(
      (round) => round !== null
    ).length === 1
  ) {
    doubleScores = true;
  }

  for (const [playerId, hand] of Object.entries(gameState.playersHands)) {
    let score = 0;
    for (const { card } of hand) {
      const [rank, _] = card.split("_");
      if (rank === "joker") {
        score += 50;
      } else if (rank === "ace") {
        score += 20;
      } else if (["8", "9", "10", "jack", "queen", "king"].includes(rank)) {
        score += 10;
      } else {
        score += 5;
      }
    }
    playersScores[playerId] = doubleScores ? score * 2 : score;
  }

  // Store the players' scores in the playersScoresByHand property
  for (const [playerId, score] of Object.entries(playersScores)) {
    gameState.playersScoresByHand[playerId].push(score);
  }

  // Check if this is the last hand
  const isLastHand = gameState.hand.number === CONTINENTAL_HANDS.length;

  // Change the game status
  gameState.gameStatus = isLastHand
    ? CONTINENTAL_GAME_STATUS.end_of_the_game
    : CONTINENTAL_GAME_STATUS.end_of_the_hand;

  // If this is the last hand, determine the winner player
  if (isLastHand) {
    gameState.winnerPlayer = determineWinnerPlayer(
      gameState.playersScoresByHand
    );

    // Save game stats if the feature is enabled and a winner is determined
    if (gameState.winnerPlayer) {
      const isEnabled = await statsManager.isStatsEnabled(gameState.lobby);
      if (isEnabled) {
        const totalScore = gameState.playersScoresByHand[
          gameState.winnerPlayer
        ].reduce((sum, score) => sum + score, 0);
        const statsId = await statsManager.saveStats(gameState, totalScore);
        if (statsId) {
          console.log(`Game stats saved with ID: ${statsId}`);
        } else {
          console.error("Failed to save game stats");
        }
      }
    }
  }

  // Return the updated game state
  return gameState;
}

function startNextHand(gameState) {
  // Check if the current hand is the last one
  const isLastHand = gameState.hand.number === CONTINENTAL_HANDS.length;

  if (isLastHand) {
    return null; // No more hands to play
  }

  // Find the index of the current dealer in the distribution of seats
  const currentDealerIndex = gameState.distributionOfSeats.indexOf(
    gameState.dealer
  );

  // Calculate the index of the next dealer
  const nextDealerIndex =
    (currentDealerIndex + 1) % gameState.distributionOfSeats.length;

  // Get the next hand from CONTINENTAL_HANDS
  const nextHand = CONTINENTAL_HANDS[gameState.hand.number];

  // Create a new deck pile and shuffle it
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile with two English decks and four jokers
  let deckPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
        isDead: false,
      }))
    ),
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
        isDead: false,
      }))
    ),
    { card: "joker_red", isDead: false },
    { card: "joker_black", isDead: false },
    { card: "joker_red", isDead: false },
    { card: "joker_black", isDead: false },
  ];

  // Shuffle the deck pile first
  deckPile = shuffle(deckPile);

  // Assign IDs after shuffling
  let cardId = 1;
  deckPile.forEach((card) => {
    card.id = cardId++;
  });

  // Create the new game state
  const newGameState = {
    lock: false,
    paused: false,
    fromDeckPile: false,
    progressId: gameState.progressId,
    initialNumberOfPlayers: gameState.initialNumberOfPlayers,
    lobby: gameState.lobby,
    gameStatus: CONTINENTAL_GAME_STATUS.ready_to_deal,
    hand: nextHand,
    playersReadyToContinue: gameState.lobby.players.reduce((ready, player) => {
      ready[player.id] = false;
      return ready;
    }, {}),
    playersInitialDropRound: gameState.lobby.players.reduce(
      (rounds, player) => {
        rounds[player.id] = null;
        return rounds;
      },
      {}
    ),
    playersScoresByHand: gameState.playersScoresByHand,
    deckPile: deckPile,
    discardPile: [],
    distributionOfSeats: gameState.distributionOfSeats,
    roundNumber: 0,
    playerInTurn: gameState.distributionOfSeats[nextDealerIndex],
    dealer: gameState.distributionOfSeats[nextDealerIndex],
    playersHands: gameState.lobby.players.reduce((hands, player) => {
      hands[player.id] = [];
      return hands;
    }, {}),
    playersCombinations: gameState.lobby.players.reduce(
      (combinations, player) => {
        combinations[player.id] = [];
        return combinations;
      },
      {}
    ),
    dealingCard: null,
    discardingCard: null,
    droppingCard: null,
    lastPickedCard: null,
    waitingForPickingCardOutOfTurn: [],
    winnerPlayer: null,
  };

  return newGameState;
}

function determineWinnerPlayer(playersScoresByHand) {
  // Sort players by their total score (ascending)
  const sortedPlayers = Object.entries(playersScoresByHand).sort(
    ([, scoresA], [, scoresB]) => {
      const totalScoreA = scoresA.reduce((sum, score) => sum + score, 0);
      const totalScoreB = scoresB.reduce((sum, score) => sum + score, 0);
      return totalScoreA - totalScoreB;
    }
  );

  // Check if there is a clear winner
  const [winnerPlayerId, winnerScores] = sortedPlayers[0];
  const totalWinnerScore = winnerScores.reduce((sum, score) => sum + score, 0);

  const possibleWinners = sortedPlayers.filter(
    ([, scores]) =>
      scores.reduce((sum, score) => sum + score, 0) === totalWinnerScore
  );

  // If there is only one possible winner, return their ID
  if (possibleWinners.length === 1) {
    return winnerPlayerId;
  }

  // Find the winner of each hand
  const handWinners = [];
  for (let i = 0; i < CONTINENTAL_HANDS.length; i++) {
    const handScores = Object.entries(playersScoresByHand).map(
      ([playerId, scores]) => [playerId, scores[i]]
    );
    const [handWinnerId] = handScores.sort(
      ([, scoreA], [, scoreB]) => scoreA - scoreB
    )[0];
    if (handScores[0] < handScores[1]) {
      handWinners.push(handWinnerId);
    }
  }

  // Find the most recent hand winner among the possible winners
  for (let i = handWinners.length - 1; i >= 0; i--) {
    const recentHandWinner = possibleWinners.find(
      ([playerId]) => playerId === handWinners[i]
    );
    if (recentHandWinner) {
      return recentHandWinner[0];
    }
  }

  // New tie-break logic
  let currentPossibleWinners = possibleWinners;
  for (let i = CONTINENTAL_HANDS.length - 1; i >= 0; i--) {
    const handScores = currentPossibleWinners.map(([playerId]) => [
      playerId,
      playersScoresByHand[playerId][i],
    ]);
    const minScore = Math.min(...handScores.map(([, score]) => score));
    currentPossibleWinners = currentPossibleWinners.filter(
      ([playerId]) => playersScoresByHand[playerId][i] === minScore
    );

    if (currentPossibleWinners.length === 1) {
      return currentPossibleWinners[0][0];
    }
  }

  // If the tie remains even after considering all hands, return null
  return null;
}

module.exports = {
  createNewGameState,
  continentalProcessUpdatedLobby,
  endHand,
  startNextHand,
};
