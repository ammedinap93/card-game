const {
  FIFTY_ONE_ID,
  PUBLIC_PAUSE_TIMER,
  PRIVATE_PAUSE_TIMER,
} = require("../../constants");
const { delay } = require("../../helpers");
const {
  createNewGameState,
  endHand,
  startNextHand,
  fiftyOneProcessUpdatedLobby,
  call,
  challenge,
  notChallenge,
} = require("./fiftyOneHelpers");
const {
  isProgressEnabled,
  findProgress,
  createProgress,
  updateProgress,
  deleteProgress,
} = require("../progressManager");
const {
  startTimer,
  stopTimer,
  pauseTimers,
  resumeTimers,
  stopTimers,
  incrementTimer,
  resumeTimer,
} = require("../timerManager");
const statsManager = require("../statsManager");
const { botNextAction } = require("../botLogic");
const { processChatMessage } = require("../chatManager");
const {
  FIFTY_ONE_GAME_STATUS,
  FIFTY_ONE_TIMERS,
} = require("./fiftyOneConstants");
const {
  emitProtectedGameState,
  protectGameStateForPlayer,
} = require("./fiftyOneAntiCheat");

let gameStateByLobby = {};
let pausedGamesByUser = new Map(); // userId -> lobbyCode
let replacePlayersInLobby = null; // callback to replace disconnected players
let deletePlayersFromLobby = null; // callback to remove players when aborting a private game

async function lockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (!gameState) {
    return false;
  }
  while (gameState.lock) {
    await delay(Math.floor(Math.random() * 401) + 100);
  }
  gameState.lock = true;
  console.log("lock");
  return true;
}

function unlockGameState(lobby) {
  const gameState = gameStateByLobby[lobby.code];
  if (gameState) {
    gameState.lock = false;
    console.log("unlock");
  }
}

function fiftyOneIsThereAGameInProgress(lobby) {
  return gameStateByLobby[lobby.code] !== undefined;
}

function fiftyOneAddDisconnectedUser(userId, lobby, io) {
  pausedGamesByUser.set(userId, lobby.code);

  // Set a timeout to replace the player
  startGamePausedTimer(lobby, io);
}

function fiftyOneRemoveDisconnectedUser(userId) {
  pausedGamesByUser.delete(userId);
}

function fiftyOneFindPausedGameLobbyCode(userId) {
  return pausedGamesByUser.get(userId);
}

function fiftyOneIsUserInPausedGame(userId) {
  return pausedGamesByUser.has(userId);
}

async function fiftyOneUpdateLobby(
  lobby,
  io,
  deletePlayersFromLobbyCallBack,
  replacePlayersInLobbyCallBack,
  removeReplacementsInLobbyCallBack
) {
  deletePlayersFromLobby = deletePlayersFromLobbyCallBack;
  replacePlayersInLobby = replacePlayersInLobbyCallBack;

  if (!fiftyOneIsThereAGameInProgress(lobby)) {
    const lobbyCopy = JSON.parse(JSON.stringify(lobby));
    fiftyOneGame(lobbyCopy, io);
  } else {
    const proceed = await lockGameState(lobby);
    if (!proceed) {
      return;
    }
    let gameState = gameStateByLobby[lobby.code];
    try {
      const realPlayers = lobby.players.filter((player) => !player.isBot);
      if (
        lobby.players.length < gameState.initialNumberOfPlayers &&
        lobby.players.length > 0
      ) {
        gameState.paused = true;

        // Pause all timers for this lobby (except the Game Paused timer)
        pauseTimers(lobby);
        resumeTimer(lobby, "gamePaused");

        emitProtectedGameState(gameState, io, lobby.players);
      } else if (realPlayers.length === 0) {
        stopTimers(lobby); // Stop all timers
        delete gameStateByLobby[lobby.code];
      } else if (lobby.players.length === gameState.initialNumberOfPlayers) {
        const lobbyCopy = JSON.parse(JSON.stringify(lobby));
        const newGameState = fiftyOneProcessUpdatedLobby(lobbyCopy, gameState);
        if (newGameState) {
          // Get the list of successfully replaced user IDs
          const replacedUserIds = [];
          for (const disconnectedUserId in lobbyCopy.replacements) {
            if (lobbyCopy.replacements[disconnectedUserId] !== null) {
              replacedUserIds.push(disconnectedUserId);
            }
          }

          // Remove processed replacements from the lobby
          if (replacedUserIds.length > 0) {
            removeReplacementsInLobbyCallBack(lobby.code, replacedUserIds);
          }

          gameStateByLobby[lobby.code] = newGameState;
          gameState = gameStateByLobby[lobby.code];
          gameState.paused = false;

          await delay(3000); // Wait for the client to set the scene
          emitProtectedGameState(gameState, io, lobbyCopy.players);

          if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand) {
            incrementTimer(lobby, "summary", 5); // Increase summary timer by 5 seconds
          }

          // Resume all timers for this lobby
          resumeTimers(lobby);

          // Check if stats feature is enabled
          const statsEnabled = await statsManager.isStatsEnabled(lobby);

          if (statsEnabled) {
            // Get players' wins
            const wins = await statsManager.getWins(lobby);

            if (wins) {
              // Emit gameStats event to all players in the lobby
              lobby.players.forEach((player) => {
                io.to(player.id).emit("gameStats", wins);
              });
            }
          }

          if (
            gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn ||
            gameState.gameStatus ===
              FIFTY_ONE_GAME_STATUS.player_turn_in_progress
          ) {
            performBotsActions(lobby, io);
          }
        } else {
          unlockGameState(lobby);
          fiftyOneGame(lobbyCopy, io);
        }
      }
    } finally {
      unlockGameState(lobby);
    }
  }
}

async function fiftyOneGame(lobby, io) {
  await lockGameState(lobby);
  try {
    let gameState;

    // Check if progress is enabled for this lobby
    const progressEnabled = await isProgressEnabled(lobby);

    if (progressEnabled) {
      // Find game progress
      const savedGameState = await findProgress(lobby);
      if (savedGameState) {
        console.log("Game progress found. Resuming game.");
        gameState = savedGameState;
      } else {
        console.log("No game progress found. Starting a new game.");
        gameState = createNewGameState(lobby);

        // Save the new game state
        const progressId = await createProgress(gameState);
        if (progressId) {
          gameState.progressId = progressId;
        }
      }
    } else {
      console.log("Game progress disabled. Starting a new game.");
      gameState = createNewGameState(lobby);
    }

    gameState.paused = false;

    gameStateByLobby[lobby.code] = gameState;

    await delay(3000); // Wait for the client to set the new scene

    // Emit the game state to all players in the lobby
    emitProtectedGameState(gameState, io, lobby.players);

    // Check if stats feature is enabled
    const statsEnabled = await statsManager.isStatsEnabled(lobby);

    if (statsEnabled) {
      // Get players' wins
      const wins = await statsManager.getWins(lobby);

      if (wins) {
        // Emit gameStats event to all players in the lobby
        lobby.players.forEach((player) => {
          io.to(player.id).emit("gameStats", wins);
        });
      }
    }

    // Check the game status and start the correspondent timer
    if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.ready_to_deal) {
      // Start the dealer timer
      startDealerTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn) {
      // Start the player's turn timer
      startPlayerTurnTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn_in_progress
    ) {
      // Start the player's turn in progress timer
      startPlayerTurnInProgressTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers
    ) {
      // Start the waiting for challengers timer
      startWaitingForChallengersTimer(lobby, io);
      performBotsActions(lobby, io);
    } else if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand) {
      // Start the summary timer
      startSummaryTimer(lobby, io);
      performBotsActions(lobby, io);
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneDealCards(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.ready_to_deal &&
      gameState.dealer === socket.id
    ) {
      // Stop the dealer timer
      stopTimer(lobby, "dealer");

      const cardsPerPlayer = 5;
      gameState.gameStatus = FIFTY_ONE_GAME_STATUS.dealing_cards;

      const dealCardsToPlayers = async () => {
        const players = gameState.distributionOfSeats;
        const startIndex =
          (players.indexOf(gameState.dealer) + 1) % players.length;

        for (let i = 0; i < cardsPerPlayer; i++) {
          for (let j = 0; j < players.length; j++) {
            const playerIndex = (startIndex + j) % players.length;
            const playerId = players[playerIndex];
            const card = gameState.deckPile.pop();
            gameState.playersHands[playerId].push(card);

            // Set the dealingCard property in the game state
            gameState.dealingCard = {
              ...card,
              playerId,
            };

            emitProtectedGameState(gameState, io, lobby.players);
            await delay(500);
          }
        }

        // Deal an extra card to the starting player
        const startingPlayerId = players[startIndex];
        const extraCard = gameState.deckPile.pop();
        gameState.playersHands[startingPlayerId].push(extraCard);

        gameState.dealingCard = {
          ...extraCard,
          playerId: startingPlayerId,
        };

        emitProtectedGameState(gameState, io, lobby.players);
        await delay(500);

        gameState.dealingCard = null;
        gameState.gameStatus = FIFTY_ONE_GAME_STATUS.player_turn_in_progress;
        gameState.playerInTurn = players[startIndex];
        gameState.roundNumber++;

        // Save progress if enabled
        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }

        emitProtectedGameState(gameState, io, lobby.players);

        // Start player's turn in progress timer
        startPlayerTurnInProgressTimer(lobby, io);
        performBotsActions(lobby, io);
      };

      await dealCardsToPlayers();
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneMoveCard(lobby, socket, io, cardId, newIndex) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (!gameState.paused) {
      const playerId = socket.id;
      const playerHand = gameState.playersHands[playerId];

      // Find the card in the player's hand
      const cardIndex = playerHand.findIndex((card) => card.id === cardId);

      if (cardIndex !== -1) {
        // Remove the card from its current position
        const [card] = playerHand.splice(cardIndex, 1);

        // Insert the card at the new index
        playerHand.splice(newIndex, 0, card);

        // Emit the updated game state to all players
        emitProtectedGameState(gameState, io, lobby.players);
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneTakeCardFromTheDeckPile(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;
    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.playerInTurn === playerId
    ) {
      // Stop the player turn timer
      stopTimer(lobby, "playerTurn");

      const pile = gameState.deckPile;
      const status = FIFTY_ONE_GAME_STATUS.taking_from_deck_pile;

      if (pile.length > 0) {
        gameState.gameStatus = status;
        const card = pile.pop();
        gameState.playersHands[playerId].push(card);
        gameState.lastPickedCard = card; // Update the last picked card

        // Save progress if enabled
        if (gameState.progressId) {
          gameState.gameStatus = FIFTY_ONE_GAME_STATUS.player_turn_in_progress;
          await updateProgress(gameState.progressId, gameState);
          gameState.gameStatus = status;
        }

        gameState.dealingCard = {
          ...card,
          playerId,
        };
        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for a brief moment to show the animation
        setTimeout(async () => {
          try {
            gameState.dealingCard = null;
            gameState.gameStatus =
              FIFTY_ONE_GAME_STATUS.player_turn_in_progress;

            // Save progress if enabled
            if (gameState.progressId) {
              await updateProgress(gameState.progressId, gameState);
            }

            // Start the player's turn in progress timer
            startPlayerTurnInProgressTimer(lobby, io);
            performBotsActions(lobby, io);

            unlockGameState(lobby);

            emitProtectedGameState(gameState, io, lobby.players);
          } catch (error) {
            unlockGameState(lobby);
          }
        }, 500);
      } else {
        unlockGameState(lobby);
      }
    } else {
      unlockGameState(lobby);
    }
  } catch (error) {
    unlockGameState(lobby);
  }
}

async function fiftyOneDiscardCard(lobby, socket, io, cardId) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;
    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn_in_progress &&
      gameState.playerInTurn === playerId
    ) {
      const playerHand = gameState.playersHands[playerId];
      const cardIndex = playerHand.findIndex((card) => card.id === cardId);

      if (cardIndex !== -1) {
        // Stop the player turn in progress timer
        stopTimer(lobby, "playerTurnInProgress");

        const discardCard = async () => {
          const [discardedCard] = playerHand.splice(cardIndex, 1);

          // Check if face-up cards have reached the limit
          if (gameState.faceUpCards.length >= 4) {
            // Move face-up cards to discard pile
            gameState.discardPile.push(...gameState.faceUpCards);
            gameState.discardingCards = [...gameState.faceUpCards];
            gameState.faceUpCards = [];

            // Change game status
            gameState.gameStatus =
              FIFTY_ONE_GAME_STATUS.discarding_from_face_up_cards;

            // Emit updated game state
            emitProtectedGameState(gameState, io, lobby.players);

            // Wait for 500 milliseconds
            await delay(500);

            // Clear discarding cards
            gameState.discardingCards = [];
          }

          // Add the discarded card to the face up cards
          gameState.faceUpCards.push(discardedCard);

          gameState.discardingCard = {
            ...discardedCard,
            playerId,
          };
          gameState.gameStatus = FIFTY_ONE_GAME_STATUS.discarding_from_player;
          emitProtectedGameState(gameState, io, lobby.players);
          await delay(500);

          gameState.discardingCard = null;

          if (gameState.deckPile.length === 0) {
            const updatedGameState = await endHand(gameState);
            if (updatedGameState) {
              // The hand has ended, use the updated game state
              gameState = updatedGameState;

              if (
                gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand
              ) {
                // Start the summary timer
                startSummaryTimer(lobby, io);
                performBotsActions(lobby, io);
              }
            }
          } else {
            // Move the turn to the next player
            const currentPlayerIndex =
              gameState.distributionOfSeats.indexOf(playerId);
            const nextPlayerIndex =
              (currentPlayerIndex + 1) % gameState.distributionOfSeats.length;
            gameState.playerInTurn =
              gameState.distributionOfSeats[nextPlayerIndex];

            const startIndex =
              (gameState.distributionOfSeats.indexOf(gameState.dealer) + 1) %
              gameState.distributionOfSeats.length;
            if (nextPlayerIndex === startIndex) {
              gameState.roundNumber++;
            }

            // Update the game status back to player_turn
            gameState.gameStatus = FIFTY_ONE_GAME_STATUS.player_turn;

            // Start the next player's turn
            startPlayerTurnTimer(lobby, io);
            performBotsActions(lobby, io);
          }

          // Save progress if enabled and game status is not end_of_the_game
          if (
            gameState.progressId &&
            gameState.gameStatus !== FIFTY_ONE_GAME_STATUS.end_of_the_game
          ) {
            await updateProgress(gameState.progressId, gameState);
          }

          // Emit the updated game state to all players
          emitProtectedGameState(gameState, io, lobby.players);

          if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_game) {
            // Check if stats feature is enabled
            const statsEnabled = await statsManager.isStatsEnabled(lobby);

            if (statsEnabled) {
              // Get players' wins
              const wins = await statsManager.getWins(lobby);

              if (wins) {
                // Emit gameStats event to all players in the lobby
                lobby.players.forEach((player) => {
                  io.to(player.id).emit("gameStats", wins);
                });
              }
            }

            // Delete the saved progress and the game state if the game has ended
            if (gameState.progressId) {
              await deleteProgress(gameState.progressId);
              console.log(`Deleted progress with ID: ${gameState.progressId}`);
              gameState.progressId = null;
            }
            stopTimers(lobby); // Stop all timers
            delete gameStateByLobby[lobby.code];
          }
        };

        discardCard();
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneExchangeCards(
  lobby,
  socket,
  io,
  takingCards,
  discardingCards
) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;

    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.playerInTurn === playerId &&
      takingCards.length === discardingCards.length
    ) {
      const playerHand = gameState.playersHands[playerId];

      // Validate that all discarding cards are in player's hand
      const allDiscardingCardsInHand = discardingCards.every((card) =>
        playerHand.some((handCard) => handCard.id === card.id)
      );

      // Validate that all taking cards are in face-up cards
      const allTakingCardsInFaceUp = takingCards.every((card) =>
        gameState.faceUpCards.some((faceUpCard) => faceUpCard.id === card.id)
      );

      if (allDiscardingCardsInHand && allTakingCardsInFaceUp) {
        // Stop the player turn timer
        stopTimer(lobby, "playerTurn");

        // Prepare the cards with their original indexes
        const preparedTakingCards = takingCards.map((card) => ({
          ...card,
          originalIndex: gameState.faceUpCards.findIndex(
            (c) => c.id === card.id
          ),
        }));

        const preparedDiscardingCards = discardingCards.map((card) => ({
          ...card,
          originalIndex: playerHand.findIndex((c) => c.id === card.id),
        }));

        // Sort the cards by their original indexes (descending order)
        preparedTakingCards.sort((a, b) => b.originalIndex - a.originalIndex);
        preparedDiscardingCards.sort(
          (a, b) => b.originalIndex - a.originalIndex
        );

        // Exchange the cards
        preparedDiscardingCards.forEach((card, index) => {
          // Remove the discarding card from player's hand
          playerHand.splice(card.originalIndex, 1);
          // Insert the taking card into player's hand at the same index
          playerHand.splice(card.originalIndex, 0, preparedTakingCards[index]);

          // Remove the taking card from face-up cards
          gameState.faceUpCards.splice(
            preparedTakingCards[index].originalIndex,
            1
          );
          // Insert the discarding card into face-up cards at the same index
          gameState.faceUpCards.splice(
            preparedTakingCards[index].originalIndex,
            0,
            card
          );
        });

        // Set up animation state
        gameState.takingCards = preparedTakingCards;
        gameState.discardingCards = preparedDiscardingCards;
        gameState.gameStatus = FIFTY_ONE_GAME_STATUS.taking_from_face_up_cards;

        // Emit updated game state
        emitProtectedGameState(gameState, io, lobby.players);

        // Wait for animation
        await delay(500);

        // Clear animation state
        gameState.takingCards = [];
        gameState.discardingCards = [];

        // Move to next player
        const currentPlayerIndex =
          gameState.distributionOfSeats.indexOf(playerId);
        const nextPlayerIndex =
          (currentPlayerIndex + 1) % gameState.distributionOfSeats.length;
        gameState.playerInTurn = gameState.distributionOfSeats[nextPlayerIndex];

        const startIndex =
          (gameState.distributionOfSeats.indexOf(gameState.dealer) + 1) %
          gameState.distributionOfSeats.length;
        if (nextPlayerIndex === startIndex) {
          gameState.roundNumber++;
        }

        gameState.gameStatus = FIFTY_ONE_GAME_STATUS.player_turn;

        // Save progress if enabled
        if (gameState.progressId) {
          await updateProgress(gameState.progressId, gameState);
        }

        // Emit updated game state
        emitProtectedGameState(gameState, io, lobby.players);

        // Start the next player's turn
        startPlayerTurnTimer(lobby, io);
        performBotsActions(lobby, io);
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneCall(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
      gameState.playerInTurn === socket.id &&
      gameState.roundNumber > 1
    ) {
      const updatedGameState = call(gameState, socket.id);
      if (updatedGameState) {
        // Stop the player's turn timer
        stopTimer(lobby, "playerTurn");

        // Emit the automatic chat message
        processChatMessage(lobby, socket, io, "I call 😎");

        // Save progress if the feature is enabled
        if (updatedGameState.progressId) {
          await updateProgress(updatedGameState.progressId, updatedGameState);
        }

        gameStateByLobby[lobby.code] = updatedGameState;

        // Emit the updated game state to the players
        emitProtectedGameState(updatedGameState, io, lobby.players);

        // Check if the challenger candidates array is empty
        if (updatedGameState.challengerCandidates.length === 0) {
          const endHandResult = await endHand(updatedGameState);
          if (endHandResult) {
            gameStateByLobby[lobby.code] = endHandResult;
            await processEndHandResult(lobby, io, endHandResult);
          }
        } else {
          // Start the waiting for challengers timer
          startWaitingForChallengersTimer(lobby, io);
          performBotsActions(lobby, io);
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneChallenge(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers
    ) {
      const updatedGameState = challenge(gameState, socket.id);
      if (updatedGameState) {
        // Emit the automatic chat message
        processChatMessage(lobby, socket, io, "I challenge 🤔");

        // Save progress if the feature is enabled
        if (updatedGameState.progressId) {
          await updateProgress(updatedGameState.progressId, updatedGameState);
        }

        gameStateByLobby[lobby.code] = updatedGameState;

        // Emit the updated game state to the players
        emitProtectedGameState(updatedGameState, io, lobby.players);

        // Check if the challenger candidates array is empty
        if (updatedGameState.challengerCandidates.length === 0) {
          const endHandResult = await endHand(updatedGameState);
          if (endHandResult) {
            gameStateByLobby[lobby.code] = endHandResult;
            await processEndHandResult(lobby, io, endHandResult);
          }
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneNotChallenge(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) return;
  try {
    let gameState = gameStateByLobby[lobby.code];
    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers
    ) {
      const updatedGameState = notChallenge(gameState, socket.id);
      if (updatedGameState) {
        // Emit the automatic chat message
        processChatMessage(lobby, socket, io, "I pass 🙄");

        // Save progress if the feature is enabled
        if (updatedGameState.progressId) {
          await updateProgress(updatedGameState.progressId, updatedGameState);
        }

        gameStateByLobby[lobby.code] = updatedGameState;

        // Emit the updated game state to the players
        emitProtectedGameState(updatedGameState, io, lobby.players);

        // Check if the challenger candidates array is empty
        if (updatedGameState.challengerCandidates.length === 0) {
          const endHandResult = await endHand(updatedGameState);
          if (endHandResult) {
            gameStateByLobby[lobby.code] = endHandResult;
            await processEndHandResult(lobby, io, endHandResult);
          }
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function fiftyOneReadyToContinue(lobby, socket, io) {
  const proceed = await lockGameState(lobby);
  if (!proceed) {
    return;
  }
  try {
    let gameState = gameStateByLobby[lobby.code];
    const playerId = socket.id;

    if (
      !gameState.paused &&
      gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand
    ) {
      gameState.playersReadyToContinue[playerId] = true;

      // Emit the updated game state to all players
      emitProtectedGameState(gameState, io, lobby.players);

      const allPlayersReady = Object.values(
        gameState.playersReadyToContinue
      ).every((ready) => ready);

      if (allPlayersReady) {
        // Stop the summary timer
        stopTimer(lobby, "summary");

        const newGameState = startNextHand(gameState);
        if (newGameState) {
          gameStateByLobby[lobby.code] = newGameState;
          gameState = newGameState;

          // Save progress if enabled
          if (gameState.progressId) {
            await updateProgress(gameState.progressId, gameState);
          }

          await delay(3000);

          // Emit the updated game state to all players
          emitProtectedGameState(gameState, io, lobby.players);

          // Start the dealer timer
          startDealerTimer(lobby, io);
          performBotsActions(lobby, io);
        }
      }
    }
  } finally {
    unlockGameState(lobby);
  }
}

async function processEndHandResult(lobby, io, gameState) {
  if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand) {
    // Save progress if the feature is enabled
    if (gameState.progressId) {
      await updateProgress(gameState.progressId, gameState);
    }

    // Emit the updated game state to the players
    emitProtectedGameState(gameState, io, lobby.players);

    // Start the summary timer
    startSummaryTimer(lobby, io);
    performBotsActions(lobby, io);
  } else if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_game) {
    // Emit the updated game state to the players
    emitProtectedGameState(gameState, io, lobby.players);

    // Check if stats feature is enabled
    const statsEnabled = await statsManager.isStatsEnabled(lobby);
    if (statsEnabled) {
      // Get players' wins
      const wins = await statsManager.getWins(lobby);
      if (wins) {
        // Emit gameStats event to all players in the lobby
        lobby.players.forEach((player) => {
          io.to(player.id).emit("gameStats", wins);
        });
      }
    }

    // Delete the progress save
    if (gameState.progressId) {
      await deleteProgress(gameState.progressId);
    }

    // Stop all timers for the lobby
    stopTimers(lobby);

    // Delete the game state in the gameStateByLobby object
    delete gameStateByLobby[lobby.code];
  }
}

async function startDealerTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "dealer",
      remainingTime: FIFTY_ONE_TIMERS.dealer,
    });
  });
  startTimer(
    lobby,
    "dealer",
    FIFTY_ONE_TIMERS.dealer,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "dealer",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      // Time's up, start dealing cards automatically
      if (
        currentGameState &&
        currentGameState.gameStatus === FIFTY_ONE_GAME_STATUS.ready_to_deal
      ) {
        fiftyOneDealCards(currentLobby, { id: currentGameState.dealer }, io);
      }
    }
  );
}

async function startPlayerTurnTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "playerTurn",
      remainingTime: FIFTY_ONE_TIMERS.playerTurn,
    });
  });
  startTimer(
    lobby,
    "playerTurn",
    FIFTY_ONE_TIMERS.playerTurn,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "playerTurn",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;

      // Time's up, pick from deck pile by default
      if (
        currentGameState &&
        currentGameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn
      ) {
        fiftyOneTakeCardFromTheDeckPile(
          currentLobby,
          { id: currentGameState.playerInTurn },
          io
        );
      }
    }
  );
}

async function startPlayerTurnInProgressTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "playerTurnInProgress",
      remainingTime: FIFTY_ONE_TIMERS.playerTurnInProgress,
    });
  });
  startTimer(
    lobby,
    "playerTurnInProgress",
    FIFTY_ONE_TIMERS.playerTurnInProgress,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "playerTurnInProgress",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      // Time's up, discard the last picked card by default
      if (
        currentGameState &&
        currentGameState.gameStatus ===
          FIFTY_ONE_GAME_STATUS.player_turn_in_progress
      ) {
        const playerId = currentGameState.playerInTurn;
        let lastPickedCard = currentGameState.lastPickedCard;

        if (lastPickedCard) {
          const playerHand = currentGameState.playersHands[playerId];
          const cardIndex = playerHand.findIndex(
            (card) => card.id === lastPickedCard.id
          );

          if (cardIndex !== -1) {
            fiftyOneDiscardCard(
              currentLobby,
              { id: playerId },
              io,
              lastPickedCard.id
            );
          } else {
            lastPickedCard = null;
          }
        }
        if (!lastPickedCard) {
          // If no card was picked (shouldn't happen normally) or the card is not in the player's hand, discard the last card in hand
          const playerHand = currentGameState.playersHands[playerId];
          if (playerHand.length > 0) {
            fiftyOneDiscardCard(
              currentLobby,
              { id: playerId },
              io,
              playerHand[playerHand.length - 1].id
            );
          }
        }
      }
    }
  );
}

function startWaitingForChallengersTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "waitingForChallengers",
      remainingTime: FIFTY_ONE_TIMERS.waitingForChallengers,
    });
  });
  startTimer(
    lobby,
    "waitingForChallengers",
    FIFTY_ONE_TIMERS.waitingForChallengers,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "waitingForChallengers",
          remainingTime,
        });
      });
    },
    async () => {
      const proceed = await lockGameState(lobby);
      if (!proceed) return;
      try {
        let currentGameState = gameStateByLobby[lobby.code];
        if (
          !currentGameState.paused &&
          currentGameState.gameStatus ===
            FIFTY_ONE_GAME_STATUS.waiting_for_challengers
        ) {
          const updatedGameState = await endHand(currentGameState);
          if (updatedGameState) {
            gameStateByLobby[lobby.code] = updatedGameState;
            await processEndHandResult(lobby, io, updatedGameState);
          }
        }
      } finally {
        unlockGameState(lobby);
      }
    }
  );
}

async function startSummaryTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "summary",
      remainingTime: FIFTY_ONE_TIMERS.summary,
    });
  });
  startTimer(
    lobby,
    "summary",
    FIFTY_ONE_TIMERS.summary,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "summary",
          remainingTime,
        });
      });
    },
    async () => {
      const proceed = await lockGameState(lobby);
      if (!proceed) {
        return;
      }
      try {
        let currentGameState = gameStateByLobby[lobby.code];
        let currentLobby = currentGameState.lobby;
        if (
          !currentGameState.paused &&
          currentGameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand
        ) {
          const newGameState = startNextHand(currentGameState);
          if (newGameState) {
            gameStateByLobby[lobby.code] = newGameState;
            currentGameState = newGameState;
            currentLobby = currentGameState.lobby;

            // Save progress if enabled
            if (currentGameState.progressId) {
              await updateProgress(
                currentGameState.progressId,
                currentGameState
              );
            }

            emitProtectedGameState(currentGameState, io, currentLobby.players);

            // Start the dealer timer
            startDealerTimer(currentLobby, io);
            performBotsActions(currentLobby, io);
          }
        }
      } finally {
        unlockGameState(lobby);
      }
    }
  );
}

async function startGamePausedTimer(lobby, io) {
  lobby.players.forEach((player) => {
    io.to(player.id).emit("timerUpdate", {
      timerName: "gamePaused",
      remainingTime: lobby.isPublic ? PUBLIC_PAUSE_TIMER : PRIVATE_PAUSE_TIMER,
    });
  });

  startTimer(
    lobby,
    "gamePaused",
    lobby.isPublic ? PUBLIC_PAUSE_TIMER : PRIVATE_PAUSE_TIMER,
    (remainingTime) => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;
      currentLobby.players.forEach((player) => {
        io.to(player.id).emit("timerUpdate", {
          timerName: "gamePaused",
          remainingTime,
        });
      });
    },
    async () => {
      let currentGameState = gameStateByLobby[lobby.code];
      let currentLobby = currentGameState.lobby;

      if (currentGameState && currentGameState.paused) {
        if (currentLobby.isPublic) {
          // Get disconnected user IDs from the pausedGamesByUser map
          const disconnectedUserIds = [];
          for (const [userId, lobbyCode] of pausedGamesByUser.entries()) {
            if (lobbyCode === currentLobby.code) {
              disconnectedUserIds.push(userId);
            }
          }

          // Call replacePlayersInLobby with the disconnected user IDs
          replacePlayersInLobby(currentLobby.code, disconnectedUserIds);
        } else {
          // Abort the game for private lobbies
          const playerIds = currentLobby.players.map((player) => player.id);
          deletePlayersFromLobby(currentLobby.code, playerIds);
        }
      }
    }
  );
}

async function performBotsActions(lobby, io) {
  const botPlayers = lobby.players.filter((player) => player.isBot);
  for (const botPlayer of botPlayers) {
    performBotAction(lobby, io, botPlayer);
  }
}

async function performBotAction(lobby, io, botPlayer) {
  const gameState = gameStateByLobby[lobby.code];
  const protectedGameState = protectGameStateForPlayer(gameState, botPlayer.id);

  const botAction = await botNextAction(
    protectedGameState, // Pass the protected game state
    botPlayer.id,
    FIFTY_ONE_ID
  );
  if (botAction) {
    switch (botAction.action) {
      case "dealCards":
        if (gameState.dealer === botPlayer.id) {
          fiftyOneDealCards(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "takeCard":
        if (gameState.playerInTurn === botPlayer.id) {
          fiftyOneTakeCardFromTheDeckPile(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "discardCard":
        if (gameState.playerInTurn === botPlayer.id) {
          fiftyOneDiscardCard(
            lobby,
            { id: botPlayer.id },
            io,
            botAction.cardId
          );
        }
        break;
      case "exchangeCards":
        if (gameState.playerInTurn === botPlayer.id) {
          fiftyOneExchangeCards(
            lobby,
            { id: botPlayer.id },
            io,
            botAction.takingCards,
            botAction.discardingCards
          );
        }
        break;
      case "call":
        if (gameState.playerInTurn === botPlayer.id) {
          fiftyOneCall(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "challenge":
        if (gameState.challengerCandidates.includes(botPlayer.id)) {
          fiftyOneChallenge(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "notChallenge":
        if (gameState.challengerCandidates.includes(botPlayer.id)) {
          fiftyOneNotChallenge(lobby, { id: botPlayer.id }, io);
        }
        break;
      case "readyToContinue":
        fiftyOneReadyToContinue(lobby, { id: botPlayer.id }, io);
        break;
    }
  }
}

exports.fiftyOneIsThereAGameInProgress = fiftyOneIsThereAGameInProgress;
exports.fiftyOneIsUserInPausedGame = fiftyOneIsUserInPausedGame;
exports.fiftyOneFindPausedGameLobbyCode = fiftyOneFindPausedGameLobbyCode;
exports.fiftyOneAddDisconnectedUser = fiftyOneAddDisconnectedUser;
exports.fiftyOneRemoveDisconnectedUser = fiftyOneRemoveDisconnectedUser;
exports.fiftyOneUpdateLobby = fiftyOneUpdateLobby;
exports.fiftyOneDealCards = fiftyOneDealCards;
exports.fiftyOneMoveCard = fiftyOneMoveCard;
exports.fiftyOneTakeCardFromTheDeckPile = fiftyOneTakeCardFromTheDeckPile;
exports.fiftyOneDiscardCard = fiftyOneDiscardCard;
exports.fiftyOneExchangeCards = fiftyOneExchangeCards;
exports.fiftyOneCall = fiftyOneCall;
exports.fiftyOneChallenge = fiftyOneChallenge;
exports.fiftyOneNotChallenge = fiftyOneNotChallenge;
exports.fiftyOneReadyToContinue = fiftyOneReadyToContinue;
