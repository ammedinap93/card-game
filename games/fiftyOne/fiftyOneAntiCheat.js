const { getCopyOfAnObject } = require("../../helpers");

/**
 * Creates a protected copy of the game state where secret cards have their rank and suit hidden
 * @param {Object} gameState - The current game state
 * @returns {Object} A protected copy of the game state
 */
function protectGameState(gameState) {
  // Create a deep copy of the game state
  let gameStateCopy = getCopyOfAnObject(gameState);

  // Hide other players' hands
  Object.keys(gameStateCopy.playersHands).forEach((playerId) => {
    gameStateCopy.playersHands[playerId] = gameStateCopy.playersHands[
      playerId
    ].map((card) => ({
      ...card,
      card: "?_?",
    }));
  });

  // Hide deck pile cards
  gameStateCopy.deckPile = gameStateCopy.deckPile.map((card) => ({
    ...card,
    card: "?_?",
  }));

  // Hide discard pile cards (all cards are facedown)
  gameStateCopy.discardPile = gameStateCopy.discardPile.map((card) => ({
    ...card,
    card: "?_?",
  }));

  // Hide dealing card (always hidden, even for the receiving player)
  if (gameStateCopy.dealingCard) {
    gameStateCopy.dealingCard = {
      ...gameStateCopy.dealingCard,
      card: "?_?",
    };
  }

  // Hide last picked card (always hidden)
  if (gameStateCopy.lastPickedCard) {
    gameStateCopy.lastPickedCard = {
      ...gameStateCopy.lastPickedCard,
      card: "?_?",
    };
  }

  return gameStateCopy;
}

/**
 * Creates a protected copy of the game state for a specific player
 * @param {Object} gameState - The current game state
 * @param {string} playerId - The ID of the player
 * @returns {Object} A protected copy of the game state for the player
 */
function protectGameStateForPlayer(gameState, playerId) {
  let protectedState = protectGameState(gameState);

  // Reveal this player's cards
  if (protectedState.playersHands[playerId]) {
    protectedState.playersHands[playerId] = gameState.playersHands[playerId];
  }

  return protectedState;
}

/**
 * Emits the protected game state to players
 * @param {Object} gameState - The current game state
 * @param {Object} io - Socket.io instance
 * @param {Array} players - Array of players to emit to
 */
function emitProtectedGameState(gameState, io, players) {
  players.forEach((player) => {
    const protectedState = protectGameStateForPlayer(gameState, player.id);
    io.to(player.id).emit("gameState", protectedState);
  });
}

module.exports = {
  protectGameState,
  protectGameStateForPlayer,
  emitProtectedGameState,
};
