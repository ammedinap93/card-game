const { delayBetween } = require("../../helpers");
const { FIFTY_ONE_GAME_STATUS } = require("./fiftyOneConstants");

const botPlayFiftyOne = async (gameState, botId) => {
  if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.ready_to_deal &&
    gameState.dealer === botId
  ) {
    await delayBetween(3000, 5000);
    return { action: "dealCards" };
  } else if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(5000, 10000);
    const botHand = gameState.playersHands[botId];

    if (isCompleteHand(botHand)) {
      return { action: "call" };
    }

    const bestExchange = findBestExchange(botHand, gameState.faceUpCards);
    if (bestExchange) {
      return {
        action: "exchangeCards",
        takingCards: bestExchange.takingCards,
        discardingCards: bestExchange.discardingCards,
      };
    }

    return { action: "takeCard", fromDeckPile: true };
  } else if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.player_turn_in_progress &&
    gameState.playerInTurn === botId
  ) {
    await delayBetween(5000, 10000);
    const botHand = gameState.playersHands[botId];
    const cardToDiscard = findWorstCard(botHand);
    return {
      action: "discardCard",
      cardId: cardToDiscard.id,
    };
  } else if (
    gameState.gameStatus === FIFTY_ONE_GAME_STATUS.waiting_for_challengers &&
    gameState.challengerCandidates.includes(botId)
  ) {
    await delayBetween(5000, 15000);
    const botHand = gameState.playersHands[botId];
    const shouldChallenge = decideChallengeAction(botHand);
    return {
      action: shouldChallenge ? "challenge" : "notChallenge",
    };
  } else if (gameState.gameStatus === FIFTY_ONE_GAME_STATUS.end_of_the_hand) {
    await delayBetween(8000, 15000);
    return { action: "readyToContinue" };
  } else {
    return null;
  }
};

const isCompleteHand = (hand) => {
  if (hand.length !== 5) return false;
  const suit = hand[0].card.split("_")[1];
  let score = 0;
  for (const card of hand) {
    if (card.card.split("_")[1] !== suit) return false;
    score += getCardValue(card);
  }
  return score >= 31;
};

const getCardValue = (card) => {
  const rank = card.card.split("_")[0];
  if (["jack", "queen", "king"].includes(rank)) return 10;
  if (rank === "ace") return 11;
  return parseInt(rank);
};

const findBestExchange = (hand, faceUpCards) => {
  let bestScore = evaluateHand(hand);
  let bestExchange = null;

  // Try all possible combinations of exchanges
  for (let i = 1; i <= Math.min(hand.length, faceUpCards.length); i++) {
    const handCombinations = getCombinations(hand, i);
    const faceUpCombinations = getCombinations(faceUpCards, i);

    for (const handCombo of handCombinations) {
      for (const faceUpCombo of faceUpCombinations) {
        const newHand = hand
          .filter((card) => !handCombo.includes(card))
          .concat(faceUpCombo);
        const newScore = evaluateHand(newHand);
        if (newScore > bestScore) {
          bestScore = newScore;
          bestExchange = {
            takingCards: faceUpCombo,
            discardingCards: handCombo,
          };
        }
      }
    }
  }

  return bestExchange;
};

const evaluateHand = (hand) => {
  const suits = {};
  const cardValues = {};

  // Count suits and store card values
  for (const card of hand) {
    const [rank, suit] = card.card.split("_");
    if (!suits[suit]) suits[suit] = 0;
    suits[suit]++;
    cardValues[card.id] = getCardValue(card);
  }

  // Calculate score
  let score = 0;
  for (const card of hand) {
    const suit = card.card.split("_")[1];
    score += cardValues[card.id] * suits[suit];
  }

  return score;
};

const findWorstCard = (hand) => {
  const handScore = evaluateHand(hand);
  return hand.reduce((worst, card) => {
    const handWithoutCard = hand.filter((c) => c.id !== card.id);
    const scoreWithoutCard = evaluateHand(handWithoutCard);
    const scoreDifference = handScore - scoreWithoutCard;

    if (!worst || scoreDifference < worst.scoreDifference) {
      return { card, scoreDifference };
    }
    return worst;
  }, null).card;
};

const getCombinations = (array, size) => {
  const result = [];

  function combine(start, combination) {
    if (combination.length === size) {
      result.push(combination);
      return;
    }

    for (let i = start; i < array.length; i++) {
      combine(i + 1, [...combination, array[i]]);
    }
  }

  combine(0, []);
  return result;
};

const decideChallengeAction = (hand) => {
  const handScore = calculateRealHandScore(hand);
  const challengeProbability = (handScore - 31) / 20; // Linear function from 0% at 31 to 100% at 51
  return Math.random() < challengeProbability;
};

const calculateRealHandScore = (hand) => {
  return hand.reduce((score, card) => score + getCardValue(card), 0);
};

module.exports = { botPlayFiftyOne };
