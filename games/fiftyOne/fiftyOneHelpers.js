const { shuffle } = require("../../helpers");
const statsManager = require("../statsManager");
const { FIFTY_ONE_GAME_STATUS } = require("./fiftyOneConstants");

function createNewGameState(lobby) {
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile without IDs first
  let deckPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
      }))
    ),
  ];

  // Shuffle the deck pile
  deckPile = shuffle(deckPile);

  // Assign IDs after shuffling
  let cardId = 1;
  deckPile.forEach((card) => {
    card.id = cardId++;
  });

  // Randomly distribute the seats
  const distributionOfSeats = shuffle(lobby.players.map((player) => player.id));

  return {
    lock: false,
    paused: false,
    progressId: null,
    initialNumberOfPlayers: lobby.players.length,
    lobby: lobby,
    gameStatus: FIFTY_ONE_GAME_STATUS.ready_to_deal,
    hand: 1,
    playersReadyToContinue: lobby.players.reduce((ready, player) => {
      ready[player.id] = false;
      return ready;
    }, {}),
    playersScoresByHand: lobby.players.reduce((scores, player) => {
      scores[player.id] = [];
      return scores;
    }, {}),
    deckPile: deckPile,
    discardPile: [],
    faceUpCards: [],
    distributionOfSeats: distributionOfSeats,
    roundNumber: 0,
    playerInTurn: distributionOfSeats[distributionOfSeats.length - 1],
    dealer: distributionOfSeats[distributionOfSeats.length - 1],
    playersHands: lobby.players.reduce((hands, player) => {
      hands[player.id] = [];
      return hands;
    }, {}),
    dealingCard: null,
    discardingCard: null,
    discardingCards: [],
    takingCards: [],
    lastPickedCard: null,
    caller: null,
    challengers: [],
    nonChallengers: [],
    challengerCandidates: [],
    winnerPlayer: null,
  };
}

function fiftyOneProcessUpdatedLobby(lobby, gameState) {
  // Create a mapping of old user IDs to their corresponding player objects
  const oldUserIdToPlayer = {};
  gameState.lobby.players.forEach((player) => {
    oldUserIdToPlayer[player.userId] = player;
  });

  // Check if all current players are either original players or valid replacements
  const allPlayersValid = lobby.players.every((currentPlayer) => {
    // Check if this is an original player
    if (currentPlayer.userId in oldUserIdToPlayer) {
      return true;
    }

    // Check if this is a valid replacement player
    if (Object.keys(lobby.replacements).length > 0) {
      for (const disconnectedUserId in lobby.replacements) {
        if (lobby.replacements[disconnectedUserId] === currentPlayer.userId) {
          return true;
        }
      }
    }
    return false;
  });

  if (!allPlayersValid) {
    return null;
  }

  // Create a new game state object
  const newGameState = {
    ...gameState,
    lobby: lobby,
    playersHands: {},
    playersScoresByHand: {},
    playersReadyToContinue: {},
  };

  // Transfer game state for each player
  lobby.players.forEach((currentPlayer) => {
    let oldPlayer;

    // Check if this is a replacement player
    let replacedUserId = null;
    if (Object.keys(lobby.replacements).length > 0) {
      for (const disconnectedUserId in lobby.replacements) {
        if (lobby.replacements[disconnectedUserId] === currentPlayer.userId) {
          replacedUserId = disconnectedUserId;
          break;
        }
      }
    }

    if (replacedUserId) {
      // This is a replacement player, use the disconnected player's data
      oldPlayer = oldUserIdToPlayer[replacedUserId];
    } else {
      // This is an original player
      oldPlayer = oldUserIdToPlayer[currentPlayer.userId];
    }

    if (oldPlayer) {
      // Transfer game state
      newGameState.playersHands[currentPlayer.id] =
        gameState.playersHands[oldPlayer.id];
      newGameState.playersScoresByHand[currentPlayer.id] =
        gameState.playersScoresByHand[oldPlayer.id];
      newGameState.playersReadyToContinue[currentPlayer.id] =
        gameState.playersReadyToContinue[oldPlayer.id];

      // Update player-specific game state properties
      [
        "challengers",
        "nonChallengers",
        "challengerCandidates",
        "distributionOfSeats",
      ].forEach((prop) => {
        if (Array.isArray(newGameState[prop])) {
          newGameState[prop] = newGameState[prop].map((id) =>
            id === oldPlayer.id ? currentPlayer.id : id
          );
        }
      });

      // Update individual player properties
      ["playerInTurn", "dealer", "caller", "winnerPlayer"].forEach((prop) => {
        if (newGameState[prop] === oldPlayer.id) {
          newGameState[prop] = currentPlayer.id;
        }
      });
    }
  });

  return newGameState;
}

async function endHand(gameState) {
  if (
    gameState.gameStatus !== FIFTY_ONE_GAME_STATUS.waiting_for_challengers &&
    gameState.deckPile.length !== 0
  ) {
    return null;
  }

  const newScores = {};

  if (gameState.deckPile.length === 0) {
    // Handle the case of an empty deck pile
    gameState.lobby.players.forEach((player) => {
      const playerHand = gameState.playersHands[player.id];
      if (isACompleteHand(playerHand)) {
        newScores[player.id] = calculateHandScore(playerHand);
      } else {
        newScores[player.id] = 0;
      }
    });
  } else {
    // Process remaining challenger candidates as non-challengers
    if (gameState.challengerCandidates.length > 0) {
      gameState.nonChallengers.push(...gameState.challengerCandidates);
      gameState.challengerCandidates = [];
    }

    // Calculate scores for non-challengers
    for (const nonChallenger of gameState.nonChallengers) {
      newScores[nonChallenger] = calculateHandScore(
        gameState.playersHands[nonChallenger]
      );
    }

    // Calculate scores for challengers and caller
    if (gameState.challengers.length === 0) {
      newScores[gameState.caller] =
        calculateHandScore(gameState.playersHands[gameState.caller]) * 2;
    } else {
      const callerScore = calculateHandScore(
        gameState.playersHands[gameState.caller]
      );
      let challengeWinner = gameState.caller;
      let highestScore = callerScore;

      for (const challenger of gameState.challengers) {
        const challengerScore = calculateHandScore(
          gameState.playersHands[challenger]
        );
        if (challengerScore > highestScore) {
          challengeWinner = challenger;
          highestScore = challengerScore;
        }
      }

      const totalScore =
        callerScore * 2 +
        gameState.challengers.reduce(
          (sum, challenger) =>
            sum + calculateHandScore(gameState.playersHands[challenger]),
          0
        );

      newScores[challengeWinner] = totalScore;
      [...gameState.challengers, gameState.caller].forEach((playerId) => {
        if (playerId !== challengeWinner) newScores[playerId] = 0;
      });
    }

    // Set scores to 0 for players not involved in the challenge
    gameState.lobby.players.forEach((player) => {
      if (!(player.id in newScores)) {
        newScores[player.id] = 0;
      }
    });
  }

  // Update playersScoresByHand
  Object.keys(newScores).forEach((playerId) => {
    gameState.playersScoresByHand[playerId].push(newScores[playerId]);
  });

  // Check for a winner
  const winner = determineWinnerPlayer(gameState.playersScoresByHand);

  if (winner) {
    gameState.winnerPlayer = winner;
    gameState.gameStatus = FIFTY_ONE_GAME_STATUS.end_of_the_game;

    // Save game stats if the feature is enabled and a winner is determined
    const isEnabled = await statsManager.isStatsEnabled(gameState.lobby);
    if (isEnabled) {
      const totalScore = gameState.playersScoresByHand[
        gameState.winnerPlayer
      ].reduce((sum, score) => sum + score, 0);
      const statsId = await statsManager.saveStats(gameState, totalScore);
      if (statsId) {
        console.log(`Game stats saved with ID: ${statsId}`);
      } else {
        console.error("Failed to save game stats");
      }
    }
  } else {
    gameState.gameStatus = FIFTY_ONE_GAME_STATUS.end_of_the_hand;
  }

  return gameState;
}

function startNextHand(gameState) {
  // Find the index of the current dealer in the distribution of seats
  const currentDealerIndex = gameState.distributionOfSeats.indexOf(
    gameState.dealer
  );

  // Calculate the index of the next dealer
  const nextDealerIndex =
    (currentDealerIndex + 1) % gameState.distributionOfSeats.length;

  // Increment the hand number
  const nextHand = gameState.hand + 1;

  // Create a new deck pile and shuffle it
  const suits = ["hearts", "diamonds", "clubs", "spades"];
  const ranks = [
    "ace",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "jack",
    "queen",
    "king",
  ];

  // Create the deck pile without IDs first
  let deckPile = [
    ...suits.flatMap((suit) =>
      ranks.map((rank) => ({
        card: `${rank}_${suit}`,
      }))
    ),
  ];

  // Shuffle the deck pile
  deckPile = shuffle(deckPile);

  // Assign IDs after shuffling
  let cardId = 1;
  deckPile.forEach((card) => {
    card.id = cardId++;
  });

  // Create the new game state
  const newGameState = {
    lock: false,
    paused: false,
    progressId: gameState.progressId,
    initialNumberOfPlayers: gameState.initialNumberOfPlayers,
    lobby: gameState.lobby,
    gameStatus: FIFTY_ONE_GAME_STATUS.ready_to_deal,
    hand: nextHand,
    playersReadyToContinue: gameState.lobby.players.reduce((ready, player) => {
      ready[player.id] = false;
      return ready;
    }, {}),
    playersScoresByHand: gameState.playersScoresByHand,
    deckPile: deckPile,
    discardPile: [],
    faceUpCards: [],
    distributionOfSeats: gameState.distributionOfSeats,
    roundNumber: 0,
    playerInTurn: gameState.distributionOfSeats[nextDealerIndex],
    dealer: gameState.distributionOfSeats[nextDealerIndex],
    playersHands: gameState.lobby.players.reduce((hands, player) => {
      hands[player.id] = [];
      return hands;
    }, {}),
    dealingCard: null,
    discardingCard: null,
    discardingCards: [],
    takingCards: [],
    lastPickedCard: null,
    caller: null,
    challengers: [],
    nonChallengers: [],
    challengerCandidates: [],
    winnerPlayer: null,
  };

  return newGameState;
}

function call(gameState, caller) {
  if (gameState.caller !== null) return null;

  const callerPlayer = gameState.lobby.players.find(
    (player) => player.id === caller
  );
  if (!callerPlayer) return null;

  const callerHand = gameState.playersHands[caller];
  if (!isACompleteHand(callerHand)) return null;

  const challengerCandidates = gameState.lobby.players
    .filter(
      (player) =>
        player.id !== caller &&
        isACompleteHand(gameState.playersHands[player.id])
    )
    .map((player) => player.id);

  return {
    ...gameState,
    caller: caller,
    challengerCandidates: challengerCandidates,
    gameStatus: FIFTY_ONE_GAME_STATUS.waiting_for_challengers,
  };
}

function challenge(gameState, challenger) {
  if (gameState.caller === null) return null;
  if (!gameState.challengerCandidates.includes(challenger)) return null;
  if (gameState.challengers.includes(challenger)) return null;

  const updatedChallengerCandidates = gameState.challengerCandidates.filter(
    (id) => id !== challenger
  );
  const updatedChallengers = [...gameState.challengers, challenger];

  return {
    ...gameState,
    challengerCandidates: updatedChallengerCandidates,
    challengers: updatedChallengers,
  };
}

function notChallenge(gameState, nonChallenger) {
  if (gameState.caller === null) return null;
  if (!gameState.challengerCandidates.includes(nonChallenger)) return null;
  if (gameState.nonChallengers.includes(nonChallenger)) return null;

  const updatedChallengerCandidates = gameState.challengerCandidates.filter(
    (id) => id !== nonChallenger
  );
  const updatedNonChallengers = [...gameState.nonChallengers, nonChallenger];

  return {
    ...gameState,
    challengerCandidates: updatedChallengerCandidates,
    nonChallengers: updatedNonChallengers,
  };
}

function notChallengeAll(gameState, nonChallengers) {
  if (gameState.caller === null) return null;

  for (const nonChallenger of nonChallengers) {
    if (!gameState.challengerCandidates.includes(nonChallenger)) return null;
    if (gameState.nonChallengers.includes(nonChallenger)) return null;
  }

  const updatedChallengerCandidates = gameState.challengerCandidates.filter(
    (id) => !nonChallengers.includes(id)
  );
  const updatedNonChallengers = [
    ...gameState.nonChallengers,
    ...nonChallengers,
  ];

  return {
    ...gameState,
    challengerCandidates: updatedChallengerCandidates,
    nonChallengers: updatedNonChallengers,
  };
}

function determineWinnerPlayer(playersScoresByHand) {
  const totalScores = Object.keys(playersScoresByHand).reduce(
    (acc, playerId) => {
      acc[playerId] = playersScoresByHand[playerId].reduce(
        (sum, score) => sum + score,
        0
      );
      return acc;
    },
    {}
  );

  const winningScore = Math.max(...Object.values(totalScores));
  if (winningScore < 500) return null;

  const potentialWinners = Object.keys(totalScores).filter(
    (playerId) => totalScores[playerId] === winningScore
  );
  if (potentialWinners.length === 1) return potentialWinners[0];

  for (
    let i = playersScoresByHand[potentialWinners[0]].length - 1;
    i >= 0;
    i--
  ) {
    const highestScoreInHand = Math.max(
      ...potentialWinners.map((playerId) => playersScoresByHand[playerId][i])
    );
    const winnersInHand = potentialWinners.filter(
      (playerId) => playersScoresByHand[playerId][i] === highestScoreInHand
    );
    if (winnersInHand.length === 1) return winnersInHand[0];
    potentialWinners.splice(0, potentialWinners.length, ...winnersInHand);
  }

  return null;
}

function isACompleteHand(cards) {
  if (cards.length !== 5) return false;

  const suit = cards[0].card.split("_")[1];
  let score = 0;

  for (const card of cards) {
    const [rank, cardSuit] = card.card.split("_");
    if (cardSuit !== suit) return false;

    if (["jack", "queen", "king"].includes(rank)) {
      score += 10;
    } else if (rank === "ace") {
      score += 11;
    } else {
      score += parseInt(rank);
    }
  }

  return score >= 31;
}

function calculateHandScore(hand) {
  return hand.reduce((score, card) => {
    const [rank] = card.card.split("_");
    if (["jack", "queen", "king"].includes(rank)) return score + 10;
    if (rank === "ace") return score + 11;
    return score + parseInt(rank);
  }, 0);
}

module.exports = {
  createNewGameState,
  fiftyOneProcessUpdatedLobby,
  endHand,
  startNextHand,
  call,
  challenge,
  notChallenge,
  notChallengeAll,
  isACompleteHand,
  calculateHandScore,
};
