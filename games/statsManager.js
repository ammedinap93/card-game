const pool = require("../db");

async function isStatsEnabled(lobby) {
  return lobby.players.every(
    (player) => player.username !== "guest" && player.userId !== null
  );
}

async function getWins(lobby) {
  try {
    const userIds = lobby.players.map((player) => player.userId);
    const query = `
        SELECT winner, COUNT(*) as wins
        FROM game_stats
        WHERE card_game_id = $1 AND users @> $2::integer[] AND cardinality(users) = $3
        GROUP BY winner
      `;
    const values = [lobby.game.id, userIds, userIds.length];
    const result = await pool.query(query, values);

    if (result.rows.length > 0) {
      const wins = {};
      lobby.players.forEach((player) => {
        wins[player.id] = 0; // Initialize all players with 0 wins
      });

      result.rows.forEach((row) => {
        const player = lobby.players.find(
          (p) => parseInt(p.userId) === row.winner
        );
        if (player) {
          wins[player.id] = parseInt(row.wins);
        }
      });
      return wins;
    }
    return null;
  } catch (error) {
    console.error("Error getting wins:", error);
    return null;
  }
}

async function getStats(lobby) {
  try {
    const userIds = lobby.players.map((player) => player.userId);
    const query = `
      SELECT 
        winner,
        COUNT(*) as wins,
        MAX(key_stat) as max_stat,
        MIN(key_stat) as min_stat,
        AVG(key_stat) as avg_stat
      FROM game_stats
      WHERE card_game_id = $1 AND users @> $2::integer[] AND cardinality(users) = $3
      GROUP BY winner
    `;
    const values = [lobby.game.id, userIds, userIds.length];
    const result = await pool.query(query, values);

    if (result.rows.length > 0) {
      const stats = {
        wins: {},
        keyStats: {
          max: null,
          min: null,
          avg: null
        }
      };

      // Initialize all players with 0 wins
      lobby.players.forEach((player) => {
        stats.wins[player.id] = 0;
      });

      let totalGames = 0;
      let sumAvgStats = 0;

      result.rows.forEach((row, index) => {
        const player = lobby.players.find(
          (p) => parseInt(p.userId) === row.winner
        );
        if (player) {
          const wins = parseInt(row.wins);
          stats.wins[player.id] = wins;
          totalGames += wins;
          
          // Initialize key stats on first iteration
          if (index === 0) {
            stats.keyStats.max = parseInt(row.max_stat);
            stats.keyStats.min = parseInt(row.min_stat);
          } else {
            // Update max and min
            stats.keyStats.max = Math.max(stats.keyStats.max, parseInt(row.max_stat));
            stats.keyStats.min = Math.min(stats.keyStats.min, parseInt(row.min_stat));
          }
          
          sumAvgStats += parseFloat(row.avg_stat) * wins;
        }
      });

      // Calculate overall average
      if (totalGames > 0) {
        stats.keyStats.avg = Math.round(sumAvgStats / totalGames);
      }

      return stats;
    }
    return null;
  } catch (error) {
    console.error("Error getting stats:", error);
    return null;
  }
}

async function saveStats(gameState, keyStat = 0) {
  if (!gameState.winnerPlayer) {
    return null;
  }

  try {
    const query = `
      INSERT INTO game_stats (card_game_id, users, winner, final_game_state, key_stat)
      VALUES ($1, $2, $3, $4, $5)
      RETURNING id
    `;
    const values = [
      gameState.lobby.game.id,
      gameState.lobby.players.map((player) => player.userId),
      gameState.lobby.players.find((p) => p.id === gameState.winnerPlayer)
        .userId,
      JSON.stringify(gameState),
      keyStat,
    ];
    const result = await pool.query(query, values);
    return result.rows[0].id;
  } catch (error) {
    console.error("Error saving game stats:", error);
    return null;
  }
}

module.exports = {
  isStatsEnabled,
  getWins,
  getStats,
  saveStats,
};
