const {
  CONTINENTAL_ID,
  FIFTY_ONE_ID,
  POKER_TEXAS_HOLDEM_ID,
} = require("../constants");
const { BOT_NAMES } = require("./botConstants");
const { botPlayContinental } = require("./continental/continentalBot");
const { botPlayFiftyOne } = require("./fiftyOne/fiftyOneBot");
const {
  botPlayPokerTexasHoldem,
} = require("./pokerTexasHoldem/pokerTexasHoldemBot");

const createBot = (lobby) => {
  const availableNames = BOT_NAMES.filter(
    (name) => !lobby.players.some((player) => player.username === name)
  );
  const randomIndex = Math.floor(Math.random() * availableNames.length);
  const botName = availableNames[randomIndex];
  const botId = `bot-${Date.now()}`;
  const nameIndex = BOT_NAMES.findIndex((name) => {
    return name === botName;
  });

  return {
    id: botId,
    username: botName,
    userId: -1 * nameIndex,
    isBot: true,
  };
};

const botNextAction = async (gameState, botId, gameId) => {
  if (gameId === CONTINENTAL_ID) {
    return await botPlayContinental(gameState, botId);
  } else if (gameId === FIFTY_ONE_ID) {
    return await botPlayFiftyOne(gameState, botId);
  } else if (gameId === POKER_TEXAS_HOLDEM_ID) {
    return await botPlayPokerTexasHoldem(gameState, botId);
  } else {
    return null;
  }
};

module.exports = { createBot, botNextAction };
