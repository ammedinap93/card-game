const {
  CONTINENTAL_ID,
  FIFTY_ONE_ID,
  POKER_TEXAS_HOLDEM_ID,
  SOLITAIRE_KLONDIKE_ID,
} = require("../constants");
const pool = require("../db");
const {
  continentalProcessUpdatedLobby,
} = require("./continental/continentalHelpers");
const { fiftyOneProcessUpdatedLobby } = require("./fiftyOne/fiftyOneHelpers");
const {
  pokerTexasHoldemProcessUpdatedLobby,
} = require("./pokerTexasHoldem/pokerTexasHoldemHelpers");
const {
  solitaireKlondikeProcessUpdatedLobby,
} = require("./solitaireKlondike/solitaireKlondikeHelpers");

async function isProgressEnabled(lobby) {
  return lobby.players.every(
    (player) => player.username !== "guest" && player.userId !== null
  );
}

async function findProgress(lobby) {
  let progressId = null;
  try {
    const userIds = lobby.players.map((player) => player.userId);
    const query = `
        SELECT id, game_state
        FROM game_progress
        WHERE card_game_id = $1 AND users @> $2::integer[] AND cardinality(users) = $3
      `;
    const values = [lobby.game.id, userIds, userIds.length];
    const result = await pool.query(query, values);

    if (result.rows.length > 0) {
      const { id, game_state } = result.rows[0];
      const savedGameState = JSON.parse(game_state);

      progressId = id;

      // Create a new game state object merging the info from the lobby and the saved progress
      if (lobby.game.id === CONTINENTAL_ID) {
        const mergedGameState = continentalProcessUpdatedLobby(
          lobby,
          savedGameState
        );
        const gameState = {
          ...mergedGameState,
          progressId: id,
        };

        return gameState;
      } else if (lobby.game.id === FIFTY_ONE_ID) {
        const mergedGameState = fiftyOneProcessUpdatedLobby(
          lobby,
          savedGameState
        );
        const gameState = {
          ...mergedGameState,
          progressId: id,
        };

        return gameState;
      } else if (lobby.game.id === POKER_TEXAS_HOLDEM_ID) {
        const mergedGameState = pokerTexasHoldemProcessUpdatedLobby(
          lobby,
          savedGameState
        );
        const gameState = {
          ...mergedGameState,
          progressId: id,
        };

        return gameState;
      } else if (lobby.game.id === SOLITAIRE_KLONDIKE_ID) {
        const mergedGameState = solitaireKlondikeProcessUpdatedLobby(
          lobby,
          savedGameState
        );
        const gameState = {
          ...mergedGameState,
          progressId: id,
        };

        return gameState;
      }

      const gameState = {
        ...savedGameState,
        progressId: id,
      };

      return gameState;
    }
    return null;
  } catch (error) {
    console.error("Error finding game progress:", error);
    if (progressId) {
      deleteProgress(progressId);
    }
    return null;
  }
}

async function createProgress(gameState) {
  try {
    const query = `
      INSERT INTO game_progress (card_game_id, users, game_state)
      VALUES ($1, $2, $3)
      RETURNING id
    `;
    const values = [
      gameState.lobby.game.id,
      gameState.lobby.players.map((player) => player.userId),
      JSON.stringify(gameState),
    ];
    const result = await pool.query(query, values);
    return result.rows[0].id;
  } catch (error) {
    console.error("Error creating game progress:", error);
    return null;
  }
}

async function updateProgress(progressId, gameState) {
  try {
    const query = `
      UPDATE game_progress
      SET users = $1, game_state = $2
      WHERE id = $3
    `;
    const values = [
      gameState.lobby.players.map((player) => player.userId),
      JSON.stringify(gameState),
      progressId,
    ];
    await pool.query(query, values);
  } catch (error) {
    console.error("Error updating game progress:", error);
  }
}

async function deleteProgress(progressId) {
  try {
    const query = `
      DELETE FROM game_progress
      WHERE id = $1
    `;
    const values = [progressId];
    await pool.query(query, values);
  } catch (error) {
    console.error("Error deleting game progress:", error);
  }
}

module.exports = {
  isProgressEnabled,
  findProgress,
  createProgress,
  updateProgress,
  deleteProgress,
};
