const generateUniqueId = () => {
  let id = `${Date.now().toString(36)}_${Math.random()
    .toString(36)
    .substring(2, 12)
    .padStart(12, 0)}`;
  return id;
};

function shuffle(array) {
  const shuffledArray = [...array];
  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }
  return shuffledArray;
}

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const delayBetween = (min, max) => {
  const delay = Math.floor(Math.random() * (max - min + 1)) + min;
  return new Promise((resolve) => setTimeout(resolve, delay));
};

const getCopyOfAnObject = (object) => {
  try {
    const objectCopy = JSON.parse(JSON.stringify(object));
    return objectCopy;
  } catch (error) {
    console.error("Error copying an object:", error);
    return null;
  }
};

module.exports = {
  generateUniqueId,
  shuffle,
  delay,
  delayBetween,
  getCopyOfAnObject,
};
